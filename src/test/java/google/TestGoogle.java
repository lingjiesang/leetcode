package google;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestGoogle {
    @Test
    public void testReplaceAdjacentDigitWithTheLargerOne() throws Exception{
        ReplaceAdjacentDigitWithTheLargerOne soln = new ReplaceAdjacentDigitWithTheLargerOne();
        //rule 1: three decreasing digits => remove the second one
        assertEquals(soln.replace(6543), 643);

        //rule 2: increasing digits  => remove the last removable one
        assertEquals(soln.replace(2345), 235);
        assertEquals(soln.replace(2357468), 235748);
        assertEquals(soln.replace(2345764), 234574);


        //rule 3: equal digits => if followed by a lower digit (or not followed by anything), remove one of them;
        // if followed by a higher digit, treat them as single digit and follow rule 2 remove the last removable one
        assertEquals(soln.replace(664), 64);
        assertEquals(soln.replace(66543), 6543);
        assertEquals(soln.replace(22345), 2235);
        assertEquals(soln.replace(22356), 2236);
        System.out.println("testReplaceAdjacentDigitWithTheLargerOne done");
    }

    @Test
    public void testCoins() throws Exception{
        Coins soln = new Coins();
        int[] array = {0, 1, 10, 100, 1000};
        for (int n : array) {
            System.out.format("There are %s ways to make change for %s cents\n" , soln.makeChange(n), n);
            assertEquals(soln.makeChange(n), soln.makeChange_benchmark(n));
        }
    }

    @Test
    public void testPayDebt() throws Exception{
        /**
         A, B, 2
         A, C, 4
         B, D, 5
         C, D, 4
         D, E, 7

         0 1 2 3 4
         A B C D E

         A, -6
         B, -3
         C, 0
         D, 2
         E, 7
         */
        int[][] debts = new int[][]{
                {0, 1, 2},
                {0, 2, 4},
                {1, 3, 5},
                {2, 3, 4},
                {3, 4, 7}
        };

        int[] balances = new int[]{-6, -3, 0, 2, 7};

        OptimalAccountBalancing soln = new OptimalAccountBalancing();
//        assertArrayEquals(balances, soln.getBalance(debts, 5));
    }

    @Test
    public void testSmallestRectangle() throws Exception{
        SmallestRectangle soln = new SmallestRectangle();

        int[][] points = new int[][]{
                {0,0},
                {1,2},
                {2,1},{2,3},
                {3,0},{3,1},{3,3}
        };
        assertEquals(soln.smallestRectangle(points), 2);

        points = new int[][]{
                {0,0},
                {1,2},
                {2,0}, {2,1},{2,3},
                {3,0},{3,1},{3,3}
        };
        assertEquals(soln.smallestRectangle(points), 1);

    }
}
