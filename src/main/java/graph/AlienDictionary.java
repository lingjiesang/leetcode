package graph;

import java.util.*;

public class AlienDictionary {
    public String alienOrder(String[] words) {
        List<Set<Integer>> outEdges = new ArrayList<>();
        int[] indegrees = new int[26];
        constructMap(words, outEdges, indegrees);

        int numOfletters = 0;
        for (int indegree: indegrees){
            if (indegree >= 0){
                numOfletters++;
            }
        }

        StringBuilder order = new StringBuilder();
        Deque<Integer> queue = new ArrayDeque<>();

        for (int i = 0 ; i < indegrees.length; i++){
            if (indegrees[i] == 0){
                queue.add(i);
                order.append((char)('a' + i));
            }
        }

        while (!queue.isEmpty()){
            int curt = queue.poll();
            for (int neighbor : outEdges.get(curt)){
                indegrees[neighbor]--;

                if (indegrees[neighbor] == 0){
                    queue.add(neighbor);
                    order.append((char)('a' + neighbor));
                }
            }
        }
        return order.length() == numOfletters ? new String(order) : "";
    }

    private void constructMap(String[] words, List<Set<Integer>> edges, int[] indegrees){
        for (int i = 0; i < 26; i++){
            edges.add(new HashSet<>());
        }
        Arrays.fill(indegrees, -1);
        for (String word : words){
            for (char c : word.toCharArray()){
                indegrees[c - 'a'] = 0;
            }
        }
        for (int i = 1; i < words.length; i++){
            String word1 = words[i - 1];
            String word2 = words[i];
            for (int pos = 0; pos < word1.length() && pos < word2.length(); pos++){
                if (word1.charAt(pos) != word2.charAt(pos)){
                    if (edges.get(word1.charAt(pos) - 'a').add(word2.charAt(pos) - 'a')){
                        indegrees[word2.charAt(pos) - 'a']++;
                    }
                    break;
                }
            }
        }
    }

    public static void main(String[] args){
        String[] words = new String[]{"wrt", "wrf", "er", "ett", "rftt"};
        AlienDictionary soln = new AlienDictionary();
        System.out.println(soln.alienOrder(words));
    }

}
