package graph;

import java.util.*;

public class NumberOfConnectedComponents {
    /**
     * method1. DFS
     */
    public int countComponents(int n, int[][] edges) {
        List<Set<Integer>> neighbors = new ArrayList<>();
        constructMap(n, edges, neighbors);
        int components = 0;
        boolean[] visited = new boolean[n];
        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                dfs(i, visited, neighbors);
                components++;
            }
        }
        return components;
    }

    private void dfs(int root, boolean[] visited, List<Set<Integer>> neighbors) {
        visited[root] = true;

        for (int neighbor : neighbors.get(root)) {
            if (!visited[neighbor]) {
                // neighbors.get(neighbor).remove(root); //有无皆可
                dfs(neighbor, visited, neighbors);
            }
        }
    }

    private void constructMap(int n, int[][] edges, List<Set<Integer>> neighbors) {
        for (int i = 0; i < n; i++) {
            neighbors.add(new HashSet<>());
        }
        for (int[] edge : edges) {
            neighbors.get(edge[0]).add(edge[1]);
            neighbors.get(edge[1]).add(edge[0]);
        }
    }

    /**
     * method2. BFS
     */
    public int countComponents_BFS(int n, int[][] edges) {
        List<Set<Integer>> neighbors = new ArrayList<>();
        constructMap(n, edges, neighbors);

        Deque<Integer> queue = new ArrayDeque<>();
        int components = 0;
        boolean[] visited = new boolean[n];

        for (int i = 0; i < n; i++) {
            if (!visited[i]) {
                components++;
                visited[i] = true;
                queue.offer(i);
                while (!queue.isEmpty()) {
                    int node = queue.poll();
                    for (int neighbor : neighbors.get(node)) {
                        if (!visited[neighbor]) {
                            visited[neighbor] = true;
                            queue.offer(neighbor);
                        }
                    }
                }
            }
        }
        return components;
    }

    /**
     * method 3. union find (3ms)
     * ref: https://discuss.leetcode.com/topic/32627/ac-java-code-union-find/1
     */
    class UnionFound{
        private int[] parent;
        private int[] rank;
        public int count;

        public UnionFound(int n){
            parent = new int[n];
            rank = new int[n];
            for (int i = 0; i < n; i++) {
                parent[i] = i;
                rank[i] = 1;
            }
            count = n;
        }


        public void union(int index1, int index2) {
            int root1 = root(index1);
            int root2 = root(index2);
            if (root1 != root2) {
                if (rank[root1] < rank[root2]) {
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                } else {
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                }
                count--;
            }
        }

        public int root(int index){
            if (parent[index] != index) {
                parent[index] = root(parent[index]);
            }
            return parent[index];
        }

    }


    public int countComponents_Union(int n, int[][] edges) {

        UnionFound uf = new UnionFound(n);

        for (int i = 0; i < edges.length; i++) {
            uf.union(edges[i][0], edges[i][1]);
        }
        return uf.count;
    }


}
