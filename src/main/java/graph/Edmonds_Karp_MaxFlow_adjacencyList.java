package graph;

import java.util.*;

public class Edmonds_Karp_MaxFlow_adjacencyList {

    private boolean bfs(Map<Integer, Integer>[] vertices, int source, int sink, int[] parents) {

        int V = vertices.length;

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[V];

        queue.offer(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int u = queue.poll();

            for (Integer v : vertices[u].keySet()) {
                if (!visited[v] && vertices[u].get(v) > 0) {
                    parents[v] = u;
                    queue.offer(v);
                    visited[v] = true;
                }
            }
        }

        return visited[sink];

    }


    public int fordFulkerson(int V, int[][] edges, int source, int sink) {
        Map<Integer, Integer>[] vertices = new HashMap[V];

        int[] parents = new int[V];

        Arrays.fill(parents, -1);

        for (int i = 0; i < V; i++) {
            vertices[i] = new HashMap<>();
        }

        for (int u = 0; u < V; u++) {
            for (int v = 0; v < V; v++) {
                if (edges[u][v] > 0) {
                    vertices[u].put(v, edges[u][v]);
                }
            }
        }

        int flow = 0;
        while (bfs(vertices, source, sink, parents)) {
            int v = sink;
            int minCapcity = Integer.MAX_VALUE;
            while (v != source) {
                int u = parents[v];
                minCapcity = Math.min(minCapcity, vertices[u].get(v));
                v = u;
            }


            v = sink;
            while (v != source) {
                int u = parents[v];
                vertices[u].put(v, vertices[u].get(v) - minCapcity);
                vertices[v].put(u, vertices[v].getOrDefault(u, 0) + minCapcity);
                v = u;
            }
            flow += minCapcity;

        }
        return flow;
    }


    public static void main(String[] args) throws java.lang.Exception {
        // Let us create a graph shown in the above example
        int graph[][] = new int[][]{
                {0, 16, 13, 0, 0, 0},
                {0, 0, 10, 12, 0, 0},
                {0, 4, 0, 0, 14, 0},
                {0, 0, 9, 0, 0, 20},
                {0, 0, 0, 7, 0, 4},
                {0, 0, 0, 0, 0, 0}
        };
        Edmonds_Karp_MaxFlow_adjacencyList m = new Edmonds_Karp_MaxFlow_adjacencyList();

        System.out.println("The maximum possible flow is " +
                m.fordFulkerson(6, graph, 0, 5));

    }
}
