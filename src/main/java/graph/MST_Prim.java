package graph;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/primsmstsub
 */
public class MST_Prim {
    class UF{
        int[] parent;
        int[] rank;

        public UF(int n){
            parent = new int[n + 1];
            rank = new int[n + 1];
            for (int i = 1; i <= n; i++){
                parent[i] = i;
                rank[i] = 1;
            }
        }

        public boolean union(int i, int j){
            int root1 = root(i);
            int root2 = root(j);
            if (root1 != root2){
                if (rank[root1] > rank[root2]){
                    parent[root2] = root1;
                    rank[root1] += rank[root2];
                } else{
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                }
                return true;
            } else{
                return false;
            }
        }

        private int root(int i){
            if (parent[i] != i){
                parent[i] = root(parent[i]);
            }
            return parent[i];
        }

    }

    public int prim(int V, List<int[]> edges, int root){
        //Collections.sort(edges, (a, b) -> (a[2] - b[2]));
        Collections.sort(edges, new Comparator<int[]>(){
            public int compare(int[] a, int[] b){
                return a[2] - b[2];
            }
        });
        UF uf = new UF(V);
        int result = 0;
        for (int[] edge : edges){
            if (uf.union(edge[0], edge[1])){
                result += edge[2];
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] counts = in.nextLine().split("\\s+");
        int V = Integer.parseInt(counts[0]);
        int E = Integer.parseInt(counts[1]);
        List<int[]> edges = new ArrayList<>();
        for (int i = 0; i < E; i++){
            String[] input = in.nextLine().split("\\s+");
            int[] edge = new int[3];
            for (int j = 0; j < 3; j++){
                edge[j] = Integer.parseInt(input[j]);
            }
            edges.add(edge);
        }
        int root = Integer.parseInt(in.nextLine());
        MST_Prim soln = new MST_Prim();
        System.out.println(soln.prim(V, edges, root));
    }
}
