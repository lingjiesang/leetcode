package graph;

import fastIO.InputReader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Dijkstra_singleSourceShortestPathes {
    class IndexMinHeap<E extends Comparable<E>> {
        List<E> heap;
        Map<E, Integer> map;

        public IndexMinHeap() {
            heap = new ArrayList<>();
            map = new HashMap<>();
        }

        public void offer(E elem) {
            heap.add(elem);
            map.put(elem, heap.size() - 1);

            siftUp(heap.size() - 1);
        }

        public E peek() {
            return heap.get(0);
        }


        public E poll() {
            E max = heap.get(0);
            swap(0, heap.size() - 1);

            map.remove(heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);

            siftDown(0);
            return max;
        }

        public void decreaseKey(E elem) {
            int index = map.get(elem);
            siftUp(index);
        }

        /*********
         * used by this problem
         ***************/
        public boolean contains(E elem) {
            return map.containsKey(elem);
        }

        public boolean isEmpty() {
            return map.size() == 0;
        }

        /*********
         * helpers
         ***************/
        private void siftUp(int index) {
            int parent = (index - 1) / 2;

            if (heap.get(parent).compareTo(heap.get(index)) > 0) {
                swap(index, parent);
                siftUp(parent);
            }
        }

        private void siftDown(int index) {
            int left = index * 2 + 1;
            int right = index * 2 + 2;

            int smallest = index;
            if (left < heap.size() && heap.get(left).compareTo(heap.get(smallest)) < 0) {
                smallest = left;
            }
            if (right < heap.size() && heap.get(right).compareTo(heap.get(smallest)) < 0) {
                smallest = right;
            }
            if (smallest != index) {
                swap(smallest, index);
                siftDown(smallest);
            }
        }

        private void swap(int pos1, int pos2) {
            if (pos1 != pos2) {
                E temp = heap.get(pos1);
                heap.set(pos1, heap.get(pos2));
                heap.set(pos2, temp);
                map.put(heap.get(pos1), pos1);
                map.put(heap.get(pos2), pos2);
            }
        }
    }

    class Edge {
        Vertex u, v;
        int weight;

        Edge(Vertex u, Vertex v, int weight) {
            this.u = u;
            this.v = v;
            this.weight = weight;
        }

        Vertex getOther(Vertex w) {
            return w == u ? v : u;
        }

    }

    public class Vertex implements Comparable<Vertex> {
        int index;
        int dist;  //dist to source
        Vertex parent;
        List<Edge> edges;

        public Vertex(int index, int dist) {
            this.index = index;
            this.dist = dist;
            edges = new ArrayList<>();
        }

        public int compareTo(Vertex other) {
            return this.dist - other.dist;
        }

        public void addEdge(Edge e) {
            edges.add(e);
        }
    }

    private boolean relax(Vertex u, Vertex v, int weight) {
        if (v.dist > u.dist + weight) {
            v.dist = u.dist + weight;
            v.parent = u;
            return true;
        } else {
            return false;
        }
    }


    public void dijkstra(int V, List<int[]> edges, int root) {
        List<Vertex> vertices = new ArrayList<>();
        vertices.add(null); //0, unused
        for (int i = 1; i <= V; i++) {
            vertices.add(new Vertex(1, Integer.MAX_VALUE));
        }
        for (int[] edge : edges) {
            Edge e = new Edge(vertices.get(edge[0]), vertices.get(edge[1]), edge[2]);
            vertices.get(edge[0]).addEdge(e);
            vertices.get(edge[1]).addEdge(e);
        }

        vertices.get(root).dist = 0;
        IndexMinHeap<Vertex> heap = new IndexMinHeap<>();

        for (int i = 1; i <= V; i++) {
            heap.offer(vertices.get(i));

        }

        while (!heap.isEmpty()) {
            Vertex u = heap.poll();
            if (u.dist == Integer.MAX_VALUE) {
                break;
            }

            for (Edge e : u.edges) {
                Vertex v = e.getOther(u);
                if (heap.contains(v) && relax(u, v, e.weight)) {
                    heap.decreaseKey(v);
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <= V; i++) {
            if (i != root) {
                int dist = vertices.get(i).dist;
                if (dist == Integer.MAX_VALUE) {
                    dist = -1;
                }
                sb.append(dist).append(" ");
            }
        }
        System.out.println(sb.toString().trim());
    }



    public static void main(String[] args) {


        try {
            long startTime = System.currentTimeMillis();

            String base = "/Users/lingjie/Dropbox/";
            String project = "leetcode_online/myJavaSoln/LeetcodeJava/";
            Path file = Paths.get(base, project, "data/Dijkstra_singleSourceShortestPathes_input07.txt");

            InputReader in 	= new InputReader(Files.newInputStream(file));

            Dijkstra_singleSourceShortestPathes soln = new Dijkstra_singleSourceShortestPathes();


            int tests = in.readInt();
            for (int t = 0; t < tests; t++) {
                int V = in.readInt();
                int E = in.readInt();
                List<int[]> edges = new ArrayList<>();
                for (int i = 0; i < E; i++) {
                    int[] edge = new int[3];
                    for (int j = 0; j < 3; j++) {
                        edge[j] = in.readInt();
                    }
                    edges.add(edge);
                }
                int root = in.readInt();

                long readTime = System.currentTimeMillis();
                System.out.println("finish reading for test " + t + " used (seconds): " + ((readTime - startTime) / 1000.0));
                soln.dijkstra(V, edges, root);
                long finishTime = System.currentTimeMillis();
                System.out.println("finish processing for test " + t + " used (seconds): " + ((finishTime - readTime) / 1000.0));


            }

        } catch (IOException x) {
            System.err.println(x);
        }
    }
}
