package graph;

import java.util.*;

/**
 * 16ms
 */
public class CourseScheduleII_DFS {

    public int[] findOrder(int numCourses, int[][] prerequisites) {
        List<Set<Integer>> outEdges = new ArrayList<>(); // use Set instead of List here to prevent prerequisites contains repetitive inputs!

        constructMap(numCourses, prerequisites, outEdges);

        BitSet visited = new BitSet(numCourses); // black + grey
        BitSet onStack = new BitSet(numCourses); // grey
        Deque<Integer> order = new ArrayDeque<>();
        for (int i = 0; i < numCourses; i++) {
            if (!visited.get(i) && !dfs_visit(i, outEdges, visited, onStack, order)) {
                return new int[0];
            }
        }
        int[] orderArray = new int[numCourses];
        for (int i = 0; i < numCourses; i++) {
            orderArray[i] = order.pollFirst();
        }
        return orderArray;

    }

    private void constructMap(int numCourses, int[][] prerequisites, List<Set<Integer>> outEdges) {
        for (int i = 0; i < numCourses; i++) {
            outEdges.add(new HashSet<>());
        }
        for (int[] prereq : prerequisites) {
            int inVertex = prereq[0];
            int outVertex = prereq[1];
            outEdges.get(outVertex).add(inVertex);
        }
    }


    private boolean dfs_visit(int outVertex, List<Set<Integer>> outEdges, BitSet visited, BitSet onStack, Deque<Integer> order) {
        visited.set(outVertex);
        onStack.set(outVertex);
        for (int inVertex : outEdges.get(outVertex)) {
            if (onStack.get(inVertex)) { // grey! back edge ==> found cycle
                return false;
            }
            if (!visited.get(inVertex) && !dfs_visit(inVertex, outEdges, visited, onStack, order)) {
                return false;
            }

        }
        onStack.clear(outVertex);
        order.addFirst(outVertex);
        return true;
    }
}
