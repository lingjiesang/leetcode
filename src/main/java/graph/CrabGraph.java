package graph;

import java.util.*;

/**
 * ref: https://www.hackerrank.com/challenges/crab-graphs
 *
 *
 * see discussion
 */

public class CrabGraph {
    private boolean bfs(Map<Integer, Integer>[] vertices, int source, int sink, int[] parents) {

        int V = vertices.length;

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[V];

        queue.offer(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int u = queue.poll();

            for (Integer v : vertices[u].keySet()) {
                if (!visited[v] && vertices[u].get(v) > 0) {
                    parents[v] = u;
                    queue.offer(v);
                    visited[v] = true;
                }
            }
        }

        return visited[sink];

    }

    private int fordFulkerson(Map<Integer, Integer>[] vertices, int source, int sink) {
        int V = vertices.length;

        int[] parents = new int[V];

        int flow = 0;
        while (bfs(vertices, source, sink, parents)) {
            int v = sink;
            int minCapcity = Integer.MAX_VALUE;
            while (v != source) {
                int u = parents[v];
                minCapcity = Math.min(minCapcity, vertices[u].get(v));
                v = u;
            }


            v = sink;
            while (v != source) {
                int u = parents[v];
                vertices[u].put(v, vertices[u].get(v) - minCapcity);
                vertices[v].put(u, vertices[v].getOrDefault(u, 0) + minCapcity);
                v = u;
            }
            flow += minCapcity;

        }
        return flow;
    }

    public int countCrabs(int V, int[][] edges, int max_feet) {

        Map<Integer, Integer>[] vertices = new HashMap[V * 2 + 2];
        for (int i = 0; i < V * 2 + 2; i++) {
            vertices[i] = new HashMap<>();
        }

        for (int[] edge : edges) {
            int u = edge[0];
            int v = edge[1];

            int u_l = u * 2;
            int u_r = u * 2 + 1;

            int v_l = v * 2;
            int v_r = v * 2 + 1;

            vertices[u_l].put(v_r, 1);
            vertices[v_l].put(u_r, 1);
        }

        int source = 0;
        for (int i = 1; i <= V; i++) {
            vertices[source].put(i * 2, Math.min(max_feet, vertices[i * 2].size()));
        }

        int sink = 1;
        for (int i = 1; i <= V; i++) {
            vertices[i * 2 + 1].put(sink, 1);
        }
        return fordFulkerson(vertices, 0, 1);

    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        CrabGraph soln = new CrabGraph();
        int test_numbers = Integer.valueOf(in.nextLine());

        for (int t = 0; t < test_numbers; t++) {
            String[] input = in.nextLine().split("\\s+");

            int V = Integer.valueOf(input[0]);
            int max_feet = Integer.valueOf(input[1]);
            int E = Integer.valueOf(input[2]);

            int[][] edges = new int[E][2];
            for (int i = 0; i < E; i++) {
                input = in.nextLine().split("\\s+");
                edges[i][0] = Integer.valueOf(input[0]);
                edges[i][1] = Integer.valueOf(input[1]);
            }
            System.out.println(soln.countCrabs(V, edges, max_feet));
        }

    }
}
