package graph;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/floyd-city-of-blinding-lights
 */
public class FloydWarshall_shortestPathes {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] input = in.nextLine().split("\\s+");
        int N = Integer.valueOf(input[0]);
        int M = Integer.valueOf(input[1]);
        int[][] W = new int[N + 1][N + 1];
        for (int[] w : W){
            Arrays.fill(w, Integer.MAX_VALUE / 2);
        }
        for (int i = 1; i <= N; i++){
            W[i][i] = 0;
        }

        for (int i = 0; i < M; i++){
            input = in.nextLine().split("\\s+");
            int u = Integer.valueOf(input[0]);
            int v = Integer.valueOf(input[1]);
            int w = Integer.valueOf(input[2]);

            W[u][v] = w;
        }

        for (int k = 1; k <= N; k++){
            int[][] W_k = new int[N + 1][N + 1];
            for (int i = 1; i <= N; i++){
                for (int j = 1; j <= N; j++){
                    W_k[i][j] = Math.min(W[i][j], W[i][k] + W[k][j]);
                }
            }
            W = W_k;
        }

        int Q = Integer.valueOf(in.nextLine());
        for (int q = 0; q < Q; q++){
            input = in.nextLine().split("\\s+");
            int u = Integer.valueOf(input[0]);
            int v = Integer.valueOf(input[1]);
            if (W[u][v] == Integer.MAX_VALUE / 2){
                System.out.println(-1);
            } else{
                System.out.println(W[u][v]);
            }
        }


    }
}
