package graph;

import java.util.*;

/**
 * 31ms
 */
public class GraphValidTree_BFS {
    /**
     * Tree
     * 1. no cycle
     * 2. one node is root (because it is undirected, any node can be root!!!)
     * 3. all other nodes must be able to visited from this root once and only once!
     *
     * Here we use BFS
     */
    public boolean validTree(int n, int[][] edges) {
        List<Set<Integer>> outEdges = new ArrayList<>();
        constructGraph(n, edges, outEdges);

        Deque<Integer> queue = new ArrayDeque<>(); //grey
        boolean[] visited = new boolean[n]; //black and grey

        visited[0] = true; // enqueue之前先变grey(算在visited里), 绝对不是queue.poll()以后才变grey
        queue.offer(0);

        while (!queue.isEmpty()){
            int curt = queue.poll();
            for (int child : outEdges.get(curt)){
                if (visited[child]){
                    return false;
                }
                outEdges.get(child).remove(curt);
                queue.add(child);
                visited[child] = true;
            }
        }
        for (int i = 0; i < n; i++){
            if (!visited[i]){
                return false;
            }
        }
        return true;
    }

    private void constructGraph(int n, int[][] edges, List<Set<Integer>> outEdges){
        for (int i = 0; i < n; i++){
            outEdges.add(new HashSet<>());
        }
        for (int[] edge : edges){
            outEdges.get(edge[0]).add(edge[1]);
            outEdges.get(edge[1]).add(edge[0]);
        }
    }
}
