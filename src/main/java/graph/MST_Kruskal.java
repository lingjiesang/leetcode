package graph;

import java.util.*;

/**
 * https://www.hackerrank.com/challenges/kruskalmstrsub
 */
public class MST_Kruskal {
    class IndexMinHeap<E extends Comparable<E>>{
        List<E> heap;
        Map<E, Integer> map;

        public IndexMinHeap(){
            heap = new ArrayList<>();
            map = new HashMap<>();
        }

        public void offer(E elem){
            heap.add(elem);
            map.put(elem, heap.size() - 1);

            siftUp(heap.size() - 1);
        }

        public E peek(){
            return heap.get(0);
        }


        public E poll() {
            E max = heap.get(0);
            swap(0, heap.size() - 1);

            map.remove(heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);

            siftDown(0);
            return max;
        }

        public void decreaseKey(E elem){
            int index = map.get(elem);
            siftUp(index);
        }

        /********* used by this problem ***************/
        public boolean contains(E elem){
            return map.containsKey(elem);
        }

        public boolean isEmpty(){
            return map.size() == 0;
        }

        /********* helpers ***************/
        private void siftUp(int index){
            int parent = (index - 1) / 2;

            if (heap.get(parent).compareTo(heap.get(index)) > 0){
                swap(index, parent);
                siftUp(parent);
            }
        }

        private void siftDown(int index){
            int left = index * 2 + 1;
            int right = index * 2 + 2;

            int smallest = index;
            if (left < heap.size() && heap.get(left).compareTo(heap.get(smallest)) < 0){
                smallest = left;
            }
            if (right < heap.size() && heap.get(right).compareTo(heap.get(smallest)) < 0){
                smallest = right;
            }
            if (smallest != index){
                swap(smallest, index);
                siftDown(smallest);
            }
        }

        private void swap(int pos1, int pos2){
            if (pos1 != pos2) {
                E temp = heap.get(pos1);
                heap.set(pos1, heap.get(pos2));
                heap.set(pos2, temp);
                map.put(heap.get(pos1), pos1);
                map.put(heap.get(pos2), pos2);
            }
        }
    }


    public class Vertex implements Comparable<Vertex>{
        int index;
        int dist;  //dist to its parent (used as key in heap)
        Vertex parent;
        List<Edge> edges;

        public Vertex(int index, int dist){
            this.index = index;
            this.dist = dist;
            edges = new ArrayList<>();
        }
        public int compareTo(Vertex other){
            if (this.dist != other.dist){
                return this.dist - other.dist;
            } else{
                return this.dist + this.index + (parent == null? 0 : parent.index) -
                        (other.dist + other.index + (other.parent == null ? 0 : other.parent.index));
            }
        }
        public void addEdge(Edge e){
            edges.add(e);
        }
    }

    class Edge{
        Vertex u, v;
        int weight;
        Edge(Vertex u, Vertex v, int weight){
            this.u = u;
            this.v = v;
            this.weight = weight;
        }

        Vertex getOther(Vertex w){
            if (w == u){
                return v;
            } else{
                return u;
            }
        }
    }

    public int kruskal(int V, List<int[]> edges, int root){
        List<Vertex> vertices = new ArrayList<>();
        vertices.add(null); // index 0 is not used!
        for (int i = 1; i <= V; i++){
            vertices.add(new Vertex(i, Integer.MAX_VALUE));
        }

        for (int[] edge : edges){
            Edge e = new Edge(vertices.get(edge[0]), vertices.get(edge[1]), edge[2]);
            vertices.get(edge[0]).addEdge(e);
            vertices.get(edge[1]).addEdge(e);
        }

        vertices.get(root).dist = 0;

        IndexMinHeap<Vertex> heap = new IndexMinHeap<>();
        for (int i = 1; i <= V; i++){
            heap.offer(vertices.get(i));
        }

        int result = 0;
        while (!heap.isEmpty()){
            Vertex v = heap.poll();
            result += v.dist;

            for (Edge e : v.edges){
                Vertex u = e.getOther(v);
                if (heap.contains(u) && e.weight < u.dist){
                    u.parent = v;
                    u.dist = e.weight;
                    heap.decreaseKey(u);
                }
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String[] counts = in.nextLine().split("\\s+");
        int V = Integer.parseInt(counts[0]);
        int E = Integer.parseInt(counts[1]);
        List<int[]> edges = new ArrayList<>();
        for (int i = 0; i < E; i++){
            String[] input = in.nextLine().split("\\s+");
            int[] edge = new int[3];
            for (int j = 0; j < 3; j++){
                edge[j] = Integer.parseInt(input[j]);
            }
            edges.add(edge);
        }
        int root = Integer.parseInt(in.nextLine());
        MST_Kruskal soln = new MST_Kruskal();
        System.out.println(soln.kruskal(V, edges, root));
    }
}
