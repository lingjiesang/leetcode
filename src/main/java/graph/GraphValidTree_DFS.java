package graph;

import java.util.*;


public class GraphValidTree_DFS {
    private void constructGraph(int n, int[][] edges, List<Set<Integer>> outEdges){
        for (int i = 0; i < n; i++){
            outEdges.add(new HashSet<>());
        }
        for (int[] edge : edges){
            outEdges.get(edge[0]).add(edge[1]);
            outEdges.get(edge[1]).add(edge[0]);
        }
    }
    public boolean validTree(int n, int[][] edges) {
        List<Set<Integer>> outEdges = new ArrayList<>();
        constructGraph(n, edges, outEdges);

        boolean[] visited = new boolean[n];
        if (!dfs(0, visited, outEdges)){
            return false;
        }
        for (boolean v : visited){
            if (!v){
                return false;
            }
        }
        return true;
    }

    private boolean dfs(int root, boolean[] visited, List<Set<Integer>> outEdges){
        visited[root] = true;

        for (int child : outEdges.get(root)) {
            if (!visited[child]){
                outEdges.get(child).remove(root);
                // 无相图中打算visit一个边, 就去掉其反向的边 (如果已经发现child已经visit了就不要remove了,
                // 因为在有cycle的时候下游的dfs recursion可能已经remove过了)
                // 这个时候这层的iterator就会报错了!
                // 这个题目倒是没事,因为有cycle就立刻会返回了,但如果不是找cycle的情况就会出问题
                // 虽然如果不是找cycle并没必要remove反向的边!

                if (!dfs(child, visited, outEdges)){
                    return false;
                }
            } else{
                return false;
            }
        }
        return true;
    }


}
