package graph;

import java.util.*;

public class CourseScheduleII_BFS {

    public int[] findOrder(int numCourses, int[][] prerequisites) {
        List<Set<Integer>> outEdges = new ArrayList<>();
        // use Set instead of List here to prevent prerequisites contains repetitive inputs!

        int[] inDegree = new int[numCourses];
        constructMap(numCourses, prerequisites, outEdges, inDegree);

        Deque<Integer> queue = new ArrayDeque<>();
        for (int i = 0; i < numCourses; i++){
            if (inDegree[i] == 0){
                queue.add(i);
            }
        }

        int[] res = new int[numCourses];
        int len = 0;

        while (!queue.isEmpty()){
            int outVertex = queue.poll();
            res[len++] = outVertex;
            Set<Integer> inVertices = outEdges.get(outVertex);
            for (int inVertex : inVertices){
                inDegree[inVertex]--;
                if (inDegree[inVertex] == 0){
                    queue.add(inVertex);
                }
            }
        }
        return len == numCourses ? res : new int[0];
    }

    private void constructMap(int numCourses, int[][] prerequisites, List<Set<Integer>> outEdges,int[] inDegree){
        for (int i = 0; i < numCourses; i++){
            outEdges.add(new HashSet<>());
        }
        for (int[] prereq : prerequisites){
            int inVertex = prereq[0];
            int outVertex = prereq[1];
            if (outEdges.get(outVertex).add(inVertex)){
                inDegree[inVertex]++;
            }
        }
    }
}
