package graph;

import java.util.*;

public class EvaluateDivision {
    Map<String, Map<String, Double>> map;
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        map = new HashMap<>();
        for (int i = 0; i < equations.length; i++){
            addToMap(equations[i][0], equations[i][1], values[i]);
            if (values[i] != 0.0){
                addToMap(equations[i][1], equations[i][0], 1 / values[i]);
            }
        }
        double[] result = new double[queries.length];
        for (int i = 0;  i < queries.length; i++){
            result[i] = bfs(queries[i][0], queries[i][1]);
        }
        return result;
    }

    private void addToMap(String dividend, String divisor, double quotient){
        if (!map.containsKey(dividend)){
            map.put(dividend, new HashMap<>());
        }
        map.get(dividend).put(divisor, quotient);
    }

    private double bfs(String dividend, String divisor){
        if (!map.containsKey(dividend)){
            return -1.0;
        }

        Deque<String> deque = new ArrayDeque<>();
        Deque<Double> curtResult = new ArrayDeque<>();
        Set<String> visited = new HashSet<>();
        deque.offer(dividend);
        curtResult.offer(1.0);
        visited.add(dividend);

        while (!deque.isEmpty()){
            dividend = deque.poll();
            double lastResult = curtResult.poll();

            for (String intermediate : map.get(dividend).keySet()){
                double result = lastResult * map.get(dividend).get(intermediate);
                if (intermediate.equals(divisor)){
                    return result;
                }
                if (map.containsKey(intermediate) && !visited.contains(intermediate)){
                    deque.offer(intermediate);
                    curtResult.offer(result);
                    visited.add(intermediate);
                }
            }
        }
        return -1.0;
    }
}
