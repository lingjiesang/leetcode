package treeMap;

// ref: https://discuss.leetcode.com/topic/46887/java-solution-using-treemap-real-o-logn-per-adding
import java.util.*;
public class SummaryRanges {
    public class Interval{
        int start;
        int end;
        Interval() { start = 0; end = 0; }
        Interval(int s, int e) {start = s; end = e;}
    }

    private TreeMap<Integer, Interval> treeMap;
    /** Initialize your data structure here. */
    public SummaryRanges() {
        treeMap = new TreeMap<>();
    }

    public void addNum(int val) {
        if (treeMap.containsKey(val)){ // don't forget this!!!
            return;
        }
        Integer l = treeMap.lowerKey(val);
        Integer h = treeMap.higherKey(val);

        if (l != null && h != null && treeMap.get(l).end + 1 == val && treeMap.get(h).start - 1 == val) {
            treeMap.get(l).end = treeMap.get(h).end;
            treeMap.remove(h);
        } else if (l != null && treeMap.get(l).end + 1 >= val) {
            treeMap.get(l).end = Math.max(treeMap.get(l).end, val);
        } else if (h != null && treeMap.get(h).start - 1 == val) {
            treeMap.put(val, new Interval(val, treeMap.get(h).end)); // key: key changed!!!
            treeMap.remove(h);
        } else {
            treeMap.put(val, new Interval(val, val));
        }

    }

    public List<Interval> getIntervals() {
        List<Interval> res = new ArrayList<>();
        res.addAll(treeMap.values());
        return res;
    }

}
