package treeMap;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/27608/java-python-one-pass-solution-o-n-time-o-n-space-using-buckets
 */
public class ContainsDuplicateIII_BucketSort {
    long getBucketNum(long val, long bucketWidth){
        return (val - Integer.MIN_VALUE) / bucketWidth;
    }
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (k < 1 || t < 0){
            return false;
        }

        Map<Long, Long> buckets = new HashMap<>();
        long bucketWidth = (long)t + 1;
        for (int i = 0; i < nums.length; i++){
            long bucketNum = getBucketNum((long)nums[i], bucketWidth);
            if (buckets.containsKey(bucketNum)){
                return true;
            }
            if (buckets.containsKey(bucketNum - 1) && nums[i] - buckets.get(bucketNum - 1) <= t){
                return true;
            }
            if (buckets.containsKey(bucketNum + 1) && buckets.get(bucketNum + 1) - nums[i] <= t){
                return true;
            }
            buckets.put(bucketNum, (long)nums[i]);
            if (i >= k){
                buckets.remove(getBucketNum((long)nums[i - k], bucketWidth));
            }
        }
        return false;
    }
}
