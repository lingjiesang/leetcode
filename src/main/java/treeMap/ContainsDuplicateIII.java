package treeMap;

import java.util.*;

public class ContainsDuplicateIII {
    public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (k < 1 || t < 0){
            return false;
        }
        TreeSet<Long> treeSet = new TreeSet<>();

        for (int i = 0; i < nums.length; i++){
            Long ceil = treeSet.floor((long)nums[i] + t);
            if (ceil != null && ceil >= nums[i] - t){
                return true;
            }

            if (i >= k){
                treeSet.remove((long)nums[i - k]);
            }
            treeSet.add((long)nums[i]);
        }
        return false;
    }

    public static void main(String[] args){
        ContainsDuplicateIII soln = new ContainsDuplicateIII();
        int[] nums = {4, 2};
        soln.containsNearbyAlmostDuplicate(nums, 2, 1);
    }
}
