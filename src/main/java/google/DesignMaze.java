package google;

import java.util.*;

public class DesignMaze {
    private int m;
    private int n;

    private int[][] maze;
    private boolean[][] isVisited;
    private List<int[]> visitedList;

    int[][] directions = new int[][]{{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    private static int TOP = 1;
    private static int BOTTOM = 2;
    private static int LEFT = 4;
    private static int RIGHT = 8;

    private Random random;

    private boolean inBound(int row, int col, int[] direction){
        return row + direction[0] >= 0 && row + direction[0] < m
                && col + direction[1] >= 0 && col + direction[1] < n;
    }

    public void designMaze(int m, int n){
        this.m = m;
        this.n = n;
        maze = new int[m][n];
        isVisited = new boolean[m][n];
        visitedList = new ArrayList<>();
        random = new Random();

        int row = random.nextInt(m);
        int col = random.nextInt(n);
        visitedList.add(new int[]{row, col});
        isVisited[row][col] = true;

        while (visitedList.size() < m * m){

            List<int[]> neighbors = new ArrayList<>();
            for (int[] dire : directions){
                if (inBound(row, col, dire) && !isVisited[row + dire[0]][col + dire[1]]){
                    neighbors.add(new int[]{row + dire[0], col + dire[1]});
                }
            }
            if (!neighbors.isEmpty()){
                int[] next = neighbors.get(random.nextInt(neighbors.size()));
                removeBrick(row, col, next[0], next[1]);
                row = next[0];
                col = next[1];
                visitedList.add(next);
                isVisited[row][col] = true;
            } else{
                int[] next = visitedList.get(random.nextInt(visitedList.size()));
                row = next[0];
                col = next[1];
            }
        }
    }

    // bit 1: no wall
    // bit 0: has wall
    private void removeBrick(int row1, int col1, int row2, int col2){
        if (row1 != row2){
            maze[Math.max(row1, row2)][col1] |= TOP;  //remove top brick
            maze[Math.min(row1, row2)][col1] |= BOTTOM; // remove bottom brick
        } else{
            maze[row1][Math.max(col1, col2)] |= LEFT; //remove left brick
            maze[row1][Math.min(col1, col2)] |= RIGHT; // remove right brick
        }
    }

    public void printMaze(){
        for (int i = 0; i < m; i++){
            //print top wall
            System.out.print(" ");
            for (int j = 0; j < n; j++){
                if ((maze[i][j] & TOP) == 0){
                    System.out.print("- ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println();

            //print left wall
            for (int j = 0; j < n; j++){
                if ((maze[i][j] & LEFT) == 0){
                    System.out.print("| ");
                }else{
                    System.out.print("  ");
                }
            }
            System.out.println("|");
        }
        //print bottom
        System.out.print(" ");

        for (int j = 0; j < n; j++){
            System.out.print("- ");
        }
        System.out.println();

    }

    public static void main(String[] args){
        DesignMaze soln = new DesignMaze();
        soln.designMaze(10, 10);
        soln.printMaze();
    }
}
