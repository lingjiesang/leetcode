package google;

import java.util.*;

public class EncodeString {


    public String encode(String s) {
        int n = s.length();
        String[][] dp = new String[n][n];

        for (int step = 0; step < s.length(); step++){
            for (int start = 0; start + step < s.length(); start++){
                String substr = s.substring(start, start + step + 1);
                dp[start][start + step] = substr;

                if (substr.length() < 4){
                    continue;
                }

                for (int i = start; i < start + step; i++){
                    if (dp[start][i].length() + dp[i + 1][start + step].length() < dp[start][start + step].length()){
                        dp[start][start + step] = dp[start][i] + dp[i + 1][start + step];
                    }

                    String pattern = s.substring(start, i + 1);
                    if (isPattern(substr, pattern) && dp[start][i].length() + 3 < dp[start][start + step].length()){
                        dp[start][start + step] = substr.length() / pattern.length() + "[" + dp[start][i] + "]";
                    }
                }


            }
        }
        return dp[0][n - 1];

    }

    private boolean isPattern(String s, String p){
        if (s.length() % p.length() != 0){
            return false;
        }

        for (int i = 0; i < s.length(); i +=  p.length()){
            if (!s.substring(i, i + p.length()).equals(p)){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        String s = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        EncodeString soln = new EncodeString();
        System.out.println(soln.encode(s));

    }
}
