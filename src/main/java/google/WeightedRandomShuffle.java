package google;

import java.util.*;

public class WeightedRandomShuffle {
    /**
     * ref: http://softwareengineering.stackexchange.com/questions/233541/how-to-implement-a-weighted-shuffle
     */
    class SgT{
        int[] sgt;

        public SgT(){
            int n = (int)Math.pow(2, Math.ceil(Math.log(nums.length)/ Math.log(2))) * 2 - 1;
            sgt = new int[n];

            for (int i = 0; i < nums.length; i++){
                update(i, nums[i]);
            }
        }

        public int totalSum(){
            return sgt[0];
        }
        /**
         * return the index in nums so that
         * nums[0] + ... + nums[index - 1] < sum
         * nums[0] + ... + nums[index] >= sum
         */
        public int findIdx(int sum){
            return findIdxUtil(0, 0, nums.length - 1, sum);
        }

        public int findIdxUtil(int root_idx, int cn_st, int cn_ed, int sum){
            if (cn_st == cn_ed){
                return cn_st;
            }

            int cn_mid = cn_st + (cn_ed - cn_st) / 2;
            if (sum < sgt[root_idx * 2 + 1]){
                return findIdxUtil(root_idx * 2 + 1, cn_st, cn_mid, sum);
            } else{
                return findIdxUtil(root_idx * 2 + 2, cn_mid + 1, cn_ed, sum - sgt[root_idx * 2 + 1]);
            }

        }

        public void update(int index, int delta){
            updateUtil(0, 0, nums.length - 1, index, delta);
        }

        private void updateUtil(int root_idx, int cn_st, int cn_ed, int n_idx, int delta){
            if (n_idx > cn_ed || n_idx < cn_st){
                return;
            }

            sgt[root_idx] += delta;
            if (cn_st != cn_ed){
                int cn_mid = cn_st + (cn_ed - cn_st) / 2;
                updateUtil(root_idx * 2 + 1, cn_st, cn_mid, n_idx, delta);
                updateUtil(root_idx * 2 + 2, cn_mid + 1, cn_ed, n_idx, delta);
            }
        }
    }

    private int[] nums;
    private SgT sgT;
    private Random random;

    public WeightedRandomShuffle(int[] nums){
        this.nums = nums.clone();
        random = new Random();
        sgT = new SgT();
    }


    public int[] shuffle(){
        sgT = new SgT(); // every time shuffle, reconstruct sgT from original input nums

        int[] result = new int[nums.length];

        TreeSet<Integer> indices = new TreeSet<>();
        for (int i = 0; i < nums.length; i++){
            indices.add(i);
        }

        for (int i = 0; i < nums.length; i++){
            int rand = random.nextInt(sgT.totalSum());

            int index = 0;
            if (rand == 0){ // should return the item not selected with minimal index, not necessary item with index = 0!!
                index = indices.first();
            } else{
                index = sgT.findIdx(rand);
            }

            result[i] = index;
            indices.remove((Integer)index);
            sgT.update(index, 0 - nums[index]);
        }
        return result;
    }

    /**
     * for test only
     */
    public int sgT_findIdx(int i){
        return sgT.findIdx(i);
    }

    public static void main(String[] args) {
        int[] item_weights = {3, 5, 2, 6, 3, 7, 9, 1};

        WeightedRandomShuffle soln = new WeightedRandomShuffle(item_weights);
        System.out.println(soln.sgT_findIdx(3));  //0
        System.out.println(soln.sgT_findIdx(7));  //1
        System.out.println(soln.sgT_findIdx(12));  //3

        for (int i = 0; i < 3; i++) {
            // every shuffle should contains random shuffle of 0 - item_weights.size() - 1
            System.out.println(Arrays.toString(soln.shuffle()));
        }

        System.out.println("Frequency of variables appearing first:");

        int round_num = 1000000;
        int[] count = new int[item_weights.length];
        for (int i = 0; i < round_num; i++) {
            int[] shuffled = soln.shuffle();
            count[shuffled[0]]++;
        }
        System.out.println(Arrays.toString(count));

        int[] theoretical = new int[item_weights.length];
        int sumWeight = 0;
        for (int weight : item_weights) {
            sumWeight += weight;
        }
        for (int i = 0; i < item_weights.length; i++) {
            theoretical[i] = round_num * item_weights[i] / sumWeight;
        }
        System.out.println(Arrays.toString(theoretical));
    }
}
