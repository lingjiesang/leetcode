package google;

import java.util.*;

// ref: http://www.csie.ntnu.edu.tw/~u91029/Matching.html#3
public class BananaList_matching {

    private static int V;
    private static boolean[][] adj;
    private static List<Integer>[] path;
    private static int[] match;
    private static int[] visited;
    private static Deque<Integer> queue;

    public static int gcd(int a, int b){
        return b == 0 ? a : gcd(b, a % b);
    }


    public static boolean loopForever(int x, int y){
        int factor = gcd(x, y);
        int n = (x + y) / factor;
        return (n & (n - 1)) != 0;
    }


    private static boolean bfs(int root){
        for (int i = 0; i < V; i++){
            path[i] = new ArrayList<>();
        }

        path[root].add(root);

        queue = new ArrayDeque<>();
        visited = new int[V];
        Arrays.fill(visited, -1);


        queue.offer(root);
        visited[root] = 0;

        while (!queue.isEmpty()){
            int prev = queue.poll();
            for (int curt = 0; curt < V; curt++){
                if (adj[prev][curt] && match[curt] != curt){
                    if (visited[curt] == -1){
                        if (match[curt] == -1){
                            for (int i = 0; i < path[prev].size() - 1; i += 2){
                                match[path[prev].get(i)] = path[prev].get(i + 1);
                                match[path[prev].get(i + 1)] = path[prev].get(i);
                            }
                            match[prev] = curt;
                            match[curt] = prev;
                            return true;
                        } else{
                            int next = match[curt];
                            path[next] = new ArrayList<>(path[prev]);
                            path[next].add(curt);
                            path[next].add(next);

                            visited[curt] = 1;
                            visited[next] = 0;
                            queue.offer(next);
                        }
                    } else if (visited[curt] == 0){
                        int index = 0;
                        while (index < path[prev].size()
                                && index < path[curt].size()
                                && path[prev].get(index).equals(path[curt].get(index))){
                            index++;
                        }
                        index--;
                        add_path(prev, curt, index);
                        add_path(curt, prev, index);
                    }
                }
            }
        }
        return false;
    }


    private static void add_path(int node1, int node2, int index_lca){
        for (int i = index_lca + 1; i < path[node1].size(); i++){
            int node = path[node1].get(i);
            if (visited[node] == 1){
                path[node] = new ArrayList<>(path[node2]);

                for (int j = path[node1].size() - 1; j >= i; j--){
                    path[node].add(path[node1].get(j));
                }
                visited[node] = 0;
                queue.offer(node);
            }
        }
    }

    public static int answer(int[] banana_list) {

        V = banana_list.length;

        adj = new boolean[V][V];

        for (int i = 0; i < V; i++){
            for (int j = 0; j < i; j++){
                if (loopForever(banana_list[i], banana_list[j])){
                    adj[i][j] = true;
                    adj[j][i] = true;
                }
            }
        }

        path = new List[V];

        match = new int[V];
        Arrays.fill(match, -1);

        int m = 0;
        for (int i = 0; i < V; i++){
            if (match[i] == -1){
                if (bfs(i)){
                    m++;
                } else{
                    match[i] = i;
                }
            }
        }
        return V - m * 2;


    }

    public static void main(String[] args){
        System.out.println(answer(new int[]{1, 7, 3, 21, 13, 19}));
        System.out.println(answer(new int[]{1, 21, 13}));
    }

}
