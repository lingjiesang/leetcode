package google;

import java.util.*;

/**
 * here is my solution using stack
 *
 * dfs: https://discuss.leetcode.com/topic/68336/5ms-java-dfs-solution
 */
public class TernaryExpressionParser {
    public String parseTernary(String expression) {
        if (expression.length() == 0){
            return "";
        }
        Deque<String> first = new ArrayDeque<>();
        Deque<String> second = new ArrayDeque<>();

        int pos = 0;
        while (pos < expression.length()){
            int next = nextDelimit(expression, pos);
            //System.out.println(pos + " " + next);
            String token = expression.substring(pos, next);

            if (next < expression.length() && expression.charAt(next) == '?'){
                if (first.size() > second.size()){
                    second.push("");
                }
                first.push(token);
            } else{
                addToken(first, second, token);
            }
            pos = next + 1;
        }
        return second.pop();
    }

    private void addToken(Deque<String> first, Deque<String> second, String token){
        //System.out.println(token);
        if (second.isEmpty() || second.size() < first.size()){
            second.push(token);
        } else if (second.peek() == ""){
            second.pop();
            second.push(token);
        } else{
            addToken(first, second, eval(first.pop(), second.pop(), token));
        }
    }

    private String eval(String first, String second, String third){
        //System.out.println(first + " " + second + " " + third);
        if (first.equals("T")){
            return second;
        } else{
            return third;
        }
    }

    private int nextDelimit(String expression, int pos){
        pos++;
        while (pos < expression.length()){
            if (expression.charAt(pos) == '?' || expression.charAt(pos) == ':'){
                return pos;
            }
            pos++;
        }
        return pos;
    }

    public static void main(String[] args){
        TernaryExpressionParser soln = new TernaryExpressionParser();
        String s = soln.parseTernary("T?T?T:F?F:5:3");
        System.out.println(s);
    }
}
