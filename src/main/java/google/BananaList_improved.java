package google;

import java.util.*;

public class BananaList_improved {
    private static boolean bfs(Map<Integer, Integer>[] vertices, int source, int sink, int[] parents) {

        int V = vertices.length;

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[V];

        queue.offer(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int u = queue.poll();

            for (Integer v : vertices[u].keySet()) {
                if (!visited[v] && vertices[u].get(v) > 0) {
                    parents[v] = u;
                    visited[v] = true;
                    if (v == sink){
                        return true;
                    } else {
                        queue.offer(v);
                    }
                }
            }
        }

        return false;

    }

    private static int fordFulkerson(Map<Integer, Integer>[] vertices, int source, int sink) {
        int V = vertices.length;

        int[] parents = new int[V];

        int flow = 0;
        while (bfs(vertices, source, sink, parents)) {
            int v = sink;
            int minCapcity = Integer.MAX_VALUE;
            while (v != source) {
                int u = parents[v];
                minCapcity = Math.min(minCapcity, vertices[u].get(v));
                v = u;
            }


            v = sink;
            while (v != source) {
                int u = parents[v];
                vertices[u].put(v, vertices[u].get(v) - minCapcity);
                int old = vertices[v].containsKey(u) ? vertices[v].get(u) : 0;
                vertices[v].put(u, old + minCapcity);
                v = u;
            }
            flow += minCapcity;

        }
        return flow;
    }

    public static int gcd(int a, int b){
        return b == 0 ? a : gcd(b, a % b);
    }


    public static boolean loopForever(int x, int y){
        int factor = gcd(x, y);
        int n = (x + y) / factor;
        return (n & (n - 1)) != 0;
    }

    private static List<List<Integer>> findConnectedComponents(List<Integer>[] vertices){
        int V = vertices.length;
        boolean[] visited = new boolean[V];
        List<List<Integer>> result = new ArrayList<>();

        for (int i = 0; i < V; i++){
            if (!visited[i]){
                visited[i] = true;
                List<Integer> cc = new ArrayList<>();
                cc.add(i);
                Deque<Integer> queue = new ArrayDeque<>();
                queue.offer(i);
                while (!queue.isEmpty()){
                    int u = queue.poll();
                    for (int v : vertices[u]){
                        if (!visited[v]){
                            visited[v] = true;
                            queue.offer(v);
                            cc.add(v);
                        }
                    }
                }
                result.add(cc);
            }
        }
        return result;
    }



    public static int answer(int[] banana_list) {

        int V = banana_list.length;
        List<Integer>[] edges = new List[V];
        for (int i = 0; i < V; i++){
            edges[i] = new ArrayList<>();
        }

        for (int i = 0; i < V; i++){
            for (int j = 0; j < i; j++){
                if (loopForever(banana_list[i], banana_list[j])){
                    edges[i].add(j);
                    edges[j].add(i);
                }
            }
        }


        List<List<Integer>> ccs = findConnectedComponents(edges);

        int result = V;
        for (List<Integer> cc : ccs){
            Map<Integer, Integer>[] vertices = new HashMap[V * 2 + 2];
            for (int i = 0; i < V * 2 + 2; i++){
                vertices[i] = new HashMap<>();
            }

            for (int u : cc){
                for (int v : edges[u]){
                    vertices[u * 2].put(v * 2 + 1, 1);
                    vertices[v * 2].put(u * 2 + 1, 1);
                }
            }

            int source = V * 2;
            for (int i = 0; i < V; i++){
                vertices[source].put(i * 2, 1);
            }

            int sink = V * 2 + 1;
            for (int i = 0; i < V; i++){
                vertices[i * 2 + 1].put(sink, 1);
            }
            result -= fordFulkerson(vertices, source, sink) / 2 * 2;
        }


        return result;
    }

    public static void main(String[] args){

        System.out.println(answer(new int[]{1, 7, 3, 21, 13, 19}));
        System.out.println(answer(new int[]{1, 21, 13}));

    }

}
