package google;

import java.util.*;

public class SmallestRectangle {
    public int smallestRectangle(int[][] points){

        //xmap: for each x value, store the set of y values
        Map<Integer, Set<Integer>> xmap = new TreeMap<>();

        //ymap: for each y value, store the set of x values
        Map<Integer, Set<Integer>> ymap = new TreeMap<>();

        for (int[] point : points){
            xmap.putIfAbsent(point[0], new TreeSet<>());
            xmap.get(point[0]).add(point[1]);

            ymap.putIfAbsent(point[1], new TreeSet<>());
            ymap.get(point[1]).add(point[0]);
        }

        int result = Integer.MAX_VALUE;
        boolean found = false;

        for (int x : xmap.keySet()){
            for (int y : xmap.get(x)){
                Iterator<Integer> yiter = xmap.get(x).iterator();
                int yy = yiter.next();

                while (yy <= y && yiter.hasNext()){
                    yy = yiter.next();
                }
                if (yy <= y){
                    break;
                }

                boolean keepSearch = true;

                while (true){
                    Iterator<Integer> xiter = ymap.get(y).iterator();
                    int xx = xiter.next();
                    while (xx <= x && xiter.hasNext()){
                        xx = xiter.next();
                    }
                    if (xx <= x || (yy - y) * (xx - x) >= result){
                        keepSearch = false;
                        break;
                    }
                    while (true){
                        if (xmap.get(xx).contains(yy)){
                            result = Math.min(result, (yy - y) * (xx - x));
                            found = true;
                            break;
                        }

                        if (xiter.hasNext()){
                            xx = xiter.next();
                        } else{
                            break;
                        }
                    }

                    if (yiter.hasNext() && keepSearch){
                        yy = yiter.next();
                    } else{
                        break;
                    }
                }
            }
        }
        return found ? result : -1;
    }
}
