package google;

import java.util.*;

public class OptimalAccountBalancing {



    int unbalanced;
    private int count;
    public int minTransfers(int[][] transactions) {
        Map<Integer, Integer> personToBalances = new HashMap<>();
        for (int[] transaction : transactions){
            int borrower = transaction[0];
            int lender = transaction[1];
            int amount = transaction[2];
            addToBalance(personToBalances, borrower, -amount);
            addToBalance(personToBalances, lender, amount);
        }

        List<Integer> balances = new ArrayList<>();

        for (int balance : personToBalances.values()){
            if (balance != 0){
                balances.add(balance);
            }
        }

        Collections.sort(balances);

        unbalanced = balances.size();

        count = 0;

        int n = 2;

        while (n <= unbalanced){
            nSum(balances, n);
            n++;
        }

        if (unbalanced > 1){
            count += unbalanced - 1;
        }
        return count;

    }

    private void addToBalance(Map<Integer, Integer> balances, int person, int amount){
        balances.put(person, balances.getOrDefault(person, 0) + amount);
    }

    private void clear(List<Integer> nums, int index){
        nums.set(index, 0);
        unbalanced--;
    }


    private void twoSum(List<Integer> nums, int start){
        int i = start;
        int j = nums.size() - 1;
        while (i < j){
            while (i < j && nums.get(i) == 0){
                i++;
            }

            while (i < j && nums.get(j) == 0){
                j--;
            }
            if (i < j) {
                if (nums.get(i) + nums.get(j) == 0) {
                    clear(nums, i);
                    clear(nums, j);
                    count++;
                    i++;
                    j--;
                } else if (nums.get(i) + nums.get(j) > 0){
                    j--;
                } else{
                    i++;
                }
            }
        }
    }

    // find n number whose sum is target
    private void nSum(List<Integer> nums, int n){
        if (n == 2){
            twoSum(nums, 0);
            return;
        }

        for (int i = 0; i <= nums.size() - n; i++){
            if (nums.get(i) == 0 || i != 0 && nums.get(i) == nums.get(i - 1)){
                continue;
            }
            if (dfs(nums, n - 1, i + 1, 0 - nums.get(i))){
                clear(nums, i);
                count += n - 1;
            }
        }
    }


    private boolean dfs(List<Integer> nums, int n, int start, int target){
        if (n > 2){
            for (int i = start; i <= nums.size() - n; i++){
                if (nums.get(i) == 0 || i != 0 && nums.get(i) == nums.get(i - 1)){
                    continue;
                }
                if (dfs(nums, n - 1, i + 1, target - nums.get(i))){
                    clear(nums, i);
                    return true;
                }
            }
        } else if (n == 2){
            int i = start;
            int j = nums.size() - 1;
            while (i < j){
                while (i < j && nums.get(i) == 0){
                    i++;
                }
                while (i < j && nums.get(j) == 0 ){
                    j--;
                }
                if (i < j){
                    if (nums.get(i) + nums.get(j) == target){
                        clear(nums, i);
                        clear(nums, j);
                        return true;
                    } else if (nums.get(i) + nums.get(j) > target){
                        j--;
                    } else{
                        i++;
                    }
                }
            }
        }
        return false;
    }


}
