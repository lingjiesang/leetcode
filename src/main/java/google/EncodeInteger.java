package google;

import java.util.*;

public class EncodeInteger {
    public static List<Byte> representInt(int n){

        LinkedList<Byte> result = new LinkedList<>();

        byte bite = 0;
        int shift = 0;

        if (n == 0){
            result.add(bite);
            return result;
        }
        while (n != 0){
            bite |= (n & 1) << shift;

            n >>>= 1;
            shift++;
            if (shift == 7 || n == 0){
                if (result.size() != 0){
                    bite |= 1 << 7;
                }
                result.addFirst(bite);
                bite = 0;
                shift = 0;
            }
        }

        return result;
    }

    public static void main(String[] args){
        int  n = 2700;
        List<Byte> bytes = representInt(n);
        for (Byte b: bytes){
            System.out.print(String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0'));
            System.out.print("|");
        }
        System.out.println();
        System.out.println(Integer.toBinaryString(n));

    }
}
