package google;

import java.util.*;

public class MaxFlowMultiSourceSink {
    /**
     * Given the starting room numbers of the groups of bunnies, the room numbers of the escape pods, and how many bunnies
     * can fit through at a time in each direction of every corridor in between, figure out how many bunnies can safely make
     * it to the escape pods at a time at peak.

     Write a function answer(entrances, exits, path) that takes an array of integers denoting where the groups of gathered
     bunnies are, an array of integers denoting where the escape pods are located, and an array of an array of integers
     of the corridors, returning the total number of bunnies that can get through at each time step as an int. The entrances
     and exits are disjoint and thus will never overlap. The path element path[A][B] = C describes that the corridor going
     from A to B can fit C bunnies at each time step.  There are at most 50 rooms connected by the corridors and at most
     2000000 bunnies that will fit at a time.

     For example, if you have:
     entrances = [0, 1]
     exits = [4, 5]
     path = [
     [0, 0, 4, 6, 0, 0],  # Room 0: Bunnies
     [0, 0, 5, 2, 0, 0],  # Room 1: Bunnies
     [0, 0, 0, 0, 4, 4],  # Room 2: Intermediate room
     [0, 0, 0, 0, 6, 6],  # Room 3: Intermediate room
     [0, 0, 0, 0, 0, 0],  # Room 4: Escape pods
     [0, 0, 0, 0, 0, 0],  # Room 5: Escape pods
     ]

     Then in each time step, the following might happen:
     0 sends 4/4 bunnies to 2 and 6/6 bunnies to 3
     1 sends 4/5 bunnies to 2 and 2/2 bunnies to 3
     2 sends 4/4 bunnies to 4 and 4/4 bunnies to 5
     3 sends 4/6 bunnies to 4 and 4/6 bunnies to 5

     So, in total, 16 bunnies could make it to the escape pods at 4 and 5 at each time step.  (Note that in this example,
     room 3 could have sent any variation of 8 bunnies to 4 and 5, such as 2/6 and 6/6, but the final answer remains the same.)
     */
    private static boolean bfs(Map<Integer, Integer>[] vertices, int source, int sink, int[] parents){
        int V = vertices.length;

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[V];

        queue.offer(source);
        visited[source] = true;
        while (!queue.isEmpty()){
            int u = queue.poll();

            for (Integer v : vertices[u].keySet()){
                if (! visited[v] && vertices[u].get(v) > 0){
                    parents[v] = u;
                    visited[v] = true;
                    if (v == sink){
                        return true;
                    } else{
                        queue.offer(v);
                    }
                }
            }
        }
        return false;
    }

    public static int answer(int[] entrances, int[] exits, int[][] path) {
        int V = path.length + 2; // add super source and super sink
        Map<Integer, Integer>[] vertices = new HashMap[V];

        for (int i = 0; i < V; i++){
            vertices[i] = new HashMap<>();
        }

        for (int u = 0; u < path.length; u++){
            for (int v = 0; v < path[u].length; v++){
                if (path[u][v] > 0){
                    vertices[u].put(v, path[u][v]);
                }
            }
        }

        for (int source : entrances){ // V - 2: super source
            vertices[V - 2].put(source, Integer.MAX_VALUE);
        }

        for (int sink : exits){ // V - 1: super sink
            vertices[sink].put(V - 1, Integer.MAX_VALUE);
        }

        int[] parents = new int[V];
        Arrays.fill(parents, -1);

        int flow = 0;
        while (bfs(vertices, V - 2, V - 1, parents)){
            int v = V - 1;
            int minCapacity = Integer.MAX_VALUE;
            while (v != V - 2){
                int u = parents[v];
                minCapacity = Math.min(minCapacity, vertices[u].get(v));
                v = u;
            }

            v = V - 1;
            while (v != V - 2){
                int u = parents[v];
                vertices[u].put(v, vertices[u].get(v) - minCapacity);
                vertices[v].put(u, vertices[v].getOrDefault(u, 0) + minCapacity);
                v = u;
            }
            flow += minCapacity;
        }
        return flow;

    }

    public static void main(String[] args){
        int[][] path = {
                {0, 0, 4, 6, 0, 0},
                {0, 0, 5, 2, 0, 0},
                {0, 0, 0, 0, 4, 4},
                {0, 0, 0, 0, 6, 6},
                {0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0}
        };
        int[] entrances = {0, 1};
        int[] exits = {4, 5};
        System.out.println(answer(entrances, exits, path));
    }

}
