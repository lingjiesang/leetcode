package google;

import java.util.*;

public class MaxXOR_dfs {
    /**
     *     ref:
     *     idea:  http://stackoverflow.com/questions/9320109/two-elements-in-array-whose-xor-is-maximum
     *     implementation: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=202685
     *
     *     did not pass LC OJ...
     */

    public int findMaximumXOR(int[] nums) {
        List<Integer> sub_0 = new ArrayList<>();
        List<Integer> sub_1 = new ArrayList<>();
        int max = 0;
        for (int k : nums) max = Math.max(max, k);
        int mask = 1;
        while ((mask << 1) <= max) {
            mask = (mask << 1);
        }
        for (int k : nums) {
            if ((k & mask) != 0) {
                sub_1.add(k);
            }
            else {
                sub_0.add(k);
            }
        }
        return dfs(sub_0, sub_1, mask >> 1);
    }

    private int dfs(List<Integer> nums_0, List<Integer> nums_1, int mask) {
        if (nums_0.size() == 0 || nums_1.size() == 0) {
            return 0;
        }
        if (mask == 0) {
            return nums_0.get(0) ^ nums_1.get(0);
        }
        List<Integer> sub_00 = new ArrayList<>();
        List<Integer> sub_01 = new ArrayList<>();
        List<Integer> sub_10 = new ArrayList<>();
        List<Integer> sub_11 = new ArrayList<>();
        for (int m : nums_0) {
            if ((m & mask) != 0) {
                sub_01.add(m);
            }
            else{
                sub_00.add(m);
            }
        }
        for (int m : nums_1) {
            if ((m & mask) != 0) sub_11.add(m);
            else sub_10.add(m);
        }
        if (sub_01.size() > 0 && sub_10.size() > 0 || sub_00.size() > 0 && sub_11.size() > 0) {
            return Math.max(dfs(sub_11, sub_00, mask >> 1), dfs(sub_01, sub_10, mask >> 1));
        } else
            return dfs(nums_0, nums_1, mask >> 1);
    }


    public static void main(String[] args){

        //110111000011  (3523)
        //      110101  (53)
        //110111110110  (3574)
        int[] nums = new int[]{1,2,4,6,32,3523,23,5,42,1,5,84,14,9,42,53};

        MaxXOR_dfs soln = new MaxXOR_dfs();
        System.out.println(soln.findMaximumXOR(nums));

        MaxXOR_binaryTree soln2 = new MaxXOR_binaryTree();
        System.out.println(soln2.findMaximumXOR(nums));
    }
}
