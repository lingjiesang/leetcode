package google;

import java.util.*;
/**
 * Write a function answer(map) that generates the length of the shortest path from the prison door to the escape pod,
 * where you are allowed to remove one wall as part of your remodeling plans. The path length is the total number of nodes
 * you pass through, counting both the entrance and exit nodes. The starting and ending positions are always passable (0).
 * The map will always be solvable, though you may or may not need to remove a wall.
 * The height and width of the map can be from 2 to 20. Moves can only be made in cardinal directions; no diagonal moves are allowed.
 */
public class Maze {
    private static int[][] dires = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    public static int answer(int[][] maze) {

        if (maze.length == 0 || maze[0].length == 0){
            return 0;
        }
        int m = maze.length;
        int n = maze[0].length;

        int[][] distFromStart = new int[m][n];
        distFromStart[0][0] = 1;
        bfs(maze, distFromStart, 0, 0, true);

        int[][] distFromEnd = new int[m][n];
        distFromEnd[m - 1][n - 1] = 1;
        bfs(maze, distFromEnd, m - 1, n - 1, true);

        //modify distFromStart to allow one wall to be removed
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (maze[i][j] == 1 && distFromStart[i][j] != 0){
                    bfs(maze, distFromStart, i, j, false);
                }
            }
        }

        int result = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (maze[i][j] == 0 && distFromStart[i][j] != 0 && distFromEnd[i][j] != 0){
                    result = Math.min(result, distFromStart[i][j] + distFromEnd[i][j] - 1);
                }
            }
        }
        return result;
    }

    private static boolean inBound(int i, int j, int m, int n){
        return i >= 0 && i < m && j >= 0 && j < n;
    }
    // start at [i, j]
    private static void bfs(int[][] maze, int[][] dist, int i, int j, boolean updateWall){
        int m = maze.length;
        int n = maze[0].length;

        Deque<int[]> queue = new ArrayDeque<>();
        queue.offer(new int[]{i, j});

        while (!queue.isEmpty()){
            int[] pos = queue.poll();
            for (int[] dire : dires){
                i = pos[0] + dire[0];
                j = pos[1] + dire[1];
                if (inBound(i, j, m, n)
                        && (dist[i][j] == 0 || dist[i][j] > dist[pos[0]][pos[1]] + 1)
                        && (maze[i][j] == 0 || updateWall)){
                    dist[i][j] = dist[pos[0]][pos[1]] + 1;
                    if (maze[i][j] == 0){
                        queue.offer(new int[]{i, j});
                    }
                }
            }
        }
    }


    public static void main(String[] args){
        int[][] maze = new int[][]{{0,1,1,0}, {0,0,0,1}, {1,1,0,0}, {1,1,1,0}};
        System.out.println(answer(maze));

        maze = new int[][]{{0,0,0,0,0,0}, {1,1,1,1,1,0}, {0,0,0,0,0,0}, {0,1,1,1,1,1}, {0,1,1,1,1,1}, {0,0,0,0,0,0,0}};
        System.out.println(answer(maze));
    }
}
