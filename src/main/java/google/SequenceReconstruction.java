package google;

import java.util.*;

public class SequenceReconstruction {
    public boolean sequenceReconstruction(int[] org, int[][] seqs) {
        int n = org.length;
        int[] indegrees = new int[n + 1];
        Set<Integer>[] children = new HashSet[n + 1];
        for (int i = 1; i <= n; i++){
            children[i] = new HashSet<>();
        }
        if (!constructGraph(n, seqs, indegrees, children)){
            return false;
        }

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[n + 1];
        int[] reconstructed = new int[n];
        int pos = 0;

        for (int i = 1; i <= n; i++){
            if (indegrees[i] == 0){
                queue.add(i);
                visited[i] = true;
                reconstructed[pos++] = i;
            }
        }

        while (!queue.isEmpty()){
            if (queue.size() > 1){
                return false;
            }

            int curt = queue.poll();
            for (int child : children[curt]){
                indegrees[child]--;

                if (indegrees[child] == 0){
                    if (visited[child]){
                        return false;
                    }
                    queue.add(child);
                    visited[child] = true;
                    reconstructed[pos++] = child;

                }
            }
        }

        for (int i = 0; i < n; i++){
            if (reconstructed[i] != org[i]){
                return false;
            }
        }

        return true;
    }

    private boolean constructGraph(int n, int[][] seqs, int[] indegrees, Set<Integer>[] children){
        boolean[] visited = new boolean[n + 1];
        visited[0] = true;
        for (int[] seq : seqs){
            if (seq.length == 1){
                int from = seq[0];
                if (from < 1 || from > n){
                    return false;
                }
                visited[from] = true;
                continue;
            }
            for (int i = 0; i < seq.length - 1; i++){
                int from = seq[i];
                int to = seq[i + 1];

                if (from < 1 || from > n || to < 1 || to > n || from == to){
                    return false;
                }

                if (children[from].contains(to)){
                    continue;
                }

                if (children[to].contains(from)){
                    return false;
                }

                children[from].add(to);
                indegrees[to]++;
                visited[from] = true;
                visited[to] = true;
            }
        }

        for (boolean v : visited){
            if (!v){
                return false;
            }
        }

        return true;
    }
}
