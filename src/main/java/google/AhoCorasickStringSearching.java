package google;

import java.util.*;

/**
 * ref:
 */
public class AhoCorasickStringSearching {
    class TrieNode{
        String word;
        TrieNode[] subtree;
        TrieNode prefixLink;
        TrieNode wordLink;

        TrieNode(){
            subtree = new TrieNode[26];
        }
    }

    private void findPrefixLink(TrieNode prev, TrieNode curt, int idx){
        while (prev != null){
            if (prev.subtree[idx] != null){
                curt.prefixLink = prev.subtree[idx];
                return;
            } else{
                prev = prev.prefixLink;
            }
        }
        curt.prefixLink = root;
    }

    private void findWordLink(TrieNode curt){
        TrieNode prev = curt.prefixLink;
        while (prev != null){
            if (prev.word != null){
                curt.wordLink = prev;
                return;
            } else{
                prev = prev.prefixLink;
            }
        }
    }

    TrieNode root;
    private void constructTrie(String[] dict){
        root = new TrieNode();

        for (String word : dict){
            TrieNode node = root;
            for (char c : word.toCharArray()){
                if (node.subtree[c - 'a'] == null){
                    node.subtree[c - 'a'] = new TrieNode();
                }
                node = node.subtree[c - 'a'];
            }
            node.word = word;
        }
        buildPrefixLinks();
        buildWordLinks();
    }


    private void buildWordLinks(){
        Deque<TrieNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            TrieNode node = queue.poll();
            for (TrieNode child : node.subtree){
                if (child != null){
                    findWordLink(child);
                    queue.offer(child);
                }
            }
        }
    }

    private void buildPrefixLinks(){
        Deque<TrieNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            TrieNode node = queue.poll();
            for (int i = 0; i < node.subtree.length; i++){
                if (node.subtree[i] != null){
                    findPrefixLink(node.prefixLink, node.subtree[i], i);
                    queue.offer(node.subtree[i]);
                }
            }
        }
    }

    private int collectCount(TrieNode node){
        if (node == null) {
            return 0;
        }
        return collectCount(node.wordLink) + (node.word != null ? 1 : 0);
    }

    private TrieNode matchTrie(char c, TrieNode node, int[] counter){
        while (node != null) {
            if (node.subtree[c - 'a'] != null) {
                node = node.subtree[c - 'a'];
                break;
            } else {
                node = node.prefixLink;
            }
        }
        counter[0] =  collectCount(node);
        return node != null ? node : root;
    }

    public int[] matchStream(String[] dict, String streams){
        int[] matches = new int[streams.length()];
        constructTrie(dict);
        TrieNode node = root;
        for (int i = 0; i < streams.length(); i++){
            int[] tmp = new int[1];
            node = matchTrie(streams.charAt(i), node, tmp);
            matches[i] = tmp[0];
        }
        return matches;
    }

    public static void main(String[] args){
        String[] dict = {"bobcat", "cat", "tag", "age"};
        String streams = "bobcatage";
        // b, o, b, c, a, t, a, g, e
        // 0, 0, 0, 0, 0, 2, 0, 1, 1
        AhoCorasickStringSearching soln = new AhoCorasickStringSearching();
        System.out.println(Arrays.toString(soln.matchStream(dict, streams)));


        dict = new String[]{"a", "ab", "bab", "bc", "bca", "c", "caa"};
        streams = "abccab";
        System.out.println(Arrays.toString(soln.matchStream(dict, streams)));
        // a, b, c, c, a, b
        // 1, 1, 2, 1, 1, 1

    }
}
