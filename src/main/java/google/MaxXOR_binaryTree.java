package google;

import binaryTree.TreeNode;

import java.util.*;

public class MaxXOR_binaryTree {

    class TreeNode{
        TreeNode left;
        TreeNode right;
        int val;
        TreeNode(){
        }
    }

    public int findMaximumXOR(int[] nums) {
        if (nums.length <= 1){
            return 0;
        }

        int max = Integer.MIN_VALUE;

        for (int num : nums) {
            max = Math.max(max, num);
        }

        int bits = 0; // bits = 3 for 100 (4)
        while (max > 0){
            bits++;
            max >>>= 1;
        }

        TreeNode root = buildTree(nums, bits);

        int result = Integer.MIN_VALUE;
        for (int num : nums){
            result = Math.max(result, searchTree(root, num, bits));
        }
        return result;
    }

    private TreeNode goToSubTree(TreeNode node, boolean isLeft){
        if (isLeft){
            if (node.left == null){
                node.left = new TreeNode();
            }
            node = node.left;
        } else{
            if (node.right == null) {
                node.right = new TreeNode();
            }
            node = node.right;
        }
        return node;
    }

    private TreeNode buildTree(int[] nums, int bits){
        TreeNode root = new TreeNode();

        for (int num : nums){
            TreeNode node = root;
            for (int i = bits - 1; i >= 0; i--){
                int mask = 1 << i;
                if ((num & mask) == 0){
                    node = goToSubTree(node, true);
                } else{
                    node = goToSubTree(node, false);
                }
                node.val = num; //left node, contains num
            }
        }
        return root;
    }


    private int searchTree(TreeNode root, int num, int bits){
        for (int i = bits - 1; i >= 0; i--){
            int mask = 1 << i;
            if ((num & mask) == 0) {
                if (root.right != null){
                    root = root.right;
                } else{
                    root = root.left;
                }
            } else{
                if (root.left != null){
                    root = root.left;
                } else{
                    root = root.right;
                }
            }
        }
        return root.val ^ num;
    }
}
