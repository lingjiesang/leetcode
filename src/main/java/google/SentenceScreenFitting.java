package google;

import java.util.*;

public class SentenceScreenFitting {
    public int wordsTyping(String[] sentence, int rows, int cols) {
        int n = sentence.length;
        int[] wordLen = new int[n];
        for (int i = 0; i < n; i++){
            wordLen[i] = sentence[i].length();
            if (wordLen[i] > cols){
                return 0;
            }
        }

        List<Integer> indices = new ArrayList<>(); //index of the last word in a row
        List<Integer> rounds = new ArrayList<>(); //round of the last word in a row

        int curt = 0;
        int round = 0;

        while (true){
            int len = wordLen[curt];
            while (true){
                int next = (curt + 1) % n;
                if (len + 1 + wordLen[next] <= cols){
                    len += 1 + wordLen[next];
                    curt = next;
                    if (next == 0){
                        round++;
                    }
                } else{
                    break;
                }
            }

            indices.add(curt);
            rounds.add(round);
            if (curt == n - 1 || indices.size() == rows){
                break;
            }

            curt++;
        }
        int count =  rows / rounds.size() * rounds.get(rounds.size() - 1);
        if (rows % rounds.size() != 0){
            count += rounds.get(rows % rounds.size() - 1);
        }
        return count;
    }


}
