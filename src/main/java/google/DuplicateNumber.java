package google;

import java.util.*;

/**
 * 给一个数字，比如12345,可以duplicate任意一位，比如第一位就得到112345第二位就得到122345，以此类推，然后求这些duplicate中最大的.
 *
 *
 * google oa
 * source: http://www.1point3acres.com/bbs/thread-147283-1-1.html]
 *
 * 下面的是别人的code
 */
public class DuplicateNumber {
    public static int[] duplicateNumber(int[] input) {
        int[] result = new int[input.length + 1];
        if (input == null || input.length == 0) return new int[0];
        if (input.length == 1) return new int[]{input[0], input[0]};
        int i = 0;
        int j = 0;
        while (i < input.length - 1) {
            if (input[i] <= input[i + 1]) {
                result[j++] = input[i++];
            } else {
                result[j++] = input[i];
                while (j < result.length)
                    result[j++] = input[i++];
                return result;

            }

        }
        result[j] = input[input.length - 1];
        result[j + 1] = input[input.length - 1];
        return result;
    }

}
