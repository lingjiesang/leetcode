package google;

import java.util.*;

/**
 * cc189, pp136
 *
 * given an infinite number of 25, 10, 5, 1 cents, calculate # of ways of representing n cents
 */
public class Coins {

    public int makeChange(int n){
        int[] faceValues = {25, 10, 5, 1};
        // map[i][j] : number of ways represent j using faceValues[i...3]
        int[][] map = new int[faceValues.length][n + 1];

        for (int i = 0; i <= n; i++){ // key 0 cents : 1 way to represent
            map[3][i] = 1;
        }
        return dfs(n, 0, faceValues, map);

    }

    private int dfs(int n, int i, int[] faceValues,  int[][] map){
        if (map[i][n] > 0){
            return map[i][n];
        }
        for (int amount = 0; amount <= n; amount += faceValues[i]){  //key: amount = [0, n]
            map[i][n] += dfs(n - amount, i + 1, faceValues, map);
        }
        return map[i][n];
    }


    /**
     * solution from cc189
     */
    public int makeChange_benchmark(int n){
        int[] demons = {25, 10, 5, 1};
        int[][] map = new int[n + 1][demons.length];
        return makeChange_benchmark(n, demons, 0, map);
    }

    public int makeChange_benchmark(int amount, int[] demons, int index, int[][] map){
        if (map[amount][index] > 0){
            return map[amount][index];
        }
        if (index >= demons.length - 1){
            return 1;
        }
        int demonAmount = demons[index];
        int ways = 0;
        for (int i = 0; i * demonAmount <= amount; i++){
            int amountRemaining = amount - i * demonAmount;
            ways += makeChange_benchmark(amountRemaining, demons, index + 1, map);
        }
        map[amount][index] = ways;
        return ways;
    }
}
