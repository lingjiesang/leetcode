package google;

import java.util.*;

/**
 * The guards, being bored, readily accept your suggestion to play the Banana Games.

 You will set up simultaneous thumb wrestling matches. In each match, two guards will pair off to thumb wrestle.
 The guard with fewer bananas will bet all their bananas, and the other guard will match the bet. The winner will
 receive all of the bet bananas. You don't pair off guards with the same number of bananas (you will see why, shortly).
 You know enough guard psychology to know that the one who has more bananas always gets over-confident and loses.
 Once a match begins, the pair of guards will continue to thumb wrestle and exchange bananas, until both of them have
 the same number of bananas. Once that happens, both of them will lose interest and go back to guarding the prisoners,
 and you don't want THAT to happen!

 For example, if the two guards that were paired started with 3 and 5 bananas, after the first round of thumb wrestling
 they will have 6 and 2 (the one with 3 bananas wins and gets 3 bananas from the loser). After the second round, they
 will have 4 and 4 (the one with 6 bananas loses 2 bananas). At that point they stop and get back to guarding.

 How is all this useful to distract the guards? Notice that if the guards had started with 1 and 4 bananas, then they
 keep thumb wrestling! 1, 4 -> 2, 3 -> 4, 1 -> 3, 2 -> 1, 4 and so on.

 Now your plan is clear. You must pair up the guards in such a way that the maximum number of guards go into an infinite thumb wrestling loop!

 Write a function answer(banana_list) which, given a list of positive integers depicting the amount of bananas the
 each guard starts with, returns the fewest possible number of guards that will be left to watch the prisoners.
 Element i of the list will be the number of bananas that guard i (counting from 0) starts with.

 The number of guards will be at least 1 and not more than 100, and the number of bananas each guard starts with will
 be a positive integer no more than 1073741823 (i.e. 2^30 -1). Some of them stockpile a LOT of bananas.
 */

/**
 * 这个方法是错误的,原因:
 * 比如1,2,3 三个人都可以互相配对,但不论怎么配对都会剩一人
 * 而用这个方法得出的结果是剩0人.
 *
 * 想了另外一个做法,有n个人连在一起但不和其他人相连
 */
public class BananaList {

    private static boolean bfs(Map<Integer, Integer>[] vertices, int source, int sink, int[] parents) {

        int V = vertices.length;

        Deque<Integer> queue = new ArrayDeque<>();
        boolean[] visited = new boolean[V];

        queue.offer(source);
        visited[source] = true;

        while (!queue.isEmpty()) {
            int u = queue.poll();

            for (Integer v : vertices[u].keySet()) {
                if (!visited[v] && vertices[u].get(v) > 0) {
                    parents[v] = u;
                    visited[v] = true;
                    if (v == sink){
                        return true;
                    } else {
                        queue.offer(v);
                    }
                }
            }
        }

        return false;

    }

    private static int fordFulkerson(Map<Integer, Integer>[] vertices, int source, int sink) {
        int V = vertices.length;

        int[] parents = new int[V];

        int flow = 0;
        while (bfs(vertices, source, sink, parents)) {
            int v = sink;
            int minCapcity = Integer.MAX_VALUE;
            while (v != source) {
                int u = parents[v];
                minCapcity = Math.min(minCapcity, vertices[u].get(v));
                v = u;
            }


            v = sink;
            while (v != source) {
                int u = parents[v];
                vertices[u].put(v, vertices[u].get(v) - minCapcity);
                vertices[v].put(u, vertices[v].getOrDefault(u, 0) + minCapcity);
                v = u;
            }
            flow += minCapcity;

        }
        return flow;
    }

    public static boolean loopForever(int x, int y){
        if (x > y){ // x <= y always
            return loopForever(y, x);
        }
        Set<Integer> prevMin = new HashSet<>();
        while (x != y){
            if (prevMin.contains(x)){
                return true;
            }
            prevMin.add(x);

            int newX = x * 2;
            int newY = y - x;

            x = Math.min(newX, newY);
            y = Math.max(newX, newY);
        }
        return false;
    }
    public static int answer(int[] banana_list) {

        int V = banana_list.length;
        Map<Integer, Integer>[] vertices = new HashMap[V * 2 + 2];
        for (int i = 0; i < V * 2 + 2; i++){
            vertices[i] = new HashMap<>();
        }

        for (int i = 0; i < V; i++){
            for (int j = 0; j < i; j++){
                if (loopForever(banana_list[i], banana_list[j])){
                    int u_l = i * 2;
                    int u_r = i * 2 + 1;

                    int v_l = j * 2;
                    int v_r = j * 2 + 1;

                    vertices[u_l].put(v_r, 1);
                    vertices[v_l].put(u_r, 1);
                }
            }
        }

        int source = V * 2;
        for (int i = 0; i < V; i++){
            vertices[source].put(i * 2, 1);
        }

        int sink = V * 2 + 1;
        for (int i = 0; i < V; i++){
            vertices[i * 2 + 1].put(sink, 1);
        }

        return V - fordFulkerson(vertices, source, sink) / 2 * 2;
    }

    public static void main(String[] args){
        System.out.println(answer(new int[]{1, 7, 3, 21, 13, 19}));
    }
}
