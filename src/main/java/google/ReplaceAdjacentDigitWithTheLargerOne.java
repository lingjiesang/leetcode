package google;

import java.util.*;


/**
 * you are given digit X, replace two adjacent digits withe the larger of these two digits
 * <p>
 * find the smallest number
 * <p>
 * google oa
 * from: http://www.1point3acres.com/bbs/thread-199304-1-1.html
 * <p>
 * <p>
 * 这种题的一个自然想法,每一位都删了试一下 O(n^2), n = # of digits, 经过优化,可以达到O(N)
 * 另一种则是找规律
 *
 */
public class ReplaceAdjacentDigitWithTheLargerOne {

    /**
     * method 1, find pattern
     */
    public int replace_method1(int num) {
        if (num < 10) {
            return 0;
        }

        List<Integer> digits = new LinkedList<>();
        while (num > 0) {
            digits.add(0, num % 10);
            num /= 10;
        }
        int lastRemovable = -1;
        for (int i = 1; i < digits.size(); ) {
            if (digits.get(i) < digits.get(i - 1)) {
                if (i == digits.size() - 1 || digits.get(i + 1) < digits.get(i)) {
                    digits.remove(i);
                    return getNum(digits);
                } else if (digits.get(i + 1) == digits.get(i)) {
                    lastRemovable = i;
                    i++;
                } else {
                    lastRemovable = i;
                    i++;
                }
            } else if (digits.get(i) == digits.get(i - 1)) {
                while (i + 1 < digits.size() && digits.get(i + 1) == digits.get(i)) {
                    i++;
                }
                if (i + 1 == digits.size() || digits.get(i + 1) < digits.get(i)) {
                    digits.remove(i);
                    return getNum(digits);
                } else {
                    lastRemovable = i + 1;
                    i++;
                }
            } else {
                lastRemovable = i - 1;
                i++;
            }
        }

        digits.remove(lastRemovable);
        return getNum(digits);
    }

    private int getNum(List<Integer> digits) {
        int result = 0;
        for (int i = 0; i < digits.size(); i++) {
            result = result * 10 + digits.get(i);
        }
        return result;
    }

    /**
     * method 2
     *
     * try to remove every digit
     *
     * 使用的思想是palindrome integer那题的
     */

    public int replace(int num) {

        if (num < 10) {
            return 0;
        }

        int result = num;

        int lastRemoved = 0;
        int removed = 0;
        int multiplier = 1;
        int toRemove = num % 10;
        num /= 10;

        while (num > 0){
            if (toRemove <= lastRemoved || toRemove <= num % 10){
                result = Math.min(result, removed + num * multiplier);
            }

            lastRemoved = toRemove;
            removed += multiplier * toRemove;

            toRemove = num % 10 ;
            num /= 10;
            multiplier *= 10;

        }
        return result;

    }


// assert is useless

//    public static void main(String[] args){
//        ReplaceAdjacentDigitWithTheLargerOne soln = new ReplaceAdjacentDigitWithTheLargerOne();
//        //rule 1: three decreasing digits => remove the second one
//        assert(soln.replace(6543) == 643);
//        //rule 2: increasing digits  => remove the last removable one
//        assert(soln.replace(2345) == 235);
//        assert(soln.replace(2357468) == 235748);
//        assert(soln.replace(2345764) == 234574);
//
//        //rule 3: equal digits => if followed by a lower digit (or not followed by anything), remove one of them;
//        // if followed by a higher digit, treat them as single digit and follow rule 2 remove the last removable one
//        assert(soln.replace(664) == 64);
//        assert(soln.replace(66543) == 6543);
//        assert(soln.replace(22345) == 2235);
//        assert soln.replace(22356) == 2235;
//
//        System.out.println(soln.replace(22345));
//
//    }
}
