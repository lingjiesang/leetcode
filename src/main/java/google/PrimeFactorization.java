package google;

import java.util.*;

public class PrimeFactorization {
    public List<Integer> factorization(int n){

        int sqrt_n = (int)Math.sqrt(n);

        List<Integer> factors = new ArrayList<>();

        boolean[] isPrimes = new boolean[n + 1];
        Arrays.fill(isPrimes, true);


        isPrimes[0] = false;
        isPrimes[1] = false;

        int lastPrime = 2;

        int remains = n;
        while (remains > 1){
            while (remains % lastPrime == 0){
                factors.add(lastPrime);
                remains /= lastPrime;
            }

            if (remains == 1){
                break;
            }
            // using primes in [2, sqrt_n] to sieve is good enough to find all primes in [2, n]
            if (lastPrime <= sqrt_n) {
                //scans is only to remains, because we are only interested in that
                for (int i = 2; i * lastPrime <= remains; i++) {
                    isPrimes[i * lastPrime] = false;
                }
            }

            lastPrime++;
            while (!isPrimes[lastPrime]){
                lastPrime++;
            }
        }

        return factors;

    }

    public static void main(String[] args){
        PrimeFactorization soln = new PrimeFactorization();
        System.out.println(soln.factorization(59));
    }

}
