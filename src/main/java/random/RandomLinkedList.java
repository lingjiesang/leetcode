package random;

import linkedlist.ListNode;

import java.util.*;

public class RandomLinkedList {
    /**
     * Definition for singly-linked list.
     * public class ListNode {
     *     int val;
     *     ListNode next;
     *     ListNode(int x) { val = x; }
     * }
     */
    public class Solution {
        /**

         Reservoir Sampling

         PROBLEM:

         Choose k entries from n numbers. Make sure each number is selected with the probability of k/n
         BASIC IDEA:

         Choose 1, 2, 3, ..., k first and put them into the reservoir.
         For k+1, pick it with a probability of k/(k+1), and randomly replace a number in the reservoir.
         For k+i, pick it with a probability of k/(k+i), and randomly replace a number in the reservoir.
         Repeat until k+i reaches n

         PROOF:

         For k+i, the probability that it is selected and will replace a number in the reservoir is k/(k+i)
         For a number in the reservoir before (let's say X), the probability that it keeps staying in the reservoir is
         P(X was in the reservoir last time) × P(X is not replaced by k+i)
         = P(X was in the reservoir last time) × (1 - P(k+i is selected and replaces X))
         = k/(k+i-1) × （1 - k/(k+i) × 1/k）
         = k/(k+i)
         When k+i reaches n, the probability of each number staying in the reservoir is k/n

         * ref:https://discuss.leetcode.com/topic/53753/brief-explanation-for-reservoir-sampling
         */

        /** @param head The linked list's head.
        Note that the head is guaranteed to be not null, so it contains at least one node. */
        private ListNode head;
        private Random random;
        public Solution(ListNode head) {
            this.head = head;
            this.random = new Random();
        }

        /** Returns a random node's value. */
        public int getRandom() {
            if (head == null){
                return -1;
            }
            ListNode curt = head;
            ListNode select = head;
            int n = 1;
            while (curt != null){
                if (random.nextInt(n++) == 0){
                    select = curt;
                }
                curt = curt.next;
            }
            return select.val;

        }
    }

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(head);
 * int param_1 = obj.getRandom();
 */
}
