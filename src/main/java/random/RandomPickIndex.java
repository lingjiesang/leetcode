package random;

import java.util.*;

public class RandomPickIndex {
    public class Solution {
        int[] nums;
        Random random;
        public Solution(int[] nums) {
            this.nums = nums;
            random = new Random();
        }

        public int pick(int target) {
            int selectIndex = -1;
            int n = 1;
            for (int i = 0; i < nums.length; i++){
                if (nums[i] == target){
                    if (random.nextInt(n++) == 0){
                        selectIndex = i;
                    }
                }
            }
            return selectIndex;
        }
    }

/**
 * Your Solution object will be instantiated and called as such:
 * Solution obj = new Solution(nums);
 * int param_1 = obj.pick(target);
 */
}
