package bitManipulation;

import java.util.*;

public class RangeBitwiseAnd {
    public int rangeBitwiseAnd(int m, int n) {
        if (m == n) {
            return m;
        }
        if (m < n) {
            return rangeBitwiseAnd(n, m);
        }

        int base = 1;
        int result = 0;
        while (base <= m && base <= n) {
            if (m - n < base && (m & base) != 0 && (n & base) != 0) {
                result += base;
            }
            if (base == (1 << 31)) { //don't forget this!!!
                break;
            }
            base <<= 1;
        }
        return result;
    }

    /**
     * ref:https://discuss.leetcode.com/topic/12093/my-simple-java-solution-3-lines
     * see Tuanjie's explanation
     */
    public int rangeBitwiseAnd_method2(int m, int n) {
        int r = Integer.MAX_VALUE;
        while ((m & r) != (n & r)) {
            r = r << 1;
        }
        return n & r;
    }
}
