package bitManipulation;

import java.util.*;

public class CountBits {
    public int[] countBits(int num){
        int[] result = new int[num + 1];

        for (int i = 1; i <= num; i <<= 1){
            for (int j = i; j <= num && j < i * 2; j++){
                result[j] = result[j - i] + 1;
            }
        }
        return result;
    }
}
