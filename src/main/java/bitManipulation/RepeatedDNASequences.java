package bitManipulation;

import java.util.*;

public class RepeatedDNASequences {
    private static int LEN = 10;
    private static int BITS = 2;

    public List<String> findRepeatedDnaSequences(String s) {
        List<String> result = new ArrayList<>();
        if (s == null || s.length() <= LEN) {
            return result;
        }
        Map<Integer, Integer> hashmap = new HashMap<>();
        char[] chars = s.toCharArray();
        int hash = 0;
        for (int i = 0; i < LEN; i++) {
            hash = (hash << BITS) + hash(chars[i]);
        }
        hashmap.put(hash, 1);
        for (int i = LEN; i < s.length(); i++) {
            hash -= hash(chars[i - LEN]) << (BITS * (LEN - 1));
            hash = (hash << BITS) + hash(chars[i]);
            hashmap.put(hash, hashmap.getOrDefault(hash, 0) + 1);
            if (hashmap.get(hash) == 2) {
                result.add(s.substring(i - LEN + 1, i + 1));
            }
        }
//        for (int st = 1; st + LEN <= s.length(); st++) {
//            hash -= hash(chars[st - 1]) << (BITS * (LEN - 1));
//            hash = (hash << BITS) + hash(chars[st + LEN - 1]);
//            hashmap.put(hash, hashmap.getOrDefault(hash, 0) + 1);
//            if (hashmap.get(hash) == 2) {
//                result.add(s.substring(st, st + LEN));
//            }
//        }
        return result;
    }

    private int hash(char c) {
        switch (c) {
            case 'A':
                return 0;
            case 'C':
                return 1;
            case 'G':
                return 2;
            case 'T':
                return 3;
            default:
                return -1;
        }
    }


    public static void main(String[] args) {
        String s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
        RepeatedDNASequences soln = new RepeatedDNASequences();
        System.out.println(soln.findRepeatedDnaSequences(s));

        System.out.println(soln.hash('G'));
    }
}
