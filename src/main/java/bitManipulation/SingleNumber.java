package bitManipulation;

import java.util.*;

public class SingleNumber {
    /**
     * I:
     * Given an array of integers, every element appears twice except for one. Find that single one.
     */
    public int singleNumber(int[] nums) {
        int xor = 0;
        for (int num : nums){
            xor ^= num;
        }
        return xor;
    }

    /**
     * Given an array of integers, every element appears three times except for one. Find that single one.
     *
     * ref (every element appears N times except for one):
     *https://discuss.leetcode.com/topic/11877/detailed-explanation-and-generalization-of-the-bitwise-operation-method-for-single-numbers
     */
    public int singleNumber_II(int[] nums){
        int[] counts = new int[]{0, 0};
        for (int num : nums){
            counts[1] ^= counts[0] & num;
            counts[0] ^= num;
            int mask = ~(counts[0] & counts[1]);
            counts[0] &= mask;
            counts[1] &= mask;
        }
        return counts[0];
    }

    /**
     * III
     *
     * Given an array of numbers nums, in which exactly two elements appear only once and all the other elements appear exactly twice.
     * Find the two elements that appear only once.
     */
    public int[] singleNumber_III(int[] nums) {
        int[] result = new int[2];
        int xor = 0;
        for (int num : nums){
            xor ^= num;
        }
        xor = xor & (-xor);
        for (int num : nums){
            if ((num & xor) == 0){
                result[0] ^= num;
            } else{
                result[1] ^= num;
            }
        }
        return result;
    }
}
