package bitManipulation;

import java.util.*;

public class UTF8Validation {
    private int firstBitMask = 128;
    private int secondBitMask = 64;
    public boolean validUtf8(int[] data) {
        for (int i = 0; i < data.length;){
            if ((data[i] & firstBitMask) == 0) {
                i++;
            } else{
                int n  = getBytes(data[i]);
                if (n == 1){ // start with "following" bytes of n-bytes character
                    return false;
                }
                for (int j = i + 1; j < i + n; j++){
                    if (j == data.length || (data[j] & firstBitMask) == 0 || (data[j] & secondBitMask) != 0){
                        return false;
                    }
                }
                i += n;
            }
        }
        return true;
    }

    private int getBytes(int x){
        int count = 0;
        int mask = firstBitMask;
        for (int i = 1; i <= 7; i++){
            if ((x & mask) != 0){
                count++;
            } else{
                return count;
            }
            mask >>= 1;
        }
        return count;
    }
}
