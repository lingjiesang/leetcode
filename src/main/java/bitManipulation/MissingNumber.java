package bitManipulation;

import java.util.*;

public class MissingNumber {
    /**
     * xor (Best method!)
     */
    public int missingNumber(int[] nums) {
        int res = nums.length;
        for (int i = 0; i < nums.length; i++) {
            res ^= i;
            res ^= nums[i];
        }
        return res;
    }

    /**
     * sum
     */
    public int missingNumber_Method2(int[] nums) {
        int len = nums.length;
        int sum = (0 + len) * (len + 1) / 2;
        for (int i = 0; i < len; i++)
            sum -= nums[i];
        return sum;
    }

    /**
     * same method as "41. first missing positive"
     */
    public int missingNumber_Method3(int[] nums) {
        for (int i = 0; i < nums.length; ) {
            if (nums[i] == i || nums[i] >= nums.length || nums[nums[i]] == nums[i]) {
                i++;
            } else {
                swap(nums, i, nums[i]);
            }
            //System.out.println(i + " " + Arrays.toString(nums));
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != i) {
                return i;
            }
        }
        return nums.length;
    }

    void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
