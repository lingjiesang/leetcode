package bitManipulation;

import java.util.*;

public class ReverseBits {
    /*
    >>> is unsigned-shift; it'll insert 0.
    >> is signed, and will extend the sign bit.

    System.out.println(Integer.toBinaryString(-1));
    // prints "11111111111111111111111111111111"
    System.out.println(Integer.toBinaryString(-1 >> 16));
    // prints "11111111111111111111111111111111"
    System.out.println(Integer.toBinaryString(-1 >>> 16));
    // prints "1111111111111111"

    ref: http://stackoverflow.com/questions/2811319/difference-between-and

    */
    public int reverseBits(int n) {
        int reversed = 0;
        for (int i = 0; i < 32; i++) {
            reversed <<= 1;
            reversed += n & 1;
            n >>>= 1;
        }
        return reversed;
    }


    public int reverseBits_method2(int n) {
        n = (n >>> 16) | (n << 16);
        n = ((n & 0xff00ff00) >>> 8) | ((n & 0x00ff00ff) << 8);
        n = ((n & 0xf0f0f0f0) >>> 4) | ((n & 0xf0f0f0f0) << 4);
        n = ((n & 0xcccccccc) >>> 2) | ((n & 0x33333333) << 2);
        n = ((n & 0xaaaaaaaa) >>> 1) | ((n & 0x55555555) << 1);
        return n;

    }


    /**
     * optimize:
     * reverse by bytes and cache
     * ref: https://discuss.leetcode.com/topic/9764/java-solution-and-optimization/2
     */
    private int[] cache = new int[256]; // store reverse of 0x00 - 0xff

    public int reverseBits_Optimized(int n) {
        int reversed = 0;
        for (int i = 0; i < 4; i++){
            reversed <<= 8;
            reversed += reverse(n & 0xff);
            n >>>= 8;
        }
        return reversed;
    }

    private int reverse(int n){
        if (n == 0){
            return 0;
        }
        if (cache[n] != 0){
            return cache[n];
        } else{
            int reversed = 0;
            for (int i = 0; i < 8; i++){
                reversed <<= 1;
                reversed += n & 1;
                n >>>= 1;
            }
            cache[n] = reversed;
            return reversed;
        }
    }

    public static void main(String[] args){
        int n = 1;
        ReverseBits soln = new ReverseBits();
        System.out.println(soln.reverseBits_Optimized(n));
    }

}
