package bitManipulation;

import java.util.*;

public class SumOfTwoIntegers {
    /**
     * explanation:
     *      a = 1101
     *      b = 1011
     * result = 0110
     * carrier= 1001 (multiply by 2 ==> 10010)
     *
     * now add 0110 and 10010
     *
     */
    public int getSum_iterative(int a, int b) {
        int result = a ^ b;
        int carrier = a & b;
        while (carrier != 0){
            carrier <<= 1;
            int lastResult = result;
            result ^= carrier;
            carrier &= lastResult;
        }
        return result;
    }


    public int getSum(int a, int b) {
        int result = a ^ b;
        int carrier = a & b;
        while (carrier != 0){
            return getSum(result, carrier << 1);
        }
        return result;
    }
}
