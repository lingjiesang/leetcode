package bitManipulation;

import java.util.*;

public class NumberOf1Bits {
    public int hammingWeight(int n) {
        int count = 0;
        while (n != 0){
            count++;
            n -= n & -n;
        }
        return count;
    }
}
