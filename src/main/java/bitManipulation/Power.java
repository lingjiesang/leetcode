package bitManipulation;

import java.util.*;

public class Power {
    public boolean isPowerOfTwo(int n) {
        if (n <= 0) {
            return false;
        }
        return (n & (n - 1)) == 0;
        //alternatively:
        //return (n & -n) == n;

    }


    public boolean isPowerOfThree(int n) {
        if (n <= 0) {
            return false;
        }
        return (n == 1 || (n % 3 == 0 && isPowerOfThree(n / 3)));
    }

    /**
     * followup:
     * ref: https://discuss.leetcode.com/topic/33536/a-summary-of-all-solutions-new-method-included-at-15-30pm-jan-8th
     */
    public boolean isPowerOfThree_nonrecursive(int n) {
        int maxPowerOfThree = (int) Math.pow(3, (int) (Math.log(Integer.MAX_VALUE) / Math.log(3)));
        // or: int maxPwerOfThree = 1162261467;
        return n > 0 && maxPowerOfThree % n == 0;
    }

    public boolean isPowerOfFour(int num) {
        return ((num - 1) & num) == 0 && (num & 0x55555555) != 0;
        // or : return ((num-1)&num)==0 && (num-1)%3==0;
    }

}
