package math;

import java.util.*;

public class BasicCalculatorII {

    public int calculate(String s) {
        int prev = 0, result = 0;
        char op = '+';
        int st = 0;
        while (st < s.length()){
            if (isNumeric(s.charAt(st))){
                int num = s.charAt(st) - '0';
                int ed = st;
                while (ed + 1 < s.length() && isNumeric(s.charAt(ed + 1))){
                    num = num * 10 + s.charAt(++ed) - '0';
                }
                result = result - prev + eval(prev, op, num);
                prev = update(prev, op, num);
                st = ed;
            } else if (isOp(s.charAt(st))){
                op = s.charAt(st);
            }
            st++;
        }
        return result;
    }


    private boolean isNumeric(char c){
        return c >= '0' && c <= '9';
    }

    private int eval(int a, char c, int b){
        if (c == '+'){
            return a + b;
        } else if (c == '-'){
            return a - b;
        } else if (c == '*'){
            return a * b;
        } else{
            return a / b;
        }
    }

    private int update(int prev, int op, int num){
        if (op == '+'){
            return num;
        } else if (op == '-'){
            return 0 - num;
        } else if (op == '*'){
            return prev * num;
        } else{
            return prev / num;
        }
    }

    private boolean isOp(char c){
        return c == '+' || c == '-' || c == '*' || c == '/' ;
    }
}
