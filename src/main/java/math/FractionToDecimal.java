package math;

import java.util.*;

public class FractionToDecimal {
    public String fractionToDecimal(int numerator, int denominator){
        StringBuilder sb = new StringBuilder();

        if (numerator > 0 && denominator < 0 || numerator < 0 && denominator > 0){ // don't use xor here!!!
            sb.append('-');
        }

        long n = Math.abs((long) numerator);
        long d = Math.abs((long) denominator);

        long quotient = n / d;
        long residue = n - d * quotient;

        sb.append(quotient);
        if (residue == 0){
            return sb.toString();
        } else{
            sb.append('.');
        }

        Map<Long, Integer> residueToPos = new HashMap<>();
        residue *= 10;

        while (!residueToPos.containsKey(residue)){

            residueToPos.put(residue, sb.length());
            sb.append(residue / d);

            //prepare for next round
            residue = residue - residue / d * d;
            residue *= 10;

            if (residue == 0){
                return sb.toString();
            }
        }

        sb.insert(residueToPos.get(residue), "(").append(')');
        return sb.toString();

    }
}
