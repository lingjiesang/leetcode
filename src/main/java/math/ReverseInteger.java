package math;

import java.util.*;

public class ReverseInteger {
    /**
     * take care of overflow and 0
     */
    public int reverse(int x) {
        long reversed = 0;
        long absX = x;
        boolean isNeg = x < 0;
        if (isNeg){
            absX = 0 - x;
        }
        while (absX > 0){
            reversed  = reversed * 10 + absX % 10;
            absX /= 10;
        }
        if (isNeg){
            reversed = 0 - reversed;
        }
        if (reversed > Integer.MAX_VALUE || reversed < Integer.MIN_VALUE){
            return 0;
        } else{
            return (int)reversed;
        }

    }
}
