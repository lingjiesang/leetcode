package math;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/59293/easiest-solution-o-logn-with-explanation
 */
public class EliminationGame {
    public int lastRemaining(int n){
        boolean fromLeft = true;
        int step = 1;
        int head = 1;
        int remainings = n;
        while (remainings > 1){
            if (fromLeft || (remainings % 2) == 1){
                head += step;
            }
            step *= 2;
            remainings /= 2;
            fromLeft = !fromLeft;
        }
        return head;
    }
}
