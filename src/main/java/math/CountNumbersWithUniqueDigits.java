package math;

import java.util.*;

public class CountNumbersWithUniqueDigits {
    public int countNumbersWithUniqueDigits(int n) {
        if (n > 10){
            n = 10;
        }
        int[] result = new int[n + 1];
        result[0] = 1;

        for (int i = 1; i <= n; i++){
            result[i] = 9;
            for (int j = 2; j <= i; j++){
                result[i] *= 11 - j;
            }
            result[i] += result[i - 1];
        }
        return result[n];
    }


    public int countNumbersWithUniqueDigits_clearer(int n) {
        if (n > 10){
            n = 10;
        }

        int result = 1; // i = 0

        for (int i = 1; i <= n; i++){
            int count = 9;

            for (int j = 1; j < i; j++){
                count *= 10 - j;
            }
            result += count;
        }
        return result;
    }
}
