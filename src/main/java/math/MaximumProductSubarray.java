package math;

import java.util.*;

public class MaximumProductSubarray {
    public int maxProduct(int[] nums) {
        if (nums.length == 0){
            return 0;
        }
        int oneNeg = 0, noNeg = 1;
        int result = nums[0];
        for (int num : nums){
            if (num == 0){
                oneNeg = 0;
                noNeg = 1;
                result = Math.max(result, 0);
            } else if (num > 0){
                noNeg *= num;
                oneNeg *= num;
                result = Math.max(result, noNeg);
            } else{
                if (oneNeg == 0){
                    oneNeg = noNeg * num;
                    noNeg = 1;
                    result = Math.max(result, oneNeg);
                } else{
                    int temp = oneNeg;
                    oneNeg = noNeg * num;
                    noNeg = temp * num;
                    result = Math.max(result, noNeg);
                }
            }
        }
        return result;
    }

}
