package math;

import java.util.*;

/**
 * 3 is a special case
 * ref: https://discuss.leetcode.com/topic/58334/a-couple-of-java-solutions-with-explanations/2
 */

public class IntegerReplacement {
    public int integerReplacement(int n) {
        int count = 0;
        while (n != 1){
            if ((n & 1) == 0){
                n >>>= 1;
            } else{
                if (n == 3 || (n - 1) % 4 == 0){
                    n -= 1;
                } else{
                    n += 1;
                }
            }
            count++;
        }
        return count;
    }
}
