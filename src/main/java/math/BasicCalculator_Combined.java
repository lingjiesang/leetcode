package math;

import java.util.*;

public class BasicCalculator_Combined {

    /**
     * This code can be accepted by both BasicCalculator I and BasicCalculator II
     *
     * rules:
     * push 0, +, 0 into evalStack, opStack, prevStack at the beginning respectively
     * 1. if space: ignore
     * 2. if number:
     *   from evalStack, opStack, prevStack, pop one elem each, using these three things and number to eval,
     *   push the evaluated result into evalStack, and push reevaluated prev into prevStack
     * 3. if op, push into opStack
     * 4. if "(", push 0, +, 0 into evalStack, opStack, prevStack respectively
     * 5. if ")": pop one elem from evalStack as number, pop and discard one elem from prev, do everything else as "if number"
     *
     *
     *
     * follow up:
     * what if the given expression might be not valid???
     *
     * what if negative number is allowed?
     *
     */

    public int calculate(String s){
        Deque<Integer> evalStack = new ArrayDeque<>();
        Deque<Character> opStack = new ArrayDeque<>();
        Deque<Integer> prevStack = new ArrayDeque<>();

        pushZeroPlus(evalStack, opStack, prevStack);
        int st = 0;
        while (st < s.length()){
            char c = s.charAt(st);
            switch (c) {
                case ' ':
                    break;
                case '(':
                    pushZeroPlus(evalStack, opStack, prevStack);
                    break;
                case ')':
                    int num = evalStack.pop();
                    prevStack.pop();
                    process(num, evalStack, opStack, prevStack);
                    break;
                default:
                    if (isNumeric(c)) {
                        int ed = st;
                        while (ed + 1 < s.length() && isNumeric(s.charAt(ed + 1))) {
                            ed++;
                        }
                        num = Integer.valueOf(s.substring(st, ed + 1));
                        process(num, evalStack, opStack, prevStack);
                        st = ed;
                    } else if (isOp(c)) {
                        opStack.push(c);
                    }
                    break;
            }
            st++;
        }
        return evalStack.pop();
    }

    private void pushZeroPlus(Deque<Integer> evalStack, Deque<Character> opStack, Deque<Integer> prevStack){
        evalStack.push(0);
        opStack.push('+');
        prevStack.push(0);
    }

    private void process(int num, Deque<Integer> evalStack, Deque<Character> opStack, Deque<Integer> prevStack){
        int prevEval = evalStack.pop();
        int prev = prevStack.pop();
        char op = opStack.pop();

        evalStack.push( prevEval - prev + eval(prev, op, num) );
        prevStack.push(update(prev, op, num));
    }


    private int eval(int prev, char op, int num){
        if (op == '+'){
            return prev + num;
        } else if (op == '-'){
            return prev - num;
        } else if (op == '*'){
            return prev * num;
        } else{
            return prev / num;
        }
    }

    private int update(int prev, int op, int num){
        if (op == '+'){
            return num;
        } else if (op == '-'){
            return 0 - num;
        } else if (op == '*'){
            return prev * num;
        } else{
            return prev / num;
        }
    }

    private boolean isNumeric(char c){
        return c >= '0' && c <= '9';
    }

    private boolean isOp(char c) { return c == '+' || c == '-' || c == '*' || c == '/'; }

}
