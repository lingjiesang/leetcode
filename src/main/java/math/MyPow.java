package math;

import java.util.*;

public class MyPow {
    public double myPow(double x, int n) {
        if (n == 0){
            return 1;
        }
        double result = 1;

        if (n < 0){
            x = 1 / x;
            n = 0 - (n + 1); // prevent n overflow!
            result = x;
        }

        while (n > 0){
            if ((n & 1) == 1){
                result *= x;
            }
            x *= x;
            n >>= 1;
        }

        return result;
    }
}
