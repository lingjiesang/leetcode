package math;

import java.util.*;

public class AddBinary {
    public String addBinary(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int pos = 0;
        int carrier = 0;
        int m = a.length();
        int n = b.length();
        while (pos < m || pos < n || carrier > 0){
            carrier += (pos < m ? a.charAt(m - 1 - pos) - '0' : 0) + (pos < n ? b.charAt(n - 1 - pos) - '0' : 0);
            sb.append(carrier & 1);
            carrier >>= 1;
            pos++;
        }
        return sb.reverse().toString();
    }
}
