package math;

import java.util.*;

public class NimGame {
    public boolean canWinNim(int n) {
        return n % 4 != 0;
    }
}
