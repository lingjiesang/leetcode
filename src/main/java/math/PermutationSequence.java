package math;

import java.util.*;

public class PermutationSequence {
    public String getPermutation(int n, int k) {
        if (n < 1 || n > 9){
            return "";
        }
        char[] result = new char[n];
        List<Integer> nums = new ArrayList<>();
        for (int i = 1; i <= n; i++){
            nums.add(i);
        }

        int factor = factorial(n - 1);
        k--; // k: 1-based ==> 0-based
        for (int i = 1; i < n; i++){
            result[i - 1] = (char)(nums.remove(k / factor) + '0');
            k %= factor;
            factor /= n - i;
        }
        result[n - 1] = (char)(nums.remove(0) + '0');
        return new String(result);
    }

    private int factorial(int n){
        int res = 1;
        for (int i = n; i >= 1; i--){
            res *= i;
        }
        return res;
    }
}
