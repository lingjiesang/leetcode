package math;

import java.util.*;

public class RectangleArea {
    public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int leftX = Math.max(A, E);     // x value of the left side of the intersection
        int rightX = Math.min(C, G);   // x value of the right side of the intersection
        int topY = Math.min(H, D);     // y value of the bottom side of the intersection
        int botY = Math.max(F, B);     //y value of the top side of the intersection
        int intersecArea = 0;
        if (leftX < rightX && topY > botY) //if one of the sides above is misplaced => the intersection is empty
            intersecArea = (rightX - leftX) * (topY - botY);
        return (C - A) * (D - B) + (G - E) * (H - F) - intersecArea;
    }
}
