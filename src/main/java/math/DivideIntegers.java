package math;

import java.util.*;

public class DivideIntegers {
    public int divide(int dividend, int divisor){
        if (divisor == 0 || (dividend == Integer.MIN_VALUE && divisor == -1)){
            return Integer.MAX_VALUE;
        }

        long dd = Math.abs((long)dividend);
        long di = Math.abs((long)divisor);

        boolean isNegative = dividend > 0 ^ divisor > 0 ;

        long result = 0;

        while (dd >= di){
            long multiplier = 1;

            while (dd >= di){
                multiplier <<= 1;
                di <<= 1;
            }

            result += multiplier >> 1;
            dd -= di >> 1;
            di = Math.abs((long)divisor);
        }
        if (isNegative){
            return 0 - (int)result;
        } else{
            return (int)result;
        }
    }
}
