package math;

import java.util.*;

public class ExcelSheetColumnTitle {
    /**
     * 168
     */
    public String convertToTitle(int n) {
        StringBuilder sb = new StringBuilder();
        while (n > 0){
            n--;
            sb.append((char)(n % 26 + 'A'));
            n /= 26;
        }
        return sb.reverse().toString();
    }

    /**
     * 171
     */
    public int titleToNumber(String s) {
        int result = 0;
        for (char c : s.toCharArray()){
            result = result * 26 + c - 'A' + 1;
        }
        return result;
    }
}
