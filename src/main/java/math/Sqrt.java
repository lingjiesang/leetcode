package math;

import java.util.*;

public class Sqrt {

    /**
     * #69
     * ans * ans <= x
     * (ans + 1) * (ans + 1) > x
     */
    public int mySqrt(int x){
        if (x == 0){
            return 0;
        }
        int start = 1, end = x;
        while (start + 1 < end){
            int mid = start + (end - start) / 2;
            if (mid > x / mid) {
                end = mid - 1;
            } else{
                start = mid;
            }
        }
        if (end <= x / end){
            return end;
        } else{
            return start;
        }
    }

    /**
     * #367
     */
    public boolean isPerfectSquare(int num) {
        if (num == 0){
            return true;
        }
        int start = 1, end = num;
        long x = num;
        while (start <= end){
            int mid = start + (end - start) / 2;
            if (mid == x / mid && mid * mid == x){
                return true;
            } else if (mid > x / mid){
                end = mid - 1;
            } else{
                start = mid + 1;
            }
        }
        return false;
    }

    /**
     * online:A square number is 1+3+5+7+..., JAVA code
     * ref: https://discuss.leetcode.com/topic/49325/a-square-number-is-1-3-5-7-java-code
     */
    public boolean isPerfectSquare_Trick(int num) {
        int i = 1;
        while (num > 0) {
            num -= i;
            i += 2;
        }
        return num == 0;
    }
}
