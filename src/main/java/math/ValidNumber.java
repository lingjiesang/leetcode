package math;

public class ValidNumber {
    /*
    *  s need to be in the format "X.YeZ"
    */
    public boolean isNumber(String s) {
        if (s == null) {
            return false;
        }
        s = s.trim(); //remove leading and trailing whitespace

        // deal with "Z"
        int eInd = s.indexOf('e');
        if (eInd > 0) {
            String Z = s.substring(eInd + 1);
            if (!judgeZ(Z)) {
                return false;
            }
            // s = X.Y
            s = s.substring(0, eInd);
        }

        if (!judgeXdotY(s)) {
            return false;
        }
        return true;
    }

    private boolean judgeXdotY(String s) {
        if (s == null || s.length() == 0){
            return false;
        }

        s = trimPlusMinus(s);
        if (s.length() == 0) {
            return false;
        }

        int dotInd = s.indexOf('.');
        String X, Y;
        if (dotInd >= 0) {
            X = s.substring(0, dotInd);
            Y = s.substring(dotInd + 1);
        } else {
            X = s;
            Y = "";
        }

        if (!numOnly(X) || !numOnly(Y)) {
            return false;
        }

        if (X.length()== 0 && Y.length() == 0) {
            return false;
        }
        return true;
    }


    /*
     * because we judge "Z", has to have "Z"
     */
    private boolean judgeZ(String s) {
        if (s == null || s.length() == 0){
            return false;
        }

        s = trimPlusMinus(s); // this step change s length, need to consider if s --> ""
        return s.length() > 0 && numOnly(s);
    }

    private String trimPlusMinus(String s) {
        if (s == null || s.length() == 0){
            return s;
        }
        if (s.charAt(0) == '+' || s.charAt(0) == '-') {
            s = s.substring(1);
        }
        return s;
    }

    private boolean numOnly(String s) {
        if (s == null || s.length() == 0){
            return true;
        }

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) < '0' || s.charAt(i) > '9') {
                return false;
            }
        }
        return true;
    }

}


