package math;

import java.util.*;

public class WaterJug {

    public int gcd(int a, int b){
        return b == 0 ? a : gcd(b, a % b);
    }
    public boolean canMeasureWater(int x, int y, int z) {
        if (z == 0){
            return true;
        }
        return z <= x + y && z % gcd(x, y) == 0;
    }
}
