package math;

import java.util.*;

public class CountPrimes {

    // ref: https://discuss.leetcode.com/topic/35033/12-ms-java-solution-modified-from-the-hint-method-beats-99-95
    public int countPrimes_Cleaner(int n) {
        if (n < 3)
            return 0;

        boolean[] notPrime = new boolean[n]; //by default, assuming 所有大于等于3的奇数(可能处理的值)都是质数, notPrime[i] = false
        int count = n / 2; //小于n的奇数(除了1加上2)有n/2个,它们才有可能是质数
        for (int i = 3; i * i < n; i += 2) {
            if (notPrime[i])
                continue;

            for (int j = i * i; j < n; j += 2 * i) { // 3 * 3, 3 * 5, 3 * 7.... ; 5 * 5, 5 * 7, 5 * 9....
                if (!notPrime[j]) {
                    --count;
                    notPrime[j] = true;
                }
            }
        }
        return count;
    }


    public int countPrimes(int n) {
        if (n <= 1) {
            return 0;
        }
        boolean[] isPrimes = new boolean[n]; // 0 ... n - 1
        Arrays.fill(isPrimes, true);

        isPrimes[0] = false;
        isPrimes[1] = false;


        int lastPrime = 2;
        while (lastPrime * lastPrime < n) {
            for (int i = 2; i * lastPrime < n; i++) {
                isPrimes[i * lastPrime] = false;
            }

            lastPrime++;
            while (!isPrimes[lastPrime]) {
                lastPrime++;
            }
        }

        int totalPrimes = 0;
        for (boolean isPrime : isPrimes) {
            if (isPrime) {
                totalPrimes++;
            }
        }
        return totalPrimes;
    }


}
