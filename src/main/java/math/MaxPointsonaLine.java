package math;

import java.util.*;

public class MaxPointsonaLine {
    class Point {
        int x;
        int y;
        Point() { x = 0; y = 0; }
        Point(int a, int b) { x = a; y = b; }
    }

    public int maxPoints(Point[] points) {
        if (points.length == 0) {
            return 0;
        }
        HashMap<Integer, HashMap<Integer, Integer>> map = new HashMap<>();
        int maxPoints = 1;
        for (int i = 0; i < points.length; i++) {
            map.clear();
            int overlap = 0;
            int maxInLine = 0;
            for (int j = i + 1; j < points.length; j++) {
                int slope1, slope2;
                if (points[i].x == points[j].x && points[i].y == points[j].y) {
                    overlap++;
                    continue;
                } else if (points[i].x == points[j].x) {
                    slope1 = 0;
                    slope2 = Integer.MAX_VALUE;

                } else if (points[i].y == points[j].y) {
                    slope1 = Integer.MAX_VALUE;
                    slope2 = 0;
                } else {
                    slope1 = points[i].x - points[j].x;
                    slope2 = points[i].y - points[j].y;
                    int dividor = gcd(slope1, slope2);
                    slope1 /= dividor;
                    slope2 /= dividor;
                }
                if (!map.containsKey(slope1)) {
                    map.put(slope1, new HashMap<>());
                }
                if (!map.get(slope1).containsKey(slope2)) {
                    map.get(slope1).put(slope2, 0);
                }
                map.get(slope1).put(slope2, map.get(slope1).get(slope2) + 1);
                maxInLine = Math.max(maxInLine, map.get(slope1).get(slope2));
            }
            // 1 is point[i] itself!
            maxPoints = Math.max(maxPoints, overlap + maxInLine + 1);
        }
        return maxPoints;

    }

    int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return gcd(b, a % b);

    }
}
