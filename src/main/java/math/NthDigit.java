package math;

import java.util.*;

public class NthDigit {
    public int findNthDigit(int n) {
        n--;
        int digit = 1;
        long nums = 9;
        long base = 1;

        while (n - nums >= 0){
            n -= nums;
            digit++;
            base *= 10;
            nums = base * 9 * digit;
        }
        return findDigit(base + n / digit, digit - 1 - n % digit);
    }

    private int findDigit(long num, int ith){
        while (ith > 0){
            num /= 10;
            ith--;
        }
        return (int)(num % 10);
    }
}
