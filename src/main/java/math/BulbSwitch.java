package math;

import java.util.*;

public class BulbSwitch {
    /**
     *  例子: 假设有9个灯...
     *  当一个数能被奇数个小于它的数整除时,它相对应的bulb是亮着的
     *  即,当一个数是完美平方数时,它对应的灯是亮的
     *  即,求小于n的完美平方数
     */
    public int bulbSwitch(int n) {
        return (int)(Math.sqrt(n));
    }
}
