package math;

import java.util.*;

public class HappyNumber {
    private int nextNum(int num){
        int next = 0;
        while (num > 0){
            next += (num % 10) * (num % 10);
            num /= 10;
        }
        return next;
    }

    /**
     * method 1 : cycle (fast method)
     */
    public boolean isHappy(int n){
        int slow = nextNum(n);
        int fast = nextNum(slow);
        while (fast != slow){
            fast = nextNum(nextNum(fast));
            slow = nextNum(slow);
        }
        return slow == 1;
    }

    /**
     * method 2 : hashset
     */
    public boolean isHappy_method2(int n) {
        Set<Integer> set = new HashSet<>();
        while (n != 1){
            if (!set.add(n)){
                return false;
            }
            n = nextNum(n);
        }
        return true;
    }
}
