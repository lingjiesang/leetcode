package math;

import java.util.*;

public class EvalReversePolishNotation {

    public int evalRPN(String[] tokens) {
        Deque<Long> stack = new ArrayDeque<>();

        for (String token : tokens) {
            if (isOperator(token)) {
                stack.push(eval(stack.pop(), stack.pop(), token));
            } else{
                stack.push(Long.valueOf(token));
            }
        }
        long result = stack.pop();
        return (int)result;
    }

    private boolean isOperator(String token) {
        return token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/");
    }

    // b, a (not a, b!)
    private long eval(long b, long a, String symbol) {
        if (symbol.equals("+")) {
            return a + b;
        }
        if (symbol.equals("-")) {
            return a - b;
        }
        if (symbol.equals("*")) {
            return a * b;
        }
        return a / b;

    }

}
