package math;

import java.util.*;

public class SuperPower {
    public int superPow(int a, int[] b) {
        int result = 1; // now result = (a ^ 0) % 1337
        int base = a % 1337;
        // a and a % 1337 should give the same result!
        // suppose a = x * 1337 + y
        // a ^ 3 % 1337 = (x * 1337 + y) * (x * 1337 + y) * (x * 1337 + y) % 1337 = y % 1337

        int baseRemainder = a % 1337; // (a ^ 1) % 1337

        for (int i = b.length - 1; i >= 0; i--){
            for (int j = 1; j < 10; j++){
                if (j == b[i]){
                    result = result * baseRemainder % 1337;
                }
                baseRemainder = baseRemainder * base % 1337;
            }
            base = baseRemainder;
            // loop once (i = b.length - 1), base = (a ^ 10) % 1337
            //     twice (i = b.length - 2), base = (a ^ 100) % 1337

        }
        return result;
    }
}
