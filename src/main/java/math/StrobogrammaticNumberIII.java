package math;

import java.util.*;

public class StrobogrammaticNumberIII {
    public int strobogrammaticInRange(String low, String high) {
        int result = strobogrammaticSmallerThan(high) + (isStrobogrammatic(high) ? 1 : 0)
                - strobogrammaticSmallerThan(low);
        return result < 0 ? 0 : result;
    }

    public int strobogrammaticSmallerThan(String limit) {
        int count = 0;
        int n = limit.length();
        for (int i = 0; i < (n + 1) / 2; i++) {
            int digit = limit.charAt(i) - '0';
            int smaller = i * 2 + 1 == limit.length() ?
                    smallerCountsSpecial(digit) : smallerCounts(digit);
            if (n != 1 && i == 0) { //非个位数的第一位不能取0
                smaller--;
            }
            if (smaller > 0) { //第i位取一个小于limit[i]的数字,后面的可以取任意数字
                for (int j = i + 1; j < (n + 1) / 2; j++) {
                    smaller *= j * 2 + 1 == n ? 3 : 5;
                }
                count += smaller;
            }
            if (!isValidDigit(digit)) { //第i位刚好取limit[i],后面数字由下轮来决定
                break;                 // 如果第i位不能取limit[i],那么已经算好
            }

            if (i == (n - 1) / 2 && limit.compareTo(genStrobogrammatic(limit)) > 0){
                count++; //假如从第0位到第(n - 1) / 2都取limit[i],判断以下这个数是不是小于limit
            }
        }
        // 假如limit = "38830"
        //  以上求出的是"10000" 到 "38830"之间的,加上 "0" 到"9999"之间的才是答案
        count += strobogrammaticNDigits(n - 1);
        return count;
    }

    private String genStrobogrammatic(String limit){
        int n = limit.length();
        char[] stro = new char[n];
        for (int i = 0; i < (n + 1) / 2; i++){
            stro[i] = limit.charAt(i);
        }
        for (int i = (n + 1) / 2; i < n; i++){
            stro[i] = flip(limit.charAt(n - 1 - i));
        }
        return new String(stro);
    }

    public int strobogrammaticNDigits(int n) {
        if (n <= 0) {
            return 0;
        }
        if (n == 1) {
            return 3;
        }
        int count = 4;
        for (int i = 1; i < n / 2; i++) {
            count *= 5;
        }
        if (n % 2 == 1) {
            count *= 3;
        }
        count += strobogrammaticNDigits(n - 1);
        return count ;
    }

    private boolean isValidDigit(int digit) {
        return (digit == 0) || (digit == 1) || (digit == 6) || (digit == 8) || (digit == 9);
    }

    /**
     * return # of conditions "smaller" than digit
     */
    private int smallerCounts(int digit) {
        if (digit == 9) { //9 : {0, 1, 6, 8}
            return 4;
        } else if (digit >= 7) { //7, 8 : {0, 1, 6}
            return 3;
        } else if (digit >= 2) {//2,3,4,5,6 : {0, 1}
            return 2;
        } else if (digit == 1) {//1 : {0}
            return 1;
        } else {//0
            return 0;
        }
    }

    private int smallerCountsSpecial(int digit) {
        if (digit == 9) { //9:  {0, 1, 8}
            return 3;
        } else if (digit >= 2) {//2,3,4,5,6,7,8 : {0, 1}
            return 2;
        } else if (digit == 1) {//1 : {0}
            return 1;
        } else {//0
            return 0;
        }
    }


    private char flip(char a) {
        switch (a) {
            case '1':
                return '1';
            case '6':
                return '9';
            case '8':
                return '8';
            case '9':
                return '6';
            case '0':
                return '0';
            default:
                return (char)0;
        }
    }

    public boolean isStrobogrammatic(String num) {
        int st = 0, ed = num.length() - 1;
        while (st <= ed) {
            if (flip(num.charAt(st)) != num.charAt(ed)) {
                return false;
            }
            st++;
            ed--;
        }
        return true;
    }

    public static void main(String[] args) {
        StrobogrammaticNumberIII soln = new StrobogrammaticNumberIII();
        System.out.println(soln.strobogrammaticNDigits(3));
        System.out.println(soln.strobogrammaticSmallerThan("69"));
        System.out.println(soln.strobogrammaticSmallerThan("11"));
        System.out.println(soln.strobogrammaticInRange("11", "69"));
        System.out.println(soln.isStrobogrammatic("69"));

    }

}
