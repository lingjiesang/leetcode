package math;

import java.util.*;

public class StrobogrammaticNumber {
    /**
     * I. 246
     */
    private boolean isStrobogrammaticDigit(char a, char b) {
        switch (a) {
            case '1':
                return b == '1';
            case '6':
                return b == '9';
            case '8':
                return b == '8';
            case '9':
                return b == '6';
            case '0':
                return b == '0';
            default:
                return false;
        }
    }

    public boolean isStrobogrammatic(String num) {
        int st = 0, ed = num.length() - 1;
        while (st <= ed) {
            if (!isStrobogrammaticDigit(num.charAt(st), num.charAt(ed))) {
                return false;
            }
            st++;
            ed--;
        }
        return true;
    }

    /**
     * II. 247
     * 1. n >= 1, not start with 0
     * 2. middle digit can only be 0, 1, 8
     */
    private char[] digits = {'0', '1', '8', '6', '9'};
    private char[] mirror = {'0', '1', '8', '9', '6'};

    public List<String> findStrobogrammatic(int n) {
        List<String> results = new ArrayList<>();
        if (n <= 0) {
            return results;
        }
        dfs(0, new char[n], results);
        return results;
    }

    private void dfs(int start, char[] strobogramaticNum, List<String> results) {
        int n = strobogramaticNum.length;
        if (start == n / 2) {
            if (start * 2 + 1 == n) {
                for (int i = 0; i < 3; i++) {
                    strobogramaticNum[start] = digits[i];
                    results.add(new String(strobogramaticNum));
                }
            } else {
                results.add(new String(strobogramaticNum));
            }
            return;
        }
        for (int i = 0; i < digits.length; i++) {
            if (start == 0 && n != 1 && i == 0) {
                continue;
            }
            strobogramaticNum[start] = digits[i];
            strobogramaticNum[n - 1 - start] = mirror[i];
            dfs(start + 1, strobogramaticNum, results);
        }
    }



}
