package math;

import java.util.*;

public class BasicCalculator {

    /**
     * Difference from BasicCalculator_Combined:
     * don't need to consider "operator precedence", no need for prevStack
     *
     * rules:
     * push 0, + into numStack, opStack at the beginning respectively
     * 1. if space: ignore
     * 2. if number:
     *   from numStack, opStack, pop one elem each, using these two things and the number to eval,
     *   push the evaluated result into numStack
     * 3. if op, push into opStack
     * 4. if "(", push 0, + into numStack, opStack at the beginning respectively
     * 5. if ")": pop one elem from numStack as number, do everything else as "if number"
     *
     */


    public int calculate(String s) {
        Deque<Integer> numStack = new ArrayDeque<>();
        Deque<Character> opStack = new ArrayDeque<>();

        int st = 0;
        numStack.push(0);
        opStack.push('+');

        while (st < s.length()){ // always remember to update st!!!
            if (isNumeric(s.charAt(st))){
                int ed = st;
                while (ed + 1 < s.length() && isNumeric(s.charAt(ed + 1))) {
                    ed++;
                }
                int num = Integer.valueOf(s.substring(st, ed + 1));
                numStack.push(eval(numStack.pop(), num, opStack.pop()));
                st = ed;
            } else if (s.charAt(st) == '+' || s.charAt(st) == '-'){
                opStack.push(s.charAt(st));
            } else if (s.charAt(st) == '('){
                numStack.push(0);
                opStack.push('+');
            } else if (s.charAt(st) == ')'){
                int num = numStack.pop();
                numStack.push(eval(numStack.pop(), num, opStack.pop()));
            }
            st++;
        }
        return numStack.pop();
    }

    private boolean isNumeric(char c){
        return c >= '0' && c <= '9';
    }

    private int eval(int a, int b, char op){
        if (op == '+'){
            return a + b;
        } else{
            return a - b;
        }
    }
}
