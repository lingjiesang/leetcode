package math;

import java.util.*;

public class DigitRoot {
    // ref: https://discuss.leetcode.com/topic/28791/3-methods-for-python-with-explains/2
    /**
     * suppose num = a0 + a1 * 10 + a2 * 10^2 + ... + an * 10^n
     * we can see num % 9 == (a0 + a1 + ... + an) % 9
     *
     * uppose y = a0 + a1 + ... + an = b0 + b1 * 10 + b2 * 10^2 + ... + bm * 10^m
     * y % 9 == (b0 + b1 + ... + bm) % 9
     * so that num % 9 == (b0 + b1 + ... + bm) % 9
     *
     *
     *  this can loop for a while, but let's just assume it stops here, i.e.
     *  (b0 + b1 + ... + bm) <= 9
     * if (b0 + b1 + ... + bm) < 9 then b0 + b1 + ... + bm = num % 9
     * if (b0 + b1 + ... + bm) == 9 then b0 + b1 + ... + bm = num % 9 + 9
     * notice unless num == 0, we always have  b0 + b1 + ... + bm >= 1
     */
    public int addDigits(int num) {
        if (num == 0){ // necessary for python, optional for java
            return 0;
        }
        return (num - 1) % 9 + 1;
    }


    public static void main(String[] args){
        int x = (0 - 1) % 9;
        System.out.println(x); // -1 for java, 8 for python!
    }
}
