package math;

import java.util.*;

public class UglyNumber {
    /**
     * Ugly Number I
     */
    public boolean isUgly(int num) {
        if (num <= 0) {
            return false;
        }
        int[] factors = {2, 3, 5};
        for (int factor : factors) {
            while (num % factor == 0) {
                num /= factor;
            }
        }
        return num == 1;
    }

    /**
     * Ugly Number II
     */
    public int nthUglyNumber(int n) {
        int[] uglyNumbers = new int[n];
        uglyNumbers[0] = 1;
        int p2 = 0, p3 = 0, p5 = 0, pUgly = 1;

        while (pUgly < n) {
            int next2 = uglyNumbers[p2] * 2;
            int next3 = uglyNumbers[p3] * 3;
            int next5 = uglyNumbers[p5] * 5;
            if (next2 <= next3 && next2 <= next5) {
                if (next2 > uglyNumbers[pUgly - 1]) {
                    uglyNumbers[pUgly++] = next2;
                }
                p2++;
            } else if (next3 <= next2 && next3 <= next5) {
                if (next3 > uglyNumbers[pUgly - 1]) {
                    uglyNumbers[pUgly++] = next3;
                }
                p3++;
            } else {
                if (next5 > uglyNumbers[pUgly - 1]) {
                    uglyNumbers[pUgly++] = next5;
                }
                p5++;
            }
        }
        return uglyNumbers[n - 1];
    }

    /**
     * Super Ugly Number
     */
    public int nthSuperUglyNumber(int n, int[] primes) {
        int[] uglyNumbers = new int[n];
        uglyNumbers[0] = 1;
        PriorityQueue<Ugly> heap = new PriorityQueue<>(new Comparator<Ugly>() {
            @Override
            public int compare(Ugly a, Ugly b) {
                return a.value - b.value;
            }
        });
        for (int prime : primes) {
            heap.add(new Ugly(prime, 0, prime));
        }

        int i = 1;
        while (i < n) {
            Ugly ugly = heap.poll();
            if (ugly.value > uglyNumbers[i - 1]) {
                uglyNumbers[i++] = ugly.value;
            }
            ugly.value = uglyNumbers[++ugly.index] * ugly.prime;
            heap.add(ugly);
        }
        return uglyNumbers[n - 1];
    }

    public class Ugly {
        int prime;
        int index;
        int value;

        Ugly(int p, int i, int v) {
            prime = p;
            index = i;
            value = v;
        }
    }


    public int nthSuperUglyNumber_method2(int n, int[] primes) {
        int[] uglyNumber = new int[n];
        uglyNumber[0] = 1;
        int[] indices = new int[primes.length];
        int[] values  = new int[primes.length];
        Arrays.fill(values, 1);
        for (int i = 1; i < n; i++){
            uglyNumber[i] = Integer.MAX_VALUE;
            for (int j = 0; j < primes.length; j++){
                if (values[j] == uglyNumber[i - 1]){
                    values[j] = uglyNumber[indices[j]++] * primes[j];
                }
                uglyNumber[i] = Math.min(uglyNumber[i], values[j]);
            }
        }
        return uglyNumber[n - 1];
    }
}
