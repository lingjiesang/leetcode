package math;

import java.util.*;

/**
 * 这里不考虑overlap的点
 * [[1,1],[-1,1], [-1, 1]] ==> return true
 * 如果腰考虑, 可以用Map<Integer, Map<Integer, Integer>>来算count
 */
public class LineReflection {
    public boolean isReflected(int[][] points) {
        Map<Integer, Set<Integer>> map = new HashMap<>();
        int xMax = Integer.MIN_VALUE, xMin = Integer.MAX_VALUE;
        for (int[] point : points){
            int x = point[0];
            int y = point[1];
            xMax = Math.max(xMax, x);
            xMin = Math.min(xMin, x);
            if (!map.containsKey(y)){
                map.put(y, new HashSet<>());
            }
            map.get(y).add(x);
        }

        int sum = xMax + xMin;
        for (int[] point : points){
            int x = point[0];
            int y = point[1];
            if (!map.get(y).contains(sum - x)){
                return false;
            }
        }
        return true;

    }
}
