package tricks;


public class FirstMissingPositive {
    public int firstMissingPositive(int[] nums) {
        for (int i = 0; i < nums.length; i++){
            if (nums[i] > 0 && nums[i] - 1 != i){
                put(nums, nums[i], nums[i] - 1);
            }
        }
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != i + 1){
                return i + 1;
            }
        }
        return nums.length + 1;
    }

    private void put(int[] nums, int val, int pos){
        if (pos >= nums.length){
            return;
        }
        int old = nums[pos];
        nums[pos] = val;
        if (old > 0 && old != val){
            put(nums, old, old - 1);
        }
    }
}
