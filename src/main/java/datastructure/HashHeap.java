package datastructure;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;
import java.util.HashMap;

/**
 * see new implementation in MST_Kruskal
 */
public class HashHeap{

    private ArrayList<Integer> heap;
    private HashMap<Integer, Integer> map;

    public HashHeap(){
        heap = new ArrayList<>();
        map = new HashMap<>();
    }

    public HashHeap(int[] list){
        heap = new ArrayList<>();
        map = new HashMap<>();

        for (int i = 0; i < list.length; i++){
            heap.add(list[i]);
            map.put(list[i], i);
        }
        for (int i = heap.size() - 1; i >= 0; i--){
            maxHeapify(i);
        }
    }

    private void maxHeapify(int i){
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int largest = i;
        if (left < heap.size() && heap.get(left) > heap.get(largest)){
            largest = left;
        }
        if (right < heap.size() && heap.get(right) > heap.get(largest)){
            largest = right;
        }
        if (largest != i){
            swap(largest, i);
            maxHeapify(largest);
        }
    }

    public void offer(int elem){
        heap.add(elem);
        map.put(elem, heap.size() - 1);
        int parent = (heap.size() - 1 - 1) / 2;
        while (parent > 0){
            maxHeapify(parent);
            parent = (parent - 1) / 2;
        }
        maxHeapify(0);
    }

    public void remove(int elem){
        if (map.containsKey(elem)){
            int pos = map.get(elem);
            swap(pos, heap.size() - 1);
            removeLast();
            maxHeapify(pos);
        }
    }

    private void removeLast(){
        map.remove(heap.get(heap.size() - 1));
        heap.remove(heap.size() - 1);
    }

    private void swap(int pos1, int pos2){
        if (pos1 != pos2) {
            int temp = heap.get(pos1);
            heap.set(pos1, heap.get(pos2));
            heap.set(pos2, temp);
            map.put(heap.get(pos1), pos1);
            map.put(heap.get(pos2), pos2);
        }
    }


    public int peek(){
        return heap.get(0);
    }

    public int poll() throws HashHeapEmptyException{
        if (!heap.isEmpty()){
            int max = heap.get(0);
            swap(0, heap.size() - 1);
            removeLast();
            maxHeapify(0);
            return max;
        } else{
            throw new HashHeapEmptyException("poll from empty heap", null);
        }

    }

    public void print(){
        System.out.println(heap.toString());
        System.out.println();
        ArrayList<StringBuilder> sblist = new ArrayList<>();
        int size = heap.size();
        int layers = 0;
        while (size > 0){
            layers++;
            size = size >> 1;
        }
        int start = 0;
        int blankStart = 0; int addStart = 2;
        int blankMiddle = 1; int addMiddle = 4;
        for (int layer = layers - 1; layer >= 0 ; layer--){
            StringBuilder sb  = new StringBuilder();
            for (int i = (1 << layer) - 1; i < (1 << (layer + 1)) - 1; i++){
                if (i == heap.size()){
                    break;
                }
                int blanks = i == (1 << layer) - 1 ? blankStart : blankMiddle;
                for (int blank = 0; blank < blanks ; blank++){
                    sb.append(" ");
                    //System.out.print(" ");
                }
                sb.append(String.format("%3d", heap.get(i)));
                //System.out.format("%3d", heap.get(i));
            }
            sblist.add(sb);
            //System.out.println();
            blankStart += addStart; addStart *= 2;
            blankMiddle += addMiddle; addMiddle *= 2;
        }

        for (int i = sblist.size() - 1; i >= 0; i--){
            System.out.println(sblist.get(i).toString());
        }
    }

    public static class HashHeapEmptyException extends Exception{
        public HashHeapEmptyException(String message, Throwable cause){
            super(message, cause);
        }
    }

    public static void main(String[] args){
        HashHeap heap = new HashHeap(new int[]{2, 6, 54, 100, 5, 43, 78, 25, 23, 98, 22, 87});
        for (int i = 10; i < 15; i++){
            heap.offer(i);
        }
        heap.print();
        for (int i = 0; i < 2; i++){
            try{
                heap.poll();
            } catch (HashHeap.HashHeapEmptyException ex){
                break;
            }
        }
        heap.print();
        heap.remove(78);
        heap.print();

    }
}
