package datastructure;

import java.util.*;

public class AllOne {

    class Bucket{
        int val;
        Set<String> keys;
        Bucket prev; // smaller direction
        Bucket next; // larger direction
        Bucket(int v){
            val = v;
            keys = new HashSet<>();
        }

        String getOne(){
            return keys.iterator().next();
        }

        void removeKey(String key){
            keys.remove(key);
            if (keys.size() == 0){
                removeBucket(this);
            }
        }
    }


    Map<String, Bucket> map;

    Bucket small;
    Bucket large;

    /** Initialize your data structure here. */
    public AllOne() {
        map = new HashMap<>();
        small = new Bucket(-1);
        large = new Bucket(-1);
        small.next = large;
        large.prev = small;
    }

    private void insertAfter(Bucket newBucket, Bucket bucket){
        newBucket.prev = bucket;
        newBucket.next = bucket.next;

        bucket.next.prev = newBucket;
        bucket.next = newBucket;
    }

    private void removeBucket(Bucket bucket){
        bucket.prev.next = bucket.next;
        bucket.next.prev = bucket.prev;
    }

    /** Inserts a new key <Key> with value 1. Or increments an existing key by 1. */
    public void inc(String key) {
        if (map.containsKey(key)){
            Bucket bucket = map.get(key);
            if (bucket.next.val != bucket.val + 1){
                insertAfter(new Bucket(bucket.val + 1), bucket);
            }
            bucket.next.keys.add(key);
            map.put(key, bucket.next);

            bucket.removeKey(key);

        } else{
            if (small.next.val != 1){
                insertAfter(new Bucket(1), small);
            }
            small.next.keys.add(key);
            map.put(key, small.next);
        }
    }

    /** Decrements an existing key by 1. If Key's value is 1, remove it from the data structure. */
    public void dec(String key) {
        if (map.containsKey(key)){
            Bucket bucket = map.get(key);
            if (bucket.val == 1){
                map.remove(key);

                bucket.removeKey(key);
            } else{
                if (bucket.prev.val != bucket.val - 1){
                    insertAfter(new Bucket(bucket.val - 1), bucket.prev);
                }
                bucket.prev.keys.add(key);
                map.put(key, bucket.prev);

                bucket.removeKey(key);
            }
        }
    }

    /** Returns one of the keys with maximal value. */
    public String getMaxKey() {
        if (large.prev == small){
            return "";
        } else{
            return large.prev.getOne();
        }
    }

    /** Returns one of the keys with Minimal value. */
    public String getMinKey() {
        if (small.next == large){
            return "";
        } else{
            return small.next.getOne();
        }
    }

    public static void main(String[] args) {
        AllOne allone = new AllOne();
        System.out.println(allone.getMaxKey());
        System.out.println(allone.getMinKey());
        allone.inc("hello");
        allone.inc("hello");
        allone.inc("world");
        allone.inc("world");
        allone.inc("hello");
        allone.dec("world");

        System.out.println(allone.getMaxKey());
        System.out.println(allone.getMinKey());

    }
}

/**
 * Your AllOne object will be instantiated and called as such:
 * AllOne obj = new AllOne();
 * obj.inc(key);
 * obj.dec(key);
 * String param_3 = obj.getMaxKey();
 * String param_4 = obj.getMinKey();
 */