package datastructure;

public class BiT {
    private int[] bit; //bit: the binary indexed tree

    private Summary summaryFunc;
    private int default_value;

    interface Summary{
        public int summary(int x, int y);
    }

    public BiT(int input[], Summary summaryFunc, int default_value){
        this.default_value = default_value;
        this.summaryFunc = summaryFunc;

        bit = new int[input.length + 1];
        for (int i = 0; i <= input.length; i++){
            bit[i] = default_value;
        }

        for (int i = 0; i < input.length; i++){
            update(i, input[i]);
        }
    }


    /**
     * suppose input[index] is changed by changed
     * update the "bit"
     */
    public void update(int index, int changed){
        index += 1;
        while (index < bit.length){
            bit[index] = summaryFunc.summary(bit[index], changed);
            index += index & -index;
        }
    }

    /**
     * summary of input[0] ... input[index]
     */
    public int getSummary(int index){
        index += 1;
        int sum = default_value;
        while (index > 0){
            sum =summaryFunc.summary(sum, bit[index]);
            index -= index & -index;
        }
        return sum;
    }



    public static void main(String args[]) {
        int nums[] = {1, 3, 5, 7, 9, 11};
        BiT bit_forSum = new BiT(nums, new Summary() {
            @Override
            public int summary(int x, int y) {
                return x + y;
            }
        }, 0);

        System.out.println("Sum of values in given range = " +
                (bit_forSum.getSummary(3) - bit_forSum.getSummary(0)));

        bit_forSum.update(1, 10 - nums[1]);

        System.out.println("Sum of values in given range = " +
                (bit_forSum.getSummary(3) - bit_forSum.getSummary(0)));

        nums = new int[]{1, 3, 2, 7, 9, 11};

        BiT bit_forMin = new BiT(nums, new Summary(){
            public int summary(int x, int y){
                return Math.min(x, y);
            }
        }, Integer.MAX_VALUE);

        System.out.println("Minimum of values in range [" + 0 + ", "
                + 5 + "] is = " + bit_forMin.getSummary(5));

        bit_forMin.update(1, -3);
        System.out.println("Minimum of values in range [" + 0 + ", "
                + 5 + "] is = " + bit_forMin.getSummary(5));
        // problem: cannot do lower range!!!
    }
}
