package datastructure;

import java.util.*;
public class LRUCache {

    public class Node {
        public int key;
        public int val;
        public Node pre;
        public Node next;
        public Node(int key, int val){
            this.key = key;
            this.val = val;
        }
    }

    private Node head;
    private Node tail;
    private HashMap<Integer, Node> map;
    private int capacity;
    // @param capacity, an integer
    public LRUCache(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>();
        head = new Node(0, 0);
        tail = new Node(0, 0);

        head.next = tail;
        tail.pre = head;

        //head.next points to the least used one
        //tail.prev poitns to the most recently used one
    }

    // @return an integer
    public int get(int key) {
        if (capacity == 0){
            return -1;
        }
        if (map.containsKey(key)){
            Node node = map.get(key);
            moveToTail(node);
            return node.val;
        } else {
            return -1;
        }

    }

    // @param key, an integer
    // @param value, an integer
    // @return nothing
    public void set(int key, int value) {
        if (capacity == 0){
            return;
        }
        if (get(key) != -1){ // this get already moved the node!!
            Node node = map.get(key);
            node.val = value;
        } else {
            if (map.size() == capacity){
                Node old = head.next;
                map.remove(old.key);
                head.next = old.next;
                head.next.pre = head;
            }
            Node node = new Node(key, value);
            moveToTail(node);
            map.put(key, node);
        }
    }

    private void moveToTail(Node node){
        if (node.pre != null){
            node.pre.next = node.next;
        }
        if (node.next != null){
            node.next.pre = node.pre;
        }
        tail.pre.next = node;
        node.pre = tail.pre;
        node.next = tail;
        tail.pre = node;
    }
}