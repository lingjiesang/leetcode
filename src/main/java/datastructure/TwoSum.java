package datastructure;

import java.util.*;

// O(N) sum, O(1) add
public class TwoSum {

    Map<Integer, Integer> numCounts = new HashMap<>();

    // Add the number to an internal data structure.
    public void add(int number) {
        numCounts.put(number, numCounts.getOrDefault(number, 0) + 1);
    }

    // Find if there exists any pair of numbers which sum is equal to the value.
    public boolean find(int value) {
        for (Integer key : numCounts.keySet()) {
            if (key == value - key && numCounts.get(key) > 1 || key != value - key && numCounts.containsKey(value - key)) {
                return true;
            }
        }
        return false;
    }

    // O(N) add, O(1) sum
    public class TwoSum_Method2 {
        Set<Integer> sum = new HashSet<Integer>();
        Set<Integer> num = new HashSet<Integer>();

        // Add the number to an internal data structure.
        public void add(int number) {
            if (num.contains(number)) {
                sum.add(number * 2);
            } else {
                Iterator<Integer> iter = num.iterator();
                while (iter.hasNext()) {
                    sum.add(iter.next() + number);
                }
                num.add(number);
            }
        }

        // Find if there exists any pair of numbers which sum is equal to the value.
        public boolean find(int value) {
            return sum.contains(value);
        }
    }
}

