package datastructure;

import java.util.*;

public class MovingAverage {

    /** Initialize your data structure here. */
    private Deque<Integer> queue;
    private int maxSize;
    private int lastSum;
    public MovingAverage(int size) {
        queue = new ArrayDeque<>();
        maxSize = size;
    }

    public double next(int val) {
        if (queue.size() == maxSize){
            lastSum -= queue.poll();
        }
        queue.offer(val);
        lastSum += val;
        return (double)lastSum / queue.size();

    }
}