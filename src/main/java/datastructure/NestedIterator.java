package datastructure;

import java.util.*;

public class NestedIterator implements Iterator<Integer> {

//    // This is the interface that allows for creating nested lists.
//    // You should not implement it, or speculate about its implementation
//    public interface NestedInteger {
//
//        // @return true if this NestedInteger holds a single integer, rather than a nested list.
//        public boolean isInteger();
//
//        // @return the single integer that this NestedInteger holds, if it holds a single integer
//        // Return null if this NestedInteger holds a nested list
//        public Integer getInteger();
//
//        // @return the nested list that this NestedInteger holds, if it holds a nested list
//        // Return null if this NestedInteger holds a single integer
//        public List<NestedInteger> getList();
//    }


    private Iterator<NestedInteger> outerIter;
    private Integer nextInt;
    private NestedIterator innerIter;

    public NestedIterator(List<NestedInteger> nestedList) {
        this.outerIter = nestedList.iterator();
        findNext();
    }

    private void findNext(){
        while (nextInt == null && (innerIter == null || !innerIter.hasNext()) && outerIter.hasNext()){
            NestedInteger i = outerIter.next();
            if (i.isInteger()){
                nextInt = i.getInteger();
                break;
            } else{
                innerIter = new NestedIterator(i.getList());
            }
        }
    }

    @Override
    public Integer next() {
        Integer result = nextInt != null ? nextInt : innerIter.next();
        if (nextInt != null){
            nextInt = null;
        }

        findNext();
        return result;
    }

    @Override
    public boolean hasNext() {
        return nextInt != null || innerIter != null && innerIter.hasNext();
    }
}

/**
 * Your NestedIterator object will be instantiated and called as such:
 * NestedIterator i = new NestedIterator(nestedList);
 * while (i.hasNext()) v[f()] = i.next();
 */
