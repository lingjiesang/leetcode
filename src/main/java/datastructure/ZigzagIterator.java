package datastructure;

import java.util.*;

public class ZigzagIterator {
    private LinkedList<Iterator<Integer>> iterList;
    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        iterList = new LinkedList<>();
        if (!v1.isEmpty()){
            iterList.addLast(v1.iterator());
        }
        if (!v2.isEmpty()){
            iterList.addLast(v2.iterator());
        }
    }

    public int next() {
        Iterator<Integer> iter = iterList.pollFirst();
        int result = iter.next();
        if (iter.hasNext()){
            iterList.addLast(iter);
        }
        return result;
    }

    public boolean hasNext() {
        return !iterList.isEmpty();
    }
}
