package datastructure;

import java.util.*;

public class NestedIntegerGenFromString_Recursive {
    int pos;

    public NestedInteger deserialize(String s) {
        s = s.replaceAll("\\s+", "");

        if (s.charAt(pos) != '[') {
            return new NestedInteger(Integer.valueOf(s));
        } else {
            pos = 1;
            return getNestedIntegerWithList(s);
        }
    }

    private NestedInteger getNestedIntegerWithList(String s) {
        NestedInteger result = new NestedInteger();

        while (pos < s.length()) {
            if (s.indexOf('[', pos) == -1 || s.indexOf('[', pos) > s.indexOf(']', pos)) {
                int nextPos = s.indexOf(']', pos);
                List<NestedInteger> list = getList(s.substring(pos, nextPos));
                for (NestedInteger ni : list) {
                    result.add(ni);
                }
                pos = nextPos + 1;
                return result;
            } else {
                int nextPos = s.indexOf('[', pos);
                List<NestedInteger> list = getList(s.substring(pos, nextPos));
                for (NestedInteger ni : list) {
                    result.add(ni);
                }
                pos = nextPos + 1;
                result.add(getNestedIntegerWithList(s));
            }
        }
        return result;
    }


    private List<NestedInteger> getList(String s) {
        List<NestedInteger> list = new ArrayList<>();
        String[] nums = s.split(",");
        for (String num : nums) {
            if (num.length() != 0) {
                list.add(new NestedInteger(Integer.valueOf(num)));
            }
        }
        return list;
    }
}
