package datastructure;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/54184/java-solution-using-knuth-shuffle
 */
public class RandomShuffleArray {
    private int[] nums;
    private int[] shuffled;

    public RandomShuffleArray(int[] nums) {
        this.nums = nums;
    }

    /** Resets the array to its original configuration and return it. */
    public int[] reset() {
        return nums;
    }

    /** Returns a random shuffling of the array. */
    public int[] shuffle() {
        shuffled = nums.clone();
        for (int i = 0; i < nums.length; i++){
            int j = new Random().nextInt(i + 1);
            swap(shuffled, i, j);
        }
        return shuffled;
    }

    private void swap(int[] nums, int i, int j){
        if (i != j){
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }
}
