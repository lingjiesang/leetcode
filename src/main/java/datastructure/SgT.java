package datastructure;

public class SgT {
    private int[] sgt;
    private int default_value;
    private Summary summaryFunc;
    int n;
    int nums_size;

    interface Summary{
        public int summary(int x, int y);
    }

    public SgT(int[] nums, Summary summaryFunc, int default_value){
        this.summaryFunc = summaryFunc;
        this.default_value = default_value;

        nums_size = nums.length;
        n = (int)Math.pow(2, Math.ceil(Math.log(nums_size) / Math.log(2))) * 2 - 1;
        sgt = new int[n];

        for (int i = 0; i < n; i++){ //key: make everything default value first!
            sgt[i] = default_value;
        }

        for (int i = 0; i < nums_size; i++){
            update(i, nums[i]);
        }
    }

    public int getSummary(int n_st, int n_ed){
        return getSummaryHelper(0, 0, nums_size - 1, n_st, n_ed);
    }

    // cn_st: covered nums index start
    // n_st: target nums index start
    // root_idx : index of in sgt
    private int getSummaryHelper(int root_idx, int cn_st, int cn_ed, int n_st, int n_ed){
        if (cn_st > n_ed || n_st > cn_ed){
            return default_value;
        }
        if (n_st <= cn_st && cn_ed <= n_ed){
            return sgt[root_idx];
        }
        int cn_mid = cn_st + (cn_ed - cn_st) / 2;
        return summaryFunc.summary(getSummaryHelper(root_idx * 2 + 1, cn_st, cn_mid, n_st, n_ed),
                getSummaryHelper(root_idx * 2 + 2, cn_mid + 1, cn_ed, n_st, n_ed));
    }

    public void update(int index, int diff){
        updateHelper(0, 0, nums_size - 1, index, diff);
    }

    private void updateHelper(int root_idx, int cn_st, int cn_ed, int n_idx, int diff){
        if (cn_st > n_idx || n_idx > cn_ed){
            return;
        }
        sgt[root_idx] = summaryFunc.summary(sgt[root_idx], diff);
        if (cn_st != cn_ed){
            int cn_mid = cn_st + (cn_ed - cn_st) / 2;
            updateHelper(root_idx * 2 + 1, cn_st, cn_mid, n_idx, diff);
            updateHelper(root_idx * 2 + 2, cn_mid + 1, cn_ed, n_idx, diff);
        }
    }



    public static void main(String[] args){

        // make sure summary(anyValue, default) == anyValue!

        int nums[] = {1, 3, 5, 7, 9, 11};
        SgT sgt_forSum = new SgT(nums, new Summary() {
            @Override
            public int summary(int x, int y) {
                return x + y;
            }
        }, 0);

        System.out.println("Sum of values in given range = " +
                sgt_forSum.getSummary(1, 3));

        sgt_forSum.update(1, 10 - nums[1]);

        System.out.println("Sum of values in given range = " +
                sgt_forSum.getSummary(1, 3));

        nums = new int[]{1, 3, 2, 7, 9, 11};
        SgT sgt_forMin = new SgT(nums, new Summary(){
            public int summary(int x, int y){
                return Math.min(x, y);
            }
        }, Integer.MAX_VALUE);

        System.out.println("Minimum of values in range [" + 1 + ", "
                + 5 + "] is = " + sgt_forMin.getSummary(1, 5));

        sgt_forMin.update(1, -3);
        System.out.println("Minimum of values in range [" + 1 + ", "
                + 5 + "] is = " + sgt_forMin.getSummary(1, 5));

    }
}
