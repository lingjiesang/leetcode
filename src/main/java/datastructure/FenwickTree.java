package datastructure;

/**
 * also called Binary Indexed Tree
 * ref: http://www.geeksforgeeks.org/binary-indexed-tree-or-fenwick-tree-2/
 * onenote > interviews > Algorithm > BIT
 *
 *   -x = ~x + 1  (remember this!)
 *
 *   x & ~x = 0   (obvious)
 *   x & (~x + 1) = a number with last 1 bit of x set to 1, all other bits set to 0
 *
 *                last 1 bit of x
 *                    |
 *    example x = 10101000
 *           ~x = 01010111
 *       ~x + 1 = 01011000
 *
 */
public class FenwickTree {
    private int[] bit; //bit: the binary indexed tree

    /**
     * suppose input[index] is changed to input[index] + val
     * update the "bit"
     */
    public void addValue(int val, int index){
        index += 1;
        while (index < bit.length){
            bit[index] += val;
            index += index & -index;
        }
    }

    /**
     * sum of input[0] ... input[index]
     */
    public int getSum(int index){
        index += 1;
        int sum = 0;
        while (index > 0){
            sum += bit[index];
            index -= index & -index;
        }
        return sum;
    }

    public FenwickTree(int input[]){
        bit = new int[input.length + 1];
        for (int i = 0; i < input.length; i++){
            addValue(input[i], i);
        }
    }

    public static void main(String args[]) {
        int input[] = {1, 2, 3, 4, 5, 6, 7};
        FenwickTree ft = new FenwickTree(input);
        assert 1 == ft.getSum(0);
        assert 3 == ft.getSum(1);
        assert 6 == ft.getSum(2);
        assert 10 == ft.getSum(3);
        assert 15 == ft.getSum(4);
        assert 21 == ft.getSum(5);
        assert 28 == ft.getSum(6);
        ft.addValue(10, 0);
        assert 11 == ft.getSum(0);
    }
}
