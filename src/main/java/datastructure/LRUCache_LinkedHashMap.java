package datastructure;

import java.util.*;

/**
 * ref:https://discuss.leetcode.com/topic/43961/linkedhashmap-takes-care-of-everything-java-short-and-efficient-solution
 */

public class LRUCache_LinkedHashMap {
    private LinkedHashMap<Integer, Integer> map;
    private final int CAPACITY;
    public LRUCache_LinkedHashMap(int capacity) {
        CAPACITY = capacity;
        map = new LinkedHashMap<Integer, Integer>(capacity, 1.0f, true){
            protected boolean removeEldestEntry(Map.Entry eldest) {
                return size() > CAPACITY;
            }
        };
    }
    public int get(int key) {
        return map.getOrDefault(key, -1);
    }
    public void set(int key, int value) {
        map.put(key, value);
    }
}
