package datastructure;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/37254/let-me-explain-the-question-with-better-examples
 *
 1) [“dog”]; isUnique(“dig”);
 //False, because “dig” has the same abbreviation with “dog" and “dog” is already in the dictionary. It’s not unique.

 2) [“dog”, “dog"]; isUnique(“dog”);
 //True, because “dog” is the only word that has “d1g” abbreviation.

 3) [“dog”, “dig”]; isUnique(“dog”);
 //False, because if we have more than one word match to the same abbreviation, this abbreviation will never be unique.
 */
public class ValidWordAbbr {
    Map<String, String> map;

    public ValidWordAbbr(String[] dictionary) {
        map = new HashMap<>();
        for (String word : dictionary){
            String abbr = getAbbr(word);
            if (map.containsKey(abbr) && !word.equals(map.get(abbr))){
                map.put(abbr, "");
            } else {
                map.put(abbr, word);
            }
        }
    }

    private String getAbbr(String word){
        if (word.length() <= 2){
            return word;
        }
        int n = word.length();
        return new StringBuilder().append(word.charAt(0)).append(n - 2).append(word.charAt(n - 1)).toString();
    }

    public boolean isUnique(String word) {
        String abbr = getAbbr(word);
        return !map.containsKey(abbr) || map.get(abbr).equals(word);
    }
}


// Your ValidWordAbbr object will be instantiated and called as such:
// ValidWordAbbr vwa = new ValidWordAbbr(dictionary);
// vwa.isUnique("Word");
// vwa.isUnique("anotherWord");