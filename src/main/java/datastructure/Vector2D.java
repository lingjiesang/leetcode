package datastructure;

import java.util.*;

public class Vector2D implements Iterator<Integer> {

    List<List<Integer>> vec2d;
    Iterator<List<Integer>> outerIter;
    Iterator<Integer> innerIter;

    public Vector2D(List<List<Integer>> vec2d) {
        this.vec2d = vec2d;
        outerIter = vec2d.iterator();
        while (outerIter.hasNext()) {
            innerIter = outerIter.next().iterator();
            if (innerIter.hasNext()){
                break;
            }
        }
    }

    @Override
    public Integer next() {
        return innerIter.next();
    }

    @Override
    public boolean hasNext() {
        if (innerIter == null){ // corner case: input is []
            return false;
        }
        if (innerIter.hasNext()){
            return true;
        } else{
            while (outerIter.hasNext()){
                innerIter = outerIter.next().iterator();
                if (innerIter.hasNext()){
                    return true;
                }
            }
        }
        return false;
    }
}

/**
 * Your Vector2D object will be instantiated and called as such:
 * Vector2D i = new Vector2D(vec2d);
 * while (i.hasNext()) v[f()] = i.next();
 */
