package datastructure;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.lang.StringBuilder;

public class Heap{

    private ArrayList<Integer> heap;

    public Heap(){
        heap = new ArrayList<>();
    }

    public Heap(int[] list){
        heap = new ArrayList<>();
        for (int i = 0; i < list.length; i++){
            heap.add(list[i]);
        }
        for (int i = heap.size() - 1; i >= 0; i--){
            maxHeapify(i);
        }
    }

    private void maxHeapify(int i){
        int left = 2 * i + 1;
        int right = 2 * i + 2;
        int largest = i;
        if (left < heap.size() && heap.get(left) > heap.get(largest)){
            largest = left;
        }
        if (right < heap.size() && heap.get(right) > heap.get(largest)){
            largest = right;
        }
        if (largest != i){
            int temp = heap.get(i);
            heap.set(i, heap.get(largest));
            heap.set(largest, temp);
            maxHeapify(largest);
        }
    }

    public void offer(int elem){
        heap.add(elem);
        int parent = (heap.size() - 1 - 1) / 2;
        while (parent > 0){
            maxHeapify(parent);
            parent = (parent - 1) / 2;
        }
        maxHeapify(0);
    }

    public void delete(int elem){
        for (int i = 0; i < heap.size(); i++){
            if (heap.get(i) == elem){
                heap.set(i, heap.get(heap.size() - 1));
                heap.remove(heap.size() - 1);
                maxHeapify(i);
                break;
            }
        }
    }

    public int peek(){
        return heap.get(0);
    }

    public int poll() throws HeapEmptyException{
        if (!heap.isEmpty()){
            int max = heap.get(0);
            heap.set(0, heap.get(heap.size() - 1));
            heap.remove(heap.size() - 1);
            maxHeapify(0);
            return max;
        } else{
            throw new HeapEmptyException("poll from empty heap", null);
        }

    }

    public void print(){
        System.out.println(heap.toString());
        System.out.println();
        ArrayList<StringBuilder> sblist = new ArrayList<>();
        int size = heap.size();
        int layers = 0;
        while (size > 0){
            layers++;
            size = size >> 1;
        }
        int start = 0;
        int blankStart = 0; int addStart = 2;
        int blankMiddle = 1; int addMiddle = 4;
        for (int layer = layers - 1; layer >= 0 ; layer--){
            StringBuilder sb  = new StringBuilder();
            for (int i = (1 << layer) - 1; i < (1 << (layer + 1)) - 1; i++){
                if (i == heap.size()){
                    break;
                }
                int blanks = i == (1 << layer) - 1 ? blankStart : blankMiddle;
                for (int blank = 0; blank < blanks ; blank++){
                    sb.append(" ");
                    //System.out.print(" ");
                }
                sb.append(String.format("%3d", heap.get(i)));
                //System.out.format("%3d", heap.get(i));
            }
            sblist.add(sb);
            //System.out.println();
            blankStart += addStart; addStart *= 2;
            blankMiddle += addMiddle; addMiddle *= 2;
        }

        for (int i = sblist.size() - 1; i >= 0; i--){
            System.out.println(sblist.get(i).toString());
        }
    }

    public static class HeapEmptyException extends Exception{
        public HeapEmptyException(String message, Throwable cause){
            super(message, cause);
        }
    }

    public static void main(String[] args){
        Heap heap = new Heap(new int[]{2, 6, 54, 100, 5, 43, 78, 25, 23, 98, 22, 87});
        for (int i = 10; i < 15; i++){
            heap.offer(i);
        }
        heap.print();
        for (int i = 0; i < 2; i++){
            try{
                heap.poll();
            } catch (Heap.HeapEmptyException ex){
                break;
            }
        }
        heap.print();
        heap.delete(78);
        heap.print();

    }
}
