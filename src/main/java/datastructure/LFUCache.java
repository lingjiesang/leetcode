package datastructure;

import java.util.*;

public class LFUCache {
    int capacity;
    int filled;
    int time;
    FreqNode head;
    Map<Integer, FreqNode> keyToFreqNode;

    class Node{
        int key;
        int val;
        int timeStamp;
        Node prev;
        Node next;
        Node(int k, int v, int t){
            key = k;
            val = v;
            timeStamp = t;
        }
    }

    class FreqNode{
        int freq;
        Map<Integer, Node> keyToNode;
        Node mostRecent;
        Node leastRecent;

        FreqNode prev;
        FreqNode next;

        FreqNode(int f){
            freq = f;
            keyToNode = new HashMap<>();
        }

        //get a node using key and remove from this
        Node get(int key){
            Node node = keyToNode.get(key);
            if (node.prev != null){
                node.prev.next = node.next;
            }
            if (node.next != null){
                node.next.prev = node.prev;
            }
            keyToNode.remove(key);

            if (node == mostRecent){
                mostRecent = node.next;
            }

            if (node == leastRecent){
                leastRecent = node.prev;
            }

            node.prev = null;
            node.next = null;
            return node;
        }

        // add a node (which is guaranteed to have the most recent time stamp)
        void addNode(Node node){

            if (mostRecent == null){
                mostRecent = leastRecent = node;
            } else{
                node.next = mostRecent;
                mostRecent.prev = node;
                mostRecent = node;
            }

            keyToNode.put(node.key, node);
        }


        int removeLeastRecent(){
            Node node = leastRecent;
            if (mostRecent == leastRecent){
                mostRecent = leastRecent = null;
            } else {
                leastRecent = leastRecent.prev;
                leastRecent.next = null;
            }
            keyToNode.remove(node.key);

            return node.key;
        }
    }

    private void removeFreqNode(FreqNode freqNode){
        if (freqNode.prev != null){
            freqNode.prev.next = freqNode.next;
        }
        if (freqNode.next != null){
            freqNode.next.prev = freqNode.prev;
        }
    }

    private void addFreqNodeAfter(FreqNode freqNode){
        FreqNode newFreqNode = new FreqNode(freqNode.freq + 1);
        newFreqNode.next = freqNode.next;
        if (freqNode.next != null){
            freqNode.next.prev = newFreqNode;
        }

        newFreqNode.prev = freqNode;
        freqNode.next = newFreqNode;
    }


    private Node getNode(int key) {
        time++;
        if (!keyToFreqNode.containsKey(key)){
            return null;
        }
        FreqNode freqNode = keyToFreqNode.get(key);
        Node node = freqNode.get(key);
        node.timeStamp = time;

        if (freqNode.next == null || freqNode.next.freq != freqNode.freq + 1){
            addFreqNodeAfter(freqNode);
        }

        FreqNode newFreqNode = freqNode.next;
        if (freqNode.mostRecent == null){
            removeFreqNode(freqNode);
        }
        newFreqNode.addNode(node);
        keyToFreqNode.put(node.key, newFreqNode);

        return node;
    }


    public LFUCache(int capacity) {
        this.capacity = capacity;
        filled = 0;
        time = 0;
        head = new FreqNode(0);
        keyToFreqNode = new HashMap<>();
    }


    public int get(int key){
        if (capacity == 0){
            return -1;
        }
        Node node = getNode(key);
        return node == null ? -1 : node.val;
    }

    public void set(int key, int value) {
        if (capacity == 0){
            return;
        }
        Node node = getNode(key);
        if (node != null){
            node.val = value;
        } else{
            if (filled == capacity){
                evictOne();
            }
            if (head.next == null || head.next.freq != 1){
                addFreqNodeAfter(head);
            }
            FreqNode newFreqNode = head.next;
            node = new Node(key, value, time);
            newFreqNode.addNode(node);
            keyToFreqNode.put(node.key, newFreqNode);
            filled++;
        }
    }

    private void evictOne(){
        FreqNode freqNode = head.next;
        int key = freqNode.removeLeastRecent();
        keyToFreqNode.remove(key);

        if (freqNode.mostRecent == null){
            removeFreqNode(freqNode);
        }
        filled--;
    }

    public static void main(String[] args){
        LFUCache cache = new LFUCache(2);
        cache.set(1, 1);
        cache.set(2, 2);
        System.out.println(cache.get(1));
        cache.set(3, 3);
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        cache.set(4, 4);
        System.out.println(cache.get(1));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));


        cache = new LFUCache(3);
        cache.set(1, 1);
        cache.set(2, 2);
        cache.set(3, 3);
        cache.set(4, 4);
        System.out.println(cache.get(4));
        System.out.println(cache.get(3));
        System.out.println(cache.get(2));
        System.out.println(cache.get(1));
        cache.set(5, 5);
        System.out.println(cache.get(1));
        System.out.println(cache.get(2));
        System.out.println(cache.get(3));
        System.out.println(cache.get(4));
        System.out.println(cache.get(5));



    }
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * LFUCache obj = new LFUCache(capacity);
 * int param_1 = obj.get(key);
 * obj.set(key,value);
 */