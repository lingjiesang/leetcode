package datastructure;

import java.util.*;

/**
 * ljs: because constructor is not allowed in interface, change it to a class
 *
 */

// This is the interface that allows for creating nested lists.
// You should not implement it, or speculate about its implementation
public class NestedInteger {

    private Integer i;
    private List<NestedInteger> list;
    private boolean isInteger;

    // Constructor initializes an empty nested list.
    public NestedInteger() {
    }

    // Constructor initializes a single integer.
    public NestedInteger(int value){
        i = value;
        isInteger = true;
    }

    // @return true if this NestedInteger holds a single integer, rather than a nested list.
    public boolean isInteger(){
        return isInteger;
    }

    // @return the single integer that this NestedInteger holds, if it holds a single integer
    // Return null if this NestedInteger holds a nested list
    public Integer getInteger(){
        return i;
    }

    // @return the nested list that this NestedInteger holds, if it holds a nested list
    // Return null if this NestedInteger holds a single integer
    public List<NestedInteger> getList(){
        return list;
    }

    // Set this NestedInteger to hold a single integer.
    public void setInteger(int value){
        list = null;
        i = value;
        isInteger = true;
    }

    // Set this NestedInteger to hold a nested list and adds a nested integer to it.
    public void add(NestedInteger ni){
        i = null;
        isInteger = false;
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(ni);
    }
}