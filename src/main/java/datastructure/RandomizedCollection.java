package datastructure;

import java.util.*;

public class RandomizedCollection {

    List<Integer> nums;
    Map<Integer, Set<Integer>> valToLocs;
    /** Initialize your data structure here. */
    public RandomizedCollection() {
        nums = new ArrayList<>();
        valToLocs = new HashMap<>();
    }

    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
        if (!valToLocs.containsKey(val)){
            valToLocs.put(val, new LinkedHashSet<>());
        }
        valToLocs.get(val).add(nums.size());
        nums.add(val);
        return valToLocs.get(val).size() == 1;
    }

    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        if (!valToLocs.containsKey(val) || valToLocs.get(val).size() == 0){
            return false;
        }
        Set<Integer> oldLocs = valToLocs.get(val);

        if (!oldLocs.contains(nums.size() - 1)){ // key
            int oldLoc = oldLocs.iterator().next();
            swap(oldLoc, nums.size() - 1);

            int newValAtLoc = nums.get(oldLoc);
            valToLocs.get(newValAtLoc).remove(nums.size() - 1);
            valToLocs.get(newValAtLoc).add(oldLoc); // without key, might not actually add

            oldLocs.remove(oldLoc);
            nums.remove(nums.size() - 1);
        } else{
            oldLocs.remove(nums.size() - 1);
            nums.remove(nums.size() - 1);
        }

        return true;
    }

    /** Get a random element from the collection. */
    public int getRandom() {
        int random = new Random().nextInt(nums.size());
        return nums.get(random);
    }

    private void swap(int i, int j){
        int temp = nums.get(i);
        nums.set(i, nums.get(j));
        nums.set(j, temp);
    }
}

/**
 * Your RandomizedCollection object will be instantiated and called as such:
 * RandomizedCollection obj = new RandomizedCollection();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
