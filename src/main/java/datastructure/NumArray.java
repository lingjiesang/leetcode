package datastructure;

public class NumArray {

    private int[] bit;
    private int[] array;

    public NumArray(int[] nums) {
        array = new int[nums.length];
        bit = new int[nums.length + 1];

        for (int i = 0; i < array.length; i++){
            update(i, nums[i]);
        }
    }

    void update(int i, int val) {
        int delta = val - array[i];
        array[i] = val;

        i = i + 1;

        while (i < bit.length){
            bit[i] += delta;
            i += i & -i;
        }
    }

    public int getSum(int i){
        i = i + 1;
        int sum = 0;
        while (i > 0){
            sum += bit[i];
            i -= i & -i;
        }
        return sum;
    }

    public int sumRange(int i, int j) {
        return getSum(j) - getSum(i - 1);
    }


    public static void main(String[] args){
        int[] nums = {1,3,5};
        NumArray numArray = new NumArray(nums);
        System.out.println(numArray.sumRange(0, 2));
        numArray.update(1, 2);
        System.out.println(numArray.sumRange(0, 2));
    }
}
