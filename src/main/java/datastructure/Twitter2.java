package datastructure;

import java.util.*;

/**
 * if copy to LC
 * change Twitter2 => Twitter
 *
 *
 * need use LinkedList and stores Iterator into PriorityQueue, need to use PeekingIterator (has to implement that)
 *
 */
public class Twitter2 {

    static int timeStamp = 0;

    class Tweet {
        int time;
        int id;

        Tweet(int tid) {
            time = timeStamp++;
            id = tid;
        }
    }

    class User {
        int id;
        Set<User> followed;
        LinkedList<Tweet> tweets;

        User(int uid) {
            id = uid;
            followed = new HashSet<>();
            followed.add(this);
            tweets = new LinkedList<>();
        }

        boolean addFollowee(User followee) {
            return followed.add(followee);
        }

        boolean removeFollowee(User followee) {
            return followed.remove(followee);
        }

        public void post(int tid) {
            tweets.addFirst(new Tweet(tid)); // don't forget to addFirst!!!
        }
    }


    class PeekingIterator<E> implements Iterator<E> {

        private Iterator<E> iterator;
        private E cache;


        public PeekingIterator(Iterator<E> iterator) {
            // initialize any member here.
            this.iterator = iterator;
        }

        // Returns the next element in the iteration without advancing the iterator.
        public E peek() {
            if (cache == null) {
                cache = iterator.next();
            }
            return cache;
        }

        // hasNext() and next() should behave the same as in the Iterator interface.
        // Override them if needed.
        @Override
        public E next() {
            E res = cache != null ? cache : iterator.next();
            cache = null;
            return res;
        }

        @Override
        public boolean hasNext() {
            return cache != null || iterator.hasNext();
        }
    }

    private Map<Integer, User> allUsers;

    /**
     * Initialize your data structure here.
     */
    public Twitter2() {
        allUsers = new HashMap<>();

    }

    /**
     * Compose a new tweet.
     */
    public void postTweet(int userId, int tweetId) {
        User user = allUsers.computeIfAbsent(userId, uid -> new User(uid));
        user.post(tweetId);
    }

    /**
     * Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent.
     */
    public List<Integer> getNewsFeed(int userId) {
        List<Integer> res = new ArrayList<>();
        PriorityQueue<PeekingIterator<Tweet>> pq = new PriorityQueue<>((a, b) -> (b.peek().time - a.peek().time));

        User user = allUsers.computeIfAbsent(userId, uid -> new User(uid));

        for (User followee : user.followed) {
            if (!followee.tweets.isEmpty()) {
                pq.add(new PeekingIterator<>(followee.tweets.iterator()));
            }
        }

        while (!pq.isEmpty() && res.size() < 10) {
            PeekingIterator<Tweet> pi = pq.poll();
            res.add(pi.next().id);
            if (pi.hasNext()) {
                pq.add(pi);
            }
        }
        return res;

    }

    /**
     * Follower follows a followee. If the operation is invalid, it should be a no-op.
     */
    public void follow(int followerId, int followeeId) {
        if (followerId != followeeId) {
            User follower = allUsers.computeIfAbsent(followerId, uid -> new User(uid));
            User followee = allUsers.computeIfAbsent(followeeId, uid -> new User(uid));
            follower.addFollowee(followee);
        }
    }

    /**
     * Follower unfollows a followee. If the operation is invalid, it should be a no-op.
     */
    public void unfollow(int followerId, int followeeId) {
        if (followerId != followeeId) {
            User follower = allUsers.computeIfAbsent(followerId, uid -> new User(uid));
            User followee = allUsers.computeIfAbsent(followeeId, uid -> new User(uid));
            follower.removeFollowee(followee);
        }
    }
}
