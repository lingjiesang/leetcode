package datastructure;

import java.util.*;

public class NestedIntegerGenFromString {


    /**
     * use "two pointer" for numbers
     */
    public NestedInteger deserialize(String s) {
        //s = s.replaceAll("\\s+", "");

        if (s == null || s.length() == 0) {
            return null;
        }

        if (s.charAt(0) != '[') {
            return new NestedInteger(Integer.valueOf(s));
        }

        Deque<NestedInteger> stack = new ArrayDeque<>();
        stack.push(new NestedInteger());

        int start = 0;

        for (int pos = 0; pos < s.length(); pos++) {
            if (s.charAt(pos) == '[' || s.charAt(pos) == ']' || s.charAt(pos) == ',') {
                if (pos > start) { //key!
                    stack.peek().add(new NestedInteger(Integer.valueOf(s.substring(start, pos))));
                }

                if (s.charAt(pos) == '[') {
                    NestedInteger ni = new NestedInteger();
                    stack.peek().add(ni);
                    stack.push(ni);
                }

                if (s.charAt(pos) == ']') {
                    stack.pop();
                }

                start = pos + 1;
            }
        }
        return stack.pop().getList().get(0);
    }

    /**
     * directly parse "numbers" (faster)
     */
    public NestedInteger deserialize_method2(String s) {
        if (s == null || s.length() == 0) {
            return null;
        }

        if (s.indexOf('[') < 0) {
            return new NestedInteger(Integer.valueOf(s));
        }


        int val = 0;
        boolean isNeg = false;

        Deque<NestedInteger> stack = new ArrayDeque<>();
        stack.push(new NestedInteger());

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);

            if (c == '['){
                NestedInteger ni = new NestedInteger();
                stack.peek().add(ni);
                stack.push(ni);
            } else if (c == ']' || c == ','){
                if (s.charAt(i - 1) >= '0' && s.charAt(i - 1) <= '9') { //key!
                    stack.peek().add(new NestedInteger(isNeg ? 0 - val : val));
                    val = 0;
                    isNeg = false;
                }
                if (c == ']') {
                    stack.pop();
                }
            } else if (c == '-'){
                isNeg = true;
            } else{
                val = val * 10 + c - '0';

            }

        }
        return stack.pop().getList().get(0);
    }
}