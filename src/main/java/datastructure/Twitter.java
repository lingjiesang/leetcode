package datastructure;

import java.util.*;

/**
 * 这个design比Twitter2要好.
 *
 * 当一个iterator create了以后,除了这个iterator以外的东西change了iterator的underlying data structure,
 * ConcurrentModificationException 会被throw
 * No iterator is thread-safe. If the underlying collection is changed amidst iteration, a ConcurrentModificationException is thrown
 *
 *
 *
 * 如果我们在getNewsFeed的过程中,一个followee post的新的内容,如果用现在这个方法,另一个thread可以安全的concurrent的改变
 * 只不过getNewsFeed里不会出现这个新post
 *
 * 但是如果我们用到iterator,那么post的thread要等getNewsFeed结束了才能post,这段时间它就被block了...
 *
 *
 */
public class Twitter {

    static int timeStamp = 0;

    class Tweet{
        int time;
        int id;
        Tweet prev;
        Tweet(int id){
            this.time = timeStamp++;
            this.id = id;
        }
    }

    class User{
        int id;
        Set<User> followed;
        Tweet lastTweet;
        public User(int id){
            this.id = id;
            followed = new HashSet<>();
            followed.add(this);
        }
        public boolean addFollowee(User followee){
            return followed.add(followee);
        }
        public boolean removeFollowee(User followee){
            return followed.remove(followee);
        }
        public void post(int tid){
            Tweet prev = lastTweet;
            lastTweet = new Tweet(tid);
            lastTweet.prev = prev;
        }
    }

    Map<Integer, User> allUsers;


    /** Initialize your data structure here. */
    public Twitter() {
        allUsers = new HashMap<>();
    }

    /** Compose a new tweet. */
    public void postTweet(int userId, int tweetId) {
        User user = allUsers.computeIfAbsent(userId, uid -> new User(uid));
        user.post(tweetId);
    }

    /** Retrieve the 10 most recent tweet ids in the user's news feed. Each item in the news feed must be posted by users who the user followed or by the user herself. Tweets must be ordered from most recent to least recent. */
    public List<Integer> getNewsFeed(int userId) {
        List<Integer> res = new ArrayList<>();
        PriorityQueue<Tweet> pq = new PriorityQueue<>((a, b) ->(b.time - a.time));
        User user = allUsers.computeIfAbsent(userId, uid -> new User(uid));
        for (User followee : user.followed){
            if (followee.lastTweet != null){
                pq.add(followee.lastTweet);
            }
        }

        while (!pq.isEmpty() && res.size() < 10){
            Tweet tweet = pq.poll();
            res.add(tweet.id);
            if (tweet.prev != null){
                pq.add(tweet.prev);
            }
        }
        return res;
    }

    /** Follower follows a followee. If the operation is invalid, it should be a no-op. */
    public void follow(int followerId, int followeeId) {
        if (followerId != followeeId) {
            User follower = allUsers.computeIfAbsent(followerId, uid -> new User(uid));
            User followee = allUsers.computeIfAbsent(followeeId, uid -> new User(uid));
            follower.addFollowee(followee);
        }
    }

    /** Follower unfollows a followee. If the operation is invalid, it should be a no-op. */
    public void unfollow(int followerId, int followeeId) {
        if (followerId != followeeId) {
            User follower = allUsers.computeIfAbsent(followerId, uid -> new User(uid));
            User followee = allUsers.computeIfAbsent(followeeId, uid -> new User(uid));
            follower.removeFollowee(followee);
        }
    }
}

/**
 * Your Twitter object will be instantiated and called as such:
 * Twitter obj = new Twitter();
 * obj.postTweet(userId,tweetId);
 * List<Integer> param_2 = obj.getNewsFeed(userId);
 * obj.follow(followerId,followeeId);
 * obj.unfollow(followerId,followeeId);
 */