package datastructure;

import java.util.*;

public class RandomizedSet {

    List<Integer> nums;
    Map<Integer, Integer> valToPos;

    /** Initialize your data structure here. */
    public RandomizedSet() {
        nums = new ArrayList<>();
        valToPos = new HashMap<>();
    }

    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        if (valToPos.containsKey(val)){
            return false;
        }
        valToPos.put(val, nums.size());
        nums.add(val);
        return true;
    }

    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        if (!valToPos.containsKey(val)){
            return false;
        }

        int pos = valToPos.get(val);

        if (pos != nums.size() - 1) {
            swap(pos, nums.size() - 1);
            int newValAtPos = nums.get(pos);
            valToPos.put(newValAtPos, pos);
        }

        valToPos.remove(val);
        nums.remove(nums.size() - 1);

        return true;
    }

    /** Get a random element from the set. */
    public int getRandom() {
        int random = new Random().nextInt(nums.size());
        return nums.get(random);
    }

    private void swap(int i, int j){
        int temp = nums.get(i);
        nums.set(i, nums.get(j));
        nums.set(j, temp);
    }
}

/**
 * Your RandomizedSet object will be instantiated and called as such:
 * RandomizedSet obj = new RandomizedSet();
 * boolean param_1 = obj.insert(val);
 * boolean param_2 = obj.remove(val);
 * int param_3 = obj.getRandom();
 */
