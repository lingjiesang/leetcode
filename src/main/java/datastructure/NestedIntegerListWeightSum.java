package datastructure;

import java.util.*;

public class NestedIntegerListWeightSum {
    /**
     * 339. I
     */
    public int depthSum(List<NestedInteger> nestedList) {
        return weightSum(nestedList, 1);
    }

    private int weightSum(List<NestedInteger> nestedList, int depth) {
        int sum = 0;
        for (NestedInteger nestedInteger : nestedList) {
            if (nestedInteger.isInteger()) {
                sum += depth * nestedInteger.getInteger();
            } else {
                sum += weightSum(nestedInteger.getList(), depth + 1);
            }
        }
        return sum;
    }

    /**
     * 364.II
     * method 1. DFS
     */
    public int depthSumInverse(List<NestedInteger> nestedList) {
        int maxDepth = 1;

        for (NestedInteger nestedInteger : nestedList) {
            maxDepth = Math.max(maxDepth, getDepth(nestedInteger, 1));
        }


        int sum = 0;

        for (NestedInteger i : nestedList) {
            sum += getWeightSum(i, maxDepth);
        }
        return sum;
    }

    private int getDepth(NestedInteger i, int depth){
        if (i.isInteger()){
            return depth;
        } else{
            int maxDepth = depth;
            for (NestedInteger nestedInteger : i.getList()){
                maxDepth = Math.max(maxDepth, getDepth(nestedInteger, depth + 1));
            }
            return maxDepth;
        }
    }

    private int getWeightSum(NestedInteger i, int depth){
        if (i.isInteger()){
            return i.getInteger() * depth;
        } else{
            int sum = 0;
            for (NestedInteger nestedInteger : i.getList()){
                sum += getWeightSum(nestedInteger, depth - 1);
            }
            return sum;
        }
    }

    /**
     * method2 : BFS
     *
     * ref: https://discuss.leetcode.com/topic/49041/no-depth-variable-no-multiplication/2
     */
    public int depthSumInverse_BFS(List<NestedInteger> nestedList) {
        int unweighted = 0, weighted = 0;
        while (!nestedList.isEmpty()) {
            List<NestedInteger> nextLevel = new ArrayList<>();
            for (NestedInteger ni : nestedList) {
                if (ni.isInteger())
                    unweighted += ni.getInteger();
                else
                    nextLevel.addAll(ni.getList());
            }
            weighted += unweighted;
            nestedList = nextLevel;
        }
        return weighted;
    }
}
