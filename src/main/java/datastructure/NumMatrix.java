package datastructure;

/**
 * ref: https://discuss.leetcode.com/topic/30343/java-2d-binary-indexed-tree-solution-clean-and-short-17ms/2
 */
public class NumMatrix {
    int[][] bit;
    int[][] matrix;
    int m;
    int n;

    public NumMatrix(int[][] matrix){
        if (matrix.length == 0 || matrix[0].length == 0){
            return;
        }
        m = matrix.length;
        n = matrix[0].length;
        bit = new int[m + 1][n + 1];
        this.matrix = new int[m][n];
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                update(i, j, matrix[i][j]);
            }
        }
    }

    public void update(int row, int col, int val){
        if (m == 0 || n == 0){
            return;
        }
        int delta = val - matrix[row][col];
        matrix[row][col] = val;
        row++;
        col++;
        for (int i = row; i <= m; i += i & -i){
            for (int j = col; j <= n; j += j & -j){
                bit[i][j] += delta;
            }
        }
    }

    public int sum(int row, int col){
        if (m == 0 || n == 0){
            return 0;
        }
        row++;
        col++;
        int result = 0;
        for (int i = row; i > 0; i -= i & -i){
            for (int j = col; j > 0; j -= j & -j){
                result += bit[i][j];
            }
        }
        return result;
    }

    public int sumRegion(int row1, int col1, int row2, int col2){
        if (m == 0 || n == 0){
            return 0;
        }
        return sum(row2, col2) - sum(row1 - 1, col2) - sum(row2, col1 - 1) + sum(row1 - 1, col1 - 1);
    }
}
