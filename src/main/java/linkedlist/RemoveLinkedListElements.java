package linkedlist;

import java.util.*;

public class RemoveLinkedListElements {
    public class Solution {
        public ListNode removeElements(ListNode head, int val) {

            ListNode dummy = new ListNode(0);
            dummy.next = head;
            ListNode prev = dummy;

            while (head != null){
                if (head.val == val){
                    prev.next = head.next;
                } else{
                    prev = head;
                }
                head = head.next;
            }
            return dummy.next;
        }
    }
}
