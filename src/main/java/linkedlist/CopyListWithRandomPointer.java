package linkedlist;

import java.util.*;
public class CopyListWithRandomPointer {
    class RandomListNode{
        int label;
        RandomListNode next, random;
        RandomListNode(int x){
            this.label = x;
        }
    }

    public RandomListNode copyRandomList(RandomListNode head){
        Map<RandomListNode, RandomListNode> oldToNew = new HashMap<>();
        if (head == null){
            return null;
        }

        RandomListNode curtOld = head;
        RandomListNode curtNew = new RandomListNode(head.label);
        oldToNew.put(curtOld, curtNew);


        while (curtOld != null){
            if (curtOld.random != null){
                if (!oldToNew.containsKey(curtOld.random)){
                    oldToNew.put(curtOld.random, new RandomListNode(curtOld.random.label));
                }
                curtNew.random = oldToNew.get(curtOld.random);
            }

            if (curtOld.next != null){
                if (!oldToNew.containsKey(curtOld.next)){
                    oldToNew.put(curtOld.next, new RandomListNode(curtOld.next.label));
                }
                curtNew.next = oldToNew.get(curtOld.next);
            }
            curtOld = curtOld.next;
            curtNew = curtNew.next;
        }
        return oldToNew.get(head);

    }
}
