package linkedlist;

import java.util.*;
public class CopyListWithRandomPointer_noHash {
    class RandomListNode {
        int label;
        RandomListNode next, random;
        RandomListNode(int x) {
            this.label = x;
        }
    }


    public RandomListNode copyRandomList(RandomListNode head) {
        if (head == null){
            return null;
        }
        RandomListNode curt = head;
        // create new nodes
        while (curt != null){
            RandomListNode newCurt = new RandomListNode(curt.label);
            RandomListNode next = curt.next;
            curt.next = newCurt;
            newCurt.next = next;
            curt = next;
        }

        // copy random pointers for new nodes
        curt = head;
        while (curt != null){
            if (curt.random != null){
                curt.next.random = curt.random.next;
            }
            curt = curt.next.next;
        }

        RandomListNode newListHead = head.next;
        // restore original list and generate new list
        curt = head;
        while (curt != null){
            RandomListNode newCurt = curt.next;
            curt.next = curt.next.next;
            if (curt.next != null){
                newCurt.next = curt.next.next;
            }
            curt = curt.next;
        }

        return newListHead;

    }
}