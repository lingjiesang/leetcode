package linkedlist;

import java.util.*;

public class ReorderList {
    public void reorderList(ListNode head) {
        if (head == null || head.next == null) {
            return;
        }
        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
        }

        ListNode head2 = slow.next; // slow ends 2 of [1,2,3,4] and [1,2,3,4,5]
        slow.next = null;
        head2 = reverse(head2);   // head2 = [4,3] for [1,2,3,4] or [5,4] for [1,2,3,4,5]
        ListNode head1 = head;    // i.e. head1's len is equal or one more than head2's len

        head = new ListNode(0);
        while (head1 != null && head2 != null){
            head.next = head1;
            head = head.next;
            head1 = head1.next;

            head.next = head2;
            head = head.next;
            head2 = head2.next;
        }
        if (head1 != null){
            head.next = head1;
        }

    }

    private ListNode reverse(ListNode node) {
        ListNode prev = null;

        while (node != null) {
            ListNode next = node.next;
            node.next = prev;

            prev = node;
            node = next;
        }
        return prev;
    }
}
