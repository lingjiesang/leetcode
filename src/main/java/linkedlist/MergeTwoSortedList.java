package linkedlist;

import java.util.*;

public class MergeTwoSortedList {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(0);
        ListNode curt = dummy;
        while (l1 != null && l2 != null){
            if (l1.val <= l2.val){
                curt.next = l1;
                l1 = l1.next;
                curt = curt.next;
            } else{
                curt.next = l2;
                l2 = l2.next;
                curt = curt.next;
            }
        }

        while (l1 != null){
            curt.next = l1;
            l1 = l1.next;
            curt = curt.next;
        }
        while (l2 != null){
            curt.next = l2;
            l2 = l2.next;
            curt = curt.next;
        }
        return dummy.next;
    }
}
