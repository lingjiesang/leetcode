package linkedlist;

import java.util.*;

public class AddTwoNumbers {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode curt = result;
        int carrier = 0;
        while (l1 != null || l2 != null || carrier != 0){
            int sum = (l1 == null ? 0 : l1.val) + (l2 == null ? 0:  l2.val) + carrier;

            curt.next = new ListNode(sum % 10);
            curt = curt.next;

            carrier = sum / 10;

            if (l1 != null){
                l1 = l1.next;
            }
            if (l2 != null){
                l2 = l2.next;
            }
        }
        return result.next;
    }
}
