package linkedlist;

import java.util.*;

public class PartitionList {
    ListNode partition(ListNode head, int x) {
        ListNode left = new ListNode(0);
        ListNode right = new ListNode(0);

        ListNode leftHead = left;
        ListNode rightHead = right;
        while (head != null){
            if (head.val < x){
                left.next = head;
                left = left.next;
            } else{
                right.next = head;
                right = right.next;
            }
            head = head.next;
        }
        left.next = rightHead.next;
        right.next = null; //don't forget this!!!
        return leftHead.next;

    }
}
