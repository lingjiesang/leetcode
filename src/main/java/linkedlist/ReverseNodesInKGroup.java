package linkedlist;


public class ReverseNodesInKGroup {


    public ListNode reverseKGroup(ListNode head, int k){
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        ListNode first = head, last = head;
        head = dummy;

        while (last != null) {

            // find k group
            for (int i = 1; i < k; i++) {
                if (last.next != null) {
                    last = last.next;
                } else {
                    return dummy.next;
                }
            }

            //reverse
            ListNode tail = last.next;
            last.next = null;
            reverse(first);

            //relink
            head.next = last;
            first.next = tail;

            //prepare for next round:
            head = first;
            first = tail;
            last = tail;

        }

        return dummy.next;
    }

    private void reverse(ListNode head){
        ListNode pre = null, curt = head;
        while (curt != null){
            ListNode next = curt.next;
            curt.next = pre;
            pre = curt;
            curt = next;
        }
    }
}
