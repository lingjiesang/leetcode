package linkedlist;

import java.util.*;

public class PlusOneLinkedList {

    public ListNode plusOne(ListNode head) {
        if (head == null) {
            return head;
        }
        if (add(head) == 1){
            ListNode newHead = new ListNode(1);
            newHead.next = head;
            return newHead;
        } else{
            return head;
        }
    }

    private int add(ListNode node){
        if (node == null){
            return 1;
        }
        int carrier = add(node.next);

        node.val += carrier;
        if (node.val == 10){
            node.val = 0;
            return 1;
        } else{
            return 0;
        }
    }

}
