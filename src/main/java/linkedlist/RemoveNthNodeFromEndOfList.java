package linkedlist;

import java.util.*;

public class RemoveNthNodeFromEndOfList {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        ListNode fast = dummy;
        for (int i = 1; i <= n; i++){
            if (fast == null){
                return head;
            }
            fast = fast.next;
        }
        ListNode slow = dummy;
        while (fast.next != null){
            fast = fast.next;
            slow = slow.next;
        }
        if (slow.next != null){
            slow.next = slow.next.next;
        }
        return dummy.next;

    }
}
