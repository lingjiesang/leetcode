package linkedlist;

import java.util.*;

public class SwapNodeInPairs {
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null){
            return head;
        }
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        ListNode prev = dummy;
        ListNode curt = head;
        ListNode next = head.next;

        while (curt != null && next != null){
            //prev -> curt -> next   ==>  prev -> next -> curt
            ListNode nextnext = next.next;
            prev.next = next;
            next.next = curt;
            curt.next = nextnext;

            prev = curt;
            curt = nextnext;
            if (curt != null){
                next = curt.next;
            }
        }

        return dummy.next;
    }
}
