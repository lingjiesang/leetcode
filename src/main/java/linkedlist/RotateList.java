package linkedlist;

import java.util.*;

public class RotateList {
    public ListNode rotateRight(ListNode head, int k) {
        if (head == null){
            return head;
        }
        ListNode curt = head;
        int length = 1;
        while (curt.next != null){
            curt = curt.next;
            length++;
        }
        curt.next = head;
        k = length - k % length;

        curt = head;
        for (int i = 1; i < k; i++){
            curt = curt.next;
        }
        head = curt.next;
        curt.next = null;
        return head;

    }
}
