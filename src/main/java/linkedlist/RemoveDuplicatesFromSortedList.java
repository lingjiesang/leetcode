package linkedlist;

import java.util.*;


public class RemoveDuplicatesFromSortedList {
    /**
     * I
     */
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null){
            return head;
        }
        ListNode pre  = head;
        ListNode curt = head.next;

        while (curt != null){
            if (curt.val == pre.val){
                pre.next = curt.next;
            } else{
                pre = pre.next;
            }
            curt = curt.next;

        }
        return head;
    }

    /**
     * II
     */
    public ListNode deleteDuplicates_II(ListNode head) {
        if (head == null){
            return head;
        }
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        ListNode pre = dummy;
        ListNode tail = head;
        while (head != null){
            while (tail.next != null && tail.next.val == head.val){
                tail = tail.next;
            }
            if (head != tail){
                pre.next = tail.next;
            } else{
                pre = head;
            }
            head = pre.next;
            tail = pre.next;
        }
        return dummy.next;

    }
}
