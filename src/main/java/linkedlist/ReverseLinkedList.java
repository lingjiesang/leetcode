package linkedlist;

import java.util.*;

public class ReverseLinkedList {


    /**
     * I
     */

    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null){
            return head;
        }
        ListNode next = head.next;
        head.next = null;
        ListNode reversed = reverseList(next);
        next.next = head;
        return reversed;
    }

    public ListNode reverseList_iterative(ListNode head) {
        ListNode prev = null;
        while (head != null){
            ListNode next = head.next;
            head.next = prev;

            prev = head;
            head = next;
        }
        return prev;
    }


        /**
         * II
         */
    public ListNode reverseBetween(ListNode head, int m, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        ListNode beforeM = dummy;

        for (int i = 1; i < m; i++){
            beforeM = beforeM.next;
        }
        ListNode mNode = beforeM.next;
        ListNode nNode = mNode;
        for (int i = m; i < n; i++){
            nNode = nNode.next;
        }
        ListNode afterN = nNode.next;

        beforeM.next = null;
        nNode.next = null;

        reverse(mNode);

        beforeM.next = nNode;
        mNode.next = afterN;
        return dummy.next;
    }

    private void reverse(ListNode node){
        ListNode prev = null;
        while (node != null){
            ListNode next = node.next;
            node.next = prev;
            prev = node;
            node = next;
        }
    }
}
