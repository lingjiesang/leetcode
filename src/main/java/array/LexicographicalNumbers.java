package array;

import java.util.*;

public class LexicographicalNumbers {
    /**
     * my solution
     */
    public List<Integer> lexicalOrder(int n){
        List<Integer> result = new ArrayList<>();
        Deque<Integer> stack = new ArrayDeque<>();

        result.add(1);
        stack.push(1);
        while (stack.peek() * 10 <= n){
            stack.push(stack.peek() * 10);
            result.add(stack.peek());
        }
        while (!stack.isEmpty()){
            if (stack.peek() % 10 < 9 && stack.peek() < n){
                stack.push(stack.pop() + 1);
                result.add(stack.peek());
            } else{
                stack.pop();
                while (!stack.isEmpty() && stack.peek() % 10 == 9) {
                    stack.pop();
                }
                if (!stack.isEmpty() && stack.peek() < n){ // stack.peek() < n is actually unnecessary
                    stack.push(stack.pop() + 1);
                    result.add(stack.peek());

                    while (stack.peek() * 10 <= n){
                        stack.push(stack.peek() * 10);
                        result.add(stack.peek());
                    }
                }

            }
        }
        return result;
    }

    /**
     * online solution: similar idea, more element way to write
     * ref: https://discuss.leetcode.com/topic/55184/java-o-n-time-o-1-space-iterative-solution-130ms
     */
    public List<Integer> lexicalOrder_Cleaner(int n) {
        List<Integer> list = new ArrayList<>(n);
        int curt = 1;
        for (int i = 1; i <= n; i++) {
            list.add(curt);
            if (curt * 10 <= n) {
                curt *= 10;
            } else if (curt % 10 != 9 && curt + 1 <= n) {
                curt++;
            } else {
                curt /= 10;
                while (curt % 10 == 9) {
                    curt /= 10;
                }
                curt++;
            }
        }
        return list;
    }

    public static void main(String[] args){
        LexicographicalNumbers soln = new LexicographicalNumbers();
        soln.lexicalOrder(25);
    }
}
