package array;

public class JumpGame {
    /**
     * 55. I
     */
    public boolean canJump(int[] nums) {
        int max = 0;
        for (int i = 0; i < nums.length - 1; i++){
            if (i > max || max >= nums.length - 1){
                break;
            }
            max = Math.max(i + nums[i], max);
        }
        return max >= nums.length - 1;
    }

    /**
     * 45. II
     */
    public int jump(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int count = 0;
        int max = 0, nextMax = nums[0];

        for (int i = 0; i <= max && i < nums.length - 1; i++) {
            nextMax = Math.max(nextMax, i + nums[i]);
            if (i == max) {
                count++;
                max = nextMax;
                if (max >= nums.length - 1) {
                    break;
                }
            }
        }

        return max >= nums.length - 1 ? count : -1;

    }

}
