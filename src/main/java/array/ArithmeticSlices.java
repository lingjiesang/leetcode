package array;

import java.util.*;

public class ArithmeticSlices {
    public int numberOfArithmeticSlices(int[] A) {
        int result = 0;

        for (int start = 0; start < A.length - 2; ){
            int end = start + 1;
            int diff = A[end] - A[start];
            while (end + 1 < A.length && A[end + 1] - A[end] == diff){
                end++;
            }
            result += count(end - start + 1);
            start = end;
        }
        return result;
    }

    private int count(int n){
        return (n - 2) * (n - 1) / 2;
    }
}
