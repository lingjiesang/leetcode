package array;

import java.util.*;

public class MajorityElementII {
    public List<Integer> majorityElement(int[] nums) {
        /**
         * initiate majority1 and majority2 with any value, BUT
         * don't initiate majority2 with the same value as majority1, so they won't end up with the same value!
         */
        int majority1 = 0, count1 = 0;
        int majority2 = 1, count2 = 0;
        for (int num : nums){
            if (num == majority1){
                count1++;
            } else if (num == majority2){
                count2++;
            } else if (count1 == 0){
                majority1 = num;
                count1++;
            } else if (count2 == 0){
                majority2 = num;
                count2++;
            } else{
                count1--;
                count2--;
            }
        }

        List<Integer> result = new ArrayList<>();
        if (count(nums, majority1) > nums.length / 3){
            result.add(majority1);
        }
        if (count(nums, majority2) > nums.length / 3){
            result.add(majority2);
        }
        return result;
    }

    private int count(int[] nums, int value){
        int count = 0;
        for (int num : nums){
            if (num == value){
                count++;
            }
        }
        return count;

    }
}
