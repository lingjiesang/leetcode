package array;

import java.util.*;

public class MaximumSubarray {
    /**
     * O(n)
     */
    public int maxSubArray(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int maxNum = nums[0];
        for (int num : nums) {
            maxNum = Math.max(maxNum, num);
        }
        if (maxNum <= 0) {
            return maxNum;
        }

        int sum = 0;
        int maxSum = 0;
        for (int num : nums) {
            sum += num;
            maxSum = Math.max(maxSum, sum);
            if (sum < 0) {
                sum = 0;
            }
        }
        return maxSum;
    }


    /**
     * O(nlogn)
     */
    public int maxSubArray_DivideConquer(int[] nums) {
        int[] sums = new int[nums.length + 1];
        for (int i = 1; i <= nums.length; i++){
            sums[i] = sums[i - 1] + nums[i - 1];
        }
        return maxSum(sums, nums, 0, sums.length - 1).maxRangeSum;
    }

    class Result{
        int minSum;
        int maxSum;
        int maxRangeSum;
        Result(int minSum, int maxSum, int maxRangeSum){
            this.minSum = minSum;
            this.maxSum = maxSum;
            this.maxRangeSum = maxRangeSum;
        }
    }

    private Result maxSum(int[] sums, int[] nums, int begin, int end){
        if (begin == end){
            return new Result(sums[begin], sums[begin], begin > 0 ? nums[begin - 1]: Integer.MIN_VALUE);
        }
        int mid = begin  + (end - begin) / 2;

        Result leftRes = maxSum(sums, nums, begin, mid);
        Result rightRes = maxSum(sums, nums, mid + 1, end);

        int maxRangeSum = Math.max(leftRes.maxRangeSum, rightRes.maxRangeSum);
        maxRangeSum = Math.max(maxRangeSum, rightRes.maxSum - leftRes.minSum);

        int minSum = Math.min(leftRes.minSum, rightRes.minSum);
        int maxSum = Math.max(leftRes.maxSum, rightRes.maxSum);
        return new Result(minSum, maxSum, maxRangeSum);
    }

    /**
     * another way to write divide and conquer
     * similar to "CountRangeSum_DivideConquer.java"
     */
    public int maxSubArray_DivideConquer2(int[] nums) {
        int[] sums = new int[nums.length + 1];
        for (int i = 1; i <= nums.length; i++){
            sums[i] = sums[i - 1] + nums[i - 1];
        }
        return maxSumSubArray(sums, nums, 0, sums.length - 1);
    }

    private int maxSumSubArray(int[] sums, int[] nums, int begin, int end){
        if (begin == end){
            return begin > 0 ? nums[begin - 1]: Integer.MIN_VALUE;
        }

        int mid = begin + (end - begin) / 2;
        int result = Math.max(maxSumSubArray(sums, nums, begin, mid), maxSumSubArray(sums, nums, mid + 1, end));
        result = Math.max(result, sums[end] - sums[begin]);
        inplace_merge(sums, begin, mid, end);
        return result;
    }

    private void inplace_merge(int[] nums, int begin, int mid, int end){
        int[] res = new int[end - begin + 1];
        int head1 = begin, head2 = mid + 1, head = 0;
        while (head1 <= mid && head2 <= end){
            if (nums[head1] < nums[head2]){
                res[head++] = nums[head1++];
            } else{
                res[head++] = nums[head2++];
            }
        }
        while (head1 <= mid){
            res[head++] = nums[head1++];
        }
        while (head2 <= end){
            res[head++] = nums[head2++];
        }

        for (int i = begin; i <= end; i++){
            nums[i] = res[i - begin];
        }
    }
}
