package array;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/17446/6-suggested-solutions-in-c-with-explanations/2
 */

public class MajorityElement {
    /**
     * Moore voting (2ms)
     */
    public int majorityElement_Voting(int[] nums) {
        int count = 0;
        int majority = nums[0];
        for (int num : nums) {
            if (num == majority) {
                count++;
            } else {
                count--;
                if (count == 0) {
                    majority = num;
                    count = 1;
                }
            }
        }
        return majority;

    }

    /**
     * Divide and Conquer (25ms)
     */
    public int majorityElement_DC(int[] nums) {
        return majorityElement(nums, 0, nums.length - 1);
    }

    private int majorityElement(int[] nums, int st, int ed) {
        if (st == ed) {
            return nums[st];
        }
        int mid = st + (ed - st) / 2;
        int majorityLeft = majorityElement(nums, st, mid);
        int majorityRight = majorityElement(nums, mid + 1, ed);
        if (majorityLeft == majorityRight) {
            return majorityLeft;
        } else {
            if (count(nums, majorityLeft) > count(nums, majorityRight)) {
                return majorityLeft;
            } else {
                return majorityRight;
            }
        }
    }

    private int count(int[] nums, int value) {
        int count = 0;
        for (int num : nums) {
            if (num == value) {
                count++;
            }
        }
        return count;

    }

    /**
     * median (7ms)
     */
    public int majorityElement_Median(int[] nums) {
        //nums.length / 2 + 1 : +1 because Kth is 1-based
        return Kth(nums, 0, nums.length - 1, nums.length / 2 + 1);
    }

    private int median(int[] nums, int st, int ed) {
        int[] temp = new int[ed - st + 1];
        for (int i = st; i <= ed; i++) {
            temp[i - st] = nums[i];
        }
        Arrays.sort(temp);
        return temp[(ed - st + 1) / 2];
    }

    private void swap(int[] nums, int i, int j) {
        if (i != j) {  // check i != j this is necessary here!!!
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }

    private int Kth(int[] nums, int st, int ed, int k) {
        int pivot = nums[ed];
        if (ed > st) {
            int[] nums_sub = new int[(ed - st) / 5 + 1];
            for (int i = st; i <= ed; i += 5) {
                nums_sub[(i - st) / 5] = median(nums, i, Math.min(i + 4, ed));
            }
            pivot = Kth(nums_sub, 0, nums_sub.length - 1, (nums_sub.length + 1) / 2);
        }

        int less = st - 1;
        int more = ed + 1;
        int curr = st;
        while (curr < more) {
            if (nums[curr] < pivot) {
                swap(nums, ++less, curr);
                curr++;
            } else if (nums[curr] > pivot) {
                swap(nums, --more, curr);
            } else {
                curr++;
            }
        }

        if (less - st + 1 >= k) {
            return Kth(nums, st, less, k);
        } else if ((more - 1) - st + 1 >= k) {
            return nums[more - 1];
        } else {
            return Kth(nums, more, ed, k - (more - st));
        }
    }

    /**
     * bit manipulation (11ms)
     */

    public int majorityElement(int[] nums) {
        int mask = 1;
        int majority = 0;

        for (int i = 0; i < 32; i++){
            int count = 0;
            for (int num : nums){
                if ((num & mask) != 0){
                    count++;
                }
            }
            if (count > nums.length / 2){
                majority |= mask;
            }
            mask <<= 1;
        }
        return majority;
    }
}
