package array;

import java.util.*;

public class RotateArray {


    /**
     * method 1: rotate (2ms)
     * ref: https://discuss.leetcode.com/topic/11349/my-three-way-to-solve-this-problem-the-first-way-is-interesting-java
     */
    public void rotate_method1(int[] nums, int k) {
        if(nums.length <= 1){
            return;
        }
        //step each time to move
        int n = nums.length;
        k %=  n;
        //find GCD between nums length and step
        int gcd = findGcd(n, k);
        int position, count;

        //gcd path to finish move
        for(int i = 0; i < gcd; i++){
            //beginning position of each path
            position = i;
            //count is the number we need swap each path
            count = n / gcd - 1;
            for(int j = 0; j < count; j++){
                position = (position + k) % nums.length;
                //swap index value in index i and position
                nums[i] ^= nums[position];
                nums[position] ^= nums[i];
                nums[i] ^= nums[position];
            }
        }
    }

    // still method1, add print for understanding:
    public void rotate(int[] nums, int k) {
        if(nums.length <= 1){
            return;
        }
        //step each time to move
        int n = nums.length;
        k = k % n;
        //find GCD between nums length and step
        int gcd = findGcd(n, k);
        int position, count;

        //gcd path to finish move
        for(int i = 0; i < gcd; i++){
            //beginning position of each path
            position = i;
            //count is the number we need swap each path
            count = n / gcd - 1;
            System.out.println(" i = " + i);
            for(int j = 0; j < count; j++){

                position = (position + k) % n;
                System.out.println("    j = " + j + " position = " + position);

                //swap index value in index i and position
                nums[i] ^= nums[position];
                nums[position] ^= nums[i];
                nums[i] ^= nums[position];
                System.out.println("   " + Arrays.toString(nums) + "\n");
            }
        }
    }

    /**

     (1) Your input

         [1,2,3,4,5,6,7]
         3

     Your stdout

         i = 0
             j = 0 position = 3
             [4, 2, 3, 1, 5, 6, 7]

             j = 1 position = 6
             [7, 2, 3, 1, 5, 6, 4]

             j = 2 position = 2
             [3, 2, 7, 1, 5, 6, 4]

             j = 3 position = 5
             [6, 2, 7, 1, 5, 3, 4]

             j = 4 position = 1
             [2, 6, 7, 1, 5, 3, 4]

             j = 5 position = 4
             [5, 6, 7, 1, 2, 3, 4]

     (2) Your input

         [1,2,3,4,5,6,7, 8]
         2

     Your stdout

         i = 0  (1, 3, 5, 7 rotate by 2)
             j = 0 position = 2
             [3, 2, 1, 4, 5, 6, 7, 8]

             j = 1 position = 4
             [5, 2, 1, 4, 3, 6, 7, 8]

             j = 2 position = 6
             [7, 2, 1, 4, 3, 6, 5, 8]

         i = 1 (2, 4, 6, 8 rotate by 2)
             j = 0 position = 3
             [7, 4, 1, 2, 3, 6, 5, 8]

             j = 1 position = 5
             [7, 6, 1, 2, 3, 4, 5, 8]

             j = 2 position = 7
             [7, 8, 1, 2, 3, 4, 5, 6]

     (3) Your input

         [1,2,3,4,5,6,7, 8]
         6

     Your stdout

         i = 0  (1, 3, 5, 7 rotate by 6)
             j = 0 position = 6
             [7, 2, 3, 4, 5, 6, 1, 8]

             j = 1 position = 4
             [5, 2, 3, 4, 7, 6, 1, 8]

             j = 2 position = 2
             [3, 2, 5, 4, 7, 6, 1, 8]

         i = 1 (2, 4, 6, 8 rotate by 6)
             j = 0 position = 7
             [3, 8, 5, 4, 7, 6, 1, 2]

             j = 1 position = 5
             [3, 6, 5, 4, 7, 8, 1, 2]

             j = 2 position = 3
             [3, 4, 5, 6, 7, 8, 1, 2]

     */

    public int findGcd(int a, int b){
        return (a == 0 || b == 0) ? a + b : findGcd(b, a % b);
    }

    /**
     * method 2: 3-step reverse
     * O(1) space, O(n) time (2ms)
     */
    public void rotate_method2(int[] nums, int k){
        if (nums == null || nums.length == 0){
            return;
        }
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);
    }

    private void reverse(int[] nums, int start, int end){
        while (start < end){
            nums[start] ^= nums[end];
            nums[end] ^= nums[start];
            nums[start] ^= nums[end];
            start++;
            end--;
        }
    }

    /**
     * method 3: extra space, trivial (1ms)
     */
    public void rotate_method3(int[] nums, int k){
        if (nums == null || nums.length == 0){
            return;
        }
        k %= nums.length;
        int n = nums.length;
        int[] temp = new int[k];
        for (int i = 0; i < k; i++){
            temp[i] = nums[i + n - k];
        }

        for (int i = nums.length - 1; i >= k; i--){
            nums[i] = nums[i - k];
        }

        for (int i = 0; i < k; i++){
            nums[i] = temp[i];
        }
    }
}
