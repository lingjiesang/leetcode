package array;

import java.util.*;

/**
 * my own implementation on 8/3/16.
 *
 * this solution is ~10% and 10 times slower than the fast ones
 * largely because of the hashmap and copy of arrays
 * but it demonstrate the idea of "sorting/merging" during "divide and conquer"
 */

public class CountSmallerNumbersAfterSelf_DivideConquerOld {
    public List<Integer> countSmaller(int[] nums){

        if (nums.length == 0){
            return new LinkedList<>();
        }
        List<Integer> res = new ArrayList<>();
        Map<Integer, Integer> newPosToOriPos = new HashMap<>();

        for (int i = 0; i < nums.length; i++){
            res.add(0);
            newPosToOriPos.put(i, i);
        }
        _countSmaller(nums, 0, nums.length - 1, res, newPosToOriPos);
        return res;

    }

    private void _countSmaller(int[] nums, int start, int end, List<Integer> res, Map<Integer, Integer> newPosToOriPos){
        if (start == end){
            return;
        }
        int mid = start + (end - start) / 2;
        _countSmaller(nums, start, mid, res, newPosToOriPos);
        _countSmaller(nums, mid + 1, end, res, newPosToOriPos);

        int[] temp = new int[end - start + 1];
        int leftHead = mid;
        int rightHead = end;
        int tempHead = end - start;

        int[][] posUpdate = new int[end - start + 1][2];

        while (leftHead >= start && rightHead  >= mid + 1){
            if (nums[leftHead] > nums[rightHead]){
                posUpdate[tempHead][0] = tempHead + start;
                posUpdate[tempHead][1] = newPosToOriPos.get(rightHead);
                temp[tempHead--] = nums[rightHead--];
            } else {
                /**
                 * key 1: if nums[leftHead] == nums[rightHead], deal with leftHead first!
                 */
                int oriPos = newPosToOriPos.get(leftHead);
                res.set(oriPos, res.get(oriPos) + end - rightHead);
                posUpdate[tempHead][0] = tempHead + start;
                posUpdate[tempHead][1] = newPosToOriPos.get(leftHead);
                temp[tempHead--] = nums[leftHead--];
            }
        }

        while (leftHead >= start){
            int oriPos = newPosToOriPos.get(leftHead);
            res.set(oriPos, res.get(oriPos) + end - rightHead);
            posUpdate[tempHead][0] = tempHead + start;
            posUpdate[tempHead][1] = newPosToOriPos.get(leftHead);
            temp[tempHead--] = nums[leftHead--];
        }

        while (rightHead >= mid + 1){
            posUpdate[tempHead][0] = tempHead + start;
            posUpdate[tempHead][1] = newPosToOriPos.get(rightHead);
            temp[tempHead--] = nums[rightHead--];
        }

        for (int i = start; i <= end; i++){
            nums[i] = temp[i - start];
            /**
             * key 2: why we update newPosToOriPos after finishing merging
             * if we update hashmap before finishing merging, we can overwrite some results we still need
             */
            newPosToOriPos.put(posUpdate[i - start][0], posUpdate[i - start][1]);
        }

    }

    public static void main(String[] args){
        int[] nums = {0, 1, 2};
        CountSmallerNumbersAfterSelf_DivideConquerOld soln = new CountSmallerNumbersAfterSelf_DivideConquerOld();
        soln.countSmaller(nums);
    }
}
