package array;

import java.util.*;

public class GasStation {
    public int canCompleteCircuit(int[] gas, int[] cost) {
        if (gas.length == 0){
            return -1;
        }
        int start = gas.length - 1;
        int end = 0;
        int remainGas = gas[start] - cost[start];
        while (start != end){
            if (remainGas >= 0){
                remainGas += gas[end] - cost[end]; // ++还是分开写比较好, 顺序也不太会错!! 这里如果++写在右边,其实++了两次!
                end++;
            } else{
                start--;
                remainGas += gas[start] - cost[start];
            }
        }
        if (remainGas >= 0) {
            return start;
        } else{
            return -1;
        }
    }
}
