package array;

import java.util.*;

public class IncreasingTripletSubsequence {
    public boolean increasingTriplet(int[] nums) {
        int[] triplet = new int[3];
        Arrays.fill(triplet, Integer.MAX_VALUE);
        for (int num : nums){
            triplet[0] = Math.min(num, triplet[0]);
            if (num > triplet[0]){
                triplet[1] = Math.min(num, triplet[1]);
            }
            if (num > triplet[1]){
                return true;
            }
        }
        return false;

    }
}
