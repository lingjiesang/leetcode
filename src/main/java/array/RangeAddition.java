package array;

import java.util.*;

public class RangeAddition {
    public int[] getModifiedArray(int length, int[][] updates) {
        int[] result = new int[length];
        for (int[] update : updates){
            int startInd = update[0];
            int endInd = update[1];
            int change = update[2];

            result[startInd] += change;
            if (endInd + 1 < result.length) {
                result[endInd + 1] -= change;
            }
        }

        for (int i = 1; i < result.length; i++){
            result[i] += result[i - 1];
        }
        return result;
    }
}
