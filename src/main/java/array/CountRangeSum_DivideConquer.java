package array;


import java.util.Arrays;

public class CountRangeSum_DivideConquer {

    public int countRangeSum(int[] nums, int lower, int upper) {
        if (nums.length == 0){
            return 0;
        }

        long[] sums = new long[nums.length + 1];
        for (int i = 1; i <= nums.length; i++){
            sums[i] = sums[i - 1] + nums[i - 1];
        }
        return countAndSort(sums, lower, upper, 1, sums.length - 1); // sums[begin] == sums[begin] - sums[0]
                                                                    // because of *, never need to consider sums[begin] - sums[0]
    }


    private int countAndSort(long[] sums, int lower, int upper, int begin, int end){
        if (begin == end){ // * consider sums[begin] here
            return sums[begin] >= lower && sums[begin] <= upper ? 1 : 0;
        }

        int mid = begin + (end - begin) / 2;

        int count = countAndSort(sums, lower, upper, begin, mid) + countAndSort(sums, lower, upper, mid + 1, end);

        int j = mid + 1;
        int k = mid + 1;
        for (int i = begin; i <= mid; i++){
            while (j <= end && sums[j] < sums[i] + lower){
                j++;
            }
            while (k <= end && sums[k] <= sums[i] + upper){
                k++;
            }
            count += k - j;
        }

        inplace_merge(sums, begin, mid, end);
        return count;
    }

    private void inplace_merge(long[] nums, int begin, int mid, int end){
        long[] res = new long[end - begin + 1];
        int head1 = begin, head2 = mid + 1, head = 0;
        while (head1 <= mid && head2 <= end){
            if (nums[head1] < nums[head2]){
                res[head++] = nums[head1++];
            } else{
                res[head++] = nums[head2++];
            }
        }
        while (head1 <= mid){
            res[head++] = nums[head1++];
        }
        while (head2 <= end){
            res[head++] = nums[head2++];
        }

        for (int i = begin; i <= end; i++){
            nums[i] = res[i - begin];
        }
    }

    public static void main(String[] args){
        int[] nums = {-2, 5, -1};
        CountRangeSum_DivideConquer soln = new CountRangeSum_DivideConquer();
        int n = soln.countRangeSum(nums, -2, 2);
        System.out.println(n);
    }
}