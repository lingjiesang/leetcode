package array;

import java.util.*;

public class NumArray {
    int[] st; // segment tree
    int[] nums;
    /**
     * ss: start of segment, se: end of segment, pos: pos of "root" in st
     * return: value of "root" of current segment ree
     */
    private int constSegTree(int[] nums, int ss, int se, int pos){
        if (ss == se){
            st[pos] = nums[ss];
            return st[pos];
        }
        int mid = ss + (se - ss) / 2;
        st[pos] = constSegTree(nums, ss, mid, 2 * pos + 1)
                + constSegTree(nums, mid + 1, se, 2 * pos + 2);
        return st[pos];

    }

    private void updateSegTree(int i, int val, int ss, int se, int pos){
        if (i < ss || i > se){
            return;
        }
        st[pos] = st[pos] - nums[i] + val;
        if (ss == se){
            return;
        }
        int mid = ss + (se - ss) / 2;
        updateSegTree(i, val, ss, mid, pos * 2 + 1);
        updateSegTree(i, val, mid + 1, se, pos * 2 + 2);

    }

    public NumArray(int[] nums) {
        if (nums.length == 0){
            return;
        }
        int n = (int)(Math.ceil(Math.log(nums.length) / Math.log(2)));
        int max_size = 2 * (int)(Math.pow(2, n)) - 1;
        st = new int[max_size];
        constSegTree(nums, 0, nums.length - 1, 0);
        this.nums = new int[nums.length];
        for (int i = 0; i < nums.length; i++){
            this.nums[i] = nums[i];
        }
    }

    void update(int i, int val) {
        if (i < 0 || i >= nums.length){
            return;
        }
        updateSegTree(i, val, 0, nums.length - 1, 0); //at the time, matrix[i] still old value
        nums[i] = val;
    }
    /**
     * rs: range start, re: range end
     */
    private int sumSegTree(int rs, int re, int ss, int se, int pos){
        if (re < ss || se < rs){
            return 0;
        }
        if (rs <= ss && se <= re){
            return st[pos];
        } else{
            int mid = ss + (se - ss) / 2;
            return sumSegTree(rs, re, ss, mid, pos * 2 + 1) + sumSegTree(rs, re, mid + 1, se, pos * 2 + 2);
        }
    }


    public int sumRange(int i, int j) {
        if (i < 0 || j >= nums.length || j < i){
            return -1;
        }
        return sumSegTree(i, j, 0, nums.length - 1, 0);
    }
}


// Your NumArray object will be instantiated and called as such:
// NumArray numArray = new NumArray(matrix);
// numArray.sumRange(0, 1);
// numArray.update(1, 10);
// numArray.sumRange(1, 2);