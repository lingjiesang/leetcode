package array;

import java.util.*;

public class SummaryRanges {

    public List<String> summaryRanges(int[] nums) {
        List<String> result = new ArrayList<>();
        if (nums == null || nums.length == 0) {
            return result;
        }
        int n = nums.length;
        int rangeStart = nums[0];
        int rangeEnd = nums[0];

        for (int i = 1; i < n; i++) {
            if (nums[i] == rangeEnd + 1) {
                rangeEnd = nums[i];
            } else {
                result.add(genRange(rangeStart, rangeEnd));
                rangeStart = nums[i];
                rangeEnd = nums[i];
            }
        }
        result.add(genRange(rangeStart, rangeEnd));
        return result;
    }

    private String genRange(int rangeStart, int rangeEnd) {
        if (rangeStart == rangeEnd) {
            return Integer.toString(rangeStart);
        } else {
            return rangeStart + "->" + rangeEnd;
        }
    }
}
