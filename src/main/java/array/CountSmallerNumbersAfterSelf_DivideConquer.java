package array;

import java.util.*;

public class CountSmallerNumbersAfterSelf_DivideConquer {
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new ArrayList<>();

        if (nums.length == 0){
            return result;
        }
        int[] index = new int[nums.length];
        for (int i = 0; i < index.length; i++){
            index[i] = i;
        }
        int[] count = new int[nums.length];
        merge(nums, index, count, 0, nums.length - 1);
        for (int c : count){
            result.add(c);
        }
        return result;
    }

    private void merge(int[] nums, int[] index, int[] count, int st, int ed){
        if (st == ed){
            return;
        }

        int mid = st + (ed - st) / 2;
        merge(nums, index, count, st, mid);
        merge(nums, index, count, mid + 1, ed);

        int[] nums_sorted = new int[ed - st + 1];
        int[] index_sorted= new int[ed - st + 1];

        int smallerAfter = 0;
        int right = ed;
        int left = mid;
        int pos = ed - st;
        while (right >= mid + 1 && left >= st){
            if (nums[right] < nums[left]){
                nums_sorted[pos] = nums[right];
                index_sorted[pos] = index[right];
                smallerAfter++;
                right--;
                pos--;
            } else{
                nums_sorted[pos] = nums[left];
                index_sorted[pos]= index[left];
                count[index[left]] += smallerAfter;
                left--;
                pos--;
            }
        }

        while (right >= mid + 1){
            nums_sorted[pos] = nums[right];
            index_sorted[pos] = index[right];
            smallerAfter++;
            right--;
            pos--;
        }

        while (left >= st){
            nums_sorted[pos] = nums[left];
            index_sorted[pos]= index[left];
            count[index[left]] += smallerAfter;
            left--;
            pos--;
        }

        for (int i = st; i <= ed; i++){
            nums[i] = nums_sorted[i - st];
            index[i] = index_sorted[i - st];
        }

    }
}
