package array;

import java.util.*;

public class GrayCode {
    public List<Integer> grayCode(int n) {
        List<Integer> result = new ArrayList<>();
        result.add(0);
        for (int i = 1; i <= n; i++){
            int firstBit = 1 << (i - 1);
            for (int j = result.size() - 1; j >= 0; j--){
                result.add(firstBit + result.get(j));
            }
        }
        return result;
    }
}
