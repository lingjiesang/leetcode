package array;

import java.util.*;

/**
 * F(i) - F(i - 1) = sum - n * A[n - i], i = 1, 2, ..., n - 1
 */

public class RotateFunction {
    public int maxRotateFunction(int[] A) {
        int n = A.length;
        int Fi = 0;
        int sum = 0;
        for (int i = 0; i < A.length; i++){
            Fi += i * A[i];
            sum += A[i];
        }
        int max = Fi;
        for (int i = 1; i < A.length; i++){
            Fi = sum + Fi - n * A[n - i];
            max = Math.max(max, Fi);
        }
        return max;
    }


}
