package array;

import java.util.*;
public class SlidingWindowMaximum {
    public int[] maxSlidingWindow(int[] nums, int k){

        if (k == 0){
            return new int[0];
        }

        if (k > nums.length){
            return new int[]{Arrays.stream(nums).max().getAsInt()};
        }

        int[] result = new int[nums.length - k + 1];
        Deque<Integer> deque = new ArrayDeque<>();
        for (int i = 0; i < k - 1; i++){
            while (!deque.isEmpty() && nums[i] > deque.getLast()){
                deque.pollLast();
            }
            deque.addLast(nums[i]);
        }

        for (int i = k - 1; i < nums.length; i++){

            while (!deque.isEmpty() && nums[i] > deque.getLast()){
                deque.pollLast();
            }
            deque.addLast(nums[i]);

            result[i - k + 1] = deque.getFirst();

            //remove the first one if necessary (prepare for the next window)
            if (deque.getFirst() == nums[i - k + 1]){
                deque.removeFirst();
            }
        }
        return result;

    }
}
