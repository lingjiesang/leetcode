package array;

import java.util.*;

public class TwoSum {
    /**
     * 1. Two Sum
     */
    public int[] twoSum(int[] nums, int target) {
        int[] result = new int[2];
        Map<Integer, Integer> myMap = new HashMap<>();

        for (int i = 0; i < nums.length; i++){
            if (myMap.containsKey(nums[i])){
                result[0] = myMap.get(nums[i]);
                result[1] = i;
                break;
            }
            myMap.put(target - nums[i], i);
        }
        return result;

    }

    /**
     * 167. Tw Sum II
     */
    public int[] twoSum_II(int[] numbers, int target) {
        int[] result = new int[2];
        int left = 0, right = numbers.length - 1;
        while (left < right){
            if (numbers[left] + numbers[right] < target){
                left++;
            } else if (numbers[left] + numbers[right] > target){
                right--;
            } else{
                result[0] = left + 1;
                result[1] = right + 1;
                return result;
            }
        }
        return result;
    }
}
