package array;

import java.util.*;

public class MovingZeroes {
    public void moveZeroes(int[] nums) {
        if (nums.length == 0){
            return;
        }
        int lastNonZero = -1;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != 0){
                nums[++lastNonZero] = nums[i];
            }
        }
        for (int i = lastNonZero + 1; i < nums.length; i++){
            nums[i] = 0;
        }
    }
}
