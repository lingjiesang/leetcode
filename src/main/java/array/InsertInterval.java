package array;

import java.util.*;
public class InsertInterval {
    public class Interval {
        int start;
        int end;
        Interval() {
            start = 0;
            end = 0;
        }
        Interval(int s, int e){
            start = s;
            end = e;
        }
    }

    public List<Interval> insert(List<Interval> intervals, Interval newInterval) {
        List<Interval> result = new ArrayList<>();
        if (intervals.size() == 0){
            result.add(newInterval);
            return result;
        }
        if (newInterval == null){
            return intervals;
        }

        // suppose newInterval overlap with [intervals(left + 1), ... intervals(right - 1)]
        //  [0, ...intervals(left)]  are before newinterval
        //  [intervals(right), end) are not overlap
        int left = -1;

        for (int i = 0; i < intervals.size(); i++){
            if (intervals.get(i).end < newInterval.start){
                left = i;
            } else {
                break;
            }
        }

        int right = intervals.size();
        for (int i = intervals.size() - 1; i >= 0; i--){
            if (intervals.get(i).start > newInterval.end){
                right = i;
            } else{
                break;
            }
        }

        for (int i = 0; i <= left; i++){
            result.add(intervals.get(i));
        }

        result.add(merge(intervals, left + 1, right - 1, newInterval));

        for (int i = right; i < intervals.size(); i++){
            result.add(intervals.get(i));
        }
        return result;
    }

    private Interval merge(List<Interval> intervals, int left, int right, Interval newInterval){
        if (left > right){
            return newInterval;
        }
        int start, end;
        if (left < intervals.size()){
            start = Math.min(intervals.get(left).start, newInterval.start);
        } else{
            start = newInterval.start;
        }

        if (right >= 0){
            end = Math.max(intervals.get(right).end, newInterval.end);
        } else{
            end = newInterval.end;
        }
        return new Interval(start, end);
    }

}
