package array;

import java.util.*;

public class LongestConsecutiveSequence {
    public int longestConsecutive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums){
            set.add(num);
        }

        int lcs = 0;
        for (int i = 0; i < nums.length; i++){
            if (set.contains(nums[i])){
                set.remove(nums[i]);
                int lower = nums[i] - 1;
                while (set.contains(lower)){
                    set.remove(lower);
                    lower--;
                }
                int higher = nums[i] + 1;
                while (set.contains(higher)){
                    set.remove(higher);
                    higher++;
                }
                lcs = Math.max(lcs, higher - lower - 1);
            }
        }
        return lcs;
    }
}
