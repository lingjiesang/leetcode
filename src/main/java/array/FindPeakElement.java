package array;

import java.util.*;

public class FindPeakElement {
    /**
     * 特殊的二分法
     * 因为比较的是周围的元素而不是直接比较start,mid,end
     * 在最后仅剩两个元素的时候,一般比较规律已不适用,所以需要在循环外解决
     * 尽管循环内用到了start + 1, 循环条件仍然是 start + 1 < end!!!
     */
    public int findPeakElement(int[] nums) {
        int start = 0, end = nums.length - 1;
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;
            if (nums[mid] > nums[mid - 1] && nums[mid] > nums[mid + 1]) {
                return mid;
            } else if (nums[mid] < nums[mid - 1]) {
                end = mid - 1;
            } else {
                start = mid + 1;
            }
        }
        if (nums[start] >= nums[end]) {
            return start;
        } else {
            return end;
        }
    }


    public int findPeakElement_linear(int[] nums) {
        for (int i = 1; i < nums.length; i++){
            if (nums[i] < nums[i - 1]){
                return i - 1;
            }
        }
        return nums.length - 1;
    }
}