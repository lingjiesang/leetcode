package array;

public class FindCelebrity {
    public int findCelebrity(int n) {
        int candidate = 0;
        for (int i = 1; i < n; i++) {
            if (knows(candidate, i))
                candidate = i;
        }
        for (int i = 0; i < n; i++) {
            if (i != candidate && (knows(candidate, i) || !knows(i, candidate))) {
                return -1;
            }
        }
        return candidate;
    }

    /**
     * somehow my method use similar idea as above (online method), but not work....not sure why
     */
    public int findCelebrity_notWork(int n) {
        int[] candidate = new int[n];
        for (int i = 0; i < n; i++) { // start with assumption everyone is celebrity
            candidate[i] = i;
        }
        int candidateLen = n;
        while (candidateLen > 1) {
            for (int i = 0; i < candidateLen / 2; i++) {
                if (knows(i, i + candidateLen / 2)) {
                    candidate[i] = i + candidateLen / 2;
                }
            }
            if (candidateLen % 2 != 0) {
                candidate[candidateLen / 2] = candidate[candidateLen - 1];
            }
            candidateLen = candidateLen - candidateLen / 2;
        }
        for (int i = 0; i < n; i++) {
            if (i != candidate[0] && !(knows(i, candidate[0]) && !knows(candidate[0], i))) {
                return -1;
            }
        }
        return candidate[0];
    }

    public boolean knows(int a, int b) {
        if (a == 0 && b == 1) {
            return false;
        }
        if (a == 1 && b == 0) {
            return false;
        }
        if (a == 2 && b == 0) {
            return false;
        }
        if (a == 2 && b == 1) {
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        FindCelebrity soln = new FindCelebrity();
        System.out.println(soln.findCelebrity_notWork(2));
        System.out.println(soln.findCelebrity(2));
    }
}