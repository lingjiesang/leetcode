package array;

import java.util.*;

public class ProductArrayExceptSelf {
    public int[] productExceptSelf(int[] nums) {
        if (nums.length == 0){
            return new int[0];
        }
        int n = nums.length;
        int[] products = new int[n];
        int left = 1;
        for (int i = 0; i < n; i++){
            products[i] = left;
            left *= nums[i];
        }

        int right = 1;
        for (int i = n - 1; i >= 0; i--){
            products[i] *= right;
            right *= nums[i];
        }
        return products;

    }
}
