package array;

import java.util.*;

public class ContainDuplicates {
    /**
     * 217. I
     */
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums){
            if (!set.add(num)){
                return true;
            }
        }
        return false;
    }

    /**
     * 297. II
     */
    public boolean containsNearbyDuplicate(int[] nums, int k) {
        Set<Integer> set = new HashSet<>();

        for (int i = 0; i <= k && i < nums.length; i++){
            if (!set.add(nums[i])){
                return true;
            }
        }
        for (int i = k + 1; i < nums.length; i++){
            set.remove(nums[i - k - 1]);
            if (!set.add(nums[i])){
                return true;
            }
        }
        return false;
    }
}