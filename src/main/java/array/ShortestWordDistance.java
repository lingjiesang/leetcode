package array;

import java.util.*;

public class ShortestWordDistance {
    /**
     * 243. I
     */
    public int shortestDistance(String[] words, String word1, String word2) {
        int minDist = words.length;
        int lastPos = -1;
        for (int i = 0; i < words.length; i++){
            if (words[i].equals(word1) || words[i].equals(word2)){
                if (lastPos != -1 && !words[lastPos].equals(words[i])){
                    minDist = Math.min(minDist, i - lastPos);
                }
                lastPos = i;
            }
        }
        return minDist;
    }

    /**
     * 244. II
     */
    public class WordDistance {

        private Map<String, List<Integer>> numPositions;

        public WordDistance(String[] words) {
            numPositions = new HashMap<>();
            for (int i = 0; i < words.length; i++){
                if (!numPositions.containsKey(words[i])){
                    numPositions.put(words[i], new ArrayList<>());
                }
                numPositions.get(words[i]).add(i);
            }

        }

        public int shortest(String word1, String word2) {
            List<Integer> word1Poses = numPositions.get(word1);
            List<Integer> word2Poses = numPositions.get(word2);
            int i = 0, j = 0;
            int minDist = Integer.MAX_VALUE;
            while (i < word1Poses.size() && j < word2Poses.size()){
                if (word1Poses.get(i) < word2Poses.get(j)){
                    minDist = Math.min(minDist, word2Poses.get(j) - word1Poses.get(i));
                    i++;
                } else{
                    minDist = Math.min(minDist, word1Poses.get(i) - word2Poses.get(j));
                    j++;
                }
            }
            return minDist;
        }
    }


    public int shortestWordDistance_III(String[] words, String word1, String word2) {
        int minDist = words.length;
        int lastPos = -1;
        for (int i = 0; i < words.length; i++){
            if (words[i].equals(word1) || words[i].equals(word2)){
                if (lastPos != -1 && (word1.equals(word2) || !words[lastPos].equals(words[i]))){
                    minDist = Math.min(minDist, i - lastPos);
                }
                lastPos = i;
            }
        }
        return minDist;
    }
}
