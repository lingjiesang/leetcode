package array;

import java.util.*;

public class TopKFrequentElements {
    /**
     * method 1: using heap
     */
    public List<Integer> topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int num : nums){
            counts.put(num, counts.getOrDefault(num, 0) + 1);
        }

        PriorityQueue<Map.Entry<Integer, Integer>> pq = new PriorityQueue<>((e1, e2) -> (e2.getValue() - e1.getValue()));
        pq.addAll(counts.entrySet());
        List<Integer> result = new ArrayList<>();
        while (result.size() < k && !pq.isEmpty()){
            result.add(pq.poll().getKey());
        }
        return result;
    }

    /**
     * method 2: bucket
     */
    public List<Integer> topKFrequent_method2(int[] nums, int k) {
        HashMap<Integer, Integer> counts = new HashMap<>();
        int maxCount = 0;
        for (int num : nums){
            counts.put(num, counts.getOrDefault(num, 0) + 1);
            maxCount = Math.max(maxCount, counts.get(num));
        }

        List<List<Integer>> buckets = new ArrayList<>();
        for (int i = 0; i < maxCount; i++){
            buckets.add(new ArrayList<>());
        }

        for (int num : counts.keySet()){
            int count = counts.get(num);
            buckets.get(count - 1).add(num);
        }

        List<Integer> result = new ArrayList<>();
        for (int i = buckets.size() - 1; i >= 0 && result.size() < k; i--){
            for (int j = 0; j < buckets.get(i).size(); j++){
                result.add(buckets.get(i).get(j));
                if (result.size() == k){
                    return result;
                }
            }
        }
        return result;
    }



}
