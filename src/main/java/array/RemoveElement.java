package array;

import java.util.*;

public class RemoveElement {
    public int removeElement(int[] nums, int val) {
        int len = 0;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != val){
                nums[len++] = nums[i];
            }
        }
        return len;
    }


    public int removeElement_method2(int[] nums, int val) {
        int len = nums.length;
        for (int i = 0; i < len;){
            if (nums[i] == val){
                swap(nums, i, --len);
            } else{
                i++;
            }
        }
        return len;
    }
    private void swap(int[] nums, int i, int j){
        if (i != j){
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }
}
