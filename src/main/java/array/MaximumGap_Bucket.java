package array;

import java.util.*;

/**
 *  number of bucket >= number of elements in nums,
 *  so either at least one bucket is empty, or every bucket has only one element
 *
 *  i.e. difference among elements in one bucket won't contribute to the maximum gap
 *  (we don't need to order elements in one bucket, just record the maximum and minimum)
 *
 *  bucket start at min(nums) and end at max(nums)
 *
 *  minimal bucket size should be 1
 *
 *
 */
public class MaximumGap_Bucket {

    public int maximumGap(int[] nums){
        if (nums.length <= 1){
            return 0;
        }

        int min = nums[0], max = nums[0];
        for (int num : nums){
            min = Math.min(min, num);
            max = Math.max(max, num);
        }
        if (max == min){
            return 0;
        }

        int bucketSize = (max - min) / nums.length;
        /*
            bucketSize * nums.length <= max - min
            nums.length <= (max - min) / bucketSize
         */


        if (bucketSize == 0){
            bucketSize = 1;
        }


        int numOfBuckets = (max - min) / bucketSize + 1;
        /*
            from above, nums.length <= (max - min) / bucketSize
            so          nums.length < (max - min) / bucketSize + 1 = numOfBuckets
             i.e.   number of elements <= numOfBuckets - 1
                    number of elements <  numOfBuckets

                 (max - min) / bucketSize * bucketSize <= max - min
            also, as (dividend / divisor + 1 ) * divisor > dividend
                 [(max - min) / bucketSize + 1] * bucketSize > max - min
            i.e. [min, max] spans at least "numOfBuckets - 1" buckets
            and  "numOfBuckets" must cover [min, max]
         */

        int[] bucketMin = new int[numOfBuckets];
        int[] bucketMax = new int[numOfBuckets];
        int[] bucketCount = new int[numOfBuckets];

        Arrays.fill(bucketMin, Integer.MAX_VALUE);
        Arrays.fill(bucketMax, Integer.MIN_VALUE);

        for (int num : nums){
            int bucket = (num - min) / bucketSize;
            bucketCount[bucket]++;
            bucketMax[bucket] = Math.max(bucketMax[bucket], num);
            bucketMin[bucket] = Math.min(bucketMin[bucket], num);
        }
        int start= min;
        int maxGap = 0;
        for (int bucket = 0; bucket < numOfBuckets; bucket++){
            if (bucketCount[bucket] > 0){
                maxGap = Math.max(maxGap, bucketMin[bucket] - start);
                start = bucketMax[bucket];
            }
        }
        return maxGap;
    }


}
