package array;

import java.util.*;


/**
 * radix sort:
 * RadixSort(A, d)
 *     A: array
 *     d: highest digit
 *
 *     for i = 1 (lowest digit) to d (highest digit):
 *          CountSort(A, B, 10)
 *
 *
 * CountSort(A, B, k):
 *      A: array to be sorted
 *      B: array sorted
 *      k: number of different values in A
 *         (k = 10 in radix sort)
 *
 *      C = new int[k]
 *      for j = 0 to A.length - 1:
 *          C[A[j]] = C[A[j]] + 1
 *
 *      for i = 1 to k - 1:
 *          C[i] = C[i] + C[i - 1]
 *
 * 为什么这里要倒着放?因为radix sort是有几位会sort几次
 * 倒着放之前sort好的低位数次序不会改变!! "stable sort"
 *      for j = A.length - 1 to 0:
 *          C[A[j]] = C[A[j]] - 1
 *          B[C[A[j]] = A[j]
 */
public class MaximumGap_Radix {
    public int maximumGap(int[] nums){
        if (nums.length == 0){
            return 0;
        }

        // ref: http://stackoverflow.com/questions/1484347/finding-the-max-min-value-in-an-array-of-primitives-using-java
        //int maxNum = Arrays.stream(nums).max().getAsInt();
        int maxNum = nums[0];
        for (int num : nums) {
            maxNum = Math.max(maxNum, num);
        }
        int divisor = 1;
        int[] sorted = new int[nums.length];

        while (maxNum / divisor > 0){
            radix(nums, sorted, divisor);
            sorted = swap(nums, nums = sorted);
            divisor *= 10;
        }

        int gap = 0;
        for (int i = 1; i < nums.length; i++){
            gap = Math.max(gap, nums[i] - nums[i - 1]);
        }
        return gap;
    }

    //ref: http://stackoverflow.com/questions/3624525/how-to-write-a-basic-swap-function-in-java
    private int[] swap(int[] a, int[] b){
        return a;
    }

    private int[] radix(int[] nums, int[] sorted, int divisor){
        int[] count = new int[10];

        for (int i = 0; i < nums.length; i++){
            int digit = (nums[i] / divisor) % 10;
            count[digit]++;
        }

        for (int i = 1; i < 10; i++){
            count[i] += count[i - 1];
        }

        for (int i = nums.length - 1; i >= 0; i--){
            int digit = (nums[i] / divisor) % 10;
            count[digit]--;
            sorted[count[digit]] = nums[i];
        }
        return sorted;
    }
}

