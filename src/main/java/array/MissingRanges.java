package array;

import java.util.*;

public class MissingRanges {
    public List<String> findMissingRanges(int[] nums, int lower, int upper) {
        List<String> result = new ArrayList<>();
        int n = nums.length;
        if (n == 0) {
            if (lower == upper) { //don't forget this possibility!
                result.add(Integer.toString(lower));
            } else {
                result.add(lower + "->" + upper);
            }
            return result;
        }
        if (nums[0] != lower) {
            if (nums[0] == lower + 1) {
                result.add(Integer.toString(lower));
            } else {
                result.add(lower + "->" + (nums[0] - 1));
            }
        }
        for (int i = 1; i < n; i++) {
            if (nums[i] - nums[i - 1] <= 1) {
                continue;
            }
            if (nums[i] - nums[i - 1] == 2) {
                result.add(Integer.toString(nums[i] - 1));
            } else {
                result.add((nums[i - 1] + 1) + "->" + (nums[i] - 1));
            }
        }
        if (nums[n - 1] != upper) {
            if (nums[n - 1] == upper - 1) {
                result.add(Integer.toString(upper));
            } else {
                result.add((nums[n - 1] + 1) + "->" + upper);
            }
        }
        return result;
    }
}
