package array;


public class PatchingArray {
    public int minPatches(int[] nums, int n) {
        long maxSum = 0; //important: use long to prevent overflow!!!
        int i = 0;
        int patches = 0;
        while (maxSum < n) {
            if (i < nums.length && nums[i] > maxSum + 1 || i >= nums.length) {
                patches++;
                maxSum = maxSum  + maxSum + 1;
            } else {
                maxSum += nums[i++];
            }
        }
        return patches;
    }
}
