package array;

import java.util.*;

public class Intersection {
    public int[] intersection(int[] nums1, int[] nums2) {
        Set<Integer> inNums1 = new HashSet<>();
        Set<Integer> intersect = new HashSet<>();
        for (int num : nums1) {
            inNums1.add(num);
        }
        for (int num : nums2) {
            if (inNums1.contains(num)) {
                intersect.add(num);
            }
        }
        int[] result = new int[intersect.size()];
        int i = 0;
        for (int num : intersect) {
            result[i++] = num;
        }
        return result;
    }

    public int[] intersection_method2(int[] nums1, int[] nums2) {
        Arrays.sort(nums1);
        Arrays.sort(nums2);
        List<Integer> intersect = new ArrayList<>();

        int i = 0, j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                intersect.add(nums1[i]);
                while (i + 1 < nums1.length && nums1[i + 1] == nums1[i]) {
                    i++;
                }
                i++;
                while (j + 1 < nums2.length && nums2[j + 1] == nums2[j]) {
                    j++;
                }
                j++;
            }
        }
        int[] result = new int[intersect.size()];
        for (i = 0; i < intersect.size(); i++) {
            result[i] = intersect.get(i);
        }
        return result;

    }

}
