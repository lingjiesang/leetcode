package array;


import java.util.*;


/**
 *
 * ref:
 * http://huntzhan.org/leetcode-count-of-range-sum/
 */

public class CountRangeSum_FenwickTreeSimplified {
    public class FenwickTree {
        int[] bit;

        FenwickTree(int arrayLen) {
            this.bit = new int[arrayLen + 1];
        }

        void addValue(int index, int value) {
            index += 1;
            while (index > 0 && index < bit.length) {
                bit[index] += value;
                index += index & -index;
            }
        }

        int getSum(int index) {
            if (index < 0) { // dealt here
                return 0;
            }
            index += 1;
            int sum = 0;
            while (index > 0) {
                sum += bit[index];
                index -= index & -index;
            }
            return sum;
        }

    }


    public int countRangeSum(int[] nums, int lower, int upper) {

        long[] sums = new long[nums.length + 1];
        /**
         * sum[0] = 0
         * sum[i] = nums[0] + ... + nums[i-1]
         */
        for (int i = 1; i <= nums.length; i++) {
            sums[i] = sums[i - 1] + nums[i - 1];
        }

        long[] sumSorted = sums.clone();
        Arrays.sort(sumSorted);

        FenwickTree fenwick = new FenwickTree(sumSorted.length);

        for (long sum : sums) {
            fenwick.addValue(Arrays.binarySearch(sumSorted, sum), 1);
        }

        int count = 0;
        for (int i = 0; i < sums.length; i++) {
            fenwick.addValue(Arrays.binarySearch(sumSorted, sums[i]), -1);
            int upperIndex = Arrays.binarySearch(sumSorted, upper + sums[i]);
            if (upperIndex < 0){
                upperIndex = - (upperIndex + 1) - 1;
            }
            count += fenwick.getSum(upperIndex);
            int lowerIndex = Arrays.binarySearch(sumSorted, lower + sums[i] - 1);
            if (lowerIndex < 0){
                lowerIndex = - (lowerIndex + 1) - 1;
            }
            count -= fenwick.getSum(lowerIndex);
        }
        return count;
    }
}

