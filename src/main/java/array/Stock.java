package array;

import java.util.*;

public class Stock {
    /**
     * 121. I
     */
    public int maxProfit(int[] prices) {
        if (prices.length == 0){
            return 0;
        }
        int minPrice = prices[0];
        int maxProfit = 0;

        for (int i = 1; i < prices.length; i++){
            maxProfit = Math.max(maxProfit, prices[i] - minPrice);
            minPrice = Math.min(minPrice, prices[i]);
        }
        return maxProfit;
    }

    /**
     * 122. II
     */
    public int maxProfit_II(int[] prices) {
        if (prices.length == 0){
            return 0;
        }
        int profit = 0;
        for (int i = 1; i < prices.length; i++){
            profit += Math.max(0, prices[i] - prices[i - 1]);
        }
        return profit;
    }
}
