package array;

import java.util.*;

public class PascalTriangle {
    /**
     * I
     */
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        if (numRows == 0) return result;
        List<Integer> list = new ArrayList<>();
        list.add(1);
        result.add(list);
        for (int i = 2; i <= numRows; i++){
            List<Integer> newList = new ArrayList<>();
            newList.add(1);
            for (int j = 1; j < list.size(); j++){
                newList.add(list.get(j - 1) + list.get(j));
            }
            newList.add(1);
            result.add(newList);
            list = newList;
        }
        return result;
    }


    /**
     * II
     */
    public List<Integer> getRow(int rowIndex) {
        List<Integer> list = new ArrayList<>();
        if (rowIndex < 0){
            return list;
        }
        list.add(1);
        for (int i = 1; i <= rowIndex; i++){
            List<Integer> newList = new ArrayList<>();
            newList.add(1);
            for (int j = 1; j < list.size(); j++){
                newList.add(list.get(j - 1) + list.get(j));
            }
            newList.add(1);
            list = newList;
        }
        return list;
    }
}
