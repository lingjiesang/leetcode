package array;

import java.util.*;

public class IntersectionII {
    public int[] intersect(int[] nums1, int[] nums2) {
        Map<Integer, Integer> counts = new HashMap<>();
        for (int num : nums1){
            counts.put(num, counts.getOrDefault(num, 0) + 1);
        }
        List<Integer> intersect = new ArrayList<>();
        for (int num : nums2){
            if (counts.containsKey(num) && counts.get(num) > 0){
                intersect.add(num);
                counts.put(num, counts.get(num) - 1);
            }
        }
        int[] result = new int[intersect.size()];
        for (int i = 0; i < intersect.size(); i++) {
            result[i] = intersect.get(i);
        }
        return result;
    }

    public int[] intersect_method2(int[] nums1, int[] nums2) {
        List<Integer> intersect = new ArrayList<>();

        int i = 0, j = 0;
        while (i < nums1.length && j < nums2.length) {
            if (nums1[i] < nums2[j]) {
                i++;
            } else if (nums1[i] > nums2[j]) {
                j++;
            } else {
                intersect.add(nums1[i]);
                i++;
                j++;
            }
        }
        int[] result = new int[intersect.size()];
        for (i = 0; i < intersect.size(); i++) {
            result[i] = intersect.get(i);
        }
        return result;

    }
}
