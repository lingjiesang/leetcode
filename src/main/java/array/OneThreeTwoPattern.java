package array;

import java.util.*;

public class OneThreeTwoPattern {
    public boolean find132pattern(int[] nums) {
        Deque<Integer> stack = new ArrayDeque<>();
        int s1Min = Integer.MIN_VALUE; // s1 value must be smaller than s1Min
        for (int i = nums.length - 1; i >= 0; i--){
            if (nums[i] < s1Min){
                return true;
            }
            while (!stack.isEmpty() && nums[i] > stack.peek()){
                s1Min = Math.max(stack.pop(),s1Min);
            }
            stack.push(nums[i]);
        }
        return false;
    }
}
