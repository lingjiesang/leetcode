package array;

import java.util.*;

public class CountSmallerNumbersAfterSelf_FenwickTree {

    public List<Integer> countSmaller(int[] nums) {
        int[] numsCopy = nums.clone();

        Arrays.sort(numsCopy);

        List<Integer> res = new LinkedList<>();

        FenwickTree fenwickTree = new FenwickTree(nums);

        for (int i = nums.length - 1; i >= 0; i--) {
            int index = Arrays.binarySearch(numsCopy, nums[i]);
            res.add(0, fenwickTree.getSum(index - 1));
            fenwickTree.addValue(index, 1);
        }
        return res;
    }

    public class FenwickTree {
        int[] bit;

        public FenwickTree(int[] array) {
            bit = new int[array.length + 1];
        }

        public void addValue(int index, int val) {
            index += 1;
            while (index < bit.length) {
                bit[index] += val;
                index += index & -index;
            }
        }

        public int getSum(int index) {
            index += 1;
            int sum = 0;
            while (index > 0) {
                sum += bit[index];
                index -= index & -index;
            }
            return sum;
        }
    }
}
