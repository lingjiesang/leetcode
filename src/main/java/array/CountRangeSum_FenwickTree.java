package array;


import java.util.*;


/**
 *
 * ref:
 * http://huntzhan.org/leetcode-count-of-range-sum/
 */

public class CountRangeSum_FenwickTree {
    public class FenwickTree {
        int[] bit;

        FenwickTree(int arrayLen) {
            this.bit = new int[arrayLen + 1];
        }

        void addValue(int index, int value) {
            index += 1;
            while (index > 0 && index < bit.length) {
                bit[index] += value;
                index += index & -index;
            }
        }

        int getSum(int index) {
            if (index < 0) { // dealt here
                return 0;
            }
            index += 1;
            int sum = 0;
            while (index > 0) {
                sum += bit[index];
                index -= index & -index;
            }
            return sum;
        }

    }

    private long[] sort_removeduplicates(long[] nums) {
        Set<Long> values = new HashSet<>();
        for (long num : nums) {
            values.add(num);
        }
        long[] res = new long[values.size()];
        int len = 0;
        for (long value : values) {
            res[len++] = value;
        }

        Arrays.sort(res);
        return res;
    }

    /**
     * return last index <= value
     * notice: might return -1 if all num in nums > value! (dealt in "getSummary")
     *
     * pretty hard to get this correct!!!
     */
    private int upper_bound(long[] nums, long value) {
        int begin = 0;
        int end = nums.length; //different from usual
        while (begin < end) {
            int mid = begin + (end - begin) / 2;
            if (value < nums[mid]) {
                end = mid;
            } else {
                begin = mid + 1;
            }
        }
        return end - 1; //different from usual!

    }

    public int countRangeSum(int[] nums, int lower, int upper) {

        long[] sums = new long[nums.length + 1];
        /**
         * sum[0] = 0
         * sum[i] = nums[0] + ... + nums[i-1]
         */
        for (int i = 1; i <= nums.length; i++) {
            sums[i] = sums[i - 1] + nums[i - 1];
        }


        long[] sumSorted = sort_removeduplicates(sums);

        FenwickTree fenwick = new FenwickTree(sumSorted.length);

        for (long sum : sums) {
            fenwick.addValue(Arrays.binarySearch(sumSorted, sum), 1);
        }

        int count = 0;
        for (int i = 0; i < sums.length; i++) {
            fenwick.addValue(Arrays.binarySearch(sumSorted, sums[i]), -1);
            count += fenwick.getSum(upper_bound(sumSorted, upper + sums[i]));
            count -= fenwick.getSum(upper_bound(sumSorted, lower + sums[i] - 1));
        }
        return count;

    }


}
