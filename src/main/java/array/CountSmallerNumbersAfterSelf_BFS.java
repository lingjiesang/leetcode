package array;

import java.util.*;

public class CountSmallerNumbersAfterSelf_BFS {
    class Node{
        int sum; //# of nodes on the left of this node (count dups)
        int dup; // # of number equals to this node's val
        int val;

        Node left, right;
        Node(int s, int v){
            sum = s;
            val = v;
            dup = 1;
        }
    }
    public List<Integer> countSmaller(int[] nums) {
        List<Integer> result = new LinkedList<>();

        if (nums.length == 0){
            return result;
        }

        int n = nums.length;
        Node root = new Node(0, nums[n - 1]);

        result.add(0, 0);

        for (int i = n - 2; i >= 0; i--){
            result.add(0, insert(root, nums[i], 0));
        }
        return result;
    }

    private int insert(Node root, int val, int preSum){
        if (val == root.val){
            root.dup++;
            return preSum + root.sum;
        } else if (val < root.val){
            root.sum++;
            if (root.left == null){
                root.left = new Node(0, val);
                return preSum;
            } else{
                return insert(root.left, val, preSum);
            }
        } else{
            preSum += root.dup + root.sum;
            if (root.right == null){
                root.right = new Node(0, val);
                return preSum;
            } else{
                return insert(root.right, val, preSum);
            }
        }

    }
}
