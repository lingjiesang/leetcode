package array;

import java.util.*;

public class NextPermutation {
    public void nextPermutation(int[] nums){

        int first = -1, second = -1;
        for (int i = nums.length - 2; i >= 0; i--){
            if (nums[i] < nums[i + 1]){
                first = i;
                second = nums.length - 1;
                for (int j = i + 2; j < nums.length; j++){
                    if (nums[j] <= nums[i]){
                        second = j - 1;
                        break;
                    }
                }
               break;
            }
        }
        if (first != -1){
            swap(nums, first, second);
            reverse(nums, first + 1, nums.length - 1);
        } else{
            reverse(nums, 0, nums.length - 1);
        }
    }

    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    private void reverse(int[] nums, int start, int end){
        for (int i = 0; start + i < end - i; i++){
            swap(nums, start + i, end - i);
        }
    }


    public void previousPermutation(int[] nums){

    }
}
