package findPatterns;


import java.util.*;

//Wrong!! see onenote for reasoning!

public class CreateMaximumNumber_Wrong {

    class Num{
        int value;
        int numOfElementsAfter;
        Num(int value, int numOfElementsAfter){
            this.value = value;
            this.numOfElementsAfter = numOfElementsAfter;
        }
    }

    public int[] maxNumber(int[] nums1, int[] nums2, int k) {

        int[] result = new int[k];

        List<List<Num>> numLists = new ArrayList<>();
        numLists.add(createNumList(nums1));
        numLists.add(createNumList(nums2));

        for (int i = 0; i < 2; i++){
            Collections.sort(numLists.get(i), (num1, num2) -> (num2.value - num1.value));
        }

        int[] numOfPossibleElements = {nums1.length, nums2.length};

        int i = 0;
        while (i < k){
            int[] starts = {0, 0};

            while (true) {
                int curt0Value = starts[0] < numLists.get(0).size() ? numLists.get(0).get(starts[0]).value : Integer.MIN_VALUE;
                int curt1Value = starts[1] < numLists.get(1).size() ? numLists.get(1).get(starts[1]).value : Integer.MIN_VALUE;

                int group = curt0Value >= curt1Value ? 0 : 1;

                Num curt = numLists.get(group).get(starts[group]);
                if (curt.numOfElementsAfter < numOfPossibleElements[group] &&
                        curt.numOfElementsAfter + numOfPossibleElements[1 - group] >= k - i - 1) { //check
                    result[i++] = curt.value;
                    numOfPossibleElements[group] = curt.numOfElementsAfter;
                    break;
                }
                starts[group]++;
            }
        }
        return result;
    }


    private List<Num> createNumList(int[] nums){
        List<Num> numList = new ArrayList<>();

        for (int i = 0; i < nums.length; i++){
            numList.add(new Num(nums[i], nums.length - i - 1));
        }

        return numList;
    }

    public static void main(String[] args){
        int[] nums1 = {6, 7};
        int[] nums2 = {6, 0, 4};
        CreateMaximumNumber_Wrong soln = new CreateMaximumNumber_Wrong();
        int[] result = soln.maxNumber(nums1, nums2, 5);
        System.out.println(result.toString());

    }
}
