package findPatterns;


public class Candy {
    public int candy(int[] ratings) {
        int pos = 0;
        int totalCandies = 0;
        int candy = 0;


        while (pos < ratings.length){
            int curtRating = ratings[pos];
            int preRating = pos > 0 ? ratings[pos - 1] : Integer.MIN_VALUE;
            int nextRating = pos + 1 < ratings.length ? ratings[pos + 1] : Integer.MIN_VALUE;

            candy = curtRating > preRating ? candy + 1 : 1;

            if (curtRating <= nextRating){
            // going up
                totalCandies += candy;
                pos++;
            } else {
            //currentRating > nextRating, i.e. current pos is a peak viewing from right hand side
                int peakPos = pos;

                //find a bottom
                while (pos + 1 < ratings.length && ratings[pos] > ratings[pos + 1]){
                    pos++;
                }
                // sum from peak bottom
                totalCandies += (1 + (pos - peakPos + 1)) * (pos - peakPos + 1) / 2;

                // peak might go higher to satisfy its left constraint:
                // candy is the left constraint for the peakPos
                if (candy > pos - peakPos + 1){
                    totalCandies += candy - (pos - peakPos + 1 );
                }

                // finish deal [peakPos, pos], start to deal the next!
                pos++;
                candy = 1;  //because pos is bottom, candy must be 1
            }
        }
        return totalCandies;

    }

}
