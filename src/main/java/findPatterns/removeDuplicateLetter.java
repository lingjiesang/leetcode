package findPatterns;

import java.util.*;
public class removeDuplicateLetter {
    public String removeDuplicateLetters(String s){

        // charMap: store positions of each character
        List<ArrayDeque<Integer>> charMap = new ArrayList<>();

        for (int i = 0; i < 26; i++){
            charMap.add(new ArrayDeque<>());
        }

        char[] chars = s.toCharArray();
        for (int i = 0; i < s.length(); i++){
            charMap.get(chars[i] - 'a').offerLast(i);
        }

        //min heap to store the position of last occurrence of each character
        PriorityQueue<Integer> lastPositions = new PriorityQueue<>();
        for (int i = 0; i < 26; i++){
            if (!charMap.get(i).isEmpty()){
                lastPositions.add(charMap.get(i).peekLast());
            }
        }

        StringBuilder sb = new StringBuilder();
        while (!lastPositions.isEmpty()) {
            for (int i = 0; i < 26; i++) {
                // search a "best" character to add to sb next
                if (!charMap.get(i).isEmpty() && charMap.get(i).peekFirst() <= lastPositions.peek()){
                    int leftPos = charMap.get(i).peekFirst();

                    sb.append((char)('a' + i));
                    lastPositions.remove(charMap.get(i).peekLast());

                    charMap.get(i).clear();
                    for (int j = 0; j < 26; j++){
                        if (!charMap.get(j).isEmpty()){
                            while (charMap.get(j).peekFirst() < leftPos){
                                charMap.get(j).pollFirst();
                            }
                        }
                    }
                    break; //search startover
                }
            }
        }
        return sb.toString();

    }
}
