package findPatterns;


public class SelfCrossing {
    public boolean isSelfCrossing(int[] x) {
        if (x.length <= 3){
            return false;
        }

        for (int i = 3; i < x.length; i++){
            if (x[i - 1] <= x[i - 3] && x[i -2] <= x[i]){
                return true;
            }

            if (i >= 4){
                if (x[i - 3] == x[i - 1] && x[i] >= x[i- 2] - x[i - 4]){
                    return true;
                }

            }

            if (i >= 5){
                if (x[i - 3] > x[i - 5] //没有这个条件会变成1
                        && x[i - 2] > x[i- 4]
                        && x[i - 1] >= x[i - 3] - x[i - 5] && x[i - 1] < x[i - 3]
                        && x[i] >= x[i - 2] - x[i - 4]){
                    return true;
                }
            }
        }
        return false;
    }
}
