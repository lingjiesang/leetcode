package findPatterns;

import java.util.*;

public class NumberOfDigitOne {
    static private int[] base = new int[10];
    static private int[] ones = new int[10];

    static{
        base[0] = 1;
        ones[0] = 0;
        for (int i = 1; i < 10; i++){
            base[i] = 10 * base[i - 1];
            ones[i] = ones[i - 1] * 10 + base[i - 1];
        }
//        System.out.println(Arrays.toString(base));
//        System.out.println(Arrays.toString(ones));
    }

    public int countDigitOne(int n) {
        if (n <= 0){
            return 0;
        }
        int result = 0;
        for (int i = 9; i >= 0; i--){
            int quotient = n / base[i];
            if (quotient == 0){
                continue;
            } else if (quotient == 1){
                result += ones[i] + (n - base[i] + 1);
            } else{
                result += quotient * ones[i] + base[i];
            }
            n = n % base[i];
        }
        return result;
    }
}
