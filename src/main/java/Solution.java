import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.net.*;

import org.json.simple.*;


public class Solution {

//    public static final String BASE_URL = "jsonmock.hackerrank.com";
//
//    public static final String PATH_URL = "/api/movies/search/";
//
//    public static final String TITLE_PARAM = "Title";
//
//    public static final String PAGE_PARAM = "page";
//
//
//    static String getJSON(String url_filled) {
//        try {
//            URL url = new URL(url_filled);
//
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setRequestMethod("GET");
//            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//            String line;
//
//            StringBuilder result = new StringBuilder();
//            while ((line = rd.readLine()) != null) {
//                result.append(line);
//            }
//            rd.close();
//
//            System.out.println(result.toString());
//            return result.toString();
//
//        } catch (Exception e) {
//
//        }
//        return null;
//
//    }
//
//
//    static String[] getMovieTitles(String substr) {
//        String url_template = "https://jsonmock.hackerrank.com/api/movies/search/?Title=tttt&page=pppp";
//
//        int page = 1;
//        String url_filled = url_template.replace("tttt", substr).replace("pppp", Integer.toString(page));
//
//        String result = getJSON(url_filled);
//
//        String pattern = "\"page\":\"(\\d+)\"";
//        pattern = "page";
//
//        Pattern r = Pattern.compile(pattern);
//
//        Matcher m = r.matcher(result);
//
//        System.out.println(m.group(0));
//
//
//        return null;
//    }
//
//
//    public static void main(String[] args) {
//        getMovieTitles("spiderman");
//
//
//    }

    static int max(int[] numbers, int indexExcluded) {
        int max_val = Integer.MIN_VALUE;
        int index = -1;
        for (int i = 0; i < numbers.length; i++) {
            if (i != indexExcluded && numbers[i] >= max_val) {
                max_val = numbers[i];
                index = i;
            }
        }
        return index;
    }

    static boolean equals(int[] numbers) {
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] != numbers[0]) {
                return false;
            }
        }
        return true;
    }

    static void add(int[] numbers, int diff, int min_index) {
        for (int i = 1; i < numbers.length; i++) {
            if (i != min_index) {
                numbers[i] += diff;
            }
        }
    }

    static long countMoves(int[] numbers) {

        if (numbers.length <= 1) {
            return 0;
        }
        int count = 0;
        while (!equals(numbers)) {
            int min_index = max(numbers, -2);
            int second_min_index = max(numbers, min_index);
            int diff = numbers[min_index] - numbers[second_min_index];
            count += diff;
            add(numbers, diff, min_index);
        }
        return count;

    }




}