package unionFind;

import java.util.*;

public class NumberOfIslands {
    private class UF{
        private int[] parent;
        private int[] rank;
        int count;
        int n;

        public UF(char[][] grid){
            int m = grid.length;
            n = grid[0].length;
            parent = new int[m * n];
            rank = new int[m * n];
            for (int i = 0; i < m; i++){
                for (int j = 0; j < n; j++){
                    if (grid[i][j] == '1'){
                        add(i, j);
                        if (i > 0 && grid[i - 1][j] == '1'){
                            union(i, j, i - 1, j);
                        }
                        if (j > 0 && grid[i][j - 1] == '1'){
                            union(i, j, i, j - 1);
                        }
                    }
                }
            }
        }

        public void add(int i, int j){
            int index = i * n + j;
            parent[index] = index;
            rank[index] = 1;
            count++;
        }

        public void union(int i1, int j1, int i2, int j2){
            int root1 = root(i1 * n + j1);
            int root2 = root(i2 * n + j2);
            if (root1 != root2){
                if (rank[root1] > rank[root2]){
                    parent[root2] = root1;
                    rank[root1] += rank[root2];
                } else{
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                }
                count--;
            }
        }

        public int root(int index){
            if (parent[index] != index){
                parent[index] = root(parent[index]);
            }
            return parent[index];
        }

        public int getCount(){
            return count;
        }
    }
    public int numIslands(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }
        UF uf = new UF(grid);
        return uf.getCount();
    }
}
