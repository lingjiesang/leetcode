package unionFind;

import java.util.*;

// ref: https://discuss.leetcode.com/topic/1944/solve-it-using-union-find
public class SurroundedRegion {
    private class UF{
        private int[] parent;
        private int[] rank;
        int n;
        int outside;

        public UF(int m, int n){
            this.n = n;
            parent = new int[m * n + 1];
            rank = new int[m * n + 1];
            outside = m * n;
            parent[outside] = outside;
            rank[outside] = 1;
        }

        public void add(int i, int j){
            int index = i * n + j;
            parent[index] = index;
            rank[index] = 1;
        }

        public void union(int i1, int j1, int i2, int j2){
            link(root(i1 * n + j1), root(i2 * n + j2));
        }

        public void unionOutside(int i1, int j1){
            link(root(i1 * n + j1), root(outside));
        }

        private void link(int root1, int root2){
            if (root1 != root2){
                if (rank[root1] > rank[root2]){
                    parent[root2] = root1;
                    rank[root1] += rank[root2];
                } else{
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                }
            }
        }

        public boolean isConnectToOutside(int i, int j){
            return root(i * n + j) == root(outside);
        }

        private int root(int index){
            if (parent[index] != index){
                parent[index] = root(parent[index]);
            }
            return parent[index];
        }

    }

    public void solve(char[][] board) {
        if (board.length == 0 || board[0].length == 0){
            return;
        }
        int m = board.length;
        int n = board[0].length;
        UF uf = new UF(m, n);

        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (board[i][j] == 'O'){
                    uf.add(i, j);
                    if (i == 0 || i == m - 1 || j == 0 || j == n - 1){
                        uf.unionOutside(i, j);
                    }
                    if (j > 0 && board[i][j - 1] == 'O'){
                        uf.union(i, j, i, j - 1);
                    }
                    if (i > 0 && board[i - 1][j] == 'O'){
                        uf.union(i, j, i - 1, j);
                    }
                }
            }
        }

        for (int i = 1; i < m - 1; i++){
            for (int j = 1; j < n - 1; j++){
                if (board[i][j] == 'O' && !uf.isConnectToOutside(i, j)) {
                    board[i][j] = 'X';
                }
            }
        }
    }
}