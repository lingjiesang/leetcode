package unionFind;

import java.util.*;

public class GraphValidTree {

    class UnionFound{
        private int[] parent;
        private int[] rank;
        public int count;

        public UnionFound(int n){
            parent = new int[n];
            rank = new int[n];
            for (int i = 0; i < n; i++) {
                parent[i] = i;
                rank[i] = 1;
            }
            count = n;
        }


        public boolean union(int index1, int index2) {
            int root1 = root(index1);
            int root2 = root(index2);
            if (root1 != root2) {
                if (rank[root1] < rank[root2]) {
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                } else {
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                }
                count--;
                return true;
            } else{
                return false;
            }
        }

        public int root(int index){
            if (parent[index] != index) {
                parent[index] = root(parent[index]);
            }
            return parent[index];
        }

    }

    public boolean validTree(int n, int[][] edges) {
        UnionFound uf = new UnionFound(n);

        for (int i = 0; i < edges.length; i++) {
            if (!uf.union(edges[i][0], edges[i][1])){
                return false;
            }
        }
        return uf.count == 1;
    }
}
