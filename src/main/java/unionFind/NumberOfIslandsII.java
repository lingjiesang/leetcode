package unionFind;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/29518/java-python-clear-solution-with-unionfind-class-weighting-and-path-compression
 */
public class NumberOfIslandsII {
    private class UnionFind {
        private int[] parent;
        private int[] rank;
        private int count;
        private int m;
        private int n;

        public UnionFind(int m, int n) {
            this.m = m;
            this.n = n;
            parent = new int[m * n];
            rank = new int[m * n];
        }

        public void add(int x, int y) {
            parent[x * n + y] = x * n + y;
            rank[x * n + y] = 1;
            count++;
        }

        public boolean find(int x, int y) {
            return rank[x * n + y] != 0;
        }

        public void union(int x1, int y1, int x2, int y2) {
            int root1 = root(x1 * n + y1);
            int root2 = root(x2 * n + y2);
            if (root1 != root2) {
                if (rank[root1] < rank[root2]) {
                    parent[root1] = root2;
                    rank[root2] += rank[root1];
                } else {
                    parent[root2] = root1;
                    rank[root1] += rank[root2];
                }
                count--;
            }
        }

        /**
         * CLRS (pp569, 21.3)
         * Rather than explicitly keeping track of the size of the subtree rooted at each node, we shall use an approach
         * that eases the analysis. For each node, we maintain a rank, which is an upper bound on the height of the node.
         *
         * but this way is slower than defining rank as the size of the subtree (as above!)
         *
         */

        public void union_CLRS(int x1, int y1, int x2, int y2) {
            int root1 = root(x1 * n + y1);
            int root2 = root(x2 * n + y2);
            if (root1 != root2) {
                if (rank[root1] < rank[root2]) {
                    parent[root1] = root2;
                } else {
                    parent[root2] = root1;
                    if (rank[root1] == rank[root2]) {
                        rank[root1]++;
                    }
                }
                count--;
            }
        }

        public int getSetNumber() {
            return count;
        }

        private int root(int i) {
            if (parent[i] != i) {
                parent[i] = root(parent[i]);
            }
            return parent[i];
        }
    }

    public List<Integer> numIslands2(int m, int n, int[][] positions) {
        UnionFind islands = new UnionFind(m, n);
        List<Integer> result = new ArrayList<>();

        for (int[] position : positions) {
            islands.add(position[0], position[1]);
            if (position[0] > 0 && islands.find(position[0] - 1, position[1])) {
                islands.union(position[0], position[1], position[0] - 1, position[1]);
            }
            if (position[0] + 1 < m && islands.find(position[0] + 1, position[1])) {
                islands.union(position[0], position[1], position[0] + 1, position[1]);
            }
            if (position[1] > 0 && islands.find(position[0], position[1] - 1)) {
                islands.union(position[0], position[1], position[0], position[1] - 1);
            }
            if (position[1] + 1 < n && islands.find(position[0], position[1] + 1)) {
                islands.union(position[0], position[1], position[0], position[1] + 1);
            }
            result.add(islands.getSetNumber());
        }
        return result;
    }
}
