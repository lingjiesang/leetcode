package bfs;

import java.util.*;

public class LargestDivisibleSubset {
    List<List<Integer>> layers;
    int[] parentIndices;

    /**
     * my method
     */
    public List<Integer> largestDivisibleSubset(int[] nums) {
        List<Integer> result = new LinkedList<>();
        if (nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        layers = new ArrayList<>();
        layers.add(new ArrayList<>());
        parentIndices = new int[nums.length];

        for (int i = 0; i < nums.length; i++) {
            foundParent(nums, i);
        }
        backTrack(nums, result, layers.get(layers.size() - 1).get(0));
        return result;
    }

    private void backTrack(int[] nums, List<Integer> result, int index) {
        if (index == -1) {
            return;
        }
        result.add(0, nums[index]);
        backTrack(nums, result, parentIndices[index]);
    }

    private void foundParent(int[] nums, int index) {
        for (int layer = layers.size() - 1; layer >= 0; layer--) {
            for (int j = 0; j < layers.get(layer).size(); j++) {
                int prevIndex = layers.get(layer).get(j);
                if (nums[index] % nums[prevIndex] == 0) {
                    if (layer == layers.size() - 1) {
                        layers.add(new ArrayList<>());
                    }
                    layers.get(layer + 1).add(index);
                    parentIndices[index] = prevIndex;
                    return;
                }
            }
        }
        layers.get(0).add(index);
        parentIndices[index] = -1;
    }


    /**
     * other people's idea
     */
    public List<Integer> largestDivisibleSubset_simpler(int[] nums) {
        int n = nums.length;
        int[] count = new int[n];
        int[] parentIndices = new int[n];
        Arrays.sort(nums);
        int max = 0, index = -1;
        for (int i = 0; i < n; i++) {
            count[i] = 1;
            parentIndices[i] = -1;
            for (int j = i - 1; j >= 0; j--) {
                if (nums[i] % nums[j] == 0) {
                    if (1 + count[j] > count[i]) {
                        count[i] = count[j] + 1;
                        parentIndices[i] = j;
                    }
                }
            }
            if (count[i] > max) {
                max = count[i];
                index = i;
            }
        }
        List<Integer> res = new LinkedList<>();
        while (index != -1) {
            res.add(0, nums[index]);
            index = parentIndices[index];
        }
        return res;
    }
}
