package bfs;

import java.util.*;

public class MinimumHeightTrees {
    public List<Integer> findMinHeightTrees(int n, int[][] edges) {
        List<Integer> res = new ArrayList<>();

        if (n == 1){
            res.add(0);
            return res;
        }

        List<Set<Integer>> adjLists = new ArrayList<>();
        constructTree(n, edges, adjLists);

        List<Integer> leaves = new ArrayList<>();

        for (int i = 0; i < adjLists.size(); i++){
            if (adjLists.get(i).size() == 1){
                leaves.add(i);
            }
        }

        while (!leaves.isEmpty()){
            res.clear();
            List<Integer> newLeaves = new ArrayList<>();
            for (int node : leaves) {
                res.add(node);
                for (int neighbor : adjLists.get(node)) {
                    adjLists.get(neighbor).remove(node);
                    if (adjLists.get(neighbor).size() == 1){
                        newLeaves.add(neighbor);
                    }
                }
            }
            leaves = newLeaves;
        }
        return res;

    }

    private void constructTree(int n, int[][] edges, List<Set<Integer>> adjLists){
        for (int i = 0; i < n; i++){
            adjLists.add(new HashSet<>());
        }
        for (int[] edge : edges){
            adjLists.get(edge[0]).add(edge[1]);
            adjLists.get(edge[1]).add(edge[0]);
        }
    }

}