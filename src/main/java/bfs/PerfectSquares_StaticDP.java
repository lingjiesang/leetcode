package bfs;

import java.util.*;

public class PerfectSquares_StaticDP {
    static List<Integer> minPath;
    static{
        minPath = new ArrayList<>();
        minPath.add(0);
    }

    public int numSquares(int n) {
        if (n <= 0) {
            return -1;
        }

        while (minPath.size() < n + 1){
            int num = minPath.size();
            int pathLen = Integer.MAX_VALUE;
            for (int i = 1; i * i <= num; i++){
                pathLen = Math.min(pathLen, minPath.get(num - i * i) + 1);
            }
            minPath.add(pathLen);
        }
        return minPath.get(n);
    }
}
