package bfs;

import binaryTree.TreeNode;

import java.util.*;

public class MinimumDepthBinaryTree {
    public int minDepth(TreeNode root) {
        int depth = 0;
        Queue<TreeNode> queue = new ArrayDeque<>();
        if (root != null){
            depth++;
            queue.add(root);
        }

        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();

                if (node.left == null && node.right == null){
                    return depth;
                } else{
                    if (node.left != null) {
                        queue.add(node.left);
                    }
                    if (node.right != null){
                        queue.add(node.right);
                    }
                }
            }
            depth++;
        }
        return depth;

    }
}
