package bfs;

import java.util.*;


public class WordLadderII {
    public List<List<String>> findLadders(String beginWord, String endWord, Set<String> wordList) {

        List<List<String>> foundPathes = new ArrayList<>();

        // consider special cases:
        if (beginWord.equals(endWord)){
            List<String> path = new ArrayList<>();
            path.add(beginWord);
            foundPathes.add(path);
            return foundPathes;
        }

        if (wordList.size() == 0)
            return foundPathes;

        //String will be the type of a "vertex"
        Set<String> vertices = wordList;
        // neighbors will be dynamically constructed
        Queue<String> queue = new ArrayDeque<>();

        // for visited vertex, store its distance
        Map<String, Integer> vertexDistance = new HashMap<>();

        //parents on shortest pathes for a vertex in vertices
        Map<String, List<String>> vertexParents = new HashMap<>();

        // minimal distance from startWord to endWord
        int minDistance = Integer.MAX_VALUE;

        //preparation
        queue.offer(beginWord);
        vertexDistance.put(beginWord, 0);
        vertices.add(endWord);

        //BFS
        while (!queue.isEmpty()){
            String vertex = queue.poll();

            if (minDistance <= vertexDistance.get(vertex)){
                break;
            }
            // start to visit neighbors of vertex
            StringBuilder sb = new StringBuilder(vertex);
            for (int i = 0; i < vertex.length(); i++){
                for (char ch = 'a'; ch <='z'; ch++){
                    if (ch == sb.charAt(i)){
                        continue;
                    }

                    char chOld = sb.charAt(i);
                    sb.setCharAt(i, ch);

                    String neighbor = sb.toString();
                    if (vertices.contains(neighbor)) { //neighbor in wordList
                        if (!vertexDistance.containsKey(neighbor)) { //first time met a vertex in graph
                            queue.offer(neighbor);
                            vertexDistance.put(neighbor, vertexDistance.get(vertex) + 1);
                            List<String> parents = new ArrayList<>();
                            parents.add(vertex);
                            vertexParents.put(neighbor, parents);

                            // first time met the endWord
                            if (neighbor.equals(endWord)) {
                                minDistance = vertexDistance.get(neighbor);
                            }
                        } else {
                            if (vertexDistance.get(neighbor) == vertexDistance.get(vertex) + 1) {
                                vertexParents.get(neighbor).add(vertex);
                            }
                        }
                    }

                    //eventually
                    sb.setCharAt(i, chOld);

                }
            }

        }

        // after BFS: construct path!
        if (vertexDistance.containsKey(endWord)) { //there might be no path!
            List<String> path = new LinkedList<>();
            path.add(endWord);
            backTrace(beginWord, endWord, vertexParents, path, foundPathes);
        }
        return foundPathes;
    }

    private void backTrace(String start, String end, Map<String, List<String>> vertexParents,
                           List<String> path, List<List<String>> foundPathes ){
        if (end.equals(start)){
            foundPathes.add(new ArrayList<>(path));
            return;
        }
        for (String parent : vertexParents.get(end)){
            path.add(0, parent);
            backTrace(start, parent, vertexParents, path, foundPathes);
            path.remove(0);
        }
    }

    public static void main(String[] args){
        WordLadderII soln = new WordLadderII();
        Set<String> dict = new HashSet<>() ;
        dict.add("hot");
        dict.add("dog");
        dict.add("dot");
        List<List<String>> foundPathes = soln.findLadders("hot", "dog", dict);
        for (List<String> path : foundPathes) {
            System.out.print("[");
            for (String word : path) {
                System.out.print(word);
                System.out.print(", ");
            }
            System.out.println("]");
        }
    }


    // rewrite on 9/18/16
    public List<List<String>> findLadders_(String beginWord, String endWord, Set<String> wordList) {
        Deque<String> queue = new ArrayDeque<>(); //1. queue
        Map<String, Integer> visitedDistance = new HashMap<>(); // 2.visited (also record distance)
        Map<String, List<String>> parents = new HashMap<>(); // 3. parents on the shortest path <== need for dfs

        queue.offer(beginWord);
        visitedDistance.put(beginWord, 0);
        wordList.add(endWord);

        boolean foundEndWord = false;
        int distance = 0;
        while (!queue.isEmpty()){
            if (foundEndWord){
                break;
            }
            distance++;
            int size = queue.size();
            for (int i = 0; i < size; i++){
                String word = queue.poll();
                for (int pos = 0; pos < word.length(); pos++){
                    char[] chars = word.toCharArray();
                    for (char c = 'a'; c <= 'z'; c++){
                        chars[pos] = c;
                        String newWord = new String(chars);
                        if (wordList.contains(newWord)){
                            if (newWord.equals(endWord)){
                                foundEndWord = true;
                            }
                            if (!visitedDistance.containsKey(newWord)){
                                queue.offer(newWord);
                                visitedDistance.put(newWord, distance);
                                List<String> parent = new ArrayList<>();
                                parent.add(word);
                                parents.put(newWord, parent);
                            } else if (visitedDistance.get(newWord) == distance){
                                parents.get(newWord).add(word);
                            }
                        }
                    } // end for c
                } // end for pos
            } //end for i
        } //end while
        List<List<String>> result = new ArrayList<>();
        if (foundEndWord){
            LinkedList<String> oneSoln = new LinkedList<>();
            oneSoln.add(endWord);
            dfs(beginWord, endWord, parents, oneSoln, result);
        }
        return result;
    }

    private void dfs(String beginWord, String prevWord, Map<String, List<String>> parents,
                     LinkedList<String> oneSoln, List<List<String>> result){
        if (beginWord.equals(prevWord)){
            result.add(new ArrayList<>(oneSoln));
            return;
        }
        for (String parent : parents.get(prevWord)){
            oneSoln.addFirst(parent);
            dfs(beginWord, parent, parents, oneSoln, result);
            oneSoln.remove(0);
        }
    }
}
