package bfs;

import java.util.*;

//reference: https://discuss.leetcode.com/topic/17975/super-fast-java-solution-two-end-bfs

public class WordLadderII_twoEndBFS {
    public List<List<String>> findLadders(String beginWord, String endWord, Set<String> wordList) {
        Set<String> beginBoundVertices = new HashSet<>();
        Set<String> endBoundVertices = new HashSet<>();

        List<List<String>> foundPathes = new ArrayList<>();

        beginBoundVertices.add(beginWord);
        endBoundVertices.add(endWord);

        // key: a parent vertex
        // value: childen of the parent vertex if:
        //        1. parent is on the shortest path from beginWord to parent
        //        2. child is on the shortest path from parent to endWord
        Map<String, List<String>> vertexChildren = new HashMap<>();

        Boolean isPathExist = buildVertexChildren(wordList, beginBoundVertices,
                endBoundVertices, vertexChildren, true);
        List<String> path = new ArrayList<>();
        path.add(beginWord);
        if (isPathExist) {
            generatePathes(beginWord, endWord, vertexChildren, path, foundPathes);
        }

        return foundPathes;
    }


    private void generatePathes(String startWord, String endWord, Map<String, List<String>> vertexChildren,
                                List<String> path, List<List<String>> foundPathes) {
        if (startWord.equals(endWord)) {
            foundPathes.add(new ArrayList<>(path));
            return;
        }
        // if reach "dead end", give up this path...
        if (!vertexChildren.containsKey(startWord)) {
            return;
        }
        for (String child : vertexChildren.get(startWord)) {
            path.add(child);
            generatePathes(child, endWord, vertexChildren, path, foundPathes);
            path.remove(path.size() - 1);
        }
    }

    private boolean buildVertexChildren(Set<String> wordList, Set<String> beginBoundVertices,
                                        Set<String> endBoundVertices, Map<String, List<String>> vertexChildren, boolean isStartToEnd) {

        if (beginBoundVertices.size() > endBoundVertices.size()) {
            return buildVertexChildren(wordList, endBoundVertices, beginBoundVertices, vertexChildren, !isStartToEnd);
        }

        if (beginBoundVertices.isEmpty()) {
            return false;
        }

        // wordList only contains "unvisited" (white) vertices
        wordList.removeAll(beginBoundVertices);

        Set<String> newBeginBoundVertices = new HashSet<>();

        Boolean done = false;

        for (String vertex : beginBoundVertices) {
            for (int i = 0; i < vertex.length(); i++) {
                char[] chars = vertex.toCharArray();
                for (char ch = 'a'; ch <= 'z'; ch++) {
                    chars[i] = ch;

                    String neighbor = new String(chars);

                    if (wordList.contains(neighbor)) {
                        newBeginBoundVertices.add(neighbor);

                        String parent = isStartToEnd? vertex : neighbor;
                        String child = isStartToEnd? neighbor : vertex;

                        if (!vertexChildren.containsKey(parent)) {
                            vertexChildren.put(parent, new ArrayList<>());
                        }

                        vertexChildren.get(parent).add(child);
                    }
                    if (endBoundVertices.contains(neighbor)) {
                        done = true;
                    }
                }
            }
        }

        return done || buildVertexChildren(wordList, endBoundVertices, newBeginBoundVertices, vertexChildren, !isStartToEnd);
    }

    public static void main(String[] args) {
        WordLadderII_twoEndBFS soln = new WordLadderII_twoEndBFS();
        Set<String> dict = new HashSet<>();
        List<List<String>> foundPathes ;

        dict.add("hot");
        dict.add("dog");
        dict.add("dot");
        foundPathes = soln.findLadders("hot", "dog", dict);
        for (List<String> path : foundPathes) {
            System.out.print("[");
            for (String word : path) {
                System.out.print(word);
                System.out.print(", ");
            }
            System.out.println("]");
        }


        dict = new HashSet<>();
        dict.add("a");
        dict.add("b");
        dict.add("c");
        foundPathes = soln.findLadders("a", "c", dict);
        for (List<String> path : foundPathes) {
            System.out.print("[");
            for (String word : path) {
                System.out.print(word);
                System.out.print(", ");
            }
            System.out.println("]");
        }
    }
}
