package bfs;

import java.util.*;

public class WordLadder {
    int pathLength;

    public int ladderLength(String beginWord, String endWord, Set<String> wordList) {
        Set<String> beginFrontend = new HashSet<>();
        beginFrontend.add(beginWord);
        Set<String> endFrontend = new HashSet<>();
        endFrontend.add(endWord);
        pathLength = 2;
        if (bfs(beginFrontend, endFrontend, wordList)){
            return pathLength;
        } else{
            return 0;
        }
    }

    private boolean bfs(Set<String> beginFrontend, Set<String> endFrontEnd, Set<String> wordList){
        if (beginFrontend.size() > endFrontEnd.size()){
            return bfs(endFrontEnd, beginFrontend, wordList);
        }

        if (beginFrontend.size() == 0){
            return false;
        }

        Set<String> newBeginFrontEnd = new HashSet<>();

        wordList.removeAll(beginFrontend);

        for (String word : beginFrontend){
            for (int i = 0; i < word.length(); i++){
                char[] word_chars = word.toCharArray();
                for (char c = 'a'; c <= 'z'; c++){
                    word_chars[i] = c;
                    String newWord = new String(word_chars);
                    if (endFrontEnd.contains(newWord)){
                        return true;
                    }
                    if (wordList.contains(newWord)){
                        newBeginFrontEnd.add(newWord);
                    }
                }
            }
        }
        pathLength++;
        return bfs(endFrontEnd, newBeginFrontEnd, wordList);

    }
}
