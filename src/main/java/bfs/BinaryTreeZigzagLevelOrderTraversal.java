package bfs;

import binaryTree.TreeNode;

import java.util.*;

public class BinaryTreeZigzagLevelOrderTraversal {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null){
            return res;
        }

        Deque<TreeNode> queueForward = new ArrayDeque<>();
        Deque<TreeNode> queueBackward = new ArrayDeque<>();

        queueForward.add(root);

        while (!queueForward.isEmpty() || !queueBackward.isEmpty()){
            List<Integer> oneLevel = new ArrayList<>();
            if (!queueForward.isEmpty()) {
                while (!queueForward.isEmpty()) {
                    root = queueForward.pop();
                    oneLevel.add(root.val);
                    if (root.left != null) {
                        queueBackward.push(root.left);
                    }
                    if (root.right != null) {
                        queueBackward.push(root.right);
                    }
                }
            } else{
                while (!queueBackward.isEmpty()){
                    root = queueBackward.pop();
                    oneLevel.add(root.val);
                    if (root.right != null){
                        queueForward.push(root.right);
                    }
                    if (root.left != null){
                        queueForward.push(root.left);
                    }
                }
            }
            res.add(oneLevel);
        }
        return res;


    }
}
