package bfs;

import java.util.*;

public class CoinChange {
    /**
     * bfs
     * similar idea to "Perfect Square"
     */
    public int coinChange(int[] coins, int amount){
        if (amount == 0){
            return 0;
        }
        int[] minCoins = new int[amount + 1];
        Deque<Integer> queue = new ArrayDeque<>();
        for (int coin : coins){
            if (coin <= amount){
                minCoins[coin] = 1;
                queue.add(coin);
            }
        }

        while (!queue.isEmpty()){
            int prev = queue.poll();
            for (int coin : coins){
                if (prev + coin <= amount && minCoins[prev + coin] == 0){
                    minCoins[prev + coin] = minCoins[prev] + 1;
                    queue.add(prev + coin);
                }
            }
        }
        return minCoins[amount] == 0 ? -1 : minCoins[amount];
    }

    /**
     * dp

     */
    public int coinChange_dp(int[] coins, int amount) {
        int[] count = new int[amount + 1];
        Arrays.fill(count, -1);
        count[0] = 0;
        Arrays.sort(coins);
        for (int i = 1; i <= amount; i++){
            for (int j = 0; j < coins.length; j++){
                if (i - coins[j] < 0) {
                    break;
                }
                if (count[i - coins[j]] >= 0){
                    if (count[i] == -1){
                        count[i] = count[i - coins[j]] + 1;
                    } else{
                        count[i] = Math.min(count[i - coins[j]] + 1, count[i]);
                    }
                }
            }
        }
        return count[amount];

    }
}
