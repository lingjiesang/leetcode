package bfs;

import java.util.*;

public class PerfectSquares {
    public int numSquares(int n) {
        if (n <= 0) {
            return -1;
        }
        List<Integer> bases = new ArrayList<>();
        int[] minPath = new int[n + 1];

        //Queue<Integer> queue = new ArrayDeque<>();
        Queue<Integer> queue = new LinkedList<>();

        int pathLen = 1;

        for (int i = 1; i <= n / i; i++) {
            int base = i * i;
            bases.add(base);
            queue.offer(base);
            minPath[base] = pathLen;
        }

        if (minPath[n] != 0){
            return minPath[n];
        }

        while (!queue.isEmpty()) {
            pathLen++;
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                int prev = queue.poll();
                for (int base : bases) {
                    if (prev > n - base) {
                        break;
                    }
                    int curt = prev + base;
                    if (curt == n) {
                        return pathLen;
                    }
                    if (minPath[curt] == 0) { // key, minPath[i] == 0 marked i as not visited!!
                        minPath[curt] = pathLen;
                        queue.offer(curt);
                    }
                }
            }
        }

        return minPath[n]; // should not reach here actually!
    }
}
