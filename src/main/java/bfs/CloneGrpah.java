package bfs;

import java.util.*;


public class CloneGrpah {
    class UndirectedGraphNode {
        int label;
        List<UndirectedGraphNode> neighbors;

        UndirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<UndirectedGraphNode>();
        }
    }

    /**
     * bfs
     */
    public UndirectedGraphNode cloneGraph_BFS(UndirectedGraphNode node) {

        Set<UndirectedGraphNode> visited = new HashSet<>();
        Deque<UndirectedGraphNode> queue = new ArrayDeque<>();
        Map<UndirectedGraphNode, UndirectedGraphNode> oldToNew = new HashMap<>();

        if (node == null) {
            return null;
        }

        UndirectedGraphNode newRoot = new UndirectedGraphNode(node.label);
        oldToNew.put(node, newRoot);
        queue.add(node);
        visited.add(node);

        while (!queue.isEmpty()) {
            node = queue.poll();
            UndirectedGraphNode newNode = oldToNew.get(node);
            for (UndirectedGraphNode neighbor : node.neighbors) {
                UndirectedGraphNode newNeighbor = oldToNew.computeIfAbsent(neighbor, key -> new UndirectedGraphNode(key.label));
                newNode.neighbors.add(newNeighbor);
                if (!visited.contains(neighbor)) {
                    queue.offer(neighbor);
                    visited.add(neighbor);
                }
            }
        }

        return newRoot;
    }

    /**
     * dfs
     */
    private Map<UndirectedGraphNode, UndirectedGraphNode> oldToNew = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();

    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node) {
        if (node == null){
            return null;
        }
        UndirectedGraphNode newNode =  new UndirectedGraphNode(node.label);
        oldToNew.put(node, newNode);

        for (UndirectedGraphNode neighbor: node.neighbors){
            if (!oldToNew.containsKey(neighbor)){
                oldToNew.put(neighbor, cloneGraph(neighbor));
            }
            newNode.neighbors.add(oldToNew.get(neighbor));
        }
        return newNode;
    }
}
