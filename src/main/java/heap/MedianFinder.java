package heap;

import java.util.*;
public class MedianFinder {
    Queue<Integer> min = new PriorityQueue<>();
    Queue<Integer> max = new PriorityQueue<>(Collections.reverseOrder());

    public void addNum(int num){
        max.offer(num);
        min.offer(max.poll());
        if (max.size() < min.size()){
            max.offer(min.poll());
        }
    }

    public double findMedian(){
        if (max.size() == min.size()){
            return (max.peek() + min.peek()) / 2.0;
        } else{
            return (double)max.peek();
        }
    }
}
