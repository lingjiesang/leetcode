package heap;

import linkedlist.ListNode;

import java.util.*;

public class MergeKSortedLists_Heap {


    public ListNode mergeKLists(ListNode[] lists){
        PriorityQueue<ListNode> heap = new PriorityQueue<>((a, b) -> (a.val - b.val));

        ListNode dummy = new ListNode(0);
        ListNode head = dummy;

        for (int i = 0; i < lists.length; i++){
            if (lists[i] != null){
                heap.offer(lists[i]);
            }
        }

        while (!heap.isEmpty()){
            head.next = heap.poll();
            head = head.next;
            if (head.next != null){
                heap.offer(head.next);
            }
        }
        return dummy.next;
    }
}
