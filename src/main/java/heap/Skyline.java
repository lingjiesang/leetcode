package heap;

import java.util.*;

public class Skyline {
    class Wall {
        int xPos;
        int height;
        boolean isStart;

        Wall(int xPos, int height, boolean isStart) {
            this.xPos = xPos;
            this.height = height;
            this.isStart = isStart;
        }
    }

    public List<int[]> getSkyline(int[][] buildings) {
        List<int[]> skylines = new ArrayList<>();

        List<Wall> walls = new ArrayList<>();

        for (int i = 0; i < buildings.length; i++) {
            walls.add(new Wall(buildings[i][0], buildings[i][2], true));
            walls.add(new Wall(buildings[i][1], buildings[i][2], false));
        }

        Collections.sort(walls, new Comparator<Wall>() {
            @Override
            public int compare(Wall w1, Wall w2) {
                if (w1.xPos - w2.xPos != 0) {
                    return w1.xPos - w2.xPos;
                } else {
                    if (w1.isStart && w2.isStart) {
                        return w2.height - w1.height; // add higher one first!
                    }
                    if (!w1.isStart && !w2.isStart) {
                        return w1.height - w2.height; // drop shorter one first!
                    }
                    if (w1.isStart) {
                        return -1; //add w1(start) before drop w2(end)
                    } else {
                        return 1;
                    }
                }
            }
        });

        PriorityQueue<Integer> pq = new PriorityQueue<>(Collections.reverseOrder());

        pq.offer(0);

        for (Wall wall : walls) {
            int prevHeight = pq.peek();
            if (wall.isStart) {
                pq.offer(wall.height);
            } else {
                pq.remove(wall.height);
            }
            if (pq.peek() != prevHeight) {
                skylines.add(new int[]{wall.xPos, pq.peek()});
            }
        }
        return skylines;
    }

}
