package heap;


public class MergeKSortedLists_NoHeap {
    public class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
        }
    }

    public ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0){
            return null;
        }

        return _mergeKList(lists, 0, lists.length - 1);
    }

    private ListNode _mergeKList(ListNode[] lists, int start, int end){
        if (start == end){
            return lists[start];
        }
        return merge(_mergeKList(lists, start, (start + end) / 2), _mergeKList(lists, (start + end) / 2 + 1, end));
    }

    private ListNode merge(ListNode l1, ListNode l2){
        if (l1 == null){
            return l2;
        }
        if (l2 == null){
            return l1;
        }
        if (l1.val < l2.val){
            l1.next = merge(l1.next, l2);
            return l1;
        } else{
            l2.next = merge(l2.next, l1);
            return l2;
        }
    }
}
