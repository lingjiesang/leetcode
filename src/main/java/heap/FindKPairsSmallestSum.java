package heap;

import java.util.*;

public class FindKPairsSmallestSum {
    public List<int[]> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        List<int[]> result = new ArrayList<>();
        if (nums1.length == 0 || nums2.length == 0){
            return result;
        }
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>(){
            public int compare(int[] pair1, int[] pair2){
                return nums1[pair1[0]] + nums2[pair1[1]] - nums1[pair2[0]] - nums2[pair2[1]];
            }
        });
        for (int i = 0; i < nums1.length && i < k; i++){
            pq.add(new int[]{i, 0});
        }

        while (!pq.isEmpty() && result.size() < k){
            int[] pair = pq.poll();
            result.add(new int[]{nums1[pair[0]], nums2[pair[1]]});
            if (pair[1] + 1 < nums2.length){
                pq.add(new int[]{pair[0],pair[1] + 1});
            }
        }
        return result;
    }
}
