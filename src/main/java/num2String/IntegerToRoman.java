package num2String;

public class IntegerToRoman {
    public String intToRoman(int num) {
        int[] nums =  {  1,  5, 10, 50, 100, 500, 1000};
        char[] chars = {'I','V','X','L','C', 'D', 'M'};
        StringBuilder sb = new StringBuilder();

        for (int i = nums.length - 1; i >=0; ){
            // nums[i] start with "1"
            if (num / nums[i] > 0){
                if (i != nums.length - 1 && num / nums[i] == 4){
                    sb.append(chars[i]).append(chars[i + 1]);
                } else {
                    for (int k = 0; k < num / nums[i]; k++){
                        sb.append(chars[i]);
                    }
                }
                num -= nums[i] * (num / nums[i]);
            }
            i--;
            // nums[i] start with "5"
            if (i > 0 && num / nums[i] > 0){
                if (num / nums[i - 1] == 9){
                    sb.append(chars[i - 1]).append(chars[i + 1]);
                    num -= 9 * nums[i - 1];
                } else {
                    sb.append(chars[i]);
                    num -= nums[i];
                }
            }
            i--;
        }

        return sb.toString();
    }
}
