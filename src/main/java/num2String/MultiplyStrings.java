package num2String;

import java.util.*;

public class MultiplyStrings {
    /**
     * method 1:
     * 123 * 456 = 3 * 456 + 20 * 456 + 100 * 456
     * slow....
     */
    public String multiply(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) { // don't forget this!
            return "0";

        }
        StringBuilder result = new StringBuilder();

        for (int i = num1.length() - 1; i >= 0; i--) {
            if (num1.charAt(i) == '0') {
                continue;
            }
            StringBuilder sb = new StringBuilder();
            for (int j = i; j < num1.length() - 1; j++) {
                sb.append('0');
            }
            int carrier = 0;
            for (int j = num2.length() - 1; j >= 0; j--) {
                carrier += (num1.charAt(i) - '0') * (num2.charAt(j) - '0');
                sb.append((char) (carrier % 10 + '0'));
                carrier /= 10;
            }
            while (carrier > 0) {
                sb.append((char) (carrier % 10 + '0'));
                carrier /= 10;
            }
            result = add(result, sb);
        }
        return result.reverse().toString();
    }

    /**
     * here num1 and num2 are in reverse direciton, i.e. number 7200 is represented by string "0027"
     */
    private StringBuilder add(StringBuilder num1, StringBuilder num2) {
        int carrier = 0;
        int pos = 0;
        while (pos < num1.length() || pos < num2.length() || carrier > 0) {
            carrier += (pos < num1.length() ? num1.charAt(pos) - '0' : 0) + (pos < num2.length() ? num2.charAt(pos) - '0' : 0);
            if (pos < num1.length()) { // key!! don't forget to check this!
                num1.setCharAt(pos, (char) (carrier % 10 + '0'));
            } else {
                num1.append((char) (carrier % 10 + '0'));
            }
            carrier /= 10;
            pos++;
        }
        return num1;
    }

    /**
     * method 2: much faster
     */

    public String multiply_method2(String num1, String num2) {
        int m = num1.length();
        int n = num2.length();
        StringBuilder result = new StringBuilder();
        int carry = 0;
        for (int i = 0; i < m + n; i++) { // result 的右边数第i位
            for (int j = 0; j < m && j <= i; j++) { // j从0到i,当然也不能超过m
                if (n - 1 >= i - j) {
                    int factor1 = num1.charAt(m - 1 - j) - '0'; // num1的从右边数第j位
                    int factor2 = num2.charAt(n - 1 - (i - j)) - '0'; // num2的从右边数第i - j位
                    carry += factor1 * factor2;
                }
            }
            result.append(carry % 10);
            carry /= 10;
        }

        for (int i = m + n - 1; i > 0; i--){
            if (result.charAt(i) == '0'){
                result.setLength(i);
            } else{
                break;
            }
        }
        return result.length() == 0 ? "0" : result.reverse().toString();
    }
}
