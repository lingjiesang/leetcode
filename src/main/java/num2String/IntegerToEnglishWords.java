package num2String;


public class IntegerToEnglishWords {
    String[] subten = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight","Nine"};
    String[] subtwenty = {"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen","Nineteen" };
    String[] tens = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
    String[] units = { "", "Thousand", "Million","Billion"};

    public String numberToWords(int num) {
        if (num == 0){
            return "Zero";
        }

        int[] threeDigits = new int[4];
        for (int i = 0; i <= 3; i++){
            threeDigits[i] = num % 1000;
            num /= 1000;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 3; i >= 0; i--){
            if (threeDigits[i] != 0){
                sb.append(threeDigitToString(threeDigits[i])).append(units[i]).append(" ");
            }
        }
        return sb.toString().trim();

    }

    private String threeDigitToString(int num){
        StringBuilder sb = new StringBuilder("");
        int[] digits = new int[3];
        for (int i = 0; i <= 2; i++){
            digits[i] = num % 10;
            num /= 10;
        }
        if (digits[2] > 0){
            sb.append(subten[digits[2] - 1]).append(" Hundred ");
        }
        if (digits[1] == 1) {
            sb.append(subtwenty[digits[0]]).append(" ");
        } else{
            if (digits[1] > 1){
                sb.append(tens[digits[1] - 2]).append(" ");
            }
            if (digits[0] > 0){
                sb.append(subten[digits[0] - 1]).append(" ");
            }
        }
        return sb.toString();

    }
}
