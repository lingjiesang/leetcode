package num2String;

import java.util.*;

public class RomanToInteger {
    /**
     * I, V, X, L, C, D, M
     */
    private int romanToVal(char c){
        switch (c){
            case 'I' : return 1;
            case 'V' : return 5;
            case 'X' : return 10;
            case 'L' : return 50;
            case 'C' : return 100;
            case 'D' : return 500;
            case 'M' : return 1000;
        }
        return 0;
    }
    public int romanToInt(String s) {
        int result = 0;
        int last = 0;
        for (int i = 0; i < s.length(); i++){
            int curt = romanToVal(s.charAt(i));
            if (last >= curt){
                result += last;
            } else{
                result -= last;
            }
            last = curt;
        }
        return result + last;
    }
}
