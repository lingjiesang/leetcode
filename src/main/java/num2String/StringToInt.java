package num2String;

import java.util.*;

public class StringToInt {
    /**
     * corner cases:
     * 1. whitespaces: trimed if in the beginning/end of str, break if in the middle
     * 2. +/- in the beginning: take as sign
     * 3. non number chars : break if in the middle, just return whatever can be interpreted
     * 4. overflow: give the boundary values
     */
    public int myAtoi(String str) {
        str = str.trim();
        if (str.length() == 0){
            return 0;
        }
        long result = 0;
        boolean isNeg = false;
        for (int i = 0; i < str.length(); i++){
            if (i == 0 && (str.charAt(i) == '+' || str.charAt(i) == '-')){
                isNeg = str.charAt(i) == '-';
            } else if (str.charAt(i) >= '0' && str.charAt(i) <= '9'){
                result = result * 10 + str.charAt(i) - '0';
                if (result > Integer.MAX_VALUE){
                    break;
                }
            } else{
                break;
            }
        }
        if (isNeg){
            result = 0 - result;
        }
        if (result > Integer.MAX_VALUE){
            return Integer.MAX_VALUE;
        } else if (result < Integer.MIN_VALUE){
            return Integer.MIN_VALUE;
        }
        return (int)result;

    }
}
