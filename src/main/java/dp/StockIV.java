package dp;

public class StockIV {
    public int maxProfit(int k, int[] prices) {
        if (k == 0 || prices == null || prices.length == 0) {
            return 0;
        }
        if (k > prices.length / 2) {
            return getAllProfit(prices);
        }
        // profit[i][j][0]: at the end of i-th day, finish at most j transaction, NOT holding stock, max profit
        // profit[i][j][1]: at the end of i-th day, finish at most j transaction,     holding stock, max profit
        // j = 0 ... k

        int[][][] profit = new int[2][k + 1][2];

        int lowest = prices[0];
        for (int i = 1; i < prices.length; i++) {
            lowest = Math.min(lowest, prices[i]);
            //profit[i % 2][0][0] = 0;
            profit[i % 2][0][1] = prices[i] - lowest;
            for (int j = 1; j <= k; j++) {
                profit[i % 2][j][0] = Math.max(profit[(i - 1) % 2][j][0], profit[(i - 1) % 2][j - 1][1] + prices[i] - prices[i - 1]);
                profit[i % 2][j][1] = Math.max(profit[(i - 1) % 2][j][0], profit[(i - 1) % 2][j][1] + prices[i] - prices[i - 1]);
            }
        }
        return profit[(prices.length - 1) % 2][k][0];
    }

    private int getAllProfit(int[] prices) {
        int profit = 0;
        for (int i = 1; i < prices.length; i++) {
            profit += Math.max(0, prices[i] - prices[i - 1]);
        }
        return profit;
    }


    // ref: https://discuss.leetcode.com/topic/26169/clean-java-dp-solution-with-comment

    /**
     * dp[i, j] represents the max profit up until prices[j] using at most i transactions.
     * dp[i, j] = max(dp[i, j-1], prices[j] - prices[jj] + dp[i-1, jj]) { jj in range of [0, j-1] }
     * = max(dp[i, j-1], prices[j] + max(dp[i-1, jj] - prices[jj]))
     * dp[0, j] = 0; 0 transactions makes 0 profit
     * dp[i, 0] = 0; if there is only one price data point you can't make any transaction.
     */

    public int maxProfit_concise(int k, int[] prices) {
        int n = prices.length;
        if (n <= 1)
            return 0;

        if (k > n / 2) {
            int maxPro = 0;
            for (int i = 1; i < n; i++) {
                if (prices[i] > prices[i - 1])
                    maxPro += prices[i] - prices[i - 1];
            }
            return maxPro;
        }

        int[][] dp = new int[k + 1][n];
        for (int i = 1; i <= k; i++) {
            int localMax = - prices[0];
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.max(dp[i][j - 1], prices[j] + localMax);
                localMax = Math.max(localMax, dp[i - 1][j - 1] - prices[j]);
            }
        }
        return dp[k][n - 1];
    }
}