package dp;

import java.util.*;

public class LongestIncreasingSubsequences {
    public int lengthOfLIS(int[] nums){
        int[] dp = new int[nums.length];
        int len = 0;
        for (int i = 0; i <  nums.length; i++){
            int pos = Arrays.binarySearch(dp, 0, len, nums[i]);
            //System.out.println(i + " " + pos);
            if (pos < 0){
                pos = -(pos + 1); // if not found, return -(insertionposition) - 1
            }
            dp[pos] = nums[i];
            if (pos == len){
                len++;
            }
            //System.out.println(Arrays.toString(dp));
        }
        return len;
    }
}
