package dp;

import java.util.*;

public class PaintFence {
    public int numWays(int n, int k) {
        if (k == 1 && n > 2 || n == 0){
            return 0;
        }
        if (n == 1){
            return k;
        }
        if (n == 2){
            return k * k;
        }
        int prevSameColor = k, prevDiffColor = k * (k - 1);
        for (int i = 3; i <= n; i++){
            int curtSameColor = prevDiffColor;
            int curtDiffColor = prevDiffColor * (k - 1) + prevSameColor * (k - 1);
            prevSameColor = curtSameColor;
            prevDiffColor = curtDiffColor;
        }
        return prevSameColor + prevDiffColor;
    }
}
