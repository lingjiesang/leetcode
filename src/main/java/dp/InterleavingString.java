package dp;


public class InterleavingString {
    public boolean isInterleave(String s1, String s2, String s3) {
        if (s1 == null || s2 == null || s3 == null){
            return false;
        }
        if (s1.length() + s2.length() != s3.length()) {
            return false;
        }

        // isSatisfied[i][j] : s3[0 ... i+j) is interleave of s1[0...i) and s2[0 ...j)
        boolean isSatisfied[][] = new boolean[s1.length() + 1][s2.length() + 1];
        isSatisfied[0][0] = true;

        for (int i = 1; i <= s1.length(); i++){
            isSatisfied[i][0] = isSatisfied[i-1][0] && s3.charAt(i - 1) == s1.charAt(i - 1);
            if (!isSatisfied[i][0]){
                break;
            }
        }

        for (int j = 1; j <= s2.length(); j++){
            isSatisfied[0][j] = isSatisfied[0][j - 1] && s3.charAt(j - 1) == s2.charAt(j - 1);
            if (!isSatisfied[0][j]){
                break;
            }
        }

        for (int i = 1; i <= s1.length(); i++){
            for (int j = 1; j <= s2.length() ; j++){
                isSatisfied[i][j] = isSatisfied[i - 1][j] && s3.charAt(i + j - 1) == s1.charAt(i - 1)
                                || isSatisfied[i][j - 1] && s3.charAt(i + j - 1) == s2.charAt(j - 1);
            }
        }

        return isSatisfied[s1.length()][s2.length()];
    }

}
