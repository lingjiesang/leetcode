package dp;

import java.util.*;

public class NumMatrix {
    int[][] mat;
    int m;
    int n;

    public NumMatrix(int[][] matrix) {
        if (matrix.length != 0 && matrix[0].length != 0) {
            m = matrix.length;
            n = matrix[0].length;
            mat = new int[m][n];
            mat[0][0] = matrix[0][0];
            for (int j = 1; j < n; j++) {
                mat[0][j] = mat[0][j - 1] + matrix[0][j];
            }
            for (int i = 1; i < m; i++) {
                mat[i][0] = mat[i - 1][0] + matrix[i][0];
                for (int j = 1; j < n; j++) {
                    mat[i][j] = mat[i][j - 1] + mat[i - 1][j] - mat[i - 1][j - 1] + matrix[i][j];
                }
            }
        }

    }

    public int sumRegion(int row1, int col1, int row2, int col2) {
        if (0 <= row1 && row1 <= row2 && row2 < m && 0 <= col1 && col1 <= col2 && col2 < n) {
            return mat[row2][col2] - (row1 > 0 ? mat[row1 - 1][col2] : 0)
                                   - (col1 > 0 ? mat[row2][col1 - 1] : 0)
                                   + (row1 > 0 && col1 > 0 ? mat[row1 - 1][col1 - 1] : 0);
        } else {
            return 0;
        }
    }
}


// Your NumMatrix object will be instantiated and called as such:
// NumMatrix numMatrix = new NumMatrix(matrix);
// numMatrix.sumRegion(0, 1, 2, 3);
// numMatrix.sumRegion(1, 2, 3, 4);
