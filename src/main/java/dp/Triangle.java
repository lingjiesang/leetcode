package dp;

import java.util.*;

public class Triangle {
    public int minimumTotal(List<List<Integer>> triangle) {
        if (triangle.size() == 0){
            return 0;
        }
        int[] sums = new int[triangle.size()];
        sums[0] = triangle.get(0).get(0);

        for (int row = 1; row < triangle.size(); row++){
            int[] newSums = new int[triangle.size()];
            for (int col = 0; col <= row; col++){
                newSums[col] = Math.min(col > 0 ? sums[col - 1] : Integer.MAX_VALUE,
                                        col < row ? sums[col] : Integer.MAX_VALUE);
                newSums[col] += triangle.get(row).get(col);
            }
            sums = newSums;
        }

        int min = sums[0];
        for (int sum : sums){
            min = Math.min(min, sum);
        }
        return min;
    }
}
