package dp;

import java.util.*;

public class UniquePaths {
    /**
     * I
     */
    public int uniquePaths(int m, int n) {
        if (m == 0 || n == 0){
            return 0;
        }
        int[] unique = new int[n];
        Arrays.fill(unique, 1);
        for (int i = 1; i < m; i++){
            unique[0] = 1;
            for (int j = 1; j < n; j++){
                unique[j] = unique[j - 1] + unique[j];
            }
        }
        return unique[n - 1];
    }

    /**
     * II
     */
    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid.length == 0 || obstacleGrid[0].length == 0){
            return 0;
        }
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;

        int[] unique = new int[n];
        for (int j = 0; j < n; j++){
            if (obstacleGrid[0][j] == 0){
                unique[j] = 1;
            } else{
                break;
            }
        }

        for (int i = 1; i < m; i++){
            if (unique[0] == 1 && obstacleGrid[i][0] == 0){
                unique[0] = 1;
            } else{
                unique[0] = 0;
            }
            for (int j = 1; j < n; j++){
                if (obstacleGrid[i][j] == 1){
                    unique[j] = 0;
                } else{
                    unique[j] = unique[j - 1] + unique[j];
                }
            }
        }
        return unique[n - 1];
    }
}
