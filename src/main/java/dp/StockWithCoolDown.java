package dp;

import java.util.*;

public class StockWithCoolDown {

    public int maxProfit_TLE(int[] prices) {
        if (prices.length == 0){
            return 0;
        }
        int n = prices.length;


        int[][] dp = new int[n][n];
        for (int step = 1; step < n; step++){
            for (int i = 0; i < n - step; i++){
                dp[i][i + step] = Math.max(0, prices[i + step] - prices[i]);
                for (int k = i; k <= i + step; k++){
                    int leftProfit = k - 1 > i ? dp[i][k - 1] : 0;
                    int rightProfit = k + 1 < i + step ? dp[k + 1][i + step] : 0;
                    dp[i][i + step] = Math.max(leftProfit + rightProfit, dp[i][i + step]);
                }
            }
        }
        return dp[0][n - 1];
    }


    public int maxProfit(int[] prices) {
        if (prices.length <= 1){
            return 0;
        }
        int[] hold = new int[]{0, 0}; // hold a share at day i (bought on day i or before)
        int[] sold = new int[]{0, 0}; // sell a share at day i
        int[] cool = new int[]{0, 0}; // in cooldown at day i
        for (int i = 1; i < prices.length; i++){
            hold[i % 2] = Math.max(hold[(i - 1) % 2] + prices[i] - prices[i - 1], cool[(i - 1) % 2]);
            sold[i % 2] = hold[(i - 1) % 2] + prices[i] - prices[i - 1];
            cool[i % 2] = Math.max(sold[(i - 1) % 2], cool[(i - 1) % 2]);
        }
        int max = Math.max(cool[(prices.length - 1) % 2], sold[(prices.length - 1) % 2]);
        return max;
    }

}
