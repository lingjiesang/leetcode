package dp;

import java.util.*;

public class RussianDollEnvelopes {
    public int maxEnvelopes(int[][] envelopes){
//        Arrays.sort(envelopes, (a, b) ->{
//            if (a[0] != b[0]){
//                return a[0] - b[0];
//            } else{
//                return b[1] - a[1];
//            }
//        });


        // explict comparator is much faster than lambda!
        Arrays.sort(envelopes, new Comparator<int[]>(){
            public int compare(int[] arr1, int[] arr2){
                if(arr1[0] == arr2[0])
                    return arr2[1] - arr1[1];
                else
                    return arr1[0] - arr2[0];
            }
        });

        int dp[] = new int[envelopes.length + 1];
        int len = 0;
        for (int i = 0; i < envelopes.length; i++){
            int pos = Arrays.binarySearch(dp, 0, len, envelopes[i][1]);
            if (pos < 0){
                pos = -(pos + 1);
            }
            dp[pos] = envelopes[i][1];
            if (pos == len){
                len++;
            }
        }
        return len;
    }
}


