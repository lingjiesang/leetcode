package dp;

import java.util.*;

public class PaintHouse {
    public int minCost(int[][] costs) {
        int n = costs.length;
        if (n == 0) {
            return 0;
        }
        for (int i = 1; i < n; i++) {
            costs[i][0] += Math.min(costs[i - 1][1], costs[i - 1][2]);
            costs[i][1] += Math.min(costs[i - 1][0], costs[i - 1][2]);
            costs[i][2] += Math.min(costs[i - 1][0], costs[i - 1][1]);
        }
        return Math.min(Math.min(costs[n - 1][0], costs[n - 1][1]), costs[n - 1][2]);
    }

    /**
     * not destroy costs...
     * ref:https://discuss.leetcode.com/topic/32408/share-my-very-simple-java-solution-with-explanation
     */
    public int minCost_method2(int[][] costs) {
        if (costs.length == 0) return 0;
        int last0 = costs[0][0];
        int last1 = costs[0][1];
        int last2 = costs[0][2];
        for (int i = 1; i < costs.length; i++) {
            int cur0 = Math.min(last1, last2) + costs[i][0];
            int cur1 = Math.min(last0, last2) + costs[i][1];
            int cur2 = Math.min(last0, last1) + costs[i][2];
            last0 = cur0;
            last1 = cur1;
            last2 = cur2;
        }
        return Math.min(Math.min(last0, last1), last2);
    }



    public int minCostII(int[][] costs) {
        if (costs.length == 0 || costs[0].length == 0){
            return 0;
        }
        int n = costs.length;
        int k = costs[0].length;
        int[] lastCosts = new int[k];
        for (int j = 0; j < k; j++){
            lastCosts[j] = costs[0][j];
        }
        for (int i = 1; i < n; i++){
            int min1 = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE, min1_index = -1;
            for (int j = 0; j < k; j++){
                if (lastCosts[j] < min1){
                    min2 = Math.min(min2, min1); // original min1 might become min2
                    min1 = lastCosts[j];
                    min1_index = j;
                } else{
                    min2 = Math.min(min2, lastCosts[j]);
                }
            }
            for (int j = 0; j < k; j++){
                if (j != min1_index){
                    lastCosts[j] = costs[i][j] + min1;
                } else{
                    lastCosts[j] = costs[i][j] + min2;
                }
            }
        }
        int minCost = Integer.MAX_VALUE;
        for (int j = 0; j < k; j++){
            minCost = Math.min(minCost, lastCosts[j]);
        }
        return minCost;
    }


    public int minCostII_Cleaner(int[][] costs) {
        if (costs.length == 0 || costs[0].length == 0){
            return 0;
        }
        int n = costs.length;
        int k = costs[0].length;

        int min1 = 0, min2 = 0, min1_index = 0;
        for (int i = 0; i < n; i++){
            int newMin1 = Integer.MAX_VALUE, newMin2 = Integer.MAX_VALUE, newMin1_Index = 0;
            for (int j = 0; j < k; j++){
                int costJ = (j != min1_index ? costs[i][j] + min1 : costs[i][j] + min2);
                if (costJ < newMin1){
                    newMin2 = Math.min(newMin2, newMin1);
                    newMin1 = costJ;
                    newMin1_Index = j;
                } else{
                    newMin2 = Math.min(newMin2, costJ);
                }
            }
            min1 = newMin1;
            min2 = newMin2;
            min1_index = newMin1_Index;
        }
        return min1;
    }
    public static void main(String[] args){
        PaintHouse soln = new PaintHouse();
        System.out.println(soln.minCostII_Cleaner(new int[][]{{1, 5, 3}, {2, 9, 4}}));
    }
}
