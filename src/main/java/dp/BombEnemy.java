package dp;

public class BombEnemy {
    public int maxKilledEnemies(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;

        int rowEnemy = 0;
        int[] colEnemy = new int[n];
        int maxEnemy = 0;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (grid[i][j] == 'W'){
                    continue;
                }
                if (j == 0 || grid[i][j - 1] == 'W'){
                    rowEnemy = updateEnemy(grid, i, j, true);
                }
                if (i == 0 || grid[i - 1][j] == 'W'){
                    colEnemy[j] = updateEnemy(grid, i, j, false);
                }
                if (grid[i][j] == '0') {
                    maxEnemy = Math.max(maxEnemy, rowEnemy + colEnemy[j]);
                }
            }
        }
        return maxEnemy;
    }

    private int updateEnemy(char[][] grid, int i, int j, boolean updateRow){
        int enemyNum = 0;
        while (i < grid.length && j < grid[0].length){
            if (grid[i][j] == 'W'){
                break;
            } else if (grid[i][j] == 'E'){
                enemyNum++;
            }
            if (updateRow) {
                j++;
            } else{
                i++;
            }
        }
        return enemyNum;
    }
}
