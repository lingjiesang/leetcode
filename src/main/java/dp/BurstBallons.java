package dp;

import java.util.*;

//ref: https://discuss.leetcode.com/topic/30746/share-some-analysis-and-explanations
public class BurstBallons {
    public int maxCoins(int[] nums) {

        if (nums.length == 0){
            return 0;
        }

        int[] numsCopy = new int[nums.length + 2];
        int len = 0;
        numsCopy[len++] = 1;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] != 0){
                numsCopy[len++] = nums[i];
            }
        }
        numsCopy[len++] = 1;

        int[][] dp =  new int[len][len];
        return _maxCoins(numsCopy, dp, 0, len - 1);
    }

    public int _maxCoins(int[] nums, int[][] dp, int start, int end){
        if (dp[start][end] != 0){
            return dp[start][end];
        }
        if (start + 1 == end){
            return 0;
        }
        int max = 0;
        for (int i = start + 1; i < end; i++){
            max = Math.max(_maxCoins(nums, dp, start, i) + _maxCoins(nums, dp, i, end) + nums[start] * nums[i] * nums[end],max);
        }
        dp[start][end] = max; //important!!
        return max;
    }

}
