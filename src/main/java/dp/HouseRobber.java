package dp;

import java.util.*;

public class HouseRobber {
    public int rob(int[] nums) {
        if (nums.length == 0){
            return 0;
        }
        int robLast = nums[0], notRobLast = 0;

        for (int i = 1; i < nums.length; i++){
            int temp = notRobLast;
            notRobLast = Math.max(robLast, notRobLast);
            robLast = temp + nums[i];
        }
        return Math.max(robLast, notRobLast);
    }
}
