package dp;

import java.util.*;

public class HouseRobberII {
    public int rob(int[] nums) {
        if (nums.length <= 3){
            int max = 0;
            for (int num : nums){
                max = Math.max(num, max);
            }
            return max;
        }

        return Math.max(rob(nums, nums[0] + nums[2], nums[0], 3, nums.length - 1),
                        rob(nums, nums[1], 0, 2, nums.length));
    }

    private int rob(int[] nums, int robLast, int notRobLast, int start, int end){
        for (int i = start; i < end; i++){
            int temp = notRobLast;
            notRobLast = Math.max(notRobLast, robLast);
            robLast = temp + nums[i];
        }
        return Math.max(robLast, notRobLast);
    }
}
