package dp;

import java.util.*;

public class CombinationSumIV {
    /**
     * dp
     */
    public int combinationSum4(int[] nums, int target) {
        if (nums.length == 0) {
            return 0;
        }
        int[] combinations = new int[target + 1];

        for (int tar = 0; tar <= target; tar++) {
            for (int num : nums) {
                if (num == tar) {
                    combinations[tar] += 1;
                } else if (num < tar) {
                    combinations[tar] += combinations[tar - num];
                }
            }
        }
        return combinations[target];
    }

    /**
     * using recursion and memorization
     * <p>
     * The DP solution goes through every possible sum from 1 to target one by one.
     * Using recursion can skip those sums that are not the combinations of the numbers in the given array.
     * <p>
     * ref: https://discuss.leetcode.com/topic/52255/java-recursion-solution-using-hashmap-as-memory/2 (yubad2000)
     * https://discuss.leetcode.com/topic/52266/2ms-recursive-java-solution-with-memorization
     */
    public int combinationSum4_method2(int[] nums, int target) {
        if (nums.length == 0) {
            return 0;
        }

        int[] combinations = new int[target + 1];
        Arrays.sort(nums); // sort so we can do "break" in "helper"
        Arrays.fill(combinations, -1);
        return helper(nums, combinations, target);
    }

    private int helper(int[] nums, int[] combinations, int target) {
        if (combinations[target] >= 0) {
            return combinations[target];
        }
        combinations[target] = 0;
        for (int num : nums) {
            if (num < target) {
                combinations[target] += helper(nums, combinations, target - num);
            } else if (num == target) {
                combinations[target] += 1;
                break;
            } else {
                break;
            }
        }
        return combinations[target];
    }

    /**
     * followup: what if negative is allowed?
     * 关键点是如果有负数存在,那么target-num可能是大于target的数,那么它的解在求target的时候是未知的
     * 所以target的解也无法由此通过顺序依次求出...
     *
     * 以下的思路关键是"当只允许再加一个数是,可能性是唯一的,要么是0,要么是1,所以长度递增的recursion肯定是收敛的"
     *
     * In order to allow negative integers, the length of the combination sum needs to be restricted,
     * or the search will not stop.
     * <p>
     * ref: https://discuss.leetcode.com/topic/52290/java-follow-up-using-recursion-and-memorization (yubad2000)
     */
    Map<Integer, Map<Integer, Integer>> map = new HashMap<>();


    public int combinationSum4_followup(int[] nums, int target, int MaxLen) {
        if (nums == null || nums.length == 0 || MaxLen <= 0) return 0;
        map = new HashMap<>();
        return helper2(nums, 0, target, MaxLen);
    }

    private int helper2(int[] nums, int len, int target, int MaxLen) {
        int count = 0;
        if (len > MaxLen) {
            return 0;
        }
        if (map.containsKey(target) && map.get(target).containsKey(len)) {
            return map.get(target).get(len);
        }
        if (target == 0) {
            count++;
        }
        for (int num : nums) {
            count += helper2(nums, len + 1, target - num, MaxLen);
        }
        if (!map.containsKey(target)) {
            map.put(target, new HashMap<>());
        }
        map.get(target).put(len, count);
        return count;
    }
}
