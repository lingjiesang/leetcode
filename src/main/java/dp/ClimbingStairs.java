package dp;

import java.util.*;

public class ClimbingStairs {
    public int climbStairs(int n) {
        if (n <= 2){
            return n;
        }

        int prevPrev = 1;
        int prev = 2;

        for (int i = 3; i <= n; i++){
            int temp = prev;
            prev = prev +  prevPrev;
            prevPrev = temp;
        }
        return prev;
    }
}
