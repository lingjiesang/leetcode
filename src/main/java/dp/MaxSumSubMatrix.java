package dp;

import  java.util.*;
/**
 * ref: https://discuss.leetcode.com/topic/48854/java-binary-search-solution-time-complexity-min-m-n-2-max-m-n-log-max-m-n
 */
public class MaxSumSubMatrix {
    public int maxSumSubmatrix(int[][] matrix, int maxAreaLimit) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }
        int m = Math.min(matrix.length, matrix[0].length);
        int n = Math.max(matrix.length, matrix[0].length);
        boolean moreRowsThanCols = matrix.length > matrix[0].length;

        int maxArea = Integer.MIN_VALUE;
        for (int i = 0; i < m; i++){
            int[] stripeSum = new int[n];  //strip means partial of a column or row (if moreRowsThanCols)

            for (int j = i; j >= 0; j--){
                TreeSet<Integer> prevAreas = new TreeSet<>();
                prevAreas.add(0);
                int area = 0;
                for (int k = 0; k < n; k++){
                    stripeSum[k] += moreRowsThanCols ? matrix[k][j] : matrix[j][k];
                    area += stripeSum[k];
                    /**
                     * subtract >= area - maxAreaLimit  (smallest value over maxArea - area)
                     * so area - subtract <= maxAreaLimit
                     */
                    Integer subtractArea = prevAreas.ceiling(area - maxAreaLimit);
                    if (subtractArea != null){
                        maxArea = Math.max(maxArea, area - subtractArea);
                    }
                    prevAreas.add(area);
                }
            }
        }
        return maxArea;

    }
}
