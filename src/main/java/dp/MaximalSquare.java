package dp;

import java.util.*;

public class MaximalSquare {
    public int maximalSquare(char[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }
        int rowNum = matrix.length;
        int colNum = matrix[0].length;
        int maxSideLen = 0;
        int[][] square = new int[rowNum][colNum];
        int[] onesInCol = new int[colNum];

        for (int i = 0; i < rowNum; i++){
            int leftOnes = 0;
            for (int j = 0; j < colNum; j++){
                if (matrix[i][j] == '1'){
                    square[i][j] = Math.min(leftOnes, onesInCol[j]);
                    square[i][j] = Math.min(square[i][j], i > 0 && j > 0 ? square[i - 1][j - 1] : 0);
                    square[i][j]++;
                    maxSideLen = Math.max(maxSideLen, square[i][j]);
                    leftOnes++;
                    onesInCol[j]++;
                } else{
                    square[i][j] = 0;
                    leftOnes = 0;
                    onesInCol[j] = 0;
                }
            }
        }
        return maxSideLen * maxSideLen;
    }


    public int maximalSquare_simpler(char[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        int[][] square = new int[m][n];
        int maxSideLen = 0;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (matrix[i][j] == '1'){
                    int a = i - 1 >= 0 ? square[i - 1][j] : 0;
                    int b = j - 1 >= 0 ? square[i][j - 1] : 0;
                    int c = i - 1 >= 0 && j - 1 >= 0 ? square[i - 1][j - 1] : 0;
                    square[i][j] = Math.min(a, Math.min(b, c)) + 1;
                    maxSideLen = Math.max(maxSideLen, square[i][j]);
                } else{
                    square[i][j] = 0;
                }
            }
        }
        return maxSideLen * maxSideLen;
    }
}
