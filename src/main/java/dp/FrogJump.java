package dp;

import java.util.*;

public class FrogJump {
    public boolean canCross(int[] stones) {
        if (stones.length <= 1) {
            return true;
        }
        int n = stones.length;
        int[] minNextStep = new int[n];
        int[] maxNextStep = new int[n];
        Arrays.fill(minNextStep, Integer.MAX_VALUE);
        minNextStep[0] = 1;
        maxNextStep[0] = 1;
        for (int i = 0; i < n; i++) {
            if (minNextStep[i] > maxNextStep[i]) {
                return false;
            }
            int j = i + 1;
            for (int k = Math.max(1, minNextStep[i]); k <= maxNextStep[i]; k++) {
                if (j >= n) {
                    break;
                }
                if (stones[i] + k < stones[j]) {
                    continue;
                } else if (stones[i] + k == stones[j]) {
                    minNextStep[j] = Math.min(minNextStep[j], k - 1);
                    maxNextStep[j] = Math.max(maxNextStep[j], k + 1);
                    j++;
                } else {
                    j++;
                }
            }
        }
        return true;
    }


    private int[] stones;

    public boolean canCross_method2(int[] stones) {
        this.stones = stones;
        return doJump(0, 0);
    }

    private boolean doJump(int nowPosition, int lastJump) {
        if (nowPosition == stones.length - 1) {
            return true;
        } else if (nowPosition >= stones.length) {
            return false;
        } else {
            for (int i = nowPosition + 1; i < stones.length; i++) {
                int willJump = stones[i] - stones[nowPosition];
                if (willJump == lastJump + 1 || willJump == lastJump || willJump == lastJump - 1) {
                    if (doJump(i, willJump)) {
                        return true;
                    } else if (willJump > lastJump) {
                        return false;
                    }
                }
            }
            return false;
        }
    }
}
