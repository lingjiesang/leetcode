package dp;

import java.util.*;

public class DecodeWays {
    /**
     * my own thoughts on 8/9/2016
     */
    int[] dp;
    public int numDecodings(String s) {
        if (s == null || s.length() == 0){
            return 0;
        }
        char[] ss = s.toCharArray();
        dp = new int[ss.length]; // dp[i]: num of coding from i to end
        Arrays.fill(dp, -1);
        return decoding(ss, 0);
    }

    private int decoding(char[] ss, int start){
        if (start == ss.length){
            return 1;
        }
        if (dp[start] >= 0){
            return dp[start];
        }
        if (start == ss.length - 1){
            dp[start] = ss[start] == '0' ? 0 : 1;
        } else{
            if (ss[start] == '0'){
                dp[start] = 0;
            } else{
                if (ss[start] == '1' || ss[start] == '2' && ss[start + 1] <= '6'){
                    dp[start] = decoding(ss, start + 1) + decoding(ss, start + 2);
                } else{
                    dp[start] = decoding(ss, start + 1);
                }
            }
        }
        return dp[start];
    }

    /**
     * more succinct way to write this!
     * ref: https://discuss.leetcode.com/topic/7025/a-concise-dp-solution
     * similar to my previous submission in c++
     */
    public int numDecodings_MoreSuccinct(String s){
        if (s == null || s.length() == 0 || s.charAt(0) == '0'){
            return 0;
        }

        int r1 = 1, r2 = 1;
        // r1: # of coding s[0...i-1]
        // r2: # of coding s[0...i-2]
        for (int i = 1; i < s.length(); i++){
            if (s.charAt(i) == '0'){
                r1 = 0;
            }
            int r1New;
            int r2New = r1;
            if (s.charAt(i - 1) == '1' || s.charAt(i - 1) == '2' && s.charAt(i) <= '6'){
                r1New = r1 + r2;
            } else{
                r1New = r1;
            }
            r1 = r1New;
            r2 = r2New;
        }
        return r1;
    }
}
