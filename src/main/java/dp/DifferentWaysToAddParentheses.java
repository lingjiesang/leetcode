package dp;

import java.util.*;

public class DifferentWaysToAddParentheses {
    /**
     * bottom up (2ms)
     */
    public List<Integer> diffWaysToCompute_DP(String input) {
        List<Integer> nums = new ArrayList<>();
        List<Character> ops = new ArrayList<>();
        processInput(input.toCharArray(), nums, ops);
        int n = nums.size();

        List<List<List<Integer>>> dp = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            dp.add(new ArrayList<>());
            for (int j = 0; j < n; j++) {
                dp.get(i).add(new ArrayList<>());
            }
            dp.get(i).get(i).add(nums.get(i));
        }

        for (int step = 1; step < n; step++) {
            for (int start = 0; start + step < n; start++) {
                List<Integer> combinations = dp.get(start).get(start + step);
                for (int mid = start; mid < start + step; mid++) {
                    List<Integer> left = dp.get(start).get(mid);
                    List<Integer> right = dp.get(mid + 1).get(start + step);
                    process(left, right, combinations, ops.get(mid));
                }
            }
        }

        return dp.get(0).get(n - 1);
    }

    private void processInput(char[] input, List<Integer> nums, List<Character> ops) {
        int num = 0;
        for (char c : input) {
            if (c >= '0' && c <= '9') {
                num = num * 10 + c - '0';
            } else {
                nums.add(num);
                ops.add(c);
                num = 0;
            }
        }
        nums.add(num);
    }

    private void process(List<Integer> left, List<Integer> right, List<Integer> result, char op) {
        for (int num1 : left) {
            for (int num2 : right) {
                result.add(eval(num1, num2, op));
            }
        }
    }

    private int eval(int a, int b, char op) {
        if (op == '+') {
            return a + b;
        } else if (op == '-') {
            return a - b;
        } else if (op == '*') {
            return a * b;
        } else {
            return a / b;
        }
    }


    /**
     * top down (8ms)
     */


    public List<Integer> diffWaysToCompute(String input){
        Map<String, List<Integer>> cache = new HashMap<>();
        return diffWaysToCompute_Cached(input, cache);
    }

    private boolean isOp(char c){
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private boolean isNumericOnly(String input){
        for (char c : input.toCharArray()){
            if (isOp(c)){
                return false;
            }
        }
        return true;
    }

    public List<Integer> diffWaysToCompute_Cached(String input, Map<String, List<Integer>> cache) {
        List<Integer> result = new ArrayList<>();
        if (isNumericOnly(input)){
            result.add(Integer.valueOf(input));
        } else {
            for (int i = 0; i < input.length(); i++) {
                if (isOp(input.charAt(i))) {
                    String leftStr = input.substring(0, i);
                    if (!cache.containsKey(leftStr)) {
                        cache.put(leftStr, diffWaysToCompute_Cached(leftStr, cache));
                    }
                    List<Integer> left = cache.get(leftStr);
                    String rightStr = input.substring(i + 1, input.length());
                    if (!cache.containsKey(rightStr)) {
                        cache.put(rightStr, diffWaysToCompute_Cached(rightStr, cache));
                    }
                    List<Integer> right = cache.get(rightStr);

                    for (int num1 : left) {
                        for (int num2 : right) {
                            result.add(eval(num1, num2, input.charAt(i)));
                        }
                    }
                }
            }
        }
        cache.put(input, result);
        return result;
    }


    public static void main(String[] args){
        DifferentWaysToAddParentheses soln = new DifferentWaysToAddParentheses();
        soln.diffWaysToCompute("2-1-1");
    }
}
