package dp;


import java.util.Arrays;

// ref: https://discuss.leetcode.com/topic/32230/share-my-21ms-java-solution-with-comments/2
public class CreateMaximumNumber {
    public int[] maxNumber(int[] nums1, int[] nums2, int k){
        int[] bestSolution = new int[k];

        for (int i = Math.max(0, k - nums2.length); i <= Math.min(k, nums1.length); i++){
            int[] res1 = maxNumFromOneArray(nums1, i);
            int[] res2 = maxNumFromOneArray(nums2, k - i);
            int[] result = new int[k];

            int pos1 = 0, pos2 = 0;
            while (pos1 < res1.length && pos2 < res2.length){
                if (compare(res1, pos1, res2, pos2)){
                    result[pos1 + pos2] = res1[pos1];
                    pos1++;
                } else{
                    result[pos1 + pos2] = res2[pos2];
                    pos2++;
                }
            }

            while (pos1 < res1.length){
                result[pos1 + pos2] = res1[pos1];
                pos1++;
            }

            while (pos2 < res2.length){
                result[pos1 + pos2] = res2[pos2];
                pos2++;
            }

            if (compare(result, 0, bestSolution, 0)){
                bestSolution = result;
            }

            //System.out.println(Arrays.toString(bestSolution));


        }
        return bestSolution;
    }

    private boolean compare(int[] nums1, int start1, int[] nums2, int start2){
        for (; start1 < nums1.length && start2 < nums2.length; start1++, start2++){
            if (nums1[start1] > nums2[start2]) return true;
            if (nums1[start1] < nums2[start2]) return false;
        }
        return start1 < nums1.length;
    }

    private int[] maxNumFromOneArray(int[] nums, int k){
        int[] result = new int[k];
        int pos = 0;

        for (int i = 0; i < nums.length; i++){
            while (pos > 0 && nums.length - i > k - pos && result[pos - 1] < nums[i]){
                pos--;
            }

            if (pos < k){
                result[pos++] = nums[i];
            }

        }
        return result;
    }

    public static void main(String[] args){

        int[] nums1 = {2,5,6,4,4,0};
        int[] nums2 = {7,3,8,0,6,5,7,6,2};
        CreateMaximumNumber soln = new CreateMaximumNumber();
        int[] result = soln.maxNumber(nums1, nums2, 15);
        System.out.println(Arrays.toString(result));

        // output (only compare current head): [7,3,8,2,5,6,4,4,0,0,6,5,7,6,2]
        // expect :                            [7,3,8,2,5,6,4,4,0,6,5,7,6,2,0]

        nums1 = new int[]{3,4,6,5};
        nums2 = new int[]{9,1,2,5,8,3};
        int k = 5;
        System.out.println(Arrays.toString(soln.maxNumber(nums1, nums2, k)));
    }

}
