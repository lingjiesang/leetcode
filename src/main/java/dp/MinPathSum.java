package dp;

import java.util.*;

public class MinPathSum {
    public int minPathSum(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;
        int[][] minSums = new int[m][n];
        minSums[0][0] = grid[0][0];
        for (int j = 1; j < n; j++){
            minSums[0][j] = minSums[0][j - 1] + grid[0][j];
        }

        for (int i = 1; i < m; i++){
            minSums[i][0] = minSums[i - 1][0] +  grid[i][0];
            for (int j = 1; j < n; j++){
                minSums[i][j] = Math.min(minSums[i - 1][j], minSums[i][j - 1]) + grid[i][j];
            }
        }
        return minSums[m - 1][n - 1];
    }
}
