package dp;

import java.util.*;

public class GuessNumberII {
    public int getMoneyAmount(int n) {
        int[][] cost = new int[n + 1][n + 1];

        for (int step = 1; step <= n - 1; step++){
            for (int i = 1; i + step <= n; i++){
                cost[i][i + step] = Integer.MAX_VALUE;
                for (int k = i; k <= i + step; k++){
                    int costLeft = k - 1 > i ? cost[i][k - 1] : 0;
                    int costRight = k + 1 < i + step ? cost[k + 1][i + step] : 0;
                    cost[i][i + step] = Math.min(cost[i][i + step], k + Math.max(costLeft, costRight));
                }
            }
        }
        return cost[1][n];
    }
}
