package dp;

import java.util.*;

public class WiggleSubsequence {
    /**
     * my own answer
     * key is "substitute" and realize we can always including the first element
     *
     */
    public int wiggleMaxLength(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        return Math.max(wiggleMaxLength(nums, true), wiggleMaxLength(nums, false));
    }

    private int wiggleMaxLength(int[] nums, boolean up) {
        int last = nums[0];
        int maxLen = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > last && up || nums[i] < last && !up) {
                maxLen++;
                up = !up;
            }
            last = nums[i];
        }
        return maxLen;
    }

    /**
     * other's result
     *
     * they realize the direction can be determined
     *
     * key is treating "equal elements"
     */
    public int wiggleMaxLength_simpler(int[] nums) {
        if (nums.length <= 1) {
            return nums.length;
        }
        int k = 0;
        while (k < nums.length - 1 && nums[k] == nums[k + 1]) {  //Skips all the same numbers from series beginning eg 5, 5, 5, 1
            k++;
        }
        if (k == nums.length - 1) {
            return 1;
        }
        boolean up =  nums[k + 1] > nums[k];       //To check series starting pattern
        up = !up;
        int maxLen = 2;     // This will track the result of result array

        for (int i = k + 1; i < nums.length - 1; i++) {
            if (up && nums[i + 1] > nums[i] || !up && nums[i + 1] < nums[i]) {
                maxLen++;
                up = !up;
            }
        }
        return maxLen;
    }
}
