package binarySearch;


public class SearchInRotatedSortedArray {
    /**
     * 33. I
     */
    public int search(int[] nums, int target) {
        if (nums.length == 0){
            return -1;
        }
        int start = 0, end = nums.length - 1;
        while (start <= end){
            int mid = start + (end - start) / 2;
            if (nums[mid] == target){
                return mid;
            }
            if (nums[start] > nums[mid]){ // right hand is sorted
                if (nums[mid] < target && target <= nums[end]){
                    start = mid + 1;
                } else{
                    end = mid - 1;
                }
            } else{ // left hand is sorted (nums[start] <= nums[mid])
                if (nums[start] <= target && target < nums[mid]){
                    end = mid - 1;
                } else{
                    start = mid + 1;
                }
            }
        }
        return -1;
    }
    /**
     * 81. II
     * ref: https://discuss.leetcode.com/topic/310/when-there-are-duplicates-the-worst-case-is-o-n-could-we-do-better/2
     * see answer of "1337c0d3r"
     */
    public boolean search_II(int[] nums, int target) {
        if (nums.length == 0){
            return false;
        }
        int start = 0, end = nums.length - 1;
        while (start <= end){
            int mid = start + (end - start) / 2;
            if (nums[mid] == target){
                return true;
            }
            if (nums[start] > nums[mid]){ // right half is sorted
                if (nums[mid] < target && target <= nums[end]){
                    start = mid + 1;
                } else{
                    end = mid - 1;
                }
            } else if (nums[start] < nums[mid]){ //left half is sorted
                if (nums[start] <= target && target < nums[mid]){
                    end = mid - 1;
                } else{
                    start = mid + 1;
                }
            } else{// nums[start] == nums[mid], this clause is what is different from SearchInRotatedArray
                start++;
            }
        }
        return false;
    }
}
