package binarySearch;

public class MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] A, int[] B) {
        // write your code here
        int len = A.length + B.length;
        if (len % 2 == 0) {
            int median1 = findKthElement(A, B, 0, 0, len / 2);
            int median2 = findKthElement(A, B, 0, 0, len / 2 + 1);
            return (median1 + median2) / 2.0;
        } else {
            return findKthElement(A, B, 0, 0, len / 2 + 1) * 1.0;
        }
    }

    /** K is 1-based, see below for 0-based (MORE NATURAL!) **/
    // A[0 ... startA - 1] and B[0 ... startB - 1] must be on the left of the K-th element
    // A[startA] and B[startB] has not be judged!
    // want to find the K-th element in A[startA...end) and B[startB...end)
    // definition of K: array[K - 1] is the K-th element of the array (K >= 1)
    //             i.e.: array[0] is the 1-th element of the array
    private int findKthElement(int[] A, int[] B, int startA, int startB, int K) {
        if (startA >= A.length) {
            return B[startB + K - 1];
        }
        if (startB >= B.length) {
            return A[startA + K - 1];
        }

        if (K == 1) {
            return Math.min(A[startA], B[startB]);
        }

        // from "startA" to "startA + K / 2 - 1", there are totally "K/2" element
        // from "startB" to "startB + K / 2 - 1", there are totally "K/2" element
        // we want to exclude one of these two sets
        int midA = startA + K / 2 - 1 < A.length
                ? A[startA + K / 2 - 1]
                : Integer.MAX_VALUE;

        int midB = startB + K / 2 - 1 < B.length
                ? B[startB + K / 2 - 1]
                : Integer.MAX_VALUE;

        if (midA < midB) {
            return findKthElement(A, B, startA + K / 2, startB, K - K / 2);
        } else {
            return findKthElement(A, B, startA, startB + K / 2, K - K / 2);
        }

    }
}



// update : ignore this....
// alternative:  findKthElement: K is 0-based

class Solution {
    /**
     * @param A: An integer array.
     * @param B: An integer array.
     * @return: a double whose format is *.5 or *.0
     */
    public double findMedianSortedArrays(int[] A, int[] B) {
        // write your code here
        int len = A.length + B.length;
        if (len % 2 == 0){
            int median1 = findKthElement(A, B, 0, 0, len / 2 - 1);
            int median2 = findKthElement(A, B, 0, 0, len / 2 );
            return (median1 + median2) / 2.0;
        } else {
            return findKthElement(A, B, 0, 0, len / 2) * 1.0;
        }

    }

    /** K is 0-based, see above for 1-based (NEATER!) **/
    private int findKthElement(int[] A, int[] B, int startA, int startB, int K){
        if (startA >= A.length){
            return B[startB + K];
        }
        if (startB >= B.length){
            return A[startA + K];
        }


        if (K == 0){
            return Math.min(A[startA], B[startB]);
        }

        int midA = startA + (K + 1)/ 2 - 1  < A.length? A[startA + (K + 1) / 2 - 1] : Integer.MAX_VALUE;

        int midB = startB + (K + 1) / 2 - 1 < B.length ? B[startB + (K + 1) / 2 - 1] : Integer.MAX_VALUE;

        if (midA < midB){
            return findKthElement(A, B, startA + (K + 1)/ 2, startB, K - (K + 1)/ 2);
        } else {
            return findKthElement(A, B, startA, startB + (K + 1) / 2, K - (K + 1) / 2);
        }

    }
}
