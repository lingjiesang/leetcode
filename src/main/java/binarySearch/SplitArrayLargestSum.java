package binarySearch;

import java.util.*;

public class SplitArrayLargestSum {
    /**
     * ref: https://discuss.leetcode.com/topic/61405/dp-java/2
     * TLE
     */
    public int splitArray_DP(int[] nums, int m) {
        int n = nums.length;
        int[] sums = new int[n + 1];

        // sums[i + 1] = nums[0] + ... + num[i], 0 <= i < n
        // sums[j + 1] = nums[0] + ... + nums[j], 0 <= j <= i
        // sums[i + 1] - sums[j + 1] = nums[j + 1] + ... + nums[i], 1 <= j <= i <= n

        for (int i = 1; i <= n; i++) {
            sums[i] = sums[i - 1] + nums[i - 1];
        }

        /**
         * dp[j][s]: divide nums[0] ... nums[j] into s parts, minimized maximum sum, 0 <= j <= n - 1
         * to calc dp[i][s + 1], find j < i where max(dp[j][s], nums[j + 1] +... + nums[i]) is minimized
         */
        int[] dp = new int[n];
        for (int i = 0; i < n; i++) {
            dp[i] = sums[i + 1];
        }

        for (int s = 2; s <= m; s++) {
            int[] dp_new = new int[n];
            for (int i = s - 1; i < n; i++) {
                dp_new[i] = Integer.MAX_VALUE;
                for (int j = i - 1; j >= s - 2; j--) {
                    int t = Math.max(dp[j], sums[i + 1] - sums[j + 1]);
                    if (t <= dp_new[i]) { // using the property that all numbers are positive,
                        // so decreasing j by one fail to give better soln, decreasing more won't
                        // note if do "t < dp_new[i]" instead, will give wrong answer for some question
                        dp_new[i] = t;
                    } else {
                        break;
                    }
                }
            }
            dp = dp_new;
        }
        return dp[n - 1];
    }

    /**
     * binary search, using value
     *
     * ref: https://discuss.leetcode.com/topic/61324/clear-explanation-8ms-binary-search-java/2
     */
    public int splitArray(int[] nums, int m) {
        if (m <= 0 || nums.length < m) {
            return -1; //error
        }

        long max = Integer.MIN_VALUE;
        long sum = 0;
        for (int num : nums) {
            max = Math.max(max, num);
            sum += num;
        }

        long start = max, end = sum;
        while (start < end) {
            long mid = start + (end - start) / 2;
            if (split(nums, mid) > m) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }

        return (int)start;
    }

    private int split(int[] nums, long maxSum) {
        int count = 1;
        long sum = 0;

        for (int i = 0; i < nums.length; i++){
            if (sum + nums[i] <= maxSum){
                sum += nums[i];
            } else{
                count++;
                sum = nums[i];
            }
        }
        return count;
    }
}
