package binarySearch;

import java.util.*;

public class SearchForRange {
    public int[] searchRange(int[] nums, int target){
        int[] result = new int[2];
        if (nums == null){
            Arrays.fill(result, -1);
            return result;
        }
        result[0] = searchFirst(nums, target);
        result[1] = searchLast(nums, target);
        return result;
    }

    public int searchFirst(int[] nums, int target){
        int start = 0, end = nums.length - 1;
        while (start + 1 < end){
            int mid = start + (end - start) / 2;
            if (nums[mid] >= target){
                end = mid;
            } else{
                start = mid + 1;
            }
        }
        if (nums[start] == target){
            return start;
        } else if (nums[end] == target){
            return end;
        } else{
            return -1;
        }

    }

    public int searchLast(int[] nums, int target){
        int start = 0, end = nums.length - 1;
        while (start + 1 < end){
            int mid = start + (end - start) / 2;
            if (nums[mid] <= target){
                start = mid;
            } else{
                end = mid - 1;
            }
        }
        if (nums[end] == target){
            return end;
        } else if (nums[start] == target){
            return start;
        } else{
            return -1;
        }
    }
}
