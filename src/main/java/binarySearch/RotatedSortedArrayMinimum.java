package binarySearch;

import java.util.*;

public class RotatedSortedArrayMinimum {
    /**
     * 153. I
     */
    public int findMin(int[] nums) {
        int st = 0, ed = nums.length - 1;
        while (st < ed){
            int mid = st + (ed - st) / 2;
            if (nums[st] < nums[ed]){ // not rotated
                return nums[st];
            } else { // rotated
                if (nums[st] <= nums[mid]) { // key: here use "<=" , not "<" !!
                    st = mid + 1;
                } else {
                    ed = mid;
                }
            }
        }
        return nums[st];
    }

    /**
     * 154.II
     */
    public int findMin_II(int[] nums) {
        int st = 0, ed = nums.length - 1;
        while (st < ed){
            int mid = st + (ed - st) / 2;
            if (nums[st] < nums[ed]){
                return nums[st];
            } else if (nums[st] > nums[ed]) {
                if (nums[st] <= nums[mid]) {
                    st = mid + 1;
                } else{
                    ed = mid;
                }
            } else{ // nums[st] == nums[ed]
                st++;
            }
        }
        return nums[st];
    }
}
