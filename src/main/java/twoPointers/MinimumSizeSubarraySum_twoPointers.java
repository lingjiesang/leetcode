package twoPointers;

import java.util.*;

public class MinimumSizeSubarraySum_twoPointers {

    public int minSubArrayLen(int s, int[] nums) {
        if (nums.length == 0){
            return 0;
        }
        int minLen = Integer.MAX_VALUE;

        int start = 0;
        int sum = 0;
        for (int end = 0; end < nums.length; end++){
            sum += nums[end];
            while (sum >= s){
                minLen = Math.min(minLen, end - start + 1);
                sum -= nums[start++];
            }
        }

        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

}
