package twoPointers;

import java.util.*;

public class MaximumRectangle {
    public int maximalRectangle(char[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }

        int m = matrix.length;   // num of rows
        int n = matrix[0].length; // num of cols;
        int[] left = new int[n];
        int[] right = new int[n];
        int[] height = new int[n];

        Arrays.fill(right, n - 1);
        int maxArea = 0;
        for (int i = 0; i < m ; i++){
            int zeroPos = -1;
            // from left to right, calc "left" and "height"
            for (int j = 0; j < n; j++){
                if (matrix[i][j] == '1'){
                    height[j] = height[j] + 1;
                    left[j] = Math.max(left[j], zeroPos + 1);
                } else {
                    height[j] = 0;
                    left[j] = 0;
                    zeroPos = j;
                }
            }

            // from right to left, calc "right" and "area"
            zeroPos = n;
            for (int j = n - 1; j >= 0; j--){
                if (matrix[i][j] == '1'){
                    right[j] = Math.min(right[j], zeroPos - 1);
                    maxArea = Math.max(maxArea, (right[j] - left[j] + 1) * height[j]);
                } else{
                    right[j] = n - 1;
                    zeroPos = j;
                }
            }
        }
        return maxArea;
    }
}
