package twoPointers;

import java.util.*;

public class LongestSubstringNoRepeatingChar {
    public int lengthOfLongestSubstring(String s) {
        if (s.length() == 0){
            return 0;
        }
        int[] map = new int[256];
        Arrays.fill(map, -1);
        int maxLen = 0;
        int start = 0;
        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (map[c] >= start){
                start = map[c] + 1;
            }
            map[c] = i;
            maxLen = Math.max(maxLen, i - start + 1);

        }
        return maxLen;
    }
}
