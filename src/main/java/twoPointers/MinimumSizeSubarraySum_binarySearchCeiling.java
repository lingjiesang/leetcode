package twoPointers;

public class MinimumSizeSubarraySum_binarySearchCeiling {
    public int minSubArrayLen(int s, int[] nums) {

        int[] sums = new int[nums.length + 1];

        for (int i = 0; i < nums.length; i++) {
            sums[i + 1] = nums[i] + sums[i];
        }

        int minLen = Integer.MAX_VALUE;
        for (int start = 0; start < nums.length; start++) {
            if (sums[nums.length] - sums[start] < s) {
                break;
            }
//            int end = ceiling(sums, start, nums.length, s + sums[start]);
            int end = lower(sums, start, nums.length, s + sums[start]);  end += 1;
            minLen = Math.min(minLen, end - start);
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

    /**
     * find the minimal index in nums (between start and end, both inclusive), where nums[index] >= target
     */
    private int ceiling(int[] nums, int start, int end, int target) {
        while (start < end) {
            int mid = start + (end - start) / 2;
            // do this only if elements in nums are distinct from each other
            if (nums[mid] == target) {
                return mid;
            }
            // end "do this"
            if (nums[mid] < target) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return nums[start] >= target ? start : -1;
    }

    /**
     * find the maximal index in nums (between start and end, both inclusive), where nums[index] < target
     */
    private int lower(int[] nums, int start, int end, int target) {
        if (nums[start] >= target) {
            return -1;
        }

        while (start < end) {
            int mid = start + (end - start) / 2;

            if (nums[mid] < target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        if (nums[start] < target){
            return start;
        } else{
            return start - 1;
        }
    }
}