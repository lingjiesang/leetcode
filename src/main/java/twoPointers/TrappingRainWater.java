package twoPointers;

import java.util.*;

public class TrappingRainWater {
    public int trap(int[] height) {
        if (height.length <= 2) {
            return 0;
        }
        int[] left = new int[height.length];
        int[] right = new int[height.length];

        left[0] = height[0];
        for (int i = 1; i < height.length; i++) {
            left[i] = Math.max(left[i - 1], height[i]);
        }

        right[height.length - 1] = height[height.length - 1];
        for (int i = height.length - 2; i >= 0; i--) {
            right[i] = Math.max(right[i + 1], height[i]);
        }

        int water = 0;
        for (int i = 1; i < height.length - 1; i++) {
            water += Math.max(Math.min(left[i], right[i]) - height[i], 0);
        }
        return water;
    }

    /**
     *  no extra space!!
     */
    public int trap_method2(int[] height) {
        if (height == null || height.length == 0){
            return 0;
        }
        int water = 0, left = 0, right = height.length - 1;
        while (left < right){
            if (height[left] <= height[right]){
                int min = height[left];
                while (height[++left] < min){
                    water += min - height[left];
                }
            } else{
                int min = height[right];
                while (height[--right] < min){
                    water += min - height[right];
                }
            }
        }
        return water;

    }
}
