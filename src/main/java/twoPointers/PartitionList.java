package twoPointers;

import linkedlist.ListNode;

import java.util.*;

public class PartitionList {
    public ListNode partition(ListNode head, int x) {
        ListNode leftHead = new ListNode(0);
        ListNode rightHead = new ListNode(0);
        ListNode left = leftHead;
        ListNode right = rightHead;

        while (head!= null){
            if (head.val < x){
                left.next = head;
                left = left.next;
            } else{
                right.next = head;
                right = right.next;
            }
            head = head.next;
        }
        left.next = rightHead.next;
        right.next = null;
        return leftHead.next;
    }
}
