package twoPointers;

import java.util.*;

public class FourSum_NSum {
    public List<List<Integer>> fourSum(int[] nums, int target) {
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> oneSoln = new ArrayList<>();
        Arrays.sort(nums);
        NSum(nums, 0, target, 4, oneSoln, res);
        return res;
    }

    private void NSum(int[] nums, int start, int target, int N, List<Integer> oneSoln, List<List<Integer>> res){
        if (N == 2){
            int i = start, j = nums.length - 1;

            while (i < j){
                if (nums[i] * 2 > target || nums[j] * 2 < target){ //prune
                    break;
                }

                if (nums[i] + nums[j] == target){
                    res.add(new ArrayList<>(oneSoln));
                    res.get(res.size() - 1).add(nums[i]);
                    res.get(res.size() - 1).add(nums[j]);
                    i++;
                    j--;
                    while (i < j && nums[i] == nums[i - 1]){
                        i++;
                    }
                    while (i < j && nums[j] == nums[j + 1]){
                        j--;
                    }
                } else if (nums[i] + nums[j] < target){
                    i++;
                } else{
                    j--;
                }
            }
        } else{
            for (int k = start; k < nums.length - N + 1; k++){
                if (nums[k] * N > target){ //prune
                    break;
                }
                if (k != start && nums[k] == nums[k - 1]){
                    continue;
                }
                oneSoln.add(nums[k]);
                NSum(nums, k + 1, target - nums[k], N - 1, oneSoln, res);
                oneSoln.remove(oneSoln.size() - 1);
            }
        }
    }
}