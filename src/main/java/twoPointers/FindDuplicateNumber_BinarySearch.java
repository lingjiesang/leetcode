package twoPointers;

import java.util.*;

//ref: https://discuss.leetcode.com/topic/31661/java-easy-version-to-understand
public class FindDuplicateNumber_BinarySearch {
    public int findDuplicate(int[] nums) {
        if (nums.length <= 1){
            return 0;
        }

        int low = 1, high = nums.length - 1;
        while (low < high){
            int mid = low + (high - low) / 2;
            int countLow = 0;
            for (int i = 0; i < nums.length; i++){ // don't get the range wrong
                if (nums[i] <= mid){
                    countLow++;
                }
            }

            if (countLow > mid){
                high = mid;
            } else{
                low = mid + 1;
            }
        }
        return low;

    }
}