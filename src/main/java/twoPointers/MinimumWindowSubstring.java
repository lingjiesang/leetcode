package twoPointers;

/*
Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).

For example,
S = "ADOBECODEBANC"
T = "ABC"
Minimum window is "BANC".
 */
public class MinimumWindowSubstring {


    public class Solution {
        public String minWindow(String s, String t) {
            int[] map = new int[128];
            int count = t.length();
            for (int i = 0; i < count; i++) {
                map[t.charAt(i)]++;
            }

            int head = 0, tail = 0;
            int minHead = 0, minTail = s.length();

            while (tail < s.length()) {
                if (map[s.charAt(tail)] > 0) {
                    count--;
                }

                map[s.charAt(tail)]--;

                while (count == 0) {
                    if (tail - head < minTail - minHead) {
                        minHead = head;
                        minTail = tail;
                    }
                    if (map[s.charAt(head)] == 0) {
                        count++;
                    }
                    map[s.charAt(head)]++;
                    head++;
                }
                tail++;
            }
            if (minTail < s.length()) {
                return s.substring(minHead, minTail + 1);
            } else {
                return "";
            }
        }
    }
}