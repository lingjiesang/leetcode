package twoPointers;

import java.util.*;

public class ThreeSumSmaller {
    public int threeSumSmaller(int[] nums, int target) {
        int count = 0;
        Arrays.sort(nums);

        for (int k = 2; k < nums.length; k++) {
            int i = 0, j = 1;
            int limit = target - nums[k];
            if (nums[i] + nums[j] >= limit) {
                continue;
            }
            while (j + 1 < k && nums[i] + nums[j + 1] < limit) {
                j++;
            }
            count += j - i;
            while (i < j) {
                i++;
                while (i < j && nums[i] + nums[j] >= limit) {
                    j--;
                }
                count += j - i;
            }
        }
        return count;
    }

    /**
     * other's solution
     * ref: https://discuss.leetcode.com/topic/23421/simple-and-easy-understanding-o-n-2-java-solution
     */

    public int threeSumSmaller_Online(int[] nums, int target) {
        int count = 0;
        Arrays.sort(nums);
        int len = nums.length;

        for (int i = 0; i < len - 2; i++) {
            int left = i + 1, right = len - 1;
            while (left < right) {
                if (nums[i] + nums[left] + nums[right] < target) {
                    count += right - left;
                    left++;
                } else {
                    right--;
                }
            }
        }
        return count;
    }
}
