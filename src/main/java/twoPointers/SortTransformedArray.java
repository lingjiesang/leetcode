package twoPointers;

import java.util.*;

public class SortTransformedArray {
    public int[] sortTransformedArray(int[] nums, int a, int b, int c) {
        int n = nums.length;
        int[] result = new int[n];
        int start = 0, end = nums.length - 1;
        int pos = a > 0 ? nums.length - 1 : 0;
        while (start <= end){
            if (a > 0) {
                if (transform(a, b, c, nums[start]) > transform(a, b, c, nums[end])) {
                    result[pos--] = transform(a, b, c, nums[start++]);
                } else {
                    result[pos--] = transform(a, b, c, nums[end--]);
                }
            } else{
                if (transform(a, b, c, nums[start]) < transform(a, b, c, nums[end])) {
                    result[pos++] = transform(a, b, c, nums[start++]);
                } else {
                    result[pos++] = transform(a, b, c, nums[end--]);
                }
            }
        }
        return result;
    }
    public int transform(int a, int b, int c, int x){
        return a * x * x + b * x + c;
    }
}
