package twoPointers;

import java.util.*;

public class FourSum {
    public List<List<Integer>> fourSum(int[] num, int target) {
        ArrayList<List<Integer>> ans = new ArrayList<>();
        if (num.length < 4) return ans;
        Arrays.sort(num);
        for (int i = 0; i < num.length - 3; i++) {
            if (num[i] * 4 > target){  // prune
                break;
            }
            if (i > 0 && num[i] == num[i - 1]) {
                continue;
            }
            for (int j = i + 1; j < num.length - 2; j++) {
                if (num[j] * 3 > target - num[i]){ //prune
                    break;
                }
                if (j > i + 1 && num[j] == num[j - 1]) {
                    continue;
                }
                int low = j + 1, high = num.length - 1;

                while (low < high) {
                    if (num[low] * 2 > target - num[i] - num[j] || num[high] * 2 < target - num[i] - num[j]){ //prune
                        break;
                    }

                    int sum = num[i] + num[j] + num[low] + num[high];
                    if (sum == target) {
                        ans.add(Arrays.asList(num[i], num[j], num[low], num[high]));
                        while (low < high && num[low] == num[low + 1]) {
                            low++;
                        }
                        while (low < high && num[high] == num[high - 1]){
                            high--;
                        }
                        low++;
                        high--;
                    } else if (sum < target) {
                        low++;
                    }
                    else {
                        high--;
                    }
                }
            }
        }
        return ans;
    }
}
