package twoPointers;


public class StockIII {
    public int maxProfit(int[] prices) {
        if (prices.length == 0){
            return 0;
        }

        //fst[i]: max profit for first transaction at day i
        int[] fst = new int[prices.length];
        //snd[i]: max profit for second transaction starting at a date later than day i
        int[] snd = new int[prices.length];

        int minPrice = prices[0];
        for (int i = 1; i < prices.length; i++){
            fst[i] = prices[i] - minPrice;
            minPrice = Math.min(minPrice, prices[i]);
        }

        int maxPrice = prices[prices.length - 1];
        minPrice = prices[prices.length - 1];
        int profit = 0;
        for (int i = prices.length - 2; i >= 0; i--){
            snd[i] = Math.max(profit, maxPrice - minPrice);
            profit = Math.max(profit, snd[i]);
            if (prices[i] < minPrice){
                minPrice = prices[i];
            } else if (prices[i] > maxPrice){
                maxPrice = prices[i];
                minPrice = prices[i];
            }
        }
        // System.out.println(Arrays.toString(fst));
        // System.out.println(Arrays.toString(snd));

        profit = 0;
        for (int i = 0; i < prices.length; i++){
            profit = Math.max(profit, fst[i] + snd[i]);
        }
        return profit;
    }
}
