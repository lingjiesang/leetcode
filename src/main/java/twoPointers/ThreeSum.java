package twoPointers;

import java.util.*;

public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++){
            if (i != 0 && nums[i] == nums[i - 1]){
                continue;
            }

            int j = i + 1;
            int k = nums.length - 1;

            while (j < k){
                if (nums[i] + nums[j] + nums[k] == 0){
                    result.add(Arrays.asList(nums[i], nums[j], nums[k]));
                    while (j < k && nums[j + 1] == nums[j]){
                        j++;
                    }
                    while (j < k && nums[k - 1] == nums[k]){
                        k--;
                    }
                    j++; // key, don't forget
                    k--; // key, don't forget
                } else if (nums[i] + nums[j] + nums[k] > 0){
                    k--;
                } else{
                    j++;
                }
            }
        }

        return result;
    }


    public List<List<Integer>> threeSum_noSort(int[] nums){
        List<List<Integer>> result = new ArrayList<>();
        Map<Integer, Integer> freq = new HashMap<>();

        for (int num : nums){
            freq.put(num, freq.getOrDefault(num, 0) + 1);
        }

        Iterator<Integer> iter1, iter2 ;

        for(iter1 = freq.keySet().iterator(); iter1.hasNext(); ){
            int num1 = iter1.next();
            int freq1 = freq.get(num1);
            for (iter2 = freq.keySet().iterator(); iter2.hasNext();){
                int num2 = iter2.next();
                int freq2 = freq.get(num2);
                if (num2 > num1 && 0 - num1 - num2 > num2 && freq.containsKey(0 - num1 - num2)){
                    result.add(Arrays.asList(num1, num2, 0 - num1 - num2));
                }
            }

            if (num1 != 0  && freq1 > 1 && freq.containsKey(0 - num1 * 2)){
                if (num1 > 0 - num1 * 2){
                    result.add(Arrays.asList(0 - num1 * 2, num1, num1));
                } else{
                    result.add(Arrays.asList(num1, num1,0 - num1 * 2));
                }
            }

            if (num1 == 0 && freq1 > 2){
                result.add(Arrays.asList(num1, num1, num1));
            }
        }
        return result;
    }
}
