package twoPointers;

public class MinimumSizeSubarraySum_binarySearchFloor {
    public int minSubArrayLen(int s, int[] nums) {

        int[] sums = new int[nums.length + 1];

        for (int i = 0; i < nums.length; i++){
            sums[i + 1] = nums[i] + sums[i];
        }

        int minLen = Integer.MAX_VALUE;
        for (int end = 1; end <= nums.length; end++){
            if (sums[end] >= s){
                int start = floor(sums, 0, end - 1, sums[end] - s); // use this line instead the 2 lines below might cause TLE
//                int start = higher(sums, 0, end, sums[end] - s);  // don't use 'end - 1' instead of 'end' in this line !!!
//                start -= 1;
                minLen = Math.min(minLen, end - start);
            }
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

    /**
     *
     * find minimal index (between start and end, both inclusive) in sums where nums[index] > target
     * if no such element exist, return -1
     *
     */
    private int higher(int[] nums, int start, int end, int target){
        while (start < end){
            int mid = start + (end - start) / 2;
            if (nums[mid] <= target){
                start = mid + 1;
            } else{
                end = mid;
            }
        }
        return nums[start] > target ? start : -1;
    }


    /**
     *
     * find maximal index in nums (between start and end, both inclusive), where nums[index] <= target
     * if using this, ensure at least nums [start] <= target
     *
     */
    private int floor(int[] nums, int start, int end, int target) {
        if (nums[start] > target) {
            return -1;
        }

        while (start < end) {
            int mid = start + (end - start) / 2;

            if (nums[mid] <= target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        if (nums[start] <= target){
            return start;
        } else{
            return start - 1;
        }
    }

}
