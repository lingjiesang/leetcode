package twoPointers;

import java.util.*;

public class RemoveDuplicatesII {
    public int removeDuplicates(int[] nums) {
        if (nums.length == 0){
            return 0;
        }

        int lastElement = nums[0], count = 1;
        int len = 1;
        for (int i = 1; i < nums.length; i++){
            if (nums[i] == lastElement){
                count++;
                if (count == 2){
                    nums[len++] = nums[i];
                }
            } else{
                nums[len++] = nums[i];
                lastElement = nums[i];
                count = 1;
            }
        }
        return len;

    }
}
