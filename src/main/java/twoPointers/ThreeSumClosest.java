package twoPointers;

import java.util.*;

public class ThreeSumClosest {
    public int threeSumClosest(int[] nums, int target) {
        if (nums.length < 3){
            return -1; //just bad!
        }

        Arrays.sort(nums);

        int res = nums[0] + nums[1] + nums[2];
        int diff = Math.abs(res - target);

        for (int i = 0; i < nums.length - 2; i++) {
            if (i != 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            int low = i + 1, hi = nums.length - 1;

            while (low < hi){
                int sum = nums[i] + nums[low] + nums[hi];
                if (sum == target){
                    return sum;
                } else if (sum < target){
                    if (target - sum < diff){
                        diff = target - sum;
                        res = sum;
                    }
                    low++;
                    while (low < hi && nums[low] == nums[low - 1]){
                        low++;
                    }
                } else{
                    if (sum - target < diff){
                        diff = sum - target;
                        res = sum;
                    }
                    hi--; // cannot switch with while loop below!! (when hi == nums.length - 1, hi + 1 is out of boundary)
                    while (low < hi && nums[hi] == nums[hi + 1]){
                        hi--;
                    }
                }
            }
        }
        return res;
    }
}
