package trie;

import java.util.*;

public class WordSearchII {
    class TrieNode {
        TrieNode[] subtree = new TrieNode[26];
        String str;
    }

    private TrieNode buidTrie(String[] words) {
        TrieNode root = new TrieNode();
        for (String word : words) {
            TrieNode node = root; // start from root always!
            for (char ch : word.toCharArray()) {
                int i = ch - 'a';
                if (node.subtree[i] == null) {
                    node.subtree[i] = new TrieNode();
                }
                node = node.subtree[i];
            }
            node.str = word;
        }
        return root;
    }

    //board[i][j] is position on board to visit, node is the node visited
    private void dfs(char[][] board, int i, int j, TrieNode node, List<String> foundWords) {
        char ch = board[i][j];

        if (ch == '#' || node.subtree[ch - 'a'] == null) {
            return;
        }

        node = node.subtree[ch - 'a'];

        // add to foundWords:
        if (node.str != null) {
            foundWords.add(node.str);
            node.str = null;
        }

        // visit next:
        board[i][j] = '#';
        if (i > 0) dfs(board, i - 1, j, node, foundWords);
        if (j > 0) dfs(board, i, j - 1, node, foundWords);
        if (i < board.length - 1) dfs(board, i + 1, j, node, foundWords);
        if (j < board[0].length - 1) dfs(board, i, j + 1, node, foundWords);
        board[i][j] = ch;
    }

    public List<String> findWords(char[][] board, String[] words) {
        List<String> foundWords = new ArrayList<>();

        TrieNode root = buidTrie(words);

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                dfs(board, i, j, root, foundWords);
            }
        }
        return foundWords;
    }

    public static void main(String[] args) {
        WordSearchII soln = new WordSearchII();
        String[] board_inString = {"oaan", "etae", "ihkr", "iflv"};
        String[] words = {"oath", "pea", "eat", "rain"};

        char[][] board = new char[board_inString.length][board_inString[0].length()];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                board[i][j] = board_inString[i].charAt(j);
            }
        }

        List<String> foundWords = soln.findWords(board, words);
        System.out.print("[");
        for (String word : foundWords) {
            System.out.print(word);
            System.out.print(", ");
        }
        System.out.println("]");

    }
}
