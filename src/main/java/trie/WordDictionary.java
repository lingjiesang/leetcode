package trie;

public class WordDictionary {
    // Adds a word into the data structure.

    class TrieNode {
        public TrieNode[] subtrees;
        public String word;

        public TrieNode(){
            subtrees = new TrieNode[26];
        }
    }

    TrieNode root = new TrieNode();

    public void addWord(String word) {
        TrieNode node = root;

        for (char c : word.toCharArray()){
            if (node.subtrees[c - 'a'] == null){
                node.subtrees[c - 'a'] = new TrieNode();
            }
            node = node.subtrees[c - 'a'];
        }
        node.word = word;
    }

    // Returns if the word is in the data structure. A word could
    // contain the dot character '.' to represent any one letter.
    public boolean search(String word) {

        return search_dfs(word.toCharArray(), 0 ,root);
    }


    private boolean search_dfs(char[] word, int start, TrieNode node){
        if (start == word.length){
            return node.word != null; // key
        }

        if (word[start] != '.'){
            int index = word[start] - 'a';
            if (node.subtrees[index] != null){
                return search_dfs(word, start + 1, node.subtrees[index]);
            } else{
                return false;
            }
        } else{
            for (TrieNode next : node.subtrees){
                if (next != null && search_dfs(word, start + 1, next)){ // key: next != null
                    return true;
                }
            }
            return false;
        }
    }

// Your WordDictionary object will be instantiated and called as such:
// WordDictionary wordDictionary = new WordDictionary();
// wordDictionary.addWord("word");
// wordDictionary.search("pattern");
}
