package matrix;

import java.util.*;

public class TrappingRainWaterII {
    public int trapRainWater(int[][] heightMap) {
        if (heightMap.length <= 2 || heightMap[0].length <= 2){
            return 0;
        }
        int m = heightMap.length;
        int n = heightMap[0].length;

        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>(){
            public int compare(int[] a, int[] b){
                return a[2] - b[2];
            }
        });

        boolean[][] visited = new boolean[m][n];

        for (int i = 0; i < m; i++){
            pq.offer(new int[]{i, 0, heightMap[i][0]});
            visited[i][0] = true;
            pq.offer(new int[]{i, n - 1, heightMap[i][n - 1]});
            visited[i][n - 1] = true;
        }
        for (int j = 1; j < n - 1; j++){ //note "1 ~ n - 2" so corners are not added twice
            pq.offer(new int[]{0, j, heightMap[0][j]});
            visited[0][j] = true;
            pq.offer(new int[]{m - 1, j, heightMap[m - 1][j]});
            visited[m - 1][j] = true;
        }


        int result = 0;
        while (!pq.isEmpty()){
            int[] tile = pq.poll();

            int x = tile[0];
            int y = tile[1];
            int h = tile[2];
            if (x > 0 && !visited[x - 1][y]){
                result += Math.max(h - heightMap[x - 1][y], 0);
                pq.offer(new int[]{x - 1, y, Math.max(heightMap[x - 1][y], h)});
                visited[x - 1][y] = true;
            }
            if (x + 1 < m && !visited[x + 1][y]){
                result += Math.max(h - heightMap[x + 1][y], 0);
                pq.offer(new int[]{x + 1, y, Math.max(heightMap[x + 1][y], h)});
                visited[x + 1][y] = true;
            }
            if (y > 0 && !visited[x][y - 1]){
                result += Math.max(h - heightMap[x][y - 1], 0);
                pq.offer(new int[]{x, y - 1, Math.max(heightMap[x][y - 1], h)});
                visited[x][y - 1] = true;
            }
            if (y + 1 < n && !visited[x][y + 1]){
                result += Math.max(h - heightMap[x][y + 1], 0);
                pq.offer(new int[]{x, y + 1, Math.max(heightMap[x][y + 1], h)});
                visited[x][y + 1] = true;
            }
        }
        return result;
    }
}
