package matrix;

import java.util.*;

public class ShortestDistanceFromAllBuildings {

    private class Building {
        int x;
        int y;
        List<int[]> frontEnds;
        int[][] visited;
        Building(int x, int y, int[][] grid){
            this.x = x;
            this.y = y;
            visited = new int[grid.length][grid[0].length];
            for (int i = 0; i < grid.length; i++){
                for (int j = 0; j < grid[0].length; j++){
                    visited[i][j] = grid[i][j];
                }
            }
            frontEnds = new ArrayList<>();
            frontEnds.add(new int[]{x, y});
        }

    }

    /**
     * method1 (mine): visited from all buildings and move forward (105ms)
     *
     * BUT, it is not necessary true that the first land we found as a reachable point from all buildings has
     * the shortest total distance to the buildings!!!
     * So it is actually better to visit building one by one!!!
     */
    public int shortestDistance(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;

        int numOfBuildings = 0;
        Deque<Building> queue = new ArrayDeque<>();

        int[][] distance = new int[m][n];
        int[][] numOfBuildVisitedThisLand = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    numOfBuildings++;
                    queue.offer(new Building(i, j, grid));
                }
            }
        }

        int step = 0;
        int minDist = Integer.MAX_VALUE;
        while (!queue.isEmpty()){
            int size = queue.size();
            step++;
            for (int i = 0; i < size; i++){
                Building b = queue.poll();
                List<int[]> newFrontEnds = new ArrayList<>();
                for (int[] frontEnd : b.frontEnds){
                    if (frontEnd[0] > 0 && visit(frontEnd[0] - 1, frontEnd[1], step, b, newFrontEnds, distance,
                            numOfBuildVisitedThisLand, numOfBuildings)){
                        minDist = Math.min(minDist, distance[frontEnd[0] - 1][frontEnd[1]]);
                    }
                    if (frontEnd[0] + 1 < m && visit(frontEnd[0] + 1, frontEnd[1], step, b, newFrontEnds, distance,
                            numOfBuildVisitedThisLand, numOfBuildings)){
                        minDist = Math.min(minDist, distance[frontEnd[0] + 1][frontEnd[1]]);
                    }
                    if (frontEnd[1] > 0 && visit(frontEnd[0], frontEnd[1] - 1, step, b, newFrontEnds, distance,
                            numOfBuildVisitedThisLand, numOfBuildings)){
                        minDist = Math.min(minDist, distance[frontEnd[0]][frontEnd[1] - 1]);
                    }
                    if (frontEnd[1] + 1 < n && visit(frontEnd[0], frontEnd[1] + 1, step, b, newFrontEnds, distance,
                            numOfBuildVisitedThisLand, numOfBuildings)){
                        minDist = Math.min(minDist, distance[frontEnd[0]][frontEnd[1] + 1]);
                    }
                }
                if (newFrontEnds.size() > 0){
                    b.frontEnds = newFrontEnds;
                    queue.offer(b);
                }
            }
        }
        return minDist == Integer.MAX_VALUE ? -1 : minDist;

    }

    //return true when numOfBuildVisitedThisLand[x][y] == numOfBuildings
    private boolean visit(int x, int y, int step, Building b, List<int[]> newFrontEnds,
                          int[][] distance, int[][] numOfBuildVisitedThisLand, int numOfBuildings){
        if (b.visited[x][y] != 0){
            return false;
        }
        b.visited[x][y] = 1;
        newFrontEnds.add(new int[]{x, y});
        distance[x][y] += step;
        if (++numOfBuildVisitedThisLand[x][y] == numOfBuildings){
            return true;
        } else{
            return false;
        }
    }

    /**
     * method2: visit building one by one (without improvement, 60ms; with improvement, 10ms)
     * ref: https://discuss.leetcode.com/topic/46896/9-10ms-java-solution-beats-98/2
     */


    /*
        - beats 98% 9ms JAVA BFS
        - basically use lee-algroithm, bfs each 1 to find out min distance to each 0, accumulate this distances to each 0 location: distance[][], finally find out min value from distance[][]
        - In the case of cannot find a house to each all house:
            - not all 0s can reach each house: reachCount[][] to count the # of house each 0 can reach, only >= houseCount valid
            - [~~improve speed~~]：not all house can reach each house, in this case, we cannot build such house,
                -count reachable house #, if < houseCount, return -1 directly!!!!!
    */

    public int shortestDistance_method2(int[][] grid) {

        if (grid.length == 0 || grid[0].length == 0) {
            return -1;
        }

        int m = grid.length;
        int n = grid[0].length;
        int[][] distance = new int[m][n];  //accumulated distance of each house (1) to each 0
        int[][] reachCount = new int[m][n]; //count reachable house for each 0
        int houseCount = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    houseCount++;
                }
            }
        }
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 1) {
                    if (!bfs(grid, distance, reachCount, houseCount, m, n, i, j)) {
                        return -1;
                    }
                }
            }
        }
        int minDistance = Integer.MAX_VALUE;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 0 && reachCount[i][j] == houseCount) {
                    minDistance = Math.min(minDistance, distance[i][j]);
                }
            }
        }
        return minDistance == Integer.MAX_VALUE ? -1 : minDistance;

    }

    private boolean bfs(int[][] grid, int[][] distance, int[][] reachCount, int houseCount, int m, int n, int x, int y) {

        int[][] visited = new int[m][n];
        Queue<int[]> q = new LinkedList<int[]>();
        q.offer(new int[]{x, y});
        int[] dx = new int[]{0, 0, -1, 1};
        int[] dy = new int[]{1, -1, 0, 0};
        int level = 0;
        int count = 0;
        while (!q.isEmpty()) {
            int size = q.size();
            level++;
            for (int i = 0; i < size; i++) {//level by level
                int[] curr = q.poll();
                for (int k = 0; k < 4; k++) { //visit all neighbors & accumulate distance
                    int nx = curr[0] + dx[k];
                    int ny = curr[1] + dy[k];
                    if (nx >=0 && ny >= 0 && nx < m && ny < n  && visited[nx][ny] == 0) {
                        if (grid[nx][ny] == 0) {
                            distance[nx][ny] += level;
                            visited[nx][ny] = 1;
                            reachCount[nx][ny]++;
                            q.offer(new int[]{nx, ny});
                        } else if (grid[nx][ny] == 1) {
                            count++;
                            visited[nx][ny] = 1;
                        }
                    }
                }
            }
        }
        return count == houseCount;
    }

}
