package matrix;

import java.util.*;

public class RotateSqaureMatrix {
    public void rotate(int[][] matrix){
        int n = matrix.length;
        int round = 0;
        while (round < n - round - 1){
            for (int dist = round; dist < n - round - 1; dist++){
                int temp = matrix[round][dist];
                matrix[round][dist] = matrix[n - 1 - dist][round];
                matrix[n - 1 - dist][round] = matrix[n - 1 - round][n - 1 - dist];
                matrix[n - 1 - round][n - 1 - dist] = matrix[dist][n - 1 - round];
                matrix[dist][n - 1 - round] = temp;
            }
            round++;
        }
    }
}
