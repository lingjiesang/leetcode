package matrix;

/**
 * ref:https://discuss.leetcode.com/topic/29006/c-java-python-binary-search-solution-with-explanation
 */





public class SmallestRectangleEnclosingBlackPixels {
    public int minArea(char[][] image, int x, int y) {
        if (image.length == 0 || image[0].length == 0) {
            return 0;
        }
        int m = image.length;
        int n = image[0].length;
        int leftInclusive = binarySearchColumn(image, 0, m - 1, 0, y, true);
        int rightExclusive = binarySearchColumn(image, 0, m - 1, y + 1, n, false);
        int topInclusive = binarySearchRow(image, 0, x, leftInclusive, rightExclusive - 1, true);
        int bottomExclusive = binarySearchRow(image, x + 1, m, leftInclusive, rightExclusive - 1, false);
        return (rightExclusive - leftInclusive) * (bottomExclusive - topInclusive);
    }

    private int binarySearchColumn(char[][] image, int rowStart, int rowEnd, int colStart, int colEnd, boolean searchLowerBound) {
        while (colStart < colEnd) {
            int colMid = colStart + (colEnd - colStart) / 2;
            if (foundBlack(image, rowStart, rowEnd, colMid, colMid) == searchLowerBound) {
                colEnd = colMid;
            } else {
                colStart = colMid + 1;
            }
        }
        return colStart;
    }

    private int binarySearchRow(char[][] image, int rowStart, int rowEnd, int colStart, int colEnd, boolean searchLowerBound) {
        while (rowStart < rowEnd) {
            int rowMid = rowStart + (rowEnd - rowStart) / 2;
            if (foundBlack(image, rowMid, rowMid, colStart, colEnd) == searchLowerBound) {
                rowEnd = rowMid;
            } else {
                rowStart = rowMid + 1;
            }
        }
        return rowStart;
    }

    private boolean foundBlack(char[][] image, int rowStart, int rowEnd, int colStart, int colEnd) {
        for (int i = rowStart; i <= rowEnd; i++) {
            for (int j = colStart; j <= colEnd; j++) {
                if (image[i][j] == '1') {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args){
        char[][] image = {{'0', '1'}};
        SmallestRectangleEnclosingBlackPixels soln = new SmallestRectangleEnclosingBlackPixels();
        System.out.println(soln.minArea(image, 0, 1));
    }
}
