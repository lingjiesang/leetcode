package matrix;

import java.util.*;

public class    KthSmallestElementInSortedMatrix {
    /**
     * heap
     */
    public int kthSmallest(int[][] matrix, int k) {
        int n = matrix.length;
        if (k < 1 || n == 0 || n * n < k){
            return -1;
        }
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>(){
            public int compare(int[] a, int [] b){
                return a[2] - b[2];
            }
        });
        for (int j = 0; j < n; j++){
            pq.add(new int[]{0, j, matrix[0][j]});
        }
        int result = 0;
        while (k > 0){
            int[] smallest = pq.poll();
            result = smallest[2];
            k--;
            if (smallest[0] + 1 < n){
                smallest[0]++;
                smallest[2] = matrix[smallest[0]][smallest[1]];
                pq.add(smallest);
            }
        }
        return result;
    }

}
