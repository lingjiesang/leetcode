package matrix;

import java.util.*;

public class SparseMatrixMultiplication {
    /**
     * TLE
     */
    public int[][] multiply_Naive(int[][] A, int[][] B) {
        int m = A.length, K = A[0].length, n = B[0].length;
        int[][] product = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                for (int k = 0; k < K; k++) {
                    product[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        return product;
    }


    public int[][] multiply(int[][] A, int[][] B) {
        int m = A.length, K = A[0].length, n = B[0].length;
        int[][] product = new int[m][n];

        for (int i = 0; i < m; i++) {
            for (int k = 0; k < K; k++) {
                if (A[i][k] != 0) {
                    for (int j = 0; j < n; j++) {
                        product[i][j] += A[i][k] * B[k][j];
                    }
                }
            }
        }
        return product;
    }

    public int[][] multiply_method2(int[][] A, int[][] B) {
        int m = A.length, K = A[0].length, n = B[0].length;
        int[][] product = new int[m][n];

        List<List<Integer>> nonZero_A = new ArrayList<>();
        for (int i = 0; i < m; i++){
            List<Integer> nonZero = new ArrayList<>();
            for (int k = 0; k < K; k++){
                if (A[i][k] != 0){
                    nonZero.add(k);
                }
            }
            nonZero_A.add(nonZero);
        }

        List<List<Integer>> nonZero_B = new ArrayList<>();
        for (int j = 0; j < n; j++){
            List<Integer> nonZero = new ArrayList<>();
            for (int k = 0; k < K; k++){
                if (B[k][j] != 0){
                    nonZero.add(k);
                }
            }
            nonZero_B.add(nonZero);
        }

        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                List<Integer> rowA = nonZero_A.get(i);
                List<Integer> colB = nonZero_B.get(j);
                if (rowA.size() == 0 || colB.size() == 0){
                    continue;
                }
                int rowApos = 0, colBpos = 0;
                while (rowApos < rowA.size() && colBpos < colB.size()){
                    int kA = rowA.get(rowApos);
                    int kB = colB.get(colBpos);
                    if (kA == kB){
                        product[i][j] += A[i][kA] * B[kB][j];
                        rowApos++;
                        colBpos++;
                    } else if (kA < kB){
                        rowApos++;
                    } else{
                        colBpos++;
                    }
                }
            }
        }
        return product;
    }



}
