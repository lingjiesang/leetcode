package matrix;

import java.util.*;

public class GameOfLife {
    public void gameOfLife(int[][] board) {
        //nc (next<-curt)
        //10  live<-dead
        //01  dead<-live
        int m = board.length;
        int n = board[0].length;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                int lives = countLiveNeighbors(board, i, j, m, n);
                if ((board[i][j] & 1) == 1){ //curt: live
                    if (lives == 2 || lives == 3){
                        board[i][j] += 2; //next: live
                    }
                } else{ // curt: dead
                    if (lives == 3){
                        board[i][j] += 2; //next: live
                    }
                }

            }
        }

        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                board[i][j] >>= 1;
            }
        }

    }
    private int countLiveNeighbors(int[][] board, int row, int col, int rowNum, int colNum){
        int lives = 0;
        for (int i = Math.max(0, row - 1); i <= Math.min(row + 1, rowNum - 1); i++){
            for (int j = Math.max(0, col - 1); j <= Math.min(col + 1, colNum - 1); j++){
                lives += (board[i][j] & 1);
            }
        }
        lives -= (board[row][col] & 1);
        return lives;
    }


    /**
     * follow up
     * ref: https://discuss.leetcode.com/topic/26236/infinite-board-solution/10
     */
    private Set<Coord> gameOfLife(Set<Coord> live) {
        Map<Coord,Integer> neighbours = new HashMap<>();
        for (Coord cell : live) {
            for (int i = cell.i-1; i<cell.i+2; i++) {
                for (int j = cell.j-1; j<cell.j+2; j++) {
                    if (i==cell.i && j==cell.j) continue;
                    Coord c = new Coord(i,j);
                    if (neighbours.containsKey(c)) {
                        neighbours.put(c, neighbours.get(c) + 1);
                    } else {
                        neighbours.put(c, 1);
                    }
                }
            }
        }
        Set<Coord> newLive = new HashSet<>();
        for (Map.Entry<Coord,Integer> cell : neighbours.entrySet())  {
            if (cell.getValue() == 3 || cell.getValue() == 2 && live.contains(cell.getKey())) {
                newLive.add(cell.getKey());
            }
        }
        return newLive;
    }

    private class Coord{
        int i;
        int j;
        Coord(int i, int j){
            this.i = i;
            this.j = j;
        }
    }
}
