package matrix;

import java.util.*;
/**
 * rank_smaller(A, a): # of elements in A smaller than a
 * rank_larger(A, a):  # of elements in A larger than a
 * A is a n * n matrix
 *
 * (1) matrix A --> submatrix of A
 *     i.e.   A --> A_bar
 * remove alternative row and column, always keep the first and last row and column
 * (examples are just in vector format for clarity)
 * Example 1: the original matrix has indices {0, 1, 2, 3, 4} (5), then the submatrix has indices   {0, 2, 4} (3).
 * Example 2: the original matrix has indices {0, 1, 2, 3, 4, 5} (6), then the submatrix has indices {0, 2, 4, 5} (4).
 *
 *
 *  suppose (a, b) = biselect(A_bar, k1_bar, k2_bar)  (k1_bar >= k2_bar)
 * (2) k1, k2  --> k1_bar, k2_bar
 * k1_bar is the smallest integer such that k1_bar-th element of A_bar (value: a) is not smaller than k1-th element of A
 * k2_bar is the largest  integer such that k2_bar-th element of A_bar (value: b) is not larger  than k2-th element of A
 * i.e. k1-th element of A is smaller or equal than a, or rank_larger(A, a) <= n * n - k1 <= n * n - k2
 *     (to make sure k1-th element equals to a, we also need rank_smaller(A, a) <= k1 - 1)
 *      k2-th element of A is larger or equal than b, or rank_smaller(A, b) <= k2 - 1 <= k1 - 1
 *     (to make sure k2-th element equals to b, we also need or rank_larger(A, b) <= n * n - k2)
 *
 *  k1_bar = (k1 + 2 * n) / 4 + 1 if n % 2 == 1 else n + 1 + (k1 + 3) / 4
 *  k2_bar = (k2 + 3) / 4
 *
 *  note: k1 >= k2, a >= b
 *
 *  ref: https://discuss.leetcode.com/topic/53126/o-n-from-paper-yes-o-rows/2 (StefanPochman, python)
 *  https://discuss.leetcode.com/topic/54262/c-o-n-time-o-n-space-solution-with-detail-intuitive-explanation/2 (c++, paper)
 */
public class KthSmallestElementInSortedMatrix_Linear {

    public int kthSmallest(int[][] matrix, int k){
        int[] indices = new int[matrix.length];
        for (int i = 0; i < indices.length; i++){
            indices[i] = i;
        }
        return biSect(matrix, indices, k, k)[0];
    }

    /**
     * @param matrix: original matrix
     * @param indices:  indices of selected rows (columns) in the matrix
     * @param k1: len(indices) * len(indices) >= k1 >= k2 >= 1
     * @param k2
     * @return Array of two elements: [value of k1-th element of matrix, value of k2-th element of matrix
     */
    public int[] biSect(int[][] matrix, int[] indices, int k1, int k2){
        int n = indices.length;
        if (n <= 2){
            return biSect_Native(matrix, indices, k1, k2);
        }

        int[] indices_bar = new int[n / 2 + 1];
        for (int i = 0; i * 2 < n; i++){
            indices_bar[i] = indices[i * 2];
        }
        if (n % 2 == 0){
            indices_bar[indices_bar.length - 1] = indices[n - 1];
        }

        int k1_bar = n % 2 == 0 ?  (k1 + 4 * n + 4 + 3) / 4 : (k1 + 2 * n + 1 + 3) / 4 ;
        int k2_bar = (k2 + 3) / 4;

        int[] ab = biSect(matrix, indices_bar, k1_bar, k2_bar); // a = ab[0], b = ab[1]

        int rank_smallerThanA = 0;
        int rank_largerThanB  = 0;
        int jb = n; // jb is the col of the first ele where matrix[indices[i]][indices[jb]] > b
        int ja = n; // ja is the col of the first ele where matrix[indices[i]][indices[jb]] >= a
        List<Integer> L = new ArrayList<>();
        for (int i = 0; i < n; i++){
            while (jb - 1 >= 0 && matrix[indices[i]][indices[jb - 1]] > ab[1]){
                jb--;
            }
            while (ja - 1 >= 0 &&  matrix[indices[i]][indices[ja - 1]] >= ab[0]){
                ja--;
            }
            rank_largerThanB += n - jb;
            rank_smallerThanA += ja;
            for (int j = jb; j < ja; j++){
                L.add(matrix[indices[i]][indices[j]]);
            }
        }
        int[] vals = new int[2];
        if (rank_smallerThanA <= k1 - 1){
            vals[0] = ab[0];
        } else if (rank_largerThanB <= n * n - k1){
            vals[0] = ab[1];
        } else{
            vals[0] = Kth(L, k1 - (n * n - rank_largerThanB), 0, L.size() - 1);
        }

        if (rank_smallerThanA <= k2 - 1){
            vals[1] = ab[0];
        } else if (rank_largerThanB <= n * n - k2){
            vals[1] = ab[1];
        } else{
            vals[1] = Kth(L, k2 - (n * n - rank_largerThanB),  0, L.size() - 1);
        }
        return vals;
    }

    public int[] biSect_Native(int[][] matrix, int[] indices, int k1, int k2){
        int[] vector = new int[indices.length * indices.length];
        int pos = 0;
        for (int i = 0; i < indices.length; i++) {
            for (int j = 0; j < indices.length; j++){
                vector[pos++] = matrix[indices[i]][indices[j]];
            }
        }
        Arrays.sort(vector);
        return new int[]{vector[k1 - 1], vector[k2 - 1]};
    }

    /**
     *
     * @param L
     * @param k
     * @return value of K-th element in L
     */
    public int Kth(List<Integer> L, int k, int st, int ed){
        int pivot = L.get(ed);
        if (ed > st) {
            List<Integer> L_bar = new ArrayList<>();
            for (int i = st; i <= ed; i = i + 5) {
                int len = Math.min(5, ed - i + 1);
                Collections.sort(L.subList(i, i + len));
                L_bar.add(L.get(i + len / 2));
            }
            pivot = Kth(L_bar, (L_bar.size() + 1) / 2, 0, L_bar.size() - 1);
        }

        int less = st - 1;
        int more = ed + 1;
        int curr = st;

        while (curr < more){
            if (L.get(curr) < pivot){
                swap(L, ++less, curr);
                curr++;
            } else if (L.get(curr) > pivot){
                swap(L, --more, curr);
            } else{
                curr++;
            }
        }

        if (less + 1 - st >= k){
            return Kth(L, k, st, less);
        } else if (more - st >= k){
            return L.get(more - 1);
        } else {
            return Kth(L, k - (more - st), more, ed);
        }

    }


    public void swap(List<Integer> L, int i, int j){
        if (i != j) {
            int temp = L.get(i);
            L.set(i, L.get(j));
            L.set(j, temp);
        }
    }


    public static void main(String[] args){
        KthSmallestElementInSortedMatrix_Linear soln = new KthSmallestElementInSortedMatrix_Linear();
        List<Integer> L = Arrays.asList(5,-2, -1, 2, 0);
        System.out.println(soln.Kth(L, 3, 0, L.size() - 1));

        int[][] matrix = {{1,3,5},{6,7,12},{11,14,14}};
        System.out.println(soln.kthSmallest(matrix, 2));
    }


}
