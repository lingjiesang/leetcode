package matrix;

import java.util.*;

public class PerfectRectangle {
    /**
     * method 1: scan y coordinates at each x position
     *
     * 1. check if sum of area == area calculated using the four boundary points
     *
     * 2. check if any two rectangle "overlaps"
     * at each x position, keep (as treeset) all y segments occupied by rectangles starting <= x and ending > x,
     * if any of these y segments "overlapped" => return false
     * Node: we need to remove y segments ending at x from treeset beforing adding y segments starting at x!
     *
     *
     * O(nlogn)
     */
    private class LRSide implements Comparable<LRSide> {
        int xCoord;
        boolean isLeft;
        int[] yCoords;

        public LRSide(int xCoord, boolean isLeft, int[] yCoord) {
            this.xCoord = xCoord;
            this.isLeft = isLeft;
            this.yCoords = yCoord;
        }

        public int compareTo(LRSide other) {
            return xCoord - other.xCoord != 0 ? xCoord - other.xCoord : (isLeft ? 1 : -1);
        }
    }

    public boolean isRectangleCover(int[][] rectangles) {
        PriorityQueue<LRSide> pq = new PriorityQueue<>();
        int[] bounds = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
        int area = 0;
        for (int[] rect : rectangles) {
            pq.add(new LRSide(rect[0], true, new int[]{rect[1], rect[3]}));
            pq.add(new LRSide(rect[2], false, new int[]{rect[1], rect[3]}));
            bounds[0] = Math.min(bounds[0], rect[0]);
            bounds[1] = Math.min(bounds[1], rect[1]);
            bounds[2] = Math.max(bounds[2], rect[2]);
            bounds[3] = Math.max(bounds[3], rect[3]);
            area += (rect[2] - rect[0]) * (rect[3] - rect[1]);
        }

        if (area != (bounds[2] - bounds[0]) * (bounds[3] - bounds[1])) {
            return false;
        }

        int yCovered = 0;
        TreeSet<int[]> sides = new TreeSet<>(new Comparator<int[]>() {
            public int compare(int[] a, int[] b) {
                if (b[0] >= a[1]) {
                    return -1;
                } else if (a[0] >= b[1]) {
                    return 1;
                } else {
                    return 0;
                }
            }
        });
        while (!pq.isEmpty()) {
            LRSide lrSide = pq.poll();
            if (lrSide.isLeft) {
                if (!sides.add(lrSide.yCoords)) {
                    return false;
                }
            } else {
                sides.remove(lrSide.yCoords);
            }
        }

    return true;
}

    /**
     * method 2: count corners at all position
     *
     * if a point is not one of the four corners of the composed rectangle,
     * it might be used as corners by two or four adjacent rectangles, in a particular way (see boolean[] inside)
     *
     * if a point is one of the four corners of the composed rectangle
     * it must be used as one corner by one retangle
     *
     * O(n)
     */
    private boolean addCorners(int x, int y, int mask, Map<Integer, Map<Integer, Integer>> maps) {
        maps.putIfAbsent(x, new HashMap<>());
        int m = maps.get(x).getOrDefault(y, 0);
        if ((m & mask) != 0) {
            return false;
        }
        maps.get(x).put(y, m | mask);
        return true;
    }

    private boolean isCorner(int x, int y, int[] border) {
        return (x == border[0] || x == border[2]) && (y == border[1] || y == border[3]);
    }

    public boolean isRectangleCover_method2(int[][] rectangles) {
        Map<Integer, Map<Integer, Integer>> maps = new HashMap<>();
        int[] border = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};
        for (int[] rect : rectangles) {
            if (!addCorners(rect[0], rect[1], 1, maps)) return false;
            if (!addCorners(rect[0], rect[3], 2, maps)) return false;
            if (!addCorners(rect[2], rect[3], 4, maps)) return false;
            if (!addCorners(rect[2], rect[1], 8, maps)) return false;
            border[0] = Math.min(border[0], rect[0]);
            border[1] = Math.min(border[1], rect[1]);
            border[2] = Math.max(border[2], rect[2]);
            border[3] = Math.max(border[3], rect[3]);
        }

        boolean[] inside = new boolean[16];
        inside[1 + 2] = inside[2 + 4] = inside[4 + 8] = inside[8 + 1] = inside[1 + 2 + 4 + 8] = true;

        for (Map.Entry<Integer, Map<Integer, Integer>> map : maps.entrySet()) {
            int x = map.getKey();
            for (Map.Entry<Integer, Integer> entry : map.getValue().entrySet()) {
                int y = entry.getKey();
                int mask = entry.getValue();
                if (isCorner(x, y, border)) {
                    if ((mask & mask - 1) != 0) {
                        return false;
                    }
                } else {
                    if (!inside[mask]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}

/* test cases:
[[1,1,3,3],[3,1,4,2],[3,2,4,4],[1,3,2,4],[2,3,3,4]]
[[1,1,2,3],[1,3,2,4],[3,1,4,2], [3,2,4,4]]
[[1,1,3,3],[3,1,4,2],[1,3,2,4], [3,2,4,4]]
[[1,1,3,3],[3,1,4,2],[1,3,2,4],[2,2,4,4]]
[[0,0,4,1],[7,0,8,2],[5,1,6,3],[6,0,7,2],[4,0,5,1],[4,2,5,3],[2,1,4,3],[-1,2,2,3],[0,1,2,2],[6,2,8,3],[5,0,6,1],[4,1,5,2]]
 */
