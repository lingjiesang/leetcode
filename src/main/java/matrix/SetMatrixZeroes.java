package matrix;

import java.util.*;

public class SetMatrixZeroes {
    /**
     * use a row / col that definitely will be set to zero eventually to store intermediate results!
     * TLE?
     */
    public void setZeroes(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        int row = -1, col = -1;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (row == -1 && matrix[i][j] == 0){
                    row = i;
                    col = j;
                    break;
                }
                if (row != -1 && j != col && matrix[i][j] == 0){
                    matrix[row][j] = 0;
                    matrix[i][col] = 0;
                }
            }
        }

        if (row != -1){
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    if (i != row && j != col && (matrix[row][j] == 0 || matrix[i][col] == 0)){
                        matrix[i][j] = 0;
                    }
                }
            }

            for (int i = 0; i < m; i++){
                matrix[i][col] = 0;
            }

            for(int j = 0; j < n; j++){
                matrix[row][j] = 0;
            }
        }
    }

    /**
     * actually just use row0 and col0 is fine!
     */

    public void setZeroes_Accepted(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        boolean row0 = false;
        for (int i = 0; i < m; i++){
            if (matrix[i][0] == 0){
                row0 = true;
            }
        }
        boolean col0 = false;
        for (int j = 0; j < n; j++){
            if (matrix[0][j] == 0){
                col0 = true;
            }
        }
        for (int i = 1; i < m ; i++){
            for (int j = 1; j < n; j++){
                if (matrix[i][j] == 0){
                    matrix[0][j] = 0;
                    matrix[i][0] = 0;
                }
            }
        }

        for (int i = 1; i < m; i++){
            for (int j = 1; j < n; j++){
                if (matrix[i][0] == 0 || matrix[0][j] == 0){
                    matrix[i][j] = 0;
                }
            }
        }


        if (row0){
            for (int i = 0; i < m ; i++){
                matrix[i][0] = 0;
            }
        }
        if (col0){
            for (int j = 0; j < n; j++){
                matrix[0][j] = 0;
            }
        }

    }
}
