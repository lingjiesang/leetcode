package matrix;

import java.util.*;

public class SearchMatrixII {
    /**
     * 削边法
     */
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        int topRow = 0, rightCol = n - 1;
        while (topRow < m && rightCol >= 0){
            if (matrix[topRow][rightCol] < target){
                topRow++;
            } else if (matrix[topRow][rightCol] > target){
                rightCol--;
            } else{
                return true;
            }
        }
        return false;
    }

    /**
     * binary search
     * ref: cc189 (v5) P408
     */

    public boolean searchMatrix_binarySearch(int[][] matrix, int target){
        if (matrix.length == 0 || matrix[0].length == 0){
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        Coordinate start = new Coordinate(0, 0);
        Coordinate end = new Coordinate(m - 1, n - 1);

        return searchMatrix_binarySearch(matrix, target, start, end);
    }

    private boolean searchMatrix_binarySearch(int[][] matrix, int target, Coordinate start, Coordinate end){
        if (!start.inBound(matrix) || !end.inBound(matrix) || !start.smallerOrEqual(end)){
            return false;
        }

        if (matrix[start.row][start.col] > target || matrix[end.row][end.col] < target){
            return false;
        }

        Coordinate startDiag = (Coordinate)start.clone();
        int dist = Math.min(end.row - start.row, end.col - start.col); //key: this way works when start.row != start.col!
        Coordinate endDiag = new Coordinate(startDiag.row + dist, startDiag.col + dist);

        while (startDiag.smallerOrEqual(endDiag)){ // key: "SmallerOrEqual" not just "Equal"
            Coordinate mid = startDiag.setAverage(endDiag);
            if (matrix[mid.row][mid.col] == target){
                return true;
            } else if (matrix[mid.row][mid.col] > target){
                endDiag = mid.minusOne();
            } else{
                startDiag = mid.plusOne();
            }
        }
        /*
          now
          1. startDiag == endDiag and
          2. matrix[startDiag.row][startDiag.col] either > target or NOT EXIST
                                  (key: not exist: lowerLeft or upperRight might not exist!)
          3. but matrix[startDiag.row  - 1][startDiag.col - 1] < target
         */

        Coordinate lowerLeftStart = new Coordinate(startDiag.row, start.col);
        Coordinate lowerLeftEnd   = new Coordinate(end.row, startDiag.col - 1);

        Coordinate upperRightStart = new Coordinate(start.row, startDiag.col);
        Coordinate upperRightEnd   = new Coordinate(startDiag.row - 1, end.col);

        return searchMatrix_binarySearch(matrix, target, lowerLeftStart, lowerLeftEnd) ||
                searchMatrix_binarySearch(matrix, target, upperRightStart, upperRightEnd);
    }


    public class Coordinate{
        public int row;
        public int col;

        public Coordinate(int r, int c){
            row = r;
            col = c;
        }

        public Coordinate setAverage(Coordinate other){
            return new Coordinate((row + other.row) / 2, (col + other.col) / 2);
        }

        public Object clone(){
            return new Coordinate(row, col);
        }
        public Coordinate minusOne(){
            row--;
            col--;
            return this;
        }

        public Coordinate plusOne(){
            row++;
            col++;
            return this;
        }

        public boolean inBound(int[][] matrix){
            return row >= 0 && row < matrix.length && col >= 0 && col < matrix[0].length;
        }

        public boolean smallerOrEqual(Coordinate other){
            return row <= other.row && col <= other.col;
        }
    }

    public static void main(String[] args){
        int[][] matrix = {
                {1,   4,  7, 11, 15},
                {2,   5,  8, 12, 19},
                {3,   6,  9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}};
        SearchMatrixII soln = new SearchMatrixII();
        System.out.println(soln.searchMatrix_binarySearch(matrix, 20));

        matrix = new int[][]{{-1, 3}};
        System.out.println(soln.searchMatrix_binarySearch(matrix, 1));

    }
}
