package matrix;

import java.util.*;

public class SpiralMatrix {
    /**
     * I
     */
    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> result = new ArrayList<>();
        if (matrix.length == 0 || matrix[0].length == 0){
            return result;
        }
        int m = matrix.length;
        int n = matrix[0].length;

        int topRow = 0, bottomRow = m - 1;
        int leftCol = 0, rightCol = n - 1;

        int round = 1;
        while (true){
            for (int col = leftCol; col <= rightCol; col++){
                result.add(matrix[topRow][col]);
            }
            topRow++;
            if (result.size() == m * n){
                break;
            }

            for (int row = topRow; row <= bottomRow; row++){
                result.add(matrix[row][rightCol]);
            }
            rightCol--;
            if (result.size() == m * n){
                break;
            }

            for (int col = rightCol; col >= leftCol; col--){
                result.add(matrix[bottomRow][col]);
            }
            bottomRow--;
            if (result.size() == m * n){
                break;
            }

            for (int row = bottomRow; row >= topRow; row--){
                result.add(matrix[row][leftCol]);
            }
            leftCol++;
            if (result.size() == m * n){
                break;
            }
        }

        return result;
    }

    /**
     * II
     */
    public int[][] generateMatrix(int n) {
        int[][] matrix = new int[n][n];
        if (n == 0){
            return matrix;
        }
        int topRow = 0, bottomRow = n - 1;
        int leftCol = 0, rightCol = n - 1;
        int num = 1;
        int maxNum = n * n;
        while (true){
            for (int col = leftCol; col <= rightCol; col++){
                matrix[topRow][col] = num++;
            }
            topRow++;
            if (num > maxNum){
                break;
            }

            for (int row = topRow; row <= bottomRow; row++){
                matrix[row][rightCol] = num++;
            }
            rightCol--;
            if (num > maxNum){
                break;
            }

            for (int col = rightCol; col >= leftCol; col--){
                matrix[bottomRow][col] = num++;
            }
            bottomRow--;
            if (num > maxNum){
                break;
            }

            for (int row = bottomRow; row >= topRow; row--){
                matrix[row][leftCol] = num++;
            }
            leftCol++;
            if (num > maxNum){
                break;
            }

        }
        return matrix;
    }


    public static void main(String[] args){
        int[][] matrix = {{1, 2, 3}, {4, 5, 6}, {7,8,9}};
        SpiralMatrix soln = new SpiralMatrix();
        System.out.println(soln.spiralOrder(matrix));
    }
}
