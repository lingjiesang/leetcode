package stack;

import java.util.*;

public class LongestValidParenthesis {

    /**
     * stick with this one!!
     *
     * difference bewteen "stack1" and "stack2":
     * "a((b))" will count as 6 by stack1, and 5 by stack2
     */
    public int longestValidParentheses_stack1(String s) {
//        Deque<Integer> stack = new ArrayDeque<>();  // ArrayDeque cause TLE, but not LinkedList!!!!
        LinkedList<Integer> stack = new LinkedList<>();

        int result = 0;
        int lastInvalid = -1;
        for (int i = 0; i < s.length(); i++){
            if (s.charAt(i) == '('){
                stack.push(i);
            } else if (s.charAt(i) == ')'){
                if (!stack.isEmpty()){
                    stack.pop();
                    if (stack.isEmpty()){
                        result = Math.max(result, i - lastInvalid); //   ")()()"
                    } else{
                        result = Math.max(result, i - stack.peek());  // "(()"
                    }
                } else{
                    lastInvalid = i;
                }
            }
        }
        return result;
    }

    /**
     * ref: the same as dpGOOD
     */
    public int longestValidParentheses_stack2(String s) {
        LinkedList<Integer> stack = new LinkedList<>();
        int result = 0;
        stack.push(-1);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ')' && stack.size() > 1 && s.charAt(stack.peek()) == '(') {
                stack.pop();
                result = Math.max(result, i - stack.peek());
            } else {
                stack.push(i);
            }
        }
        return result;
    }

    /**
     * ref: https://discuss.leetcode.com/topic/35776/two-java-solutions-with-explanation-stack-dp-short-easy-to-understand/2
     */
    public int longestValidParentheses_dpGOOD(String s) {
        int[] dp = new int[s.length()];
        int result = 0;
        int leftCount = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                leftCount++;
            } else if (leftCount > 0){
                dp[i] = dp[i - 1] + 2; // "(())"
                dp[i] += (i - dp[i]) >= 0 ? dp[i - dp[i]] : 0; // "()()"
                result = Math.max(result, dp[i]);
                leftCount--;
            }
        }
        return result;
    }

    /**
     * dp is correct, but too slow
     */
    public int longestValidParentheses_dpBAD(String s) {
        if (s == null || s.length() == 0){
            return 0;
        }
        int n = s.length();
        char[] chars = s.toCharArray();

        boolean[][] dp = new boolean[n][n];

        int maxLen = 0;

        for (int start = 0; start + 1 < n; start++){
            if (chars[start] == '(' && chars[start + 1] == ')'){
                dp[start][start + 1] = true;
                maxLen = 2;
            }
        }

        for (int step = 3; step < n; step += 2){
            for (int start = 0; start < n - step; start++){
                if (chars[start] == '(' && chars[start + step] == ')'){
                    if (dp[start + 1][start + step - 1]){
                        dp[start][start + step] = true;
                        maxLen = step + 1;
                    } else{
                        for (int leftLen = 2; leftLen < step ; leftLen += 2){
                            if (dp[start][start + leftLen - 1] && dp[start + leftLen][start + step]){
                                dp[start][start + step] = true;
                                maxLen = step + 1;
                                break;
                            }
                        }
                    }
                }
            }
        }
        return maxLen;
    }


}
