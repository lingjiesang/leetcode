package stack;

import java.util.*;

public class LargestRectangleInHistogram_DivideConquer {
    public int largestRectangleArea(int[] heights) {
        return _largestRectangleArea(heights, 0, heights.length - 1);
    }

    private int _largestRectangleArea(int[] heights, int start, int end){
        if (start > end){
            return 0;
        }
        if (start == end){
            return heights[start];
        }
        int mid = start + (end - start) / 2;
        int maxArea = Math.max(_largestRectangleArea(heights, start, mid - 1), _largestRectangleArea(heights, mid + 1, end));
        int left = mid, right = mid;
        int h = heights[mid];
        while (left >= start && right <= end) {
            h = Math.min(Math.min(heights[left], heights[right]), h);
            maxArea = Math.max(maxArea, h * (right - left + 1));

            if (left == start) {
                right++;
            } else if (right == end) {
                left--;
            } else {
                if (heights[left - 1] >= heights[right + 1]) {
                    left--;
                } else {
                    right++;
                }
            }
        }
        return maxArea;
    }
}
