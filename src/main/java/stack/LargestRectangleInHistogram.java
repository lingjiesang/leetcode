package stack;

import java.util.*;

public class LargestRectangleInHistogram {
    public int largestRectangleArea(int[] heights) {
        if (heights == null || heights.length == 0){
            return 0;
        }
        Deque<Integer> stack = new ArrayDeque<>();

        int n = heights.length;
        int[] left = new int[n];
        int[] right = new int[n];

        int maxArea = 0;

        // find leftBound
        for (int i = 0; i < left.length; i++){
            while (!stack.isEmpty() && heights[i] <= heights[stack.peek()]){
                stack.pop();
            }
            if (!stack.isEmpty()){
                left[i] = stack.peek() + 1;
            } else{
                left[i] = 0;
            }
            stack.push(i);
        }

        // find rightBound
        stack.clear();

        for (int i = right.length - 1; i >= 0; i--){
            while (!stack.isEmpty() && heights[i] <= heights[stack.peek()]){
                stack.pop();
            }
            if (!stack.isEmpty()){
                right[i] = stack.peek() - 1;
            } else{
                right[i] = n - 1;
            }
            stack.push(i);
            maxArea = Math.max(maxArea, heights[i] * (right[i] - left[i] + 1));
        }
        return maxArea;

    }
}
