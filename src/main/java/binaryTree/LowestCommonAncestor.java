package binaryTree;

public class LowestCommonAncestor {

    public TreeNode lowestCommonAncestor_Cleaner(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null){
            return null;
        }

        if (root == p || root == q){
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);

        if (left != null && right != null){
            return root;
        }
        if (left != null){
            return left;
        }
        if (right != null){
            return right;
        }
        return null;

    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {

        if (root == null){
            return null;
        }

        TreeNode leftRes = lowestCommonAncestor(root.left, p, q);
        if (leftRes != null && leftRes != p && leftRes != q){ //lca found in root's left subtree
            return leftRes;
        }

        TreeNode rightRes = lowestCommonAncestor(root.right, p, q);
        if (rightRes != null && rightRes != p && rightRes != q){ //lca found in root's right subtree
            return rightRes;
        }

        if (leftRes != null && rightRes != null){ // one found in root's left, one found in root's right
            return root;
        }

        // now either leftRes or rightRes is null or both are null
        if (leftRes == null){
            leftRes = rightRes;
        }

        if (leftRes != null && (root == p || root == q)){ // one found as root, one found in root's one subtree
            return root;
        } else{
            if (root == p || root == q){ // root is one node, the other not found
                return root;
            } else{
                return leftRes; // root is not a node
            }
        }
    }




}
