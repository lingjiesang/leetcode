package binaryTree;

import java.util.*;

public class UniqueBinarySearchTrees {
    public int numTrees(int n) {
        int[] count = new int[n + 1];
        count[0] = 1;
        count[1] = 1;
        for (int i = 2; i <= n; i++){
            for (int left = 0; left < i; left++){
                int right = i - left - 1;
                count[i] += count[left] * count[right];
            }
        }
        return count[n];

    }
}
