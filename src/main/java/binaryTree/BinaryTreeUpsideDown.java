package binaryTree;

import java.util.*;

public class BinaryTreeUpsideDown {
    public TreeNode upsideDownBinaryTree(TreeNode root) {
        if (root == null || root.left == null) {
            return root;
        }
        TreeNode left = root.left;
        TreeNode newRoot = upsideDownBinaryTree(root.left);

        left.right = root;
        left.left = root.right;

        root.left = null;
        root.right = null;

        return newRoot;

    }


    public TreeNode upsideDownBinaryTree_iterative(TreeNode root) {
        if (root == null || root.left == null) {
            return root;
        }
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode curt = root;
        while (curt != null){
            stack.push(curt);
            curt = curt.left;
        }
        TreeNode dummy = new TreeNode(0);
        curt = dummy;
        while (!stack.isEmpty()){
            TreeNode next = stack.pop();
            curt.left = next.right;
            curt.right = next;

            next.left = null;
            next.right = null;

            curt = next;
        }
        return dummy.right;
    }
}