package binaryTree;

import java.util.*;

public class BinaryTreeRightSideView {
    public List<Integer> rightSideView_BFS(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Deque<TreeNode> queue = new ArrayDeque<>();

        if (root == null){
            return res;
        }

        queue.offer(root);
        while (!queue.isEmpty()){
            res.add(queue.getLast().val);
            int size = queue.size();
            for (int i = 0; i < size; i++){
                root = queue.poll();
                if (root.left != null){
                    queue.offer(root.left);
                }
                if (root.right != null){
                    queue.offer(root.right);
                }
            }
        }

        return res;
    }


    public List<Integer> rightSideView(TreeNode root){
        List<Integer> res = new ArrayList<>();
        if (root == null){
            return res;
        }
        dfs(root, 0, res);
        return res;
    }

    private void dfs(TreeNode root, int height, List<Integer> res ){
        if (height == res.size()){
            res.add(root.val);
        } else{
            res.set(height, root.val);
        }
        if (root.left != null){
            dfs(root.left, height + 1, res);
        }
        if (root.right != null){
            dfs(root.right, height + 1, res);
        }
    }
}
