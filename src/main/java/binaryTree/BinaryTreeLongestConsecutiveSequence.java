package binaryTree;

import java.util.*;

public class BinaryTreeLongestConsecutiveSequence {
    /**
     * DFS: 2ms
     */
    public int longestConsecutive(TreeNode root) {
        if (root == null){
            return 0;
        }
        return dfs(root, 1);
    }

    private int dfs(TreeNode root, int curtLen){
        int result = curtLen;
        if (root.left != null ){
            result = Math.max(result, dfs(root.left, root.left.val == root.val + 1 ? curtLen + 1 : 1));
        }
        if (root.right != null ){
            result = Math.max(result, dfs(root.right, root.right.val == root.val + 1 ? curtLen + 1 : 1));
        }
        return result;
    }

    /**
     * BFS: 21ms
     */
    public int longestConsecutive_BFS(TreeNode root){
        if (root == null){
            return 0;
        }
        Deque<TreeNode> nodeStack = new ArrayDeque<>();
        Deque<Integer> lenStack = new ArrayDeque<>();
        nodeStack.push(root);
        lenStack.push(1);

        int maxLen = 1;
        while (!nodeStack.isEmpty()){
            root = nodeStack.pop();
            int len = lenStack.pop();
            maxLen = Math.max(maxLen, len);
            if (root.left != null) {
                nodeStack.push(root.left);
                lenStack.push(root.left.val == root.val + 1 ? len + 1 : 1);
            }
            if (root.right != null){
                nodeStack.push(root.right);
                lenStack.push(root.right.val == root.val + 1 ? len + 1 : 1);
            }
        }
        return maxLen;
    }
}
