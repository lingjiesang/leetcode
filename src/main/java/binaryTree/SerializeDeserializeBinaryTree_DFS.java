package binaryTree;

public class SerializeDeserializeBinaryTree_DFS {

    StringBuilder sb;
    int i;

    public String serialize(TreeNode root){
        sb = new StringBuilder();
        dfsSerialize(root);
        return sb.toString();
    }

    private void dfsSerialize(TreeNode root){
        if (root == null){
            sb.append("null ");
        } else{
            sb.append(Integer.toString(root.val));
            sb.append(" ");
            dfsSerialize(root.left);
            dfsSerialize(root.right);
        }
    }

    public TreeNode deserialize(String data){
        String[] nodeValues = data.split(" ");
        i = 0;
        return dfsDeserialize(nodeValues);
    }

    private TreeNode dfsDeserialize(String[] nodeValues){
        if (nodeValues[i].equals("null")){
            i++;  // don't forget here!!!
            return null;
        }
        TreeNode node = new TreeNode(Integer.valueOf(nodeValues[i]));
        i++;
        node.left = dfsDeserialize(nodeValues);
        node.right = dfsDeserialize(nodeValues);
        return node;
    }
}
