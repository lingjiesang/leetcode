package binaryTree;

import java.util.*;

public class BinaryTreeVerticalOrderTraversal {
    public List<List<Integer>> verticalOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        if (root == null){
            return result;
        }

        Map<Integer, List<Integer>> map = new HashMap<>();
        Deque<TreeNode> nodeQueue = new ArrayDeque<>();
        Deque<Integer> colNumQueue = new ArrayDeque<>();

        nodeQueue.offer(root);
        colNumQueue.offer(0);
        int minColNum = 0, maxColNum = 0;

        while (!nodeQueue.isEmpty()){
            root = nodeQueue.poll();
            int rootColNum = colNumQueue.poll();
            while (!map.containsKey(rootColNum)){
                map.put(rootColNum, new ArrayList<>());
                minColNum = Math.min(minColNum, rootColNum);
                maxColNum = Math.max(maxColNum, rootColNum);
            }
            map.get(rootColNum).add(root.val);
            if (root.left != null){
                nodeQueue.offer(root.left);
                colNumQueue.offer(rootColNum - 1);
            }
            if (root.right != null){
                nodeQueue.offer(root.right);
                colNumQueue.offer(rootColNum + 1);
            }
        }

        for (int i = minColNum; i <= maxColNum; i++){
            result.add(map.get(i));
        }
        return result;
    }


}
