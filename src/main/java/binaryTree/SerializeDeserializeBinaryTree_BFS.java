package binaryTree;


import java.util.*;

public class SerializeDeserializeBinaryTree_BFS {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if (root == null) {
            return "null";
        }
        StringBuilder sb = new StringBuilder();
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);

        sb.append(Integer.toString(root.val));

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node.left == null) {
                sb.append(" null");
            } else {
                sb.append(' ');
                sb.append(Integer.toString(node.left.val));
                queue.offer(node.left);
            }
            if (node.right == null) {
                sb.append(" null");
            } else {
                sb.append(' ');
                sb.append(Integer.toString(node.right.val));
                queue.offer(node.right);
            }
        }
        return sb.toString();

    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if (data.equals("null")) {
            return null;
        }
        Queue<TreeNode> queue = new ArrayDeque<>();
        String[] nodeValues = data.split(" ");

        TreeNode root = new TreeNode(Integer.valueOf(nodeValues[0]));
        queue.offer(root);
        int i = 1;
        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (nodeValues[i].equals("null")) {
                node.left = null;
            } else {
                node.left = new TreeNode(Integer.valueOf(nodeValues[i]));
                queue.offer(node.left);
            }
            i++;
            if (nodeValues[i].equals("null")) {
                node.right = null;
            } else {
                node.right = new TreeNode(Integer.valueOf(nodeValues[i]));
                queue.offer(node.right);
            }
            i++;
        }
        return root;
    }

}
