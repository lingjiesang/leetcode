package binaryTree;

import java.util.*;

public class CountUnivalSubstrees {
    public int countUnivalSubtrees(TreeNode root) {
        return countUniSubTrees(root)[0];
    }

    /**
     * return value int[3]:
     * 0.  # of uniSubTrees
     * 1. value of uniSubTrees
     * 2. if the whole tree at root are all uni-value: 1 - true, 2 -false
     */

    private int[] countUniSubTrees(TreeNode root) {
        if (root == null) {
            return new int[]{0, 0, 1};
        }

        int[] left = countUniSubTrees(root.left);
        int[] right = countUniSubTrees(root.right);
        if ((left[0] == 0 || left[2] == 1 && left[1] == root.val) && (right[0] == 0 || right[2] == 1 && right[1] == root.val)) {
            return new int[]{left[0] + 1 + right[0], root.val, 1};
        } else {
            return new int[]{left[0] + right[0], 0, 0};
        }
    }
}
