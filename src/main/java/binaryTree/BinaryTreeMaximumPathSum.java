package binaryTree;


public class BinaryTreeMaximumPathSum {

    class PathSum{
        int leftSum; //from root (include) to leftsubtree (might not include)
        int rightSum; //from root (include) to rightsubtree (might not include)
        int maxSum;
        PathSum(int l, int r, int m){
            leftSum = l;
            rightSum = r;
            maxSum = m;
        }
    }
    public int maxPathSum(TreeNode root) {
        if (root == null){
            return 0;
        }
        return _maxPathSum(root).maxSum;
    }

    private PathSum _maxPathSum(TreeNode root){
        PathSum leftPathSum = root.left != null ? _maxPathSum(root.left) : new PathSum(0, 0, Integer.MIN_VALUE);
        int leftSum = Math.max(leftPathSum.leftSum, leftPathSum.rightSum);
        leftSum = Math.max(leftSum, 0);
        leftSum += root.val;

        PathSum rightPathSum = root.right != null ? _maxPathSum(root.right) : new PathSum(0, 0, Integer.MIN_VALUE);
        int rightSum = Math.max(rightPathSum.leftSum, rightPathSum.rightSum);
        rightSum = Math.max(rightSum, 0);
        rightSum += root.val;

        int maxSum = Math.max(leftPathSum.maxSum, rightPathSum.maxSum);
        maxSum = Math.max(maxSum, leftSum + rightSum - root.val);

        return new PathSum(leftSum, rightSum, maxSum);

    }
}
