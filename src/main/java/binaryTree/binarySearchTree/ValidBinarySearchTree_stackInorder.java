package binaryTree.binarySearchTree;

import binaryTree.TreeNode;

import java.util.*;

public class ValidBinarySearchTree_stackInorder {
    public boolean isValidBST(TreeNode root) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode pre = null;
        while (root != null || !stack.isEmpty()){
            if (root != null){
                stack.push(root);
                root = root.left;
            } else{
                root = stack.pop();
                if (!check(pre, root)){
                    return false;
                }
                pre = root;
                root = root.right;
            }
        }
        return true;
    }

    private boolean check(TreeNode pre, TreeNode curt){
        return pre == null || pre.val < curt.val;
    }
}