package binaryTree.binarySearchTree;


import binaryTree.TreeNode;

public class RecoverBinarySearchTree {


    private TreeNode first;
    private TreeNode second;
    public void recoverTree(TreeNode root){
        first = null; // java pass by value, is pass first and second into "visit", outside value won't change!!
        second = null;

        TreeNode prev = new TreeNode(Integer.MIN_VALUE);

        while (root != null){
            TreeNode node = root.left; // node is the right most node on the left subtree of root

            if (node != null) {
                while (node.right != null && node.right != root) {
                    node = node.right;
                }

                if (node.right == null) {
                    node.right = root; // add "thread"
                    root = root.left;   // enter left tree of root

                } else{  //curt.right == root
                    node.right = null; // remove "thread"
                    visit(prev, root);
                    prev = root;
                    root = root.right;
                }
            } else{
                // this is the leftmost node!
                visit(prev, root);
                prev = root;
                root = root.right;
            }

        }

        if (first != null){
            int val = first.val;
            first.val = second.val;
            second.val = val;
        }
    }



    private void visit(TreeNode prev, TreeNode curt){
        if (prev.val > curt.val){
            if (first == null){
                first = prev;
            }
            second = curt;
        }
    }
}
