package binaryTree.binarySearchTree;

import binaryTree.TreeNode;

import java.util.*;

public class ValidBinarySearchTree_bottomUp {
    class Result{
        long max;
        long min;
        boolean isValid;
        public Result(long max, long min, boolean isValid){
            this.max = max;
            this.min = min;
            this.isValid = isValid;
        }
    }
    private Result check(TreeNode root){
        if (root == null){
            return new Result(Long.MIN_VALUE, Long.MAX_VALUE, true);
        } else{
            Result left = check(root.left);
            Result right = check(root.right);
            long min = Math.min(left.min, right.min);
            min = Math.min(min, root.val);
            long max = Math.max(left.max, right.max);
            max = Math.max(max, root.val);
            if (left.isValid && right.isValid && root.val > left.max && root.val < right.min){
                return new Result(max, min, true);
            } else{
                return new Result(max, min, false);
            }
        }
    }

    public boolean isValidBST(TreeNode root) {
        return check(root).isValid;
    }
}
