package binaryTree.binarySearchTree;

import binaryTree.TreeNode;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/4659/c-in-order-traversal-and-please-do-not-rely-on-buggy-int_max-int_min-solutions-any-more/36
 */

public class ValidBinarySearchTree_recursiveInorder {

    TreeNode prev;
    public boolean isValidBST(TreeNode root) {
        prev = null;
        return checkTree(root);
    }

    private boolean checkTree(TreeNode root){
        if (root == null){
            return true;
        }

        if (!checkTree(root.left)){
            return false;
        }

        if (!checkNode(root)){
            return false;
        }

        if (!checkTree(root.right)){
            return false;
        }

        return true;

    }

    private boolean checkNode(TreeNode node){
        if (prev != null && prev.val >= node.val){
            return false;
        }
        prev = node;
        return true;
    }
}
