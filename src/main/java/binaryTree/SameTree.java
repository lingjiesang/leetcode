package binaryTree;

import java.util.*;

public class SameTree {
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p != null & q != null) {
            return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        } else {
            return false;
        }
    }


    public boolean isSameTree_iterative(TreeNode p, TreeNode q) {
        Deque<TreeNode> stack = new ArrayDeque<>();

        if (p == null){
            return q == null;
        }

        while (p != null || !stack.isEmpty()){
            if (p != null){
                if (q == null || p.val != q.val){
                    return false;
                }
                stack.push(p);
                stack.push(q);
                p = p.left;
                q = q.left;
            } else{
                if (q != null){
                    return false;
                }
                q = stack.pop().right;
                p = stack.pop().right;

            }
        }
        return true;
    }
}