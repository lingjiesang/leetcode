package binaryTree;

import java.util.*;

public class MaximumDepthOfBinaryTree {
    private int depth;
    public int maxDepth(TreeNode root) {
        if (root == null){
            return 0;
        }
        depth = 0;
        dfs(root, 1);
        return depth;
    }

    private void dfs(TreeNode root, int d){
        if (root.left == null && root.right == null){
            depth = Math.max(depth, d);
            return;
        }
        if (root.left != null){
            dfs(root.left, d + 1);
        }
        if (root.right != null){
            dfs(root.right, d + 1);
        }
    }
}
