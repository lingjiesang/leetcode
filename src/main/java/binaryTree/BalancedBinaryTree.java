package binaryTree;

import java.util.*;

public class BalancedBinaryTree {
    public boolean isBalanced(TreeNode root) {
        return height(root) != -1;
    }

    int height(TreeNode root){
        if (root == null){
            return 0;
        }
        int leftHeight = height(root.left);
        int rightHeight = height(root.right);
        if (leftHeight != -1 && rightHeight != -1 && leftHeight  - rightHeight <= 1 && rightHeight - leftHeight <= 1){
            return Math.max(leftHeight, rightHeight) + 1;
        } else{
            return -1;
        }
    }
}
