package binaryTree;


public class NextRightPointersII {
    public class TreeLinkNode {
        int val;
        TreeLinkNode left;
        TreeLinkNode right;
        TreeLinkNode next;

        TreeLinkNode(int x) {
            val = x;
        }
    }

    public void connect(TreeLinkNode root){
        TreeLinkNode last_start = root;
        TreeLinkNode curt_start = null;

        while (last_start != null) {
            // find curt_start and save a copy for next iteration
            while (last_start != null && curt_start == null) {
                if (last_start.left != null) {
                    curt_start = last_start.left;
                } else if (last_start.right != null) {
                    curt_start = last_start.right;
                    last_start = last_start.next; //key
                } else {
                    last_start = last_start.next;
                }
            }
            TreeLinkNode curt_start_copy = curt_start;

            // start to link curt layer, starting from curt_start
            while (curt_start != null){
                TreeLinkNode next = null;
                //find "next" of curt_start
                while (next == null && last_start != null){
                    if (last_start.left == curt_start){
                        if (last_start.right != null){
                            next = last_start.right;
                        }
                        last_start = last_start.next;
                    } else{
                        if (last_start.left != null){
                            next = last_start.left;
                        } else if (last_start.right != null){
                            next = last_start.right;
                            last_start = last_start.next; // key
                        } else{
                            last_start = last_start.next;
                        }
                    }
                }
                curt_start.next = next;
                curt_start = next;
            }
            // go to next layer
            last_start = curt_start_copy;
        }

    }
}
