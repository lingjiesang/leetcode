package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class Postorder {
    /**
     * 1. iterative
     */
    public List<Integer> postorderTraversal(TreeNode root) {
        Deque<TreeNode> stack = new ArrayDeque<>();

        LinkedList<Integer> res = new LinkedList<>();

        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                res.addFirst(root.val);
                stack.push(root);
                root = root.right;
            } else {
                root = stack.pop();
                root = root.left;
            }
        }
        return res;
    }

    /**
     * 2. recursive
     */
    public List<Integer> postorderTraversal_recursive(TreeNode root) {
        LinkedList<Integer> res = new LinkedList<>();
        postorder_recursive(res, root);
        return res;
    }

    private void postorder_recursive(List<Integer> result, TreeNode root) {
        if (root == null) {
            return;
        }
        postorder_recursive(result, root.left);
        postorder_recursive(result, root.right);
        result.add(root.val);

    }

    /**
     * 3. Morris threading
     * ref: http://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html
     */
    TreeNode pre;
    public List<Integer> postorderTraversal_morris(TreeNode root) {
        List<Integer> res = new LinkedList<>();

        pre = null;
        while (root != null){
            TreeNode node = root.right;
            if (node != null){
                while (node.left != null && node.left != root){
                    node = node.left;
                }
                if (node.left == null){
                    node.left = root;
                    visit(root, res);
                    root = root.right;
                } else{
                    node.left = null;
                    root = root.left;
                }
            } else{
                visit(root, res);
                root = root.left;
            }
        }
        return res;
    }

    private void visit(TreeNode curt, List<Integer> res) {
        pre = curt;
        res.add(0, curt.val);
    }
}
