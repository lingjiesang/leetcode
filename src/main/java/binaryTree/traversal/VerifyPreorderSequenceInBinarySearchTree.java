package binaryTree.traversal;

import java.util.*;

public class VerifyPreorderSequenceInBinarySearchTree {
    /**
     * 25ms
     */
    public boolean verifyPreorder_Natural(int[] preorder) {
        if (preorder.length == 0){
            return true;
        }
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(preorder[0]);
        int root = Integer.MIN_VALUE;
        for (int i = 1; i < preorder.length; i++){
            if (preorder[i] < stack.peek()){
                if (preorder[i] < root){
                    return false;
                }
                stack.push(preorder[i]);
            } else{
                while(!stack.isEmpty() && stack.peek() < preorder[i]){
                    root = stack.pop();
                }
                stack.push(preorder[i]);
            }
        }
        return true;
    }

    /**
     * 26ms
     */
    public boolean verifyPreorder_Cleaner(int[] preorder) {
        Deque<Integer> stack = new ArrayDeque<>();
        int root = Integer.MIN_VALUE;
        for (int i = 0; i < preorder.length; i++) {
            if (preorder[i] < root) {
                return false;
            }
            while (!stack.isEmpty() && stack.peek() < preorder[i]) {
                root = stack.pop();
            }
            stack.push(preorder[i]);

        }
        return true;
    }

    /**
     * followup:
     * use original preorder to store content in stack (this will destroy preorder)
     * 5ms
     */
    public boolean verifyPreorder_Followup(int[] preorder) {
        int root = Integer.MIN_VALUE;
        int stack_top = -1;
        for (int i = 0; i < preorder.length; i++) {
            if (preorder[i] < root) {
                return false;
            }
            while (stack_top >= 0 && preorder[stack_top] < preorder[i]) {
                root = preorder[stack_top--];
            }
            preorder[++stack_top] = preorder[i];

        }
        return true;
    }

    /**
     * followup: not destroy original preorder, but really slow...
     * 332ms
     */
    public boolean verifyPreorder_Followup2(int[] preorder) {
        int root = Integer.MIN_VALUE;
        int stack_top = -1;
        for (int i = 0; i < preorder.length; i++) {
            if (preorder[i] < root) {
                return false;
            }
            while (stack_top >= 0 && preorder[stack_top] < preorder[i]) {
                if (preorder[stack_top] > root) {
                    root = preorder[stack_top];
                }
                stack_top--;
            }
            stack_top = i;

        }
        return true;
    }

}
