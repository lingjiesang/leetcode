package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class ConstructFromPreorderInorder_Cache {

    Map<Integer, Integer> inorderMap;
    public TreeNode buildTree(int[] preorder, int[] inorder) {

        inorderMap = new HashMap<>();
        for (int i = 0; i < inorder.length; i++) {
            inorderMap.put(inorder[i], i);
        }

        return _build(preorder, 0, preorder.length - 1, inorder, 0);
    }

    // no need to have ied as parameter (redundant because ped - pst == ied - ist)
    private TreeNode _build(int[] preorder, int pst, int ped, int[] inorder, int ist){
        if (pst > ped){
            return null;
        }

//        if (pst == ped){
//            return new TreeNode(preorder[pst]);
//        }

        int leftSize = inorderMap.get(preorder[pst]) - ist;

        TreeNode root = new TreeNode(preorder[pst]);
        root.left = _build(preorder, pst + 1, pst + leftSize, inorder, ist);
        root.right = _build(preorder, pst + leftSize + 1, ped, inorder, ist + leftSize + 1);
        return root;
    }



}

