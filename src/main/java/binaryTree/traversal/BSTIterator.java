package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;


public class BSTIterator {
    Deque<TreeNode> stack;

    public BSTIterator(TreeNode root) {
        stack = new ArrayDeque<>();
        while (root != null){
            stack.push(root);
            root = root.left;
        }
    }

    /** @return whether we have a next smallest number */
    public boolean hasNext() {
        return !stack.isEmpty();
    }

    /** @return the next smallest number */
    public int next() {
        TreeNode node = stack.pop();
        TreeNode next = node.right;
        while (next != null){
            stack.push(next);
            next = next.left;
        }
        return node.val;
    }
}
