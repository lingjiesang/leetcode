package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class ConstructFromPreorderInorder {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        return _build(preorder, 0, preorder.length - 1, inorder, 0);
    }

    // no need to have ied as parameter (redundant because ped - pst == ied - ist)
    private TreeNode _build(int[] preorder, int pst, int ped, int[] inorder, int ist){
        if (pst > ped){
            return null;
        }

//        if (pst == ped){
//            return new TreeNode(preorder[pst]);
//        }

        int leftSize = 0;
        while (inorder[ist + leftSize] != preorder[pst]){
            leftSize++;
        }

        TreeNode root = new TreeNode(preorder[pst]);
        root.left = _build(preorder, pst + 1, pst + leftSize, inorder, ist);
        root.right = _build(preorder, pst + leftSize + 1, ped, inorder, ist + leftSize + 1);
        return root;
    }
}
