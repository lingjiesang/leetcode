package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class FlatternBinaryTreeToLinkedList {
    /**
     * top down (1ms)
     * Morris traversal
     * ref: https://discuss.leetcode.com/topic/3995/share-my-simple-non-recursive-solution-o-1-space-complexity/2
     */
    public void flatten_fast(TreeNode root) {
        while (root != null){
            if (root.left != null){
                TreeNode node = root.left;
                while (node.right != null){
                    node = node.right;
                }

                // move root.right to the right of rightmost node on the left subtree of root
                node.right = root.right;

                // move root.left to root.right
                root.right = root.left;
                root.left = null;
            }

            //enter right subtree of root and continue
            root = root.right;
        }
    }

    /**
     * bottom up (6ms)
     */
    public void flatten(TreeNode root) {
        if (root == null){
            return;
        }

        flatten(root.right);

        if (root.left != null){
            TreeNode node = root.left;
            while (node.right != null){
                node = node.right;
            }
            // move root.right to node.right
            node.right = root.right;
            root.right = null;

            flatten(root.left);

            //move root.left to root.right
            root.right = root.left;
            root.left = null;

        }
    }
}
