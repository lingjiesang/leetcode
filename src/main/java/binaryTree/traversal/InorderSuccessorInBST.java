package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class InorderSuccessorInBST {
    /**
     * if a node has no right subtree, successor is a closet node whose left subtree include this node
     * otherwise, sucessor is the leftmost node of its right subtree
     */
    public TreeNode inorderSuccessor(TreeNode root, TreeNode p) {
        TreeNode sucessor = null;
        while (root != p && root != null) {
            if (p.val < root.val) {
                sucessor = root;
                root = root.left;
            } else {
                root = root.right;
            }
        }
        if (root == null) {
            return null;
        }
        if (root.right != null) {
            sucessor = root.right;
            while (sucessor.left != null) {
                sucessor = sucessor.left;
            }
        }
        return sucessor;
    }

    /**
     * ref:https://discuss.leetcode.com/topic/25698/java-python-solution-o-h-time-and-o-1-space-iterative
     */
    public TreeNode inorderSuccessor_Cleaner(TreeNode root, TreeNode p) {
        TreeNode succ = null;
        while (root != null) {
            if (p.val < root.val) {
                succ = root;
                root = root.left;
            }
            else
                root = root.right;
        }
        return succ;
    }

}
