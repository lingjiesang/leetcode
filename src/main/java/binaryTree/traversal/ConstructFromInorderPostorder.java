package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class ConstructFromInorderPostorder {
    Map<Integer, Integer> inorderPos;
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        inorderPos = new HashMap<>();
        for (int i = 0; i < inorder.length; i++){
            inorderPos.put(inorder[i], i);
        }
        return build(inorder, 0, postorder, 0, postorder.length - 1);
    }

    private TreeNode build(int[] inorder, int ist, int[] postorder, int pst, int ped){
        if (pst > ped){
            return null;
        }

        TreeNode root = new TreeNode(postorder[ped]);
//        if (pst == ped){
//            return root;
//        }

        int leftSize = inorderPos.get(postorder[ped]) - ist;
        root.left = build(inorder, ist, postorder, pst, pst + leftSize - 1);
        root.right = build(inorder, ist + leftSize + 1, postorder, pst + leftSize, ped - 1);
        return root;
    }
}
