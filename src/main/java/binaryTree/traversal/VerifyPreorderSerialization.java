package binaryTree.traversal;

import java.util.*;

public class VerifyPreorderSerialization {
    /**
     * method 1: count # of needed leaf nodes
     */
    public boolean isValidSerialization(String preorder) {
        int leafNodes = 1;
        String[] nodes = preorder.split(",");

        for (int i = 0; i < nodes.length; i++){
            if (nodes[i].equals("#")){
                leafNodes--;
            } else{
                leafNodes++;
            }
            if (leafNodes == 0){
                return i == nodes.length - 1;
            } else if (leafNodes < 0){
                return false;
            }
        }
        return false;
    }

    /**
     * method2 : stack
     */
    public boolean isValidSerialization_Stack(String preorder) {
        String[] splitted = preorder.split(",");
        int stackDepth = 0;
        boolean foundLeft = false;
        for (int i = 0; i < splitted.length; i++){
            String str = splitted[i];
            if (str.charAt(0) == '#'){
                if (!foundLeft){
                    foundLeft = true;
                } else{
                    if (--stackDepth < 0) {
                        return false;
                    }
                    foundLeft = true;
                }
            } else{
                if (foundLeft){
                    if (--stackDepth < 0) {
                        return false;
                    }
                    stackDepth++;
                    foundLeft = false;
                } else{
                    stackDepth++;
                }
            }
        }
        return stackDepth == 0;
    }
}
