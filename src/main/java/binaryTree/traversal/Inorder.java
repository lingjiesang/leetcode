package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class Inorder {
    /**
     * 1. iterative
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        Deque<TreeNode> stack = new ArrayDeque<>();

        List<Integer> res = new ArrayList<>();

        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                stack.push(root);
                root = root.left;
            } else {
                root = stack.pop();
                res.add(root.val);
                root = root.right;
            }
        }
        return res;
    }

    /**
     * 2. recursive
     */
    public List<Integer> inorderTraversal_recursive(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        inorder_recursive(root, res);
        return res;
    }

    private void inorder_recursive(TreeNode root, List<Integer> res) {
        if (root == null) {
            return;
        }
        inorder_recursive(root.left, res);
        res.add(root.val);
        inorder_recursive(root.right, res);
    }

    /**
     * 3. Morris threading
     * ref: http://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html
     */
    TreeNode pre;
    public List<Integer> inorderTraversal_morris(TreeNode root) {
        List<Integer> res = new ArrayList<>();

        pre = null;

        while (root != null){
            TreeNode node = root.left;
            if (node != null){
                while (node.right != null && node.right != root){
                    node = node.right;
                }
                if (node.right == null){
                    node.right = root;
                    root = root.left;
                } else{
                    node.right = null;
                    visit(root, res);
                    root = root.right;
                }
            } else{
                visit(root, res);
                root = root.right;
            }
        }
        return res;
    }

    private void visit(TreeNode curt, List<Integer> res){
        pre = curt;
        res.add(curt.val);
    }
}
