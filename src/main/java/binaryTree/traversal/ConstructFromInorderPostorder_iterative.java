package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;


public class ConstructFromInorderPostorder_iterative {
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        if (postorder.length == 0){
            return null;
        }
        Deque<TreeNode> stack = new ArrayDeque<>();

        TreeNode root = new TreeNode(postorder[postorder.length - 1]);
        TreeNode lastNode = root;
        stack.push(lastNode);

        for (int i = postorder.length - 2, j = inorder.length - 1; i >= 0; i--){
            if (lastNode.val != inorder[j]){
                lastNode.right = new TreeNode(postorder[i]);
                lastNode = lastNode.right;
                stack.push(lastNode);
            } else{
                while (!stack.isEmpty() && stack.peek().val == inorder[j]){
                    j--;
                    lastNode = stack.pop();
                }
                lastNode.left = new TreeNode(postorder[i]);
                lastNode = lastNode.left;
                stack.push(lastNode);
            }
        }
        return root;
    }
}