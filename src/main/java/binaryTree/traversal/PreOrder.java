package binaryTree.traversal;

import binaryTree.TreeNode;
import java.util.*;

public class PreOrder {
    /**
     * 1. iterative
     */
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                stack.push(root);
                res.add(root.val);
                root = root.left;
            } else {
                root = stack.pop();
                root = root.right;
            }
        }
        return res;
    }

    /**
     * 3. Morris threading
     * ref: http://www.cnblogs.com/AnnieKim/archive/2013/06/15/morristraversal.html
     */
    TreeNode pre;
    public List<Integer> preorderTraversal_morris(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        pre = null;

        while (root != null){
            TreeNode node = root.left;
            if (node != null){
                while (node.right != null && node.right != root){
                    node = node.right;
                }
                if (node.right == null){
                    node.right = root;
                    visit(root, res);
                    root = root.left;
                } else{
                    node.right = null;
                    root = root.right;
                }
            } else{
                visit(root, res);
                root = root.right;
            }
        }
        return res;
    }

    private void visit(TreeNode curt, List<Integer> res){
        pre = curt;
        res.add(curt.val);
    }
}