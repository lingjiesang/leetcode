package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class ConstructFromPreorderInorder_iterative {
    /**
     * ref: https://discuss.leetcode.com/topic/795/the-iterative-solution-is-easier-than-you-think/17
     * see answer from "iambright"
     */
    public TreeNode buildTree(int[] preorder, int[] inorder){
        if (preorder == null || preorder.length == 0){
            return null;
        }
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode root = new TreeNode(preorder[0]);
        TreeNode lastNode = root;
        stack.push(lastNode);

        for (int i = 1, j = 0; i < preorder.length; i++){
            if (lastNode.val != inorder[j]){
                lastNode.left = new TreeNode(preorder[i]);
                lastNode = lastNode.left;
                stack.push(lastNode);
            } else{
                while (!stack.isEmpty() && stack.peek().val == inorder[j]){
                    lastNode = stack.pop();
                    j++;
                }
                lastNode.right = new TreeNode(preorder[i]);
                lastNode = lastNode.right;
                stack.push(lastNode);
            }
        }
        return root;
    }
}
