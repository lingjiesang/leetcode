package binaryTree.traversal;

import binaryTree.TreeNode;

import java.util.*;

public class ClosestBinarySearchTreeValue {
    /**
     * 270. I
     */
    public int closestValue(TreeNode root, double target) {
        int result = root.val;
        while (root != null) {
            if (Math.abs(root.val - target) < Math.abs(result - target)) {
                result = root.val;
            }
            if (root.val > target) {
                root = root.left;
            } else {
                root = root.right;
            }
        }
        return result;
    }

    /**
     * 272.II
     * method1: O(N)
     */
    public List<Integer> closestKValues_method1(TreeNode root, double target, int k) {
        LinkedList<Integer> result = new LinkedList<>();
        Deque<TreeNode> stack = new ArrayDeque<>();
        while (root != null || !stack.isEmpty()) {
            if (root != null) {
                stack.push(root);
                root = root.left;

            } else {
                root = stack.pop();
                if (result.size() < k) {
                    result.addLast(root.val);
                } else {
                    if (Math.abs(result.getFirst() - target) > Math.abs(root.val - target)) {
                        result.pollFirst();
                        result.addLast(root.val);
                    }
                }
                root = root.right;
            }
        }
        return result;
    }

    /**
     * method2: O(Kh), h = logN for balanced tree
     */
    public List<Integer> closestKValues(TreeNode root, double target, int k) {
        Deque<TreeNode> predecessor = new ArrayDeque<>();
        Deque<TreeNode> successor = new ArrayDeque<>();
        while (root != null){
            if (root.val < target){
                predecessor.push(root);
                root = root.right;
            } else{
                successor.push(root);
                root = root.left;
            }
        }
        LinkedList<Integer> result = new LinkedList<>();
        while (!predecessor.isEmpty() || !successor.isEmpty()){
            if (result.size() == k){
                break;
            }
            if (predecessor.isEmpty()){
                result.addLast(getSuccessor(successor));
            } else if (successor.isEmpty()){
                result.addFirst(getPredecessor(predecessor));
            } else if (Math.abs(predecessor.peek().val - target) < Math.abs(successor.peek().val - target)){
                result.addFirst(getPredecessor(predecessor));
            } else{
                result.addLast(getSuccessor(successor));
            }
        }
        return result;
    }

    private int getSuccessor(Deque<TreeNode> stack){
        TreeNode root = stack.pop();
        TreeNode node = root.right;
        while (node != null){
            stack.push(node);
            node = node.left;
        }
        return root.val;
    }

    private int getPredecessor(Deque<TreeNode> stack){
        TreeNode root = stack.pop();
        TreeNode node = root.left;
        while (node != null){
            stack.push(node);
            node = node.right;
        }
        return root.val;
    }
}
