package binaryTree;

import java.util.*;

public class NextRightPointer {
    public class TreeLinkNode {
        int val;
        TreeLinkNode left;
        TreeLinkNode right;
        TreeLinkNode next;

        TreeLinkNode(int x) {
            val = x;
        }
    }

    public void connect(TreeLinkNode root) {
        if (root == null){
            return;
        }

        while (root.left != null) {
            TreeLinkNode node = root;
            while (node != null) {
                node.left.next = node.right;
                if (node.next != null){
                    node.right.next = node.next.left;
                }
                node = node.next;
            }

            root = root.left;
        }
    }
}
