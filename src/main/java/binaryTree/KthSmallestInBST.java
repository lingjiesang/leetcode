package binaryTree;

import java.util.*;

/**
 * k-th is 1-based, i.e. 1-st smallest is the smallest element in BST
 */
public class KthSmallestInBST {
//    public int kthSmallest(TreeNode root, int k) {
//        Deque<TreeNode> stack = new ArrayDeque<>();
//        while (root != null || !stack.isEmpty()){
//            if (root != null){
//                stack.push(root);
//                root = root.left;
//            } else{
//                root = stack.pop();
//                k--;
//
//                if (k == 0){
//                    return root.val;
//                }
//                root = root.right;
//            }
//        }
//        return -1;
//    }

    /**
     * a similar idea:
     * https://discuss.leetcode.com/topic/17668/what-if-you-could-modify-the-bst-node-s-structure
     */
    Map<TreeNode, Integer> countMap;
    public int kthSmallest(TreeNode root, int k){
        countMap = new HashMap<>();
        return _Kth(root, k);
    }

    private int _Kth(TreeNode root, int k){
        if (root == null){
            return -1;
        }

        int leftCount = count(root.left);
        if (leftCount >= k){
            return _Kth(root.left, k);
        } else if (leftCount == k - 1){
            return root.val;
        } else{
            return _Kth(root.right, k - leftCount - 1);
        }
    }


    private int count(TreeNode root){
        if (root == null){
            return 0;
        }
        if (!countMap.containsKey(root)){
            countMap.put(root, count(root.left) + 1 + count(root.right));
        }
        return countMap.get(root);
    }
}
