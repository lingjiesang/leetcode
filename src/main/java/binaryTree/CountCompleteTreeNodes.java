package binaryTree;

/**
 * ref:
 * https://discuss.leetcode.com/topic/31392/my-java-solution-with-explanation-which-beats-99/2
 *
 * key for binary search of last layer is not start from root, but use lower and lower layers as root
 */
public class CountCompleteTreeNodes {
    public int countNodes(TreeNode root) {
        int leftHeight = getHeight(root, true);
        int rightHeight = getHeight(root, false);
        int count = (1 << rightHeight) - 1;
        if (leftHeight != rightHeight){
            count += countLastLayer(root, leftHeight);
        }
        return count;
    }

    private int countLastLayer(TreeNode root, int leftHeight){
        if (root == null){
            return 0;
        }
        TreeNode midNode = root.left;
        int height = 2;
        while (height < leftHeight){
            midNode = midNode.right;
            height++;
        }
        if (midNode == null){
            return countLastLayer(root.left, leftHeight - 1);
        } else{
            return (1 << (leftHeight - 2)) + countLastLayer(root.right, leftHeight - 1);
        }
    }

    private int getHeight(TreeNode root, boolean isLeft){
        int result = 0;
        while (root != null){
            result++;
            if (isLeft){
                root = root.left;
            } else{
                root = root.right;
            }
        }
        return result;
    }
}
