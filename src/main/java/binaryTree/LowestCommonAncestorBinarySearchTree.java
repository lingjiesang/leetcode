package binaryTree;

import java.util.*;

public class LowestCommonAncestorBinarySearchTree {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        while (root != null){
            if ( (root.val - p.val) * (root.val - q.val) <= 0){
                return root;
            } else if (root.val > p.val){
                root = root.left;
            } else{
                root = root.right;
            }
        }
        return null;
    }
}
