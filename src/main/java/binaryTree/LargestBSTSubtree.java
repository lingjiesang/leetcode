package binaryTree;

import java.util.*;

public class LargestBSTSubtree {
    public class Result {
        int max;
        int min;
        int size;

        public Result(int max, int min, int size) {
            this.max = max;
            this.min = min;
            this.size = size;
        }
    }

    public int largestBSTSubtree(TreeNode root) {
        return subTree(root).size;
    }

    private Result subTree(TreeNode root) {
        if (root == null) {
            return new Result(Integer.MIN_VALUE, Integer.MAX_VALUE, 0);
        }
        Result left = subTree(root.left);
        Result right = subTree(root.right);
        if (root.val >= left.max && root.val <= right.min) {
            return new Result(Math.max(root.val, right.max), Math.min(root.val, left.min), left.size + 1 + right.size);
        } else {
            return new Result(Integer.MAX_VALUE, Integer.MIN_VALUE, Math.max(left.size, right.size));
        }
    }
}
