package binaryTree;

import linkedlist.ListNode;

import java.util.*;

public class SortedListToBST {

    private ListNode curt;

    public TreeNode sortedListToBST(ListNode head) {
        if (head == null) {
            return null;
        }
        int size = getListSize(head);
        curt = head;
        return helper(size);
    }

    private TreeNode helper(int size) {
        if (size <= 0) {
            return null;
        }
        TreeNode left = helper(size / 2);
        TreeNode root = new TreeNode(curt.val);
        curt = curt.next;
        TreeNode right = helper(size - 1 - size / 2);
        root.left = left;
        root.right = right;
        return root;

    }

    private int getListSize(ListNode head) {
        int size = 0;
        while (head != null) {
            size++;
            head = head.next;
        }
        return size;
    }

}
