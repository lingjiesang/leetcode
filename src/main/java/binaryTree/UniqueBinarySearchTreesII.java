package binaryTree;

import java.util.*;

public class UniqueBinarySearchTreesII {
    public List<TreeNode> generateTrees(int n) {
        if (n <= 0){
            return new ArrayList<>();
        }
        return _generateTress(1, n); //start from 1, not 0!!
    }

    private List<TreeNode> _generateTress(int start, int end){
        List<TreeNode> res = new ArrayList<>();
        if (start > end){
            res.add(null); //key!!!
            return res;
        }

        if (start == end){
            res.add(new TreeNode(start));
            return res;
        }

        for (int rootVal = start; rootVal <= end; rootVal++){
            List<TreeNode> leftTree = _generateTress(start, rootVal - 1);
            List<TreeNode> rightTree = _generateTress(rootVal + 1, end);
            for (int j = 0; j < leftTree.size(); j++){
                for (int k = 0; k < rightTree.size(); k++){
                    TreeNode root = new TreeNode(rootVal);
                    root.left = leftTree.get(j);
                    root.right = rightTree.get(k);
                    res.add(root);
                }
            }
        }
        return res;

    }
}
