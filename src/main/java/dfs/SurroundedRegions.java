package dfs;

import java.util.*;

/**
 * this solution uses dfs
 *
 * refer : https://discuss.leetcode.com/topic/38370/bfs-based-solution-in-java
 * for bfs (one benefit to use bfs is avoid stack overflow!)
 */
public class SurroundedRegions {
    public void solve(char[][] board) {
        if (board.length == 0 || board[0].length == 0){
            return;
        }
        for (int i = 0; i < board[0].length; i++){
            if (board[0][i] == 'O'){
                dfs(board, 0, i);
            }
            if (board[board.length - 1][i] == 'O'){
                dfs(board, board.length - 1, i);
            }
        }

        for (int i = 1; i < board.length - 1; i++){
            if (board[i][0] == 'O'){
                dfs(board, i, 0);
            }
            if (board[i][board[0].length - 1] == 'O'){
                dfs(board, i, board[0].length - 1);
            }
        }

        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[0].length; j++){
                if (board[i][j] == 'O'){
                    board[i][j] = 'X';
                } else if (board[i][j] == 'B'){
                    board[i][j] = 'O';
                }

            }
        }
    }

    private void dfs(char[][] board, int row, int col){
        board[row][col] = 'B';
        if (row - 1 > 0 && board[row - 1][col] == 'O'){ // using row > 0 might cause TLE
            dfs(board, row - 1, col);
        }
        if (row + 1 < board.length - 1 && board[row + 1][col] == 'O'){
            dfs(board, row + 1, col);
        }
        if (col - 1 > 0 && board[row][col - 1] == 'O'){
            dfs(board, row, col - 1);
        }
        if (col + 1 < board[0].length - 1 && board[row][col + 1] == 'O'){
            dfs(board, row, col + 1);
        }
    }

//    int[][] directions = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
//
//    private void dfs(char[][] board, int row, int col) {
//        board[row][col] = 'B';
//        for (int[] direction : directions) {
//            if (row + direction[0] > 0 && row + direction[0] < board.length - 1
//                    && col + direction[1] > 0 && col + direction[1] < board[0].length - 1
//                    && board[row + direction[0]][col + direction[1]] == 'O') {
//                dfs(board, row + direction[0], col + direction[1]);
//            }
//        }
//    }
}
