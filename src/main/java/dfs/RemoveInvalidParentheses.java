package dfs;

import java.util.*;

//ref: https://discuss.leetcode.com/topic/34875/easy-short-concise-and-fast-java-dfs-3-ms-solution/2
public class RemoveInvalidParentheses {
    public List<String> removeInvalidParentheses(String s) {
        List<String> res = new ArrayList<>();
        dfs(res, new StringBuilder(s), 0, 0, new char[]{'(', ')'});
        return res;
    }

    private void dfs(List<String> res, StringBuilder sb, int last_i, int last_j, char[] parens) {
        int stack = 0;

        for (int i = last_i; i < sb.length(); i++) {
            if (sb.charAt(i) == parens[0]) {
                stack++;
            }
            if (sb.charAt(i) == parens[1]) {
                stack--;
            }
            if (stack < 0) {
                for (int j = last_j; j <= i; j++){
                    if (j != last_j && sb.charAt(j) == sb.charAt(j - 1)){
                        continue;
                    }
                    if (sb.charAt(j) == parens[1]){
                        sb.deleteCharAt(j);
                        dfs(res, sb, i, j, parens); //hidden i++ and j++ because of the deletion!
                        sb.insert(j, parens[1]);
                    }
                }
                return;
            }
        }

        sb.reverse();
        if (parens[0] == ')') {
            res.add(sb.toString());
        } else {
            dfs(res, sb, 0, 0, new char[]{')', '('});
        }
        sb.reverse();
    }

}


