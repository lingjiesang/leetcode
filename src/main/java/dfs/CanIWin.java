package dfs;

import java.util.*;

//ref: https://discuss.leetcode.com/topic/68896/java-solution-using-hashmap-with-detailed-explanation/2

//ref's idea: using integer to represent "state" of "unselected numbers"

public class CanIWin {
    Map<Integer, Boolean> map;
    public boolean canIWin(int maxChoosableInteger, int desiredTotal) {
        map = new HashMap<>();

        int state = 0;
        int mask = 1;
        int sum = 0;
        for (int i = 1; i <= maxChoosableInteger; i++){
            sum += i;
            state |= mask;
            mask <<= 1;
        }
        if (sum < desiredTotal){
            return false;
        }
        return canWinCached(maxChoosableInteger, state, desiredTotal);
    }

    private boolean canWinCached(int maxChoosableInteger, int state, int desiredTotal){


        if (map.containsKey(state)){
            return map.get(state);
        }

        int mask = 1;
        for (int i = 1; i <= maxChoosableInteger; i++){
            if ((state & mask) != 0){
                if (desiredTotal - i <= 0 || !canWinCached(maxChoosableInteger, state - mask, desiredTotal - i)){
                    map.put(state, true);
                    return true;
                }
            }
            mask <<= 1;
        }
        map.put(state, false);
        return false;
    }
}
