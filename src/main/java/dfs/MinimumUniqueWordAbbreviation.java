package dfs;

import java.util.*;


public class MinimumUniqueWordAbbreviation {
    public String minAbbreviation(String target, String[] dictionary) {
        if (dictionary.length == 0){
            return Integer.toString(target.length());
        }
        Set<String> allPossibles = generateAbbreviations(target);
        Set<String> impossibles = generateConflicted(target, dictionary);

        for (String impossible : impossibles){
            remove(allPossibles, impossible);
        }
        int minLen = target.length() + 1;
        String result = null;


        List<String> allPossiblesList = new ArrayList<>();
        allPossiblesList.addAll(allPossibles);
        Collections.sort(allPossiblesList);
        //reason for adding this sort is because OJ would misjudge a case otherwise
        // see: https://leetcode.com/submissions/detail/77063997/
        for (String possible : allPossiblesList){
            if (possible.length() < minLen){
                result = possible;
                minLen = possible.length();
            }
        }
        return result;
    }

    private void remove(Set<String> allPossibles, String impossible){
        if (allPossibles.remove(getString(impossible.toCharArray()))){
            char[] chars = impossible.toCharArray();
            for (int i = 0; i < chars.length; i++){
                if (chars[i] != '1'){
                    char c = chars[i];
                    chars[i] = '1';
                    remove(allPossibles, new String(chars));
                    chars[i] = c;
                }
            }
        }
    }


    private Set<String> generateConflicted(String target, String[] dictionary){
        int n = target.length();
        Set<String> impossibles = new HashSet<>();
        for (String word : dictionary){
            if (word.length() != n){
                continue;
            }
            char[] abbr = new char[n];
            for (int i = 0; i < n; i++){
                if (target.charAt(i) != word.charAt(i)){
                    abbr[i] = '1';
                } else{
                    abbr[i] = target.charAt(i);
                }
            }
            impossibles.add(new String(abbr)); //key: keep impossible list as fully extended version
        }
        return impossibles;
    }

    private Set<String> generateAbbreviations(String word) {
        Set<String> result = new HashSet<>();
        dfs(word.toCharArray(), 0, result);
        return result;
    }

    private void dfs(char[] chars, int start, Set<String> result) {
        result.add(getString(chars));

        if (start == chars.length) {
            return;
        }
        for (int i = start; i < chars.length; i++) {
            if (chars[i] == '1') {
                continue;
            }
            char c = chars[i];
            chars[i] = '1';
            dfs(chars, i + 1, result);
            chars[i] = c;
        }
    }

    private String getString(char[] chars) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chars.length; ) {
            if (chars[i] == '1') {
                int ed = i;
                while (ed < chars.length && chars[ed] == '1') {
                    ed++;
                }
                sb.append(ed - i);
                i = ed;
            } else {
                sb.append(chars[i]);
                i++;
            }
        }
        return sb.toString();
    }
}
