package dfs;

import java.util.*;

public class FlipGame {
    /**
     * 293. I
     */
    public List<String> generatePossibleNextMoves(String s) {
        StringBuilder sb = new StringBuilder(s);
        List<String> result = new ArrayList<>();
        for (int i = 1; i < s.length(); i++) {
            if (sb.charAt(i) == '+' && sb.charAt(i - 1) == '+') {
                flip(sb, i, i - 1);
                result.add(sb.toString());
                flip(sb, i, i - 1);
            }
        }
        return result;
    }

    private void flip(StringBuilder sb, int i, int j) {
        if (sb.charAt(i) == '+') {
            sb.setCharAt(i, '-');
            sb.setCharAt(j, '-');
        } else {
            sb.setCharAt(i, '+');
            sb.setCharAt(j, '+');
        }
    }

    /**
     * 294.II
     * method1: dfs with memorization
     */
    public boolean canWin(String s) {
        Map<String, Boolean> map = new HashMap<>();
        return canWinCached(s, map);
    }

    private boolean canWinCached(String s, Map<String, Boolean> map){
        if (map.containsKey(s)){
            return map.get(s);
        }
        for (int i = 1; i < s.length(); i++){
            if (s.charAt(i - 1) == '+' && s.charAt(i) == '+'){
                if (!canWinCached(s.substring(0, i - 1) + "--" + s.substring(i + 1), map)){
                    map.put(s, true);
                    return true;
                }
            }
        }
        map.put(s, false);
        return false;
    }

    /**
     * method2: game theory
     *
     * https://discuss.leetcode.com/topic/27282/theory-matters-from-backtracking-128ms-to-dp-0ms/27
     *
     * see the first post and also one of the replier from op
     * theory1:   if x_0, ... x_k represents k possible board arrangement after the first move, then
     * g(x) = FirstMissingNumber(g(x_0), g(x_1), g(x_2), ... , g(x_k)).
     *
     * @jianchao, In my understanding, the S-G function basically says: If ANY subsequent state of state x is a losing
     * position (g == 0), then x itself must be a winning position (g != 0); if ALL subsequent states are winning
     * positions (g != 0), then x must be a losing position (g == 0). The above 2 situations can be unified by a
     * FirstMissingNumber operation. For any normal impartial game, simply find out all states x_0, x_1, ... x_k
     * reachable from a state x, then use Concept 3 and Theorem 2 to calculate S-G function.
     *
     *
     * theory 2: suppose (s_1, s_2, ..., s_n) are n group of pluses with length more than 2, then
     * g(x) = g(s_1) ^ g(s_2) ... ^ g(s_n)
     *
     * http://web.mit.edu/sp.268/www/nim.pdf
     * (more about this course: http://web.mit.edu/sp.268/www/index.html
     *
     */
}
