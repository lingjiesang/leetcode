package dfs;

import java.util.*;

public class RestoreIPAddresses {
    private static int SEGMENTS = 4;
    private static int MAX = 255;

    public List<String> restoreIpAddresses(String s) {
        List<String> result = new ArrayList<>();
        char[] restored = new char[s.length() + 4];
        dfs(restored, 0, s.toCharArray(), 0, result);
        return result;
    }

    private void dfs(char[] ip, int segNum, char[] raw, int len, List<String> result) {
        if (segNum == SEGMENTS) {
            if (len + segNum == ip.length) {
                result.add(new String(ip, 0, ip.length - 1));
            }
            return;
        }

        int num = 0;
        for (int i = len; i < len + 3 && i < raw.length; i++) {
            if (raw[len] == '0' && i != len) { // num start with 0 can only have one digit
                break;
            }

            ip[i + segNum] = raw[i];
            num = num * 10 + raw[i] - '0';
            if (num > MAX) {
                return;
            }
            ip[i + segNum + 1] = '.';
            dfs(ip, segNum + 1, raw, i + 1, result);
        }
    }
}
