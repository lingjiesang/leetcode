package dfs;

import binaryTree.TreeNode;

import java.util.*;

public class HouseRobberIII {
    public int rob(TreeNode root) {
        int[] res = _rob(root);
        return Math.max(res[0], res[1]);
    }

    /**
     *     result[0]: rob root, maximal gain
     *     result[1]: not rob root, maximal gain
     */
    private int[] _rob(TreeNode root){
        if (root == null){
            return new int[]{0, 0};
        }

        int[] left = _rob(root.left);
        int[] right = _rob(root.right);

        int robRoot = root.val + Math.max(left[1], 0) + Math.max(right[1], 0);

        int notRobRoot = 0;
        notRobRoot += Math.max(Math.max(left[0], left[1]), 0);
        notRobRoot += Math.max(Math.max(right[0], right[1]), 0);
        return new int[]{robRoot, notRobRoot};
    }
}
