package dfs;

import java.util.*;

public class Combinations {
    public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        dfs(n, k, new ArrayList<>(), result);
        return result;
    }

    private void dfs(int n, int k, List<Integer> oneSoln, List<List<Integer>> result){
        if (oneSoln.size() == k){
            result.add(new ArrayList<>(oneSoln));
            return;
        }

        int start = oneSoln.isEmpty() ? 1 : oneSoln.get(oneSoln.size() - 1) + 1;
        for (int i = start; i <= n - (k - oneSoln.size()) + 1; i++){ // optimize: i <= n - (k - oneSoln.size()) + 1 (if use i <= n: much slower!)
            oneSoln.add(i);
            dfs(n, k, oneSoln, result);
            oneSoln.remove(oneSoln.size() - 1);
        }
    }
}
