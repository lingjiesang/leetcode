package dfs;

import java.util.*;
public class GenerateParentheses {
    public List<String> generateParenthesis(int n){
        List<String> res = new ArrayList<>();

        char[] generated = new char[n * 2];
        _generateParenthesis(generated, 0, n, n, res);
        return res;
    }

    private void _generateParenthesis(char[] generated, int len, int left, int right, List<String> res){
        if (left == 0 && right == 0){
            res.add(new String(generated));
            return;
        }

        if (left > 0){
            generated[len] = '(';
            _generateParenthesis(generated, len + 1, left - 1, right, res);
        }

        if (left < right){
            generated[len] = ')';
            _generateParenthesis(generated, len + 1, left, right - 1, res);
        }
    }
}
