package dfs;

import binaryTree.TreeNode;

import java.util.*;

public class BinaryTreePaths {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> result = new ArrayList<>();
        if (root == null){
            return result;
        }
        dfs(root, result, new StringBuilder());
        return result;
    }

    private void dfs(TreeNode root, List<String> result, StringBuilder path){
        int len = path.length();
        if (root.left == null && root.right == null){
            path.append(root.val);
            result.add(new String(path));
            path.setLength(len);
            return;
        }

        path.append(root.val).append("->");
        if (root.left != null){
            dfs(root.left, result, path);
        }
        if (root.right != null){
            dfs(root.right, result, path);
        }
        path.setLength(len);
    }
}
