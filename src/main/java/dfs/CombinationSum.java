package dfs;

import java.util.*;

public class CombinationSum {
    /**
     * 39. I
     */
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> results = new ArrayList<>();
        dfs(candidates, target, 0, new ArrayList<>(), results);
        return results;
    }

    private void dfs(int[] candidates, int target, int start, List<Integer> oneSoln, List<List<Integer>> results){
        if (target == 0){
            results.add(new ArrayList<>(oneSoln));
            return;
        }
        for (int i = start; i < candidates.length; i++){
            if (candidates[i] <= target){
                oneSoln.add(candidates[i]);
                dfs(candidates, target - candidates[i], i, oneSoln, results);
                oneSoln.remove(oneSoln.size() - 1);
            } else{
                break;
            }
        }
    }

    /**
     * 40. II
     */
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        Arrays.sort(candidates);
        List<List<Integer>> results = new ArrayList<>();
        dfs_2(candidates, target, 0, new ArrayList<>(), results);
        return results;
    }


    private void dfs_2(int[] candidates, int target, int start, List<Integer> oneSoln, List<List<Integer>> results){
        if (target == 0){
            results.add(new ArrayList<>(oneSoln));
            return;
        }
        for (int i = start; i < candidates.length; i++){
            if (i != start && candidates[i] == candidates[i - 1]){
                continue;
            }
            if (candidates[i] <= target){
                oneSoln.add(candidates[i]);
                dfs_2(candidates, target - candidates[i], i + 1, oneSoln, results);
                oneSoln.remove(oneSoln.size() - 1);
            } else{
                break;
            }
        }
    }

    /**
     * III
     */
    public List<List<Integer>> combinationSum3(int k, int n){
        List<List<Integer>> results = new ArrayList<>();
        dfs_3(k, n, 1, new ArrayList<>(), results);
        return results;
    }

    private void dfs_3(int len, int target, int start, List<Integer> oneSoln, List<List<Integer>> results){
        if (target == 0 && oneSoln.size() == len){
            results.add(new ArrayList<>(oneSoln));
            return;
        }
        for (int i = start; i <= 9; i++){
            if (i <= target && oneSoln.size() < len){
                oneSoln.add(i);
                dfs_3(len, target - i, i + 1, oneSoln, results);
                oneSoln.remove(oneSoln.size() - 1);
            } else{
                break;
            }
        }
    }
}
