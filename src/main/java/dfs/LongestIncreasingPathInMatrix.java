package dfs;


public class LongestIncreasingPathInMatrix {
    int[][] cached;
    int[][] directions = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
    public int longestIncreasingPath(int[][] matrix) {
        if (matrix.length == 0 || matrix[0].length == 0){
            return 0;
        }
        cached = new int[matrix.length][matrix[0].length];

        int longest = 0;
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                longest = Math.max(longest, longestPath(matrix, i, j));
            }
        }
        return longest;
    }

    private int longestPath(int[][] matrix, int i, int j){
        if (cached[i][j] != 0){
            return cached[i][j];
        }

        int longest = 1;
        for (int[] direction : directions){
            int i_new = i + direction[0];
            int j_new = j + direction[1];
            if (i_new >= 0 && i_new < matrix.length && j_new >= 0 && j_new < matrix[0].length && matrix[i_new][j_new] > matrix[i][j]){
                longest = Math.max(longest, 1 + longestPath(matrix, i_new, j_new));
            }
        }
        cached[i][j] = longest; // important!!!
        return longest;
    }

}
