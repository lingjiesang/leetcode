package dfs;

import java.util.*;

public class WordSquares {
    class TrieNode{
        TrieNode[] subtree;
        String word;

        TrieNode(){
            subtree = new TrieNode[26];
        }
    }

    private void insertToTrie(String word){
        TrieNode node = root;
        for (char c : word.toCharArray()){
            if (node.subtree[c - 'a'] == null){
                node.subtree[c - 'a'] = new TrieNode();
            }
            node = node.subtree[c - 'a'];
        }
        node.word = word;
    }

    private List<String> matchPrefix(StringBuilder prefix){
        TrieNode node = root;
        List<String> result = new ArrayList<>();
        for (int i = 0; i < prefix.length(); i++){
            char c = prefix.charAt(i);
            if (node.subtree[c - 'a'] != null){
                node = node.subtree[c - 'a'];
            } else{
                return result; //empty
            }
        }

        matchPrefixHelper(node, result);
        return result;
    }

    private void matchPrefixHelper(TrieNode node, List<String> result){
        if (node.word != null){
            result.add(node.word);
            return;
        }
        for (TrieNode child : node.subtree){
            if (child != null){
                matchPrefixHelper(child, result);
            }
        }
    }

    TrieNode root;

    public List<List<String>> wordSquares(String[] words) {
        List<List<String>> result = new ArrayList<>();

        if (words.length == 0){
            return result;
        }

        int n = words[0].length();

        root = new TrieNode();

        for (String word : words){
            insertToTrie(word);
        }

        List<String> oneSoln = new ArrayList<>();
        for (int i = 0; i < words.length; i++){
            oneSoln.add(words[i]);
            dfs(oneSoln, result, n);
            oneSoln.remove(0);
        }
        return result;
    }


    private void dfs(List<String> oneSoln, List<List<String>> result, int n){
        int i = oneSoln.size();
        if (i == n){
            result.add(new ArrayList<>(oneSoln));
            return;
        }

        StringBuilder prefix = new StringBuilder();
        for (int j = 0; j < i; j++){
            prefix.append(oneSoln.get(j).charAt(i));
        }
        List<String> candidates = matchPrefix(prefix);
        for (String candidate : candidates){
            oneSoln.add(candidate);
            dfs(oneSoln, result, n);
            oneSoln.remove(i);
        }

    }
}
