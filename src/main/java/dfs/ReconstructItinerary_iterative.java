package dfs;

import java.util.*;

public class ReconstructItinerary_iterative {
    public List<String> findItinerary(String[][] tickets) {
        Map<String, PriorityQueue<String>> departToArrivals = new HashMap<>();
        constructMap(departToArrivals, tickets);

        String firstStop = "JFK";
        List<String> path = new LinkedList<>();

        Deque<String> stack = new ArrayDeque<>();
        stack.push(firstStop);

        while (!stack.isEmpty()){
            while (departToArrivals.get(stack.peek()) != null && !departToArrivals.get(stack.peek()).isEmpty()){
                stack.push(departToArrivals.get(stack.peek()).poll());
            }
            path.add(0, stack.pop());
        }

        return path;
    }

    private void constructMap(Map<String, PriorityQueue<String>> departToArrivals, String[][] tickets){
        for (String[] ticket : tickets){
            String depart = ticket[0];
            String arrival = ticket[1];
            departToArrivals.putIfAbsent(depart, new PriorityQueue<>()); // key
            departToArrivals.get(depart).add(arrival);
        }
    }
}
