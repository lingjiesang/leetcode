package dfs;

import java.util.*;

public class Subsets {
    /**
     * I
     */
    public List<List<Integer>> subsets(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }
        result.add(new ArrayList<>());
        for (int i = 0; i < nums.length; i++) {
            int size = result.size();
            for (int j = 0; j < size; j++) {
                ArrayList<Integer> list = new ArrayList<>(result.get(j));
                list.add(nums[i]);
                result.add(list);
            }
        }
        return result;

    }


    public List<List<Integer>> subsets_method2(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        dfs_subsets(nums, 0, new ArrayList<>(), result);
        return result;
    }

    public void dfs_subsets(int[] nums, int pos, List<Integer> list, List<List<Integer>> result) {
        result.add(new ArrayList<>(list));
        for (int i = pos; i < nums.length; i++) {
            list.add(nums[i]);
            dfs_subsets(nums, i + 1, list, result);
            list.remove(list.size() - 1);
        }
    }

    /**
     * II
     */
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }
        result.add(new ArrayList<>());
        for (int start = 0; start < nums.length; ) {
            int end = start;
            while (end + 1 < nums.length && nums[end + 1] == nums[start]) {
                end++;
            }
            List<Integer> append = new ArrayList<>();
            int size = result.size();
            for (int i = 1; i <= end - start + 1; i++) {
                append.add(nums[start]);
                for (int j = 0; j < size; j++) {
                    ArrayList<Integer> list = new ArrayList<>(result.get(j));
                    list.addAll(append);
                    result.add(list);
                }
            }
            start = end + 1;
        }
        return result;
    }


    public List<List<Integer>> subsetsWithDup_method2(int[] nums) {
        Arrays.sort(nums);
        List<List<Integer>> result = new ArrayList<>();
        dfs(nums, 0, new ArrayList<>(), result);
        return result;
    }

    public void dfs(int[] nums, int pos, List<Integer> list, List<List<Integer>> result) {
        result.add(new ArrayList<>(list));
        for (int i = pos; i < nums.length; i++) {
            if (i > pos && nums[i] == nums[i - 1]){
                continue;
            }
            list.add(nums[i]);
            dfs(nums, i + 1, list, result);
            list.remove(list.size() - 1);
        }
    }
}
