package dfs;

import java.util.*;

public class Permutations {
    /**
     * I
     */
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }
        dfs(nums, new ArrayList<>(), result);
        return result;
    }

    private void dfs(int[] nums, List<Integer> oneSoln, List<List<Integer>> result) {
        if (oneSoln.size() == nums.length) {
            result.add(new ArrayList<>(oneSoln));
            return;
        }

        for (int i = 0; i < nums.length; i++) {
            if (!oneSoln.contains(nums[i])) {
                oneSoln.add(nums[i]);
                dfs(nums, oneSoln, result);
                oneSoln.remove(oneSoln.size() - 1);
            }
        }
    }

    /**
     * II
     */
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0) {
            return result;
        }
        Arrays.sort(nums);
        boolean[] added = new boolean[nums.length];
        dfsUnique(nums, added, new ArrayList<>(), result);
        return result;
    }

    private void dfsUnique(int[] nums, boolean[] added, List<Integer> oneSoln, List<List<Integer>> result) {
        if (oneSoln.size() == nums.length) {
            result.add(new ArrayList<>(oneSoln));
            return;
        }

        int lastAdded = -1;
        for (int i = 0; i < nums.length; i++) {
            if (lastAdded != -1 && nums[i] == nums[lastAdded]){
                continue;
            }
            if (!added[i]) {
                added[i] = true;
                oneSoln.add(nums[i]);
                lastAdded = i;
                dfsUnique(nums, added, oneSoln, result);
                added[i] = false;
                oneSoln.remove(oneSoln.size() - 1);
            }
        }
    }


    /**
     * II (slightly slower)
     */
    public List<List<Integer>> permuteUnique_Method2(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        if (nums.length == 0){
            return result;
        }
        Arrays.sort(nums);
        List<Integer> list = new ArrayList<>();
        List<Integer> used = new ArrayList<>();
        dfsUnique_Method2(nums, list, result, used);
        return result;
    }
    public void dfsUnique_Method2(int[] nums, List<Integer> list, List<List<Integer>> result, List<Integer> used){
        if (list.size() == nums.length){
            result.add(new ArrayList<>(list));
            return;
        }
        for (int i = 0; i < nums.length; i++){
            if (i != 0 && nums[i] == nums[i - 1] && !used.contains(i - 1)) continue;
            if (used.contains(i)) continue;
            list.add(nums[i]);
            used.add(i);
            dfsUnique_Method2(nums, list, result, used);
            list.remove(list.size() - 1);
            used.remove(used.size() - 1);
        }
    }
}
