package dfs;

import java.util.*;

public class AdditiveNumber {
    public boolean isAdditiveNumber(String num) {
        if (num == null || num.length() == 0){
            return false;
        }

        long num1, num2;

        for (int i = 1; i < num.length(); i++){
            if (num.charAt(0) == '0' && i != 1){
                break;
            }
            num1 = Long.parseLong(num.substring(0, i));
            for (int j = i + 1; j < num.length(); j++){
                if (num.charAt(i) == '0' && j != i + 1){
                    break;
                }
                num2 = Long.parseLong(num.substring(i, j));
                if (dfs(num1, num2, j, num)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean dfs(long prevNum1, long prevNum2, int start, String num){
        //System.out.println(prevNum1 + " " + prevNum2 + " " + start);
        if (start >= num.length()){
            return true;
        }
        for (int i = start + 1; i <= num.length(); i++){
            if (num.charAt(start) == '0' && i != start + 1){
                break;
            }
            long curtNum = Long.parseLong(num.substring(start, i));
            if (curtNum == prevNum1 + prevNum2 && dfs(prevNum2, curtNum, i, num)){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args){
        String s = "112358";
        AdditiveNumber soln = new AdditiveNumber();
        System.out.println(soln.isAdditiveNumber(s));

    }

    /**
     * followup: use string addition to handle very large integer
     * https://discuss.leetcode.com/topic/29872/0ms-concise-c-solution-perfectly-handles-the-follow-up-and-leading-0s
     */
}
