package dfs;

import java.util.*;

public class WordSearch {
    public boolean exist(char[][] board, String word) {
        if (board.length == 0 || board[0].length == 0 || word.length() == 0){
            return false;
        }
        char[] chars = word.toCharArray();
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[0].length; j++){
                if (dfs(board, i, j, chars, 0)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean dfs(char[][] board, int i, int j, char[] word, int len){
        boolean result = false;
        if (board[i][j] == word[len]){
            if (len == word.length - 1){
                return true;
            }
            char c = word[len];
            board[i][j] = '#';
            if (i > 0) {
                result = dfs(board, i - 1, j, word, len + 1);
            }
            if (!result && i + 1 < board.length){
                result = dfs(board, i + 1, j, word, len + 1);
            }
            if (!result && j > 0){
                result = dfs(board, i, j - 1, word, len + 1);
            }
            if (!result && j + 1 < board[0].length){
                result = dfs(board, i, j + 1, word, len + 1);
            }
            board[i][j] = c;
        }
        return result;
    }
}
