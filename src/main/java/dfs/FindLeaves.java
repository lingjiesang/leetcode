package dfs;

import binaryTree.TreeNode;

import java.util.*;

public class FindLeaves {
    public List<List<Integer>> findLeaves(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        dfs(root, result);
        return result;
    }

    private int dfs(TreeNode root, List<List<Integer>> result) {
        if (root == null) {
            return -1;
        }

        int left = dfs(root.left, result);
        int right = dfs(root.right, result);
        int depth = Math.max(left, right) + 1;

        while (result.size() <= depth) {
            result.add(new ArrayList<>());
        }
        result.get(depth).add(root.val);

        return depth;
    }
}
