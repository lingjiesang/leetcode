package dfs;

import binaryTree.TreeNode;

import java.util.*;

public class SumRootToLeafNums {
    int sum;
    public int sumNumbers(TreeNode root) {
        sum = 0;
        if (root != null){
            dfs(root, 0);
        }
        return sum;
    }

    private void dfs(TreeNode root, int num){
        num = num * 10 + root.val;

        if (root.left == null && root.right == null){
            sum += num;
        } else {
            if (root.left != null) {
                dfs(root.left, num);
            }
            if (root.right != null){
                dfs(root.right, num);
            }
        }
    }

}
