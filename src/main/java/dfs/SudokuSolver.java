package dfs;

import java.util.*;
public class SudokuSolver {
    private int[][] possible;
    private int allPossible = 0x1ff;

    /**
     * 1. set:
     *      return true / false if set successfully / unsuccesfully
     *      if already set, return (to avoid loop forever!)
     *      propagate to all blocks affected by this block (unset them)
     *
     * 2. unset:
     *      if and only if after set, the block have only one possible value, propagate!
     *
     *
     * 3. propagate:
     *      return true / false if set successfully / unsuccesfully
     *
     *
     * queue for dfs
     * try blocks with minimal possibilities
     * queue order fixed after initiation, not change during dfs!
     *
     */

    public void solveSudoku(char[][] board){

        if (board.length == 0 || board[0].length != board.length){
            return;
        }

        initiatePossible(board.length);

        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[0].length; j++){
                if (board[i][j] != '.'){
                    int mask = 1 << (board[i][j] - '1');
                    if (!set(i, j, mask)){
                        return;
                    }
                }
            }
        }

        List<int[]> queue = new LinkedList<>();
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[0].length; j++){
                if ((possible[i][j] & possible[i][j] - 1) != 0){
                    queue.add(new int[]{i, j, possible[i][j]});
                }
            }
        }

        Collections.sort(queue, (a, b) -> (count(a[2]) - count(b[2])));

        if (!dfs(queue, 0)){
            return;
        }

        writeBoard(board);
    }

    private boolean dfs(List<int[]> queue, int pos){
        if (pos == queue.size()){
            return true;
        }

        int i = queue.get(pos)[0];
        int j = queue.get(pos)[1];

        int[][] possibleCopy = new int[possible.length][possible[0].length];
        for (int mask = 1; mask < allPossible; mask <<= 1){
            if ((possible[i][j] & mask) != 0){
                clone(possible, possibleCopy);
                if (set(i, j, mask) && dfs(queue, pos + 1)){
                    return true;
                }
                clone(possibleCopy, possible);
            }
        }
        return false;
    }

    private void clone(int[][] ori, int[][] dest){
        for (int i = 0; i < ori.length; i++){
            for (int j = 0; j < ori[0].length; j++){
                dest[i][j] = ori[i][j];
            }
        }
    }

    private void initiatePossible(int n){
        possible = new int[n][n];
        for (int i = 0; i < n; i++){
            Arrays.fill(possible[i], allPossible);
        }
    }

    private boolean set(int i, int j, int mask){
        if (possible[i][j] == mask) {
            return true;
        }
        if ((possible[i][j] & mask) == 0){
            //System.out.println("error" + i + " " + j + " " + possible[i][j] + " " + mask);
            return false;
        }
        possible[i][j] =  mask;
        return propagate(i, j, mask);
    }

    private boolean unset(int i, int j, int mask){
        mask =  ~mask & allPossible;
        if ((possible[i][j] & mask) == 0) {
            return false;
        }
        if ((possible[i][j] & (possible[i][j] - 1)) == 0){
            return true;
        } else{
            possible[i][j] &= mask;
            if ((possible[i][j] & (possible[i][j] - 1)) == 0){
                return propagate(i, j, possible[i][j]);
            }
        }
        return true;

    }

    private boolean propagate(int i, int j, int mask){
        //System.out.println(i + " " + j + " " + mask + " " + (~mask & allPossible));
        for (int ii = 0; ii < 9; ii++){
            if (ii != i && !unset(ii, j, mask)){
                return false;
            }
        }

        for (int jj = 0; jj < 9; jj++){
            if (jj != j && !unset(i, jj, mask)){
                return false;
            }
        }

        for (int ii = i / 3 * 3; ii < i / 3 * 3 + 3; ii++){
            for (int jj = j / 3 * 3; jj < j / 3 * 3 + 3; jj++){
                if ((ii != i || jj != j) && !unset(ii, jj, mask)){
                    return false;
                }
            }
        }
        return true;
    }

    private int count(int n){
        int c = 0;
        while (n != 0){
            c++;
            n = n & (n - 1);
        }
        return c;
    }

    private void writeBoard(char[][] board){
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[0].length; j++){
                if (board[i][j] == '.'){
                    board[i][j] = (char)('0' + value(possible[i][j]));
                }
            }
        }
    }

    private int value(int num){
        int v = 0;
        while (num != 0){
            v++;
            num >>= 1;
        }
        return v;
    }

    public static void main(String[] args){
        SudokuSolver solver = new SudokuSolver();
        String[] boardString = {"..9748...","7........",".2.1.9...","..7...24.",".64.1.59.",".98...3..","...8.3.2.","........6","...2759.."};
        char[][] board = new char[9][9];
        for (int i = 0; i < 9; i++){
            for (int j = 0; j < 9; j++){
                board[i][j] = boardString[i].charAt(j);
            }
        }
        solver.solveSudoku(board);
    }

}
