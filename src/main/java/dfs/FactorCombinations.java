package dfs;

import java.util.*;

public class FactorCombinations {
    public List<List<Integer>> getFactors(int n) {
        List<List<Integer>> result = new ArrayList<>();
        dfs(2, new ArrayList<>(), result, n);
        return result;
    }

    private void dfs(int lower, List<Integer> oneSoln, List<List<Integer>> result, int n){
        if (oneSoln.size() > 0 && n >= lower){
            oneSoln.add(n);
            result.add(new ArrayList<>(oneSoln));
            oneSoln.remove(oneSoln.size() - 1);
        }

        int upper = (int)Math.sqrt(n);
        for (int i = lower; i <= upper; i++){
            if (n % i == 0){
                oneSoln.add(i);
                dfs(i, oneSoln, result, n / i);
                oneSoln.remove(oneSoln.size() - 1);
            }
        }
    }
}
