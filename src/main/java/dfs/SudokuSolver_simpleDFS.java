package dfs;


public class SudokuSolver_simpleDFS {
    public void solveSudoku(char[][] board) {
        if (board.length == 0 || board[0].length == 0){
            return;
        }
        dfs(board, 0);
    }

    private boolean dfs(char[][] board, int pos){
        if (pos == board.length * board[0].length){
            return true;
        }
        int i = pos / 9;
        int j = pos % 9;

        if (board[i][j] != '.'){
            return dfs(board, pos + 1);
        }

        for (char c = '1'; c <= '9'; c++){
            board[i][j] = c;
            if (isValid(board, i, j, c) && dfs(board, pos + 1) ){
                return true;
            }
        }
        board[i][j] = '.';
        return false;
    }

    private boolean isValid(char[][] board, int i, int j, char c){
        for (int ii = 0; ii < board.length; ii++){
            if (ii != i && board[ii][j] == c){
                return false;
            }
        }

        for (int jj = 0; jj < board[0].length; jj++){
            if (jj != j && board[i][jj] == c){
                return false;
            }
        }

        for (int ii = i / 3 * 3; ii < i / 3 * 3 + 3; ii++){
            for (int jj = j / 3 * 3; jj < j / 3 * 3 + 3; jj++){
                if ((ii != i || jj != j) && board[ii][jj] == c){
                    return false;
                }
            }
        }
        return true;
    }
}