package dfs;

import binaryTree.TreeNode;

import java.util.*;

public class PathSumII {
    public List<List<Integer>> pathSum(TreeNode root, int sum) {
        List<List<Integer>> res = new ArrayList<>();
        if (root != null){
            dfs(root, sum, 0, new ArrayList<Integer>(), res);
        }
        return res;
    }

    private void dfs(TreeNode root, int target, int partialSum, List<Integer> path, List<List<Integer>> res){
        path.add(root.val);

        partialSum += root.val;
        if (root.left == null && root.right == null ){
            if (partialSum == target) {
                res.add(new ArrayList<>(path));
            }
        } else {
            if (root.left != null) {
                dfs(root.left, target, partialSum, path, res);
            }
            if (root.right != null) {
                dfs(root.right, target, partialSum, path, res);
            }
        }
        path.remove(path.size() - 1);
    }
}
