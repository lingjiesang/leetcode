package dfs;

import java.util.*;

public class ExpressionAddOperator {
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<>();
        dfs(num, 0, target, "", 0, 0, result);
        return result;
    }

    public void dfs(String s, int pos, int target, String exp, long eval, long prev, List<String> result){
        if (pos == s.length()){
            if (eval == target){
                result.add(exp);
            }
            return;
        }

        for (int i = pos; i < s.length(); i++){
            if (s.charAt(pos) == '0' && i != pos){
                break;
            }
            long number = Long.parseLong(s.substring(pos, i + 1));
            if (pos == 0){
                dfs(s, i + 1, target, s.substring(pos, i + 1), number, number, result);
            } else {
                dfs(s, i + 1, target, exp + '+' + s.substring(pos, i + 1), eval + number, number, result);
                dfs(s, i + 1, target, exp + '-' + s.substring(pos, i + 1), eval - number, -number, result);
                dfs(s, i + 1, target, exp + '*' + s.substring(pos, i + 1), eval - prev + prev * number, prev * number, result);
            }
        }
    }
}
