package dfs;

public class AndroidUnlockPattern {
    private static int[][] blockedBy;

    static {
        blockedBy = new int[10][10];
        // rows:
        for (int i = 1; i <= 7; i += 3) {
            blockedBy[i][i + 2] = blockedBy[i + 2][i] = i + 1;
        }
        //cols:
        for (int j = 1; j <= 3; j++) {
            blockedBy[j][j + 6] = blockedBy[j + 6][j] = j + 3;
        }
        blockedBy[1][9] = blockedBy[9][1] = 5;
        blockedBy[3][7] = blockedBy[7][3] = 5;
    }

    public int numberOfPatterns(int m, int n) {
        int totalNum = 0;
        int[] count = new int[1];
        dfs(new boolean[10], 0, 1, m, n, count);
        dfs(new boolean[10], 0, 2, m, n, count);
        totalNum += count[0] * 4;
        count[0] = 0;
        dfs(new boolean[10], 0, 5, m, n, count);
        totalNum += count[0];
        return totalNum;
    }

    private void dfs(boolean[] visited, int prevVisitedNum, int curt, int m, int n, int[] count) {
        visited[curt] = true;
        int visitedNum = prevVisitedNum + 1;
        if (visitedNum >= m && visitedNum <= n) {
            count[0]++;
        }
        if (visitedNum < n) {
            for (int i = 1; i <= 9; i++) {
                if (!visited[i] && (blockedBy[curt][i] == 0 || visited[blockedBy[curt][i]])) {
                    dfs(visited, visitedNum, i, m, n, count);
                }
            }
        }
        visited[curt] = false;
    }
}
