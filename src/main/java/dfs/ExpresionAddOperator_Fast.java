package dfs;

import java.util.*;

public class ExpresionAddOperator_Fast {
    public List<String> addOperators(String num, int target) {
        List<String> result = new ArrayList<>();
        if (num.length() == 0) {
            return result;
        }
        char[] digits = num.toCharArray();
        char[] path = new char[digits.length * 2 - 1];
        dfs(digits, 0, path, 0, 0, 0, target, result);

        return result;
    }

    private void dfs(char[] digits, int pos, char[] path, int len, long eval, long prev, int target, List<String> result){
        if (pos == digits.length){
            if (eval + prev == target){
                result.add(new String(path, 0, len));
            }
            return;
        }
        long curt = 0;
        int prev_len = len;
        if (pos != 0){ // leave a space for the symbol
            len++;
        }
        for (int i = pos; i < digits.length; i++){
            if (digits[pos] == '0' && i != pos){
                break;
            }
            path[len++] = digits[i];
            curt = curt * 10 + digits[i] - '0';
            if (pos == 0){
                dfs(digits, i + 1, path, len, eval, curt, target, result);
            } else{
                path[prev_len] = '+';
                dfs(digits, i + 1, path, len, eval + prev, curt, target, result);
                path[prev_len] = '-';
                dfs(digits, i + 1, path, len, eval + prev, -curt, target, result);
                path[prev_len] = '*';
                dfs(digits, i + 1, path, len, eval, prev * curt, target, result);
            }
        }
    }


    /**
     * slightly slower by easier to write
     */
    public List<String> addOperators_Fast2(String num, int target) {
        List<String> result = new ArrayList<>();
        if (num.length() == 0){
            return result;
        }
        char[] expn = new char[num.length() * 2 - 1];
        dfs(expn, 0, 0, 0, '+', num, 0, target, result);
        return result;
    }
    private void dfs(char[] expn, int expnLen, long eval, long prev, char op, String num, int start, int target, List<String> result){
        if (start != 0){
            expn[expnLen++] = op;
        }
        long curt = 0;
        for (int i = start; i < num.length(); i++){
            if (num.charAt(start) == '0' && i != start){
                break;
            }
            curt = curt * 10 + num.charAt(i) - '0';
            expn[expnLen++] = num.charAt(i);
            long curtEval = eval + curt;
            long curtPrev = curt;
            if (op == '-'){
                curtEval = eval - curt;
                curtPrev = -curt;
            } else if (op == '*'){
                curtEval = eval - prev + prev * curt;
                curtPrev = prev * curt;
            }
            if (i + 1 == num.length()){
                if (curtEval == target){
                    result.add(new String(expn).substring(0, expnLen));
                }
                return;
            } else{
                dfs(expn, expnLen, curtEval, curtPrev, '+', num, i + 1, target, result);
                dfs(expn, expnLen, curtEval, curtPrev, '-', num, i + 1, target, result);
                dfs(expn, expnLen, curtEval, curtPrev, '*', num, i + 1, target, result);
            }
        }
    }
}
