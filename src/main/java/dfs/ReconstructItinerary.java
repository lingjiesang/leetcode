package dfs;

import java.util.*;

/**
 * ref: https://discuss.leetcode.com/topic/36370/short-ruby-python-java-c/14
 *
 *
 * https://discuss.leetcode.com/topic/36385/share-solution-java-greedy-stack-15ms-with-explanation
 *
 *  Hierholzer's algorithm to find a Eulerian path
 */
public class ReconstructItinerary {

    public List<String> findItinerary(String[][] tickets) {
        Map<String, PriorityQueue<String>> departToArrivals = new HashMap<>();
        constructMap(departToArrivals, tickets);

        String firstStop = "JFK";
        List<String> path = new LinkedList<>();
        dfs(firstStop, path, departToArrivals);
        return path;
    }

    private void dfs(String depart, List<String> path, Map<String, PriorityQueue<String>> departToArrivals){
        PriorityQueue<String> arrivals = departToArrivals.get(depart);
        while (arrivals != null && !arrivals.isEmpty()){
            dfs(arrivals.poll(), path, departToArrivals);
        }
        path.add(0, depart);
    }


    private void constructMap(Map<String, PriorityQueue<String>> departToArrivals, String[][] tickets){
        for (String[] ticket : tickets){
            String depart = ticket[0];
            String arrival = ticket[1];
            departToArrivals.putIfAbsent(depart, new PriorityQueue<>()); // key
            departToArrivals.get(depart).add(arrival);
        }
    }
}