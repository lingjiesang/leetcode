package dfs;

import java.util.*;

public class GeneralizedAbbreviation {
    public List<String> generateAbbreviations(String word) {
        List<String> result = new ArrayList<>();
        dfs(word.toCharArray(), 0, result);
        return result;
    }

    private void dfs(char[] chars, int start, List<String> result) {
        result.add(getString(chars));

        if (start == chars.length) {
            return;
        }
        for (int i = start; i < chars.length; i++) {
            if (chars[i] == '1') {
                continue;
            }
            char c = chars[i];
            chars[i] = '1';
            dfs(chars, i + 1, result);
            chars[i] = c;
        }
    }

    private String getString(char[] chars) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < chars.length; ) {
            if (chars[i] == '1') {
                int ed = i;
                while (ed < chars.length && chars[ed] == '1') {
                    ed++;
                }
                sb.append(ed - i);
                i = ed;
            } else {
                sb.append(chars[i]);
                i++;
            }
        }
        return sb.toString();
    }


    public List<String> generateAbbreviations_BitManipulation(String word) {
        List<String> result = new ArrayList<>();

        if (word.length() == 0){
            result.add("");
            return result;
        }

        int n = 1 << word.length();
        for (int i = 0; i < n; i++){
            char[] chars = word.toCharArray();
            for (int shift = 0; shift < word.length(); shift++){
                int mask = 1 << shift;
                if ((i & mask) > 0){
                    chars[shift] = '1';
                }
            }
            result.add(getString(chars));
        }
        return result;
    }
}
