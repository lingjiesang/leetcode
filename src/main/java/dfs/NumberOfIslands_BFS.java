package dfs;

import java.util.*;

public class NumberOfIslands_BFS {
    public int numIslands(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }

        int m = grid.length, n = grid[0].length;

        int count = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == '1') {
                    grid[i][j] = '0';
                    Deque<int[]> queue = new ArrayDeque<>();
                    queue.offer(new int[]{i, j});
                    count++;

                    while (!queue.isEmpty()){
                        int[] coord = queue.poll();
                        int row = coord[0];
                        int col = coord[1];
                        if (row > 0 && grid[row - 1][col] == '1'){
                            grid[row - 1][col] = '0';
                            queue.offer(new int[]{row - 1, col});
                        }
                        if (row + 1 < m && grid[row + 1][col] == '1'){
                            grid[row + 1][col] = '0';
                            queue.offer(new int[]{row + 1, col});
                        }
                        if (col > 0 && grid[row][col - 1] == '1'){
                            grid[row][col - 1] = '0';
                            queue.offer(new int[]{row, col - 1});
                        }
                        if (col + 1 < n && grid[row][col + 1] == '1'){
                            grid[row][col + 1] = '0';
                            queue.offer(new int[]{row, col + 1});
                        }
                    }
                }
            }
        }
        return count;
    }
}
