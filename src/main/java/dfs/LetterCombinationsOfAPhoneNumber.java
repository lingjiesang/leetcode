package dfs;

import java.util.*;

public class LetterCombinationsOfAPhoneNumber {
    public List<String> letterCombinations(String digits) {
        List<String> combinations = new ArrayList<>();
        dfs(digits, 0, new char[digits.length() + 1], 0, combinations);
        return combinations;
    }

    private void dfs(String digits, int start, char[] oneSoln, int oneSolnLen, List<String> combinations) {
        if (start == digits.length()) {
            if (oneSolnLen > 0){
                combinations.add(new String(oneSoln, 0, oneSolnLen));
            }
            return;
        }
        if (digits.charAt(start) <= '1') {
            dfs(digits, start + 1, oneSoln, oneSolnLen, combinations);
        } else {
            char[] letters = getLetters(digits.charAt(start));
            for (int j = 0; j < letters.length; j++) {
                oneSoln[oneSolnLen] = letters[j];
                dfs(digits, start + 1, oneSoln, oneSolnLen + 1, combinations);
            }
        }
    }

    private char[] getLetters(char n) {
        if (n == '9') {
            return new char[]{'w', 'x', 'y', 'z'};
        } else if (n == '8') {
            return new char[]{'t', 'u', 'v'};
        } else if (n == '7') {
            return new char[]{'p', 'q', 'r', 's'};
        } else {
            char[] result = new char[3];
            for (int i = 0; i < 3; i++) {
                result[i] = (char) ('a' + (n - '2') * 3 + i);
            }
            return result;
        }
    }

}
