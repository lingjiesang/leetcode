package dfs;

import java.util.*;

public class NumberOfIslands {
    public int numIslands(char[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }

        int m = grid.length, n = grid[0].length;
        int count = 0;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (grid[i][j] == '1'){
                    count++;
                    dfs(grid, i, j);
                }
            }
        }
        return count;
    }

    private void dfs(char[][] grid, int row, int col){
        grid[row][col] = '0';
        if (row > 0 && grid[row - 1][col] == '1'){
            dfs(grid, row - 1, col);
        }
        if (row + 1 < grid.length && grid[row + 1][col] == '1'){
            dfs(grid, row + 1, col);
        }
        if (col > 0 && grid[row][col - 1] == '1'){
            dfs(grid, row, col - 1);
        }
        if (col + 1 < grid[0].length && grid[row][col + 1] == '1'){
            dfs(grid, row, col + 1);
        }
    }
}
