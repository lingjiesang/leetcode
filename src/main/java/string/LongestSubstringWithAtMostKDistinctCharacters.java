package string;

public class LongestSubstringWithAtMostKDistinctCharacters {
    /**
     * 340 (13ms)
     */
    public int lengthOfLongestSubstringKDistinct(String s, int k) {
        if (k == 0) {
            return 0;
        }
        int[] distinctChars = new int[256];
        int totalDistinct = 0;

        int st = 0, ed = 0;
        int maxLen = 0;
        while (ed < s.length()) {
            if (distinctChars[s.charAt(ed)] != 0) {
                distinctChars[s.charAt(ed)]++;
            } else {
                if (totalDistinct == k) {
                    while (st < ed) {
                        if (--distinctChars[s.charAt(st++)] == 0) {
                            totalDistinct--;
                            break;
                        }
                    }
                }
                distinctChars[s.charAt(ed)]++;
                totalDistinct++;
            }
            ed++;
            maxLen = Math.max(maxLen, ed - st);
        }
        return maxLen;
    }

    /**
     * 159 (17ms)
     */
    public int lengthOfLongestSubstringTwoDistinct(String s) {
        char[] distinctChars = new char[2];
        int[] counts = new int[2];

        int st = 0, ed = 0;
        int maxLen = 0;

        while (ed < s.length()){
            if (s.charAt(ed) == distinctChars[0]){
                counts[0]++;
                ed++;
            } else if(s.charAt(ed) == distinctChars[1]){
                counts[1]++;
                ed++;
            } else if (counts[0] == 0){
                distinctChars[0] = s.charAt(ed);
                counts[0]++;
                ed++;
            } else if (counts[1] == 0){
                distinctChars[1] = s.charAt(ed);
                counts[1]++;
                ed++;
            } else{
                while (st < ed) {
                    if (s.charAt(st++) == distinctChars[0]) {
                        if (--counts[0] == 0){
                            break;
                        }
                    } else {
                        if (--counts[1] == 0){
                            break;
                        }
                    }
                }
            }
            maxLen = Math.max(maxLen, ed - st);
        }
        return maxLen;

    }
}
