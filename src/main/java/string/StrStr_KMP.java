package string;

public class StrStr_KMP {
    public int strStr(String haystack, String needle) {
        if (needle.length() == 0){
            return 0;
        }
        if (haystack.length() < needle.length()){
            return -1;
        }
        int[] prefixTable = getPrefixTable(needle);
        return match(haystack, needle, prefixTable);
    }

    private int[] getPrefixTable(String pattern){
        int[] prefixTable = new int[pattern.length()];
        int prefixEnd = 0, suffixEnd = 1;
        while (suffixEnd < pattern.length()){
            if (pattern.charAt(prefixEnd) == pattern.charAt(suffixEnd)){
                prefixTable[suffixEnd] = prefixEnd + 1;
                prefixEnd++;
                suffixEnd++;
            } else if (prefixEnd != 0){
                prefixEnd = prefixTable[prefixEnd - 1];
            } else{
                prefixTable[suffixEnd] = 0;
                suffixEnd++;
            }
        }
        return prefixTable;
    }

    private int match(String str, String pattern, int[] prefixTable){
        int sIndex = 0, pIndex = 0;
        while (sIndex < str.length()){
            if (str.charAt(sIndex) == pattern.charAt(pIndex)){
                sIndex++;
                pIndex++;
                if (pIndex == pattern.length()){
                    return sIndex - pIndex;
                }
            } else if (pIndex > 0){
                pIndex = prefixTable[pIndex - 1];
            } else{
                sIndex++;
            }
        }
        return -1;
    }
}
