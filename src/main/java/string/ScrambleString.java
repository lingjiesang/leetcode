package string;

public class ScrambleString {
    private int[] prime26 = {
            2,  3,  5,  7, 11,
            13, 17, 19, 23, 29,
            31, 37, 41, 43, 47,
            53, 59, 61, 67, 71,
            73, 79, 83, 89, 97,
            101
    };

    public boolean isScramble(String s1, String s2) {
        if (s1.length() != s2.length()){
            return false;
        }

        if (s1.length() == 0){
            return true;
        }


        return _isScramble(s1, 0, s1.length(), s2, 0, s2.length());
    }

    // return if s1.substring(start1, end1) and s2.substring(start2, end2) is "scramble"
    private boolean _isScramble(String s1, int start1, int end1, String s2, int start2, int end2){
        if (start1 + 1 == end1){
            return s1.charAt(start1) == s2.charAt(start2);
        }

        if (s1.equals(s2)){
            return true;
        }

        for (int len = 1; len < end1 - start1; len++){
            if (isSameCompose(s1, start1, start1 + len, s2, start2, start2 + len)
                    && isSameCompose(s1, start1 + len, end1, s2, start2 + len, end2)
                    &&   _isScramble(s1, start1, start1 + len, s2, start2, start2 + len)
                    &&   _isScramble(s1, start1 + len, end1, s2, start2 + len, end2)) {
                return true;
            }

            if (isSameCompose(s1, start1, start1 + len, s2, end2 - len, end2)
                    &&  isSameCompose(s1, start1 + len, end1, s2, start2, end2 - len)
                    &&    _isScramble(s1, start1, start1 + len, s2, end2 - len, end2)
                    &&    _isScramble(s1, start1 + len, end1, s2, start2, end2 - len)){
                return true;
            }
        }
        return false;
    }

    // return if s1.substring(start1, end1) and s2.substring(start2, end2) have the same composition
    private boolean isSameCompose(String s1, int start1, int end1, String s2, int start2, int end2){
        if (start1 == end1){
            return true;
        }
        long hash1 = 1;
        for (int i = start1; i < end1; i++){
            hash1 *= prime26[s1.charAt(i) - 'a'];
        }
        long hash2 = 1;
        for (int i = start2; i < end2; i++){
            hash2 *= prime26[s2.charAt(i) - 'a'];
        }
        return hash1 == hash2;
    }
}
