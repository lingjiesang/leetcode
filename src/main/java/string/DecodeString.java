package string;

import java.util.*;

public class DecodeString {
    public String decodeString(String s) {

        Deque<Integer> kStack = new ArrayDeque<>();
        Deque<StringBuilder> prefixStack = new ArrayDeque<>();

        StringBuilder result = new StringBuilder();

        int k = 0;
        StringBuilder prefix = new StringBuilder();
        for (char c : s.toCharArray()){
            if (c >= '0' && c <= '9'){
                k = k * 10 + c - '0';
            } else if (c == '['){
                kStack.push(k);
                prefixStack.push(prefix);
                k = 0;
                prefix = new StringBuilder();
            } else if (c == ']'){
                k = kStack.pop();
                for (int i = 0; i < k; i++){
                    prefixStack.peek().append(prefix);
                }
                k = 0;
                prefix = prefixStack.pop();
            } else{
                prefix.append(c);
            }
        }
        result.append(prefix.toString());
        return result.toString();
    }

    public static void main(String[] args){
        DecodeString soln = new DecodeString();
//        System.out.println(soln.decodeString("3[a]2[bc]"));
        System.out.println(soln.decodeString("3[a2[c]]"));

    }


    /**
     * trick : add "Default value" to stack
     */
    public String decodeString_useStackTrick(String s) {
        if (s == null || s.length() == 0){
            return s;
        }
        Deque<Integer> numStack = new ArrayDeque<>();
        Deque<StringBuilder> strStack = new ArrayDeque<>();
        //numStack.push(1);
        strStack.push(new StringBuilder());
        int num = 0;

        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if (c == '['){
                numStack.push(num);
                strStack.push(new StringBuilder());
                num = 0;
            } else if (c == ']'){
                String str = strStack.pop().toString();
                int count = numStack.pop();
                while (count-- > 0){
                    strStack.peek().append(str);
                }
            } else if (c >= '0' && c <= '9'){
                num = num * 10 + c - '0';
            } else{
                strStack.peek().append(c);
            }
        }
        return strStack.pop().toString();

    }
}
