package string;

import java.util.*;

/**

 calculate minReplace to satisfy 1

 if s.length() < 6
    insertion and minReplace is good enough

 if s.length() >= 6 && s.length <= 20:
    divide chars into groups (continuous same characters), replace minimal chars in each groups to make them satisfy 3
    group size |  e.g.   | after replace  | # of replace
        1         a         -> a           replace = 0
        2         aa        -> aa          replace = 0
        3         aaa       -> a9a         replace = 1
        4         aaaa      -> aa9a        replace = 1
        5         aaaaa     -> aa9aa       replace = 1
        6         aaaaaa    -> aa9aBa      replace = 2
        7         aaaaaaa   -> aa9aaBa     replace = 2
        8         aaaaaaaa  -> aa9aaBaa    replace = 2

     i.e. # of replace = group size / 3;

     also note if replace >= minReplace, 1 is automatically satisfied!

 if s.length() > 20:
   divide chars into groups (continuous same characters), sorted by group size,  select first n groups so that:
    total # of chars in (n - 1) groups < 20
    total # of chars in n groups >= 20

    delete all chars in non-selected groups

    delete chars in selected groups so that total # of chars in selected groups == 20:
        first make sure each group size is at most 20

        while (total # of chars in selected groups > 20)
            first  delete 1 chars from groups  size > 2 && size % 3 == 0
            second delete 2 chars from groups  size > 2 && size % 3 == 1
            third  delete 3 chars from groups  size > 2 && size % 3 == 2
            (note: careful do not delete more than needed!)

            corner case: if all groups have size <= 2, but still total # of chars in selected groups > 20
                         just delete extra chars and break

    calculate replace as in 2

    return delete + replace




 */
public class StrongPasswordChecker {
    public int strongPasswordChecker(String s) {
        int minReplace = getMinReplace(s);

        if (s.length() < 6){
            int insersion = 6 - s.length();
            return Math.max(insersion, minReplace);
        } else if (s.length() <= 20){
            List<Integer> groups = generateGroups(s);
            int replace = 0;
            for (int group : groups){
                replace += group / 3;
            }
            return Math.max(replace, minReplace);
        } else {

            List<Integer> groups = generateGroups(s);

            Collections.sort(groups);

            int charCount = 0;
            int n = 0;
            while (charCount < 20 && n < groups.size()) {
                charCount += groups.get(n);
                n++;
            }

            while (groups.size() > n) {
                groups.remove(groups.size() - 1);
            }

            charCount = 0;

            int badGroup = 0;
            for (int i = 0; i < groups.size(); i++) {
                if (groups.get(i) > 20) {
                    groups.set(i, 20);
                }
                charCount += groups.get(i);
                if (groups.get(i) > 2){
                    badGroup++;
                }
            }

            int deletion = s.length() - 20;

            int toDelete = charCount - 20;

            int remainder = 0;
            while (toDelete > 0 && badGroup > 0) {

                for (int i = 0; i < n; i++) {
                    if (groups.get(i) > 2 && groups.get(i) % 3 == remainder) {
                        int del = Math.min(toDelete, remainder + 1);
                        groups.set(i, groups.get(i) - del);
                        toDelete -= del;
                        if (groups.get(i) <= 2){
                            badGroup--;
                        }
                        if (toDelete == 0 || badGroup == 0) {
                            break;
                        }
                    }
                }
                remainder = (remainder + 1) % 3;
            }


            int replace = 0;
            for (int i = 0; i < groups.size(); i++) {
                replace += groups.get(i) / 3;
            }

            return deletion + Math.max(replace, minReplace);
        }
    }




    /**
     * generate sorted groups
     * groups: # of continuous characters in S
     */
    private List<Integer> generateGroups(String s){
        List<Integer> groups = new ArrayList<>();
        for (int i = 0 ; i < s.length();){
            int j = i;
            while (j < s.length() && s.charAt(j) == s.charAt(i)){
                j++;
            }
            groups.add(j - i);
            i = j;
        }
        return groups;
    }

    /**
     * return # of replacements needed to satisfy 2
     */
    private int getMinReplace(String s){
        boolean[] dls = new boolean[3];
        for(char c : s.toCharArray()){
            dls[getClass(c)] = true;
        }
        int replace = 0;
        for (int i = 0; i < dls.length; i++){
            if (!dls[i]) replace++;
        }
        return replace;
    }

    private int getClass(char c){
        if (c >= '0' && c <= '9'){
            return 0;
        } else if (c >= 'a' && c <= 'z'){
            return 1;
        } else{
            return 2;
        }
    }
}
