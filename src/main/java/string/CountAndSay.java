package string;

import java.util.*;

public class CountAndSay {
    /**
     * my method (5ms)
     */
    public String countAndSay(int n) {
        List<Integer> num = new ArrayList<>();
        num.add(1);

        for (int i = 2; i <= n; i++) {
            num = next(num);
        }

        StringBuilder sb = new StringBuilder();
        for (int i : num) {
            sb.append(i);
        }
        return sb.toString();
    }

    private List<Integer> next(List<Integer> num) {
        List<Integer> result = new ArrayList<>();
        int last = num.get(0);
        int count = 1;
        for (int i = 1; i < num.size(); i++) {
            if (num.get(i) == last) {
                count++;
            } else {
                result.add(count);
                result.add(last);
                last = num.get(i);
                count = 1;
            }
        }
        result.add(count);
        result.add(last);
        return result;
    }

    /**
     * similar idea, cleaner and faster (3ms)
     * ref: https://discuss.leetcode.com/topic/2309/show-an-answer-in-java
     */
    public String countAndSay_Cleaner(int n) {
        StringBuilder curr = new StringBuilder("1");
        StringBuilder prev;
        int count;
        char say;
        for (int i = 1; i < n; i++) {
            prev = curr;
            curr = new StringBuilder();
            count = 1;
            say = prev.charAt(0);

            for (int j = 1, len = prev.length(); j < len; j++) {
                if (prev.charAt(j) != say) {
                    curr.append(count).append(say);
                    count = 1;
                    say = prev.charAt(j);
                } else count++;
            }
            curr.append(count).append(say);
        }
        return curr.toString();

    }
}
