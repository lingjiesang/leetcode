package string;

import java.util.*;

public class RemoveKDigits {
    public String removeKdigits(String num, int k) {
        if (k >= num.length()){
            return "0";
        }
        StringBuilder sb = new StringBuilder(num);
        Deque<Integer> stack = new ArrayDeque<>();
        stack.push(0);
        int st = 1;

        while (k > 0){
            if (st >= sb.length()){
                sb.deleteCharAt(stack.pop());
                k--;
                st--;
            } else{
                if (stack.isEmpty() || sb.charAt(st) >= sb.charAt(stack.peek())){
                    stack.push(st);
                    st++;
                } else{
                    sb.deleteCharAt(stack.pop());
                    k--;
                    st--;
                }
            }
        }

        st = 0;
        while (st < sb.length() && sb.charAt(st) == '0'){
            st++;
        }
        return st == sb.length() ? "0" : sb.toString().substring(st);
    }
}
