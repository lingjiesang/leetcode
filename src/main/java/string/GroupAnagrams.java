package string;

import java.util.*;

public class GroupAnagrams {
    public List<List<String>> groupAnagrams(String[] strs) {
        Map<Long, List<String>> map = new HashMap<>();
        for (String str : strs){
            long hash = hash(str);
            if (!map.containsKey(hash)){
                map.put(hash, new ArrayList<>());
            }
            map.get(hash).add(str);
        }

        List<List<String>> result = new ArrayList<>();
        for (List<String> list : map.values()){
            result.add(list);
        }
        return result;
    }

    private int[] prime26 =
            { 2, 3,  5,  7, 11,
            13, 17, 19, 23, 29,
            31, 37, 41, 43, 47,
            53, 59, 61, 67, 71,
            73, 79, 83, 89, 97, 101};

    private long hash(String str){
        long result = 1;
        for (char c : str.toCharArray()){
            result *= prime26[c - 'a'];
        }
        return result;
    }
}
