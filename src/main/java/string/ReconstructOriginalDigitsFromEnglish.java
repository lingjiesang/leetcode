package string;

import java.util.*;

public class ReconstructOriginalDigitsFromEnglish {



    /**
     Simulate the thinking process of people. A little complicate
     (I just pick the handy data structure without too much optimization).

     data structures used:

     charInS : count # of occurrences of each character in the give string s.
     charUsedByDigit: for each character, list of the digits whose English word contains this character.
     unCountedWord: the English words (digits) not processed yet.
     count: count # of each digits in the output string
     Every time we pick a character which can uniquely predict count of a digit in output string; when we processed a
     digit, we delete its traces in charInS, charUsedByDigit, unCountedWord.

     I am just curious about a follow up: what if the given words (English words of digits in this case) do not allow
     learning of unique rules, then backtracking and heuristics is required.

     my post: https://discuss.leetcode.com/topic/63597/program-self-learn-the-rules
     */

    public String originalDigits(String s) {
        int[] charInS = new int[26];
        for (char c : s.toCharArray()){
            charInS[c - 'a']++;
        }


        List<List<Integer>> charUsedByDigit = new ArrayList<>();
        for (int i = 0; i < 26; i++){
            charUsedByDigit.add(new ArrayList<>());
        }

        Set<String> unCountedWord = new HashSet<>();
        List<String> words = Arrays.asList("zero", "one", "two", "three",
                "four", "five", "six", "seven","eight", "nine");
        unCountedWord.addAll(words);
        for (int i = 0; i < words.size(); i++){
            String word = words.get(i);
            for (int j = 0; j < word.length(); j++){
                char c = word.charAt(j);
                charUsedByDigit.get(c - 'a').add(i);
            }
        }

        int[] count = new int[10];

        while (!unCountedWord.isEmpty()){
            for (int i = 0; i < 26; i++){
                if (charUsedByDigit.get(i).size() == 1){
                    int digit = charUsedByDigit.get(i).get(0);
                    count[digit] = charInS[i] / getOccurence(words.get(digit), (char)(i + 'a'));
                    unCountedWord.remove(words.get(digit));
                    for (char c : words.get(digit).toCharArray()){
                        charInS[c - 'a'] -= count[digit];
                        charUsedByDigit.get(c - 'a').remove((Integer)digit);
                    }
                }
            }
        }

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < count.length; i++){
            while (count[i]-- > 0){
                sb.append((char)(i + '0'));
            }
        }
        return sb.toString();
    }

    private int getOccurence(String word, char c){
        int count = 0;
        for (char cc : word.toCharArray()){
            if (cc == c){
                count++;
            }
        }
        return count;
    }
}
