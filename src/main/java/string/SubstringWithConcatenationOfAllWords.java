package string;

import java.util.*;

public class SubstringWithConcatenationOfAllWords {
    public List<Integer> findSubstring(String s, String[] words) {
        List<Integer> result = new ArrayList<>();
        if (s.length() == 0 || words.length == 0){
            return result;
        }
        Map<String, Integer> map = generateMap(words);

        int frameSize = words[0].length();
        int totalLen = words.length * frameSize;


        for (int frame = 0; frame < frameSize; frame++){
            int left = frame;
            int right = frame;
            Map<String, Integer> count = new HashMap<>();

            while (right + frameSize <= s.length()){
                String segment = s.substring(right, right + frameSize);
                if (!map.containsKey(segment)){
                    left = right + frameSize;
                    right = right + frameSize;
                    count = new HashMap<>();
                } else{
                    count.put(segment, count.getOrDefault(segment, 0) + 1);
                    right += frameSize;
                    while (count.get(segment) > map.get(segment)){
                        String prevSegment= s.substring(left, left + frameSize);
                        count.put(prevSegment, count.get(prevSegment) - 1);
                        left += frameSize;
                    }
                    if (right - left == totalLen){
                        result.add(left);
                    }
                }
            }
        }
        return result;
    }

    private Map<String, Integer> generateMap(String[] words){
        Map<String,Integer> map = new HashMap<>();
        for (String word : words){
            map.put(word, map.getOrDefault(word, 0) + 1);
        }
        return map;
    }

}
