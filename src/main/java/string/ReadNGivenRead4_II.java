package string;

public class ReadNGivenRead4_II extends Reader4 {

    private char[] cached = new char[4];
    private int cachedEnd = 0;
    private int cachedStart = 0;

    public int read(char[] buf, int n) {
        int bufCount = 0;
        while (bufCount < n){
            if (cachedStart == cachedEnd){
                cachedStart = 0;
                cachedEnd = read4(cached);
                if (cachedEnd == 0){ // end of file
                    break;
                }
            }
            buf[bufCount++] = cached[cachedStart++];
        }
        return bufCount;
    }
}
