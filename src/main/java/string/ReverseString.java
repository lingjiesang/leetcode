package string;

public class ReverseString {
    /**
     * 344
     */
    public String reverseString(String s) {
        char[] sChar = s.toCharArray();
        int pos1 = 0, pos2 = s.length() - 1;

        while (pos1 < pos2){
            swap(sChar, pos1, pos2);
            pos1++;
            pos2--;
        }
        return new String(sChar);
    }
    private void swap(char[] chars, int i, int j){
        if (i != j){
            chars[i] ^= chars[j];
            chars[j] ^= chars[i];
            chars[i] ^= chars[j];
        }
    }


    /**
     * 345
     */
    public String reverseVowels(String s) {
        String vowels = "aeiouAEIOU";

        char[] chars = s.toCharArray();
        int pos1 = 0, pos2 = s.length() - 1;
        while (pos1 < pos2){
            while (pos1 < pos2 && vowels.indexOf(chars[pos1]) < 0){
                pos1++;
            }
            while (pos1 < pos2 && vowels.indexOf(chars[pos2]) < 0){
                pos2--;
            }
            if (pos1 < pos2){
                swap(chars, pos1, pos2);
                pos1++;
                pos2--;
            }
        }
        return new String(chars);

    }
}
