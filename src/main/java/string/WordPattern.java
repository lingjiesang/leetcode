package string;

import java.util.*;

public class WordPattern {
    public boolean wordPattern(String pattern, String str) {
        String[] words = str.split("\\s+");
        if (words.length != pattern.length()){
            return false;
        }
        String[] mapP = new String[256];
        Map<String, Character> mapS = new HashMap<>();
        for (int i = 0; i < pattern.length(); i++){
            if (mapP[pattern.charAt(i)] == null && !mapS.containsKey(words[i])){
                mapP[pattern.charAt(i)] = words[i];
                mapS.put(words[i], pattern.charAt(i));
            } else{
                if (!words[i].equals(mapP[pattern.charAt(i)]) || pattern.charAt(i) != mapS.getOrDefault(words[i], (char)0)){
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * online method: store in map is the position of last appearance of word[i] / pattern.charAt(i)
     *
     * ref:https://discuss.leetcode.com/topic/26339/8-lines-simple-java
     */
    public boolean wordPattern_method2(String pattern, String str) {
        String[] words = str.split(" ");
        if (words.length != pattern.length())
            return false;
        Map index = new HashMap();
        for (Integer i=0; i<words.length; ++i)
            if (index.put(pattern.charAt(i), i) != index.put(words[i], i))
                return false;
        return true;
    }
}
