package string;

public class ReadNGivenRead4 extends Reader4 {
    public int read(char[] buf, int n) {
        if (n <= 0){
            return 0;
        }

        char[] temp = new char[4];
        int count = 0; // number of chars actually read
        for (int i = 0; i < (n + 3) / 4; i++){
            int m = read4(temp);
            count = i * 4;
            while (count < i * 4 + m && count < n) {
                buf[count] = temp[count - i * 4];
                count++;
            }
            if (m < 4){
                break;
            }
        }
        return count;
    }


    public int read_method2(char[] buf, int n) {

        char[] temp = new char[4];
        int count = 0; // number of chars actually read
        while (count < n){
            int m = read4(temp);
            if (m == 0){
                break;
            }
            int i = 0;
            while (count < n && i < m){
                buf[count++] = temp[i++];
            }
        }
        return count;
    }
}
