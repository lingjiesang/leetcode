package string;

import java.util.*;

public class WordPatternII {
    public boolean wordPatternMatch(String pattern, String str) {
        String[] mapP = new String[256];
        Map<String, Character> mapS = new HashMap<>();
        return dfs(pattern, 0,  str, 0, mapP, mapS);
    }

    private boolean dfs(String pattern,  int startP, String str, int startS, String[] mapP, Map<String, Character> mapS) {
        if (startP == pattern.length() && startS == str.length()){
            return true;
        }
        if (startP == pattern.length() || startS == str.length()){
            return false;
        }

        String matchCharAtStartP = mapP[pattern.charAt(startP)];
        if (matchCharAtStartP != null){
            if (str.indexOf(matchCharAtStartP, startS) == startS){
                return dfs(pattern, startP + 1, str, startS + matchCharAtStartP.length(), mapP, mapS);
            } else{
                return false;
            }
        } else{
            // one key optimization is to decide minRemainder
            int minRemainder = 0; // minimal length of left for matching in the next round
            for (int i = startP + 1; i < pattern.length(); i++){
                String matchIthChar = mapP[pattern.charAt(i)];
                minRemainder += matchIthChar == null ? 1 : matchIthChar.length();
            }
            for (int i = startS; i < str.length() - minRemainder; i++){
                matchCharAtStartP = str.substring(startS, i + 1);
                if (mapS.containsKey(matchCharAtStartP)){ // cannot match both pattern "a" and pattern "b"
                    continue;
                }
                mapP[pattern.charAt(startP)] = matchCharAtStartP;
                mapS.put(matchCharAtStartP, pattern.charAt(startP));
                if (dfs(pattern, startP + 1, str, i + 1, mapP, mapS)){
                    return true;
                }
                mapP[pattern.charAt(startP)] = null;
                mapS.remove(matchCharAtStartP);
            }
        }
        return false;
    }
}
