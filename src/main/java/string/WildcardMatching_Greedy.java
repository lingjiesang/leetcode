package string;

public class WildcardMatching_Greedy {

    // my rewritting based on reference
    public boolean isMatch(String s, String p) {
        int sPointer = 0, pPointer = 0; // s.charAt(sPointer) and p.charAt(pPointer) is what to be matched next
        int starIdx = -1 ; // "last" star location in p
        int match = -1;    // p[0...starIdx] match s[0...match]

        while (sPointer < s.length() ){
            if (pPointer < p.length() && (s.charAt(sPointer) == p.charAt(pPointer) || p.charAt(pPointer) == '?')){
                sPointer++;
                pPointer++;
            } else if (pPointer < p.length() && p.charAt(pPointer) == '*'){
                starIdx = pPointer;
                match = sPointer - 1; // greedy: let star match nothing
                pPointer++;
            } else {
                if (starIdx == -1){
                    return false;
                } else {
                    match++; // increase star matching by one
                    sPointer = match + 1;
                    pPointer = starIdx + 1;
                }
            }
        }
        while (pPointer < p.length() && p.charAt(pPointer) == '*'){
            pPointer++;
        }

        return pPointer == p.length();
    }


    // reference: https://discuss.leetcode.com/topic/3040/linear-runtime-and-constant-space-solution
    // copied below
    boolean comparison(String str, String pattern) {
        int s = 0, p = 0, match = 0, starIdx = -1;
        while (s < str.length()){
            // advancing both pointers
            if (p < pattern.length()  && (pattern.charAt(p) == '?' || str.charAt(s) == pattern.charAt(p))){
                s++;
                p++;
            }
            // * found, only advancing pattern pointer
            else if (p < pattern.length() && pattern.charAt(p) == '*'){
                starIdx = p;
                match = s;
                p++;
            }
            // last pattern pointer was *, advancing string pointer
            else if (starIdx != -1){
                p = starIdx + 1;
                match++;
                s = match;
            }
            //current pattern pointer is not star, last patter pointer was not *
            //characters do not match
            else return false;
        }

        //check for remaining characters in pattern
        while (p < pattern.length() && pattern.charAt(p) == '*')
            p++;

        return p == pattern.length();
    }
}
