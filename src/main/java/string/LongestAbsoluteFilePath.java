package string;

import java.util.*;

public class LongestAbsoluteFilePath {
    public int lengthLongestPath(String input) {
        String[] dirs = input.split("\n");
        Deque<Integer> stack = new ArrayDeque<>();
        int maxLen = 0;
        for (String dir : dirs){
            int level = numOfSlashT(dir);
            while (stack.size() > level){
                stack.pop();
            }
            stack.push(dir.length() - level + (level == 0 ? 0 : stack.peek() + 1));
            if (dir.contains(".")) {
                maxLen = Math.max(maxLen, stack.peek());
            }
        }
        return maxLen;
    }

    // or use Array so don't need pop, see https://discuss.leetcode.com/topic/55247/9-lines-4ms-java-solution/2
    public int lengthLongestPath_ArrayForStack(String input) {
        String[] dirs = input.split("\n");
        int n = dirs.length;

        int[] pathLens = new int[n];
        int maxLen = 0;

        for (String dir : dirs){
            int level = numOfSlashT(dir);
            pathLens[level] = (level == 0 ? 0 : pathLens[level - 1] + 1) + (dir.length() - level);
            if (dir.indexOf(".") >= 0){
                maxLen = Math.max(maxLen, pathLens[level]);
            }
        }
        return maxLen;
    }

    private int numOfSlashT(String s){
        // or just return: s.lastIndexOf("\t")+1;
        int result = 0;
        for (int i = 0; i < s.length(); i ++){
            if (s.charAt(i) != '\t'){
                return result;
            }
            result++;
        }
        return result;
    }


}
