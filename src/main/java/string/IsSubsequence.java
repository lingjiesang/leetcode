package string;

public class IsSubsequence {
    public boolean isSubsequence(String s, String t){
        if (s.length() == 0){
            return true;
        }

        int lastFound = -1;
        for (int i = 0; i < t.length(); i++){
            if (t.charAt(i) == s.charAt(lastFound + 1)){
                if (++lastFound == s.length() - 1){
                    return true;
                }
            }
        }

        return false;
    }
}
