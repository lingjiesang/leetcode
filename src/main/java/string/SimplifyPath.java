package string;

import java.util.*;

public class SimplifyPath {
    public String simplifyPath(String path) {

        LinkedList<String> stack = new LinkedList<>();
        int head = 0, tail = 0;
        while (tail < path.length()){
            if (path.charAt(tail) != '/'){
                tail++;
            } else{
                String seg = path.substring(head, tail);
                addToStack(seg, stack);
                head = tail + 1;
                tail = head;
            }
        }
        if (head < path.length()){
            String seg = path.substring(head, tail);
            addToStack(seg, stack);
        }

        if (stack.isEmpty()){
            return "/";
        } else {
            StringBuilder sb = new StringBuilder();
            for (String s : stack){
                sb.append("/").append(s);
            }
            return sb.toString();
        }


    }

    private void addToStack(String seg, LinkedList<String> stack){
        if (seg.equals(".") || seg.equals("")){
            return;
        } else if (seg.equals("..")){
            if (!stack.isEmpty()){
                stack.removeLast();
            }
        } else {
            stack.addLast(seg);
        }
    }
}
