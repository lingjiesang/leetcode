package string;

import java.util.*;

public class TextJustification {
    public List<String> fullJustify(String[] words, int maxWidth) {
        List<String> result = new ArrayList<>();

        if (words == null || words.length == 0) {
            return result;
        }
        int st = 0;

        while (st < words.length) {
            int ed = st + 1; //ed: exclusive !!!
            int wordWidth = words[st].length();
            while (ed < words.length) {
                if (wordWidth + words[ed].length() + ed - st > maxWidth) {
                    break;
                }
                wordWidth += words[ed].length();
                ed++;
            }
            StringBuilder sb = new StringBuilder();
            if (ed == words.length || ed == st + 1) {
                for (int i = st; i < ed; i++) {
                    sb.append(words[i]);
                    if (sb.length() < maxWidth) {
                        sb.append(' ');
                    }
                }
                while (sb.length() < maxWidth) {
                    sb.append(' ');
                }
            } else {
                int space = (maxWidth - wordWidth) / (ed - 1 - st);
                int extra = (maxWidth - wordWidth) % (ed - 1 - st);
                for (int i = st; i < ed - 1; i++) {
                    sb.append(words[i]);
                    for (int j = 0; j < space; j++) {
                        sb.append(' ');
                    }
                    if (extra-- > 0) {
                        sb.append(' ');
                    }
                }
                sb.append(words[ed - 1]);
            }

            result.add(sb.toString());
            st = ed;
        }
        return result;
    }
}
