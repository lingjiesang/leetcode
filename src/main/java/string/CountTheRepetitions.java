package string;

import java.util.*;

// ref: https://discuss.leetcode.com/topic/71256/easy-understanding-java-solution-with-detailed-explanation-21ms/2

public class CountTheRepetitions {
    class ReturnValue{
        int count;
        String left;
        ReturnValue(int n, String s){
            count = n;
            left = s;
        }
    }

    public int getMaxRepetitions(String s1, int n1, String s2, int n2) {

        if (s2.length() == 0 || n2 == 0){
            return Integer.MAX_VALUE;
        }
        if (s1.length() == 0 || n1 == 0 || !hasAllChar(s1, s2)){
            return 0;
        }

        List<String> remainings = new ArrayList<>();
        List<Integer> contains = new ArrayList<>();

        remainings.add("");
        contains.add(0);

        int leading = 0;
        int cycleS1 = 0;
        int cycleS2 = 0;
        List<Integer> cumulative = new ArrayList<>();
        cumulative.add(0);

        String s = s1;
        while (true){
            ReturnValue returnValue = contains(s, s2);
            remainings.add(returnValue.left);
            contains.add(returnValue.count);
            s = returnValue.left + s1;

            int repeat = getRepeat(remainings);
            if (repeat != -1){
                leading = repeat;
                cycleS1 = remainings.size() - 1 - leading;
                for (int i = leading + 1; i < contains.size(); i++){
                    cycleS2 += contains.get(i);
                    cumulative.add(cycleS2);
                }
                break;
            }

        }

        int leadingContains = 0;
        for (int i = 0; i <= leading; i++){
            leadingContains += contains.get(i);
        }
        return (leadingContains + (n1 - leading) / cycleS1 * cycleS2 + cumulative.get((n1 - leading) % cycleS1)) / n2;
    }

    // judge if s1 has all char in s2
    private boolean hasAllChar(String s1, String s2){
        Set<Character> set = new HashSet<>();
        for (char c : s1.toCharArray()){
            set.add(c);
        }

        for (char c : s2.toCharArray()){
            if (!set.contains(c)){
                return false;
            }
        }
        return true;
    }

    private int getRepeat(List<String> remainings){
        String lastleft = remainings.get(remainings.size() - 1);
        for (int i = 0; i < remainings.size() - 1; i++){
            if (lastleft.equals(remainings.get(i))){
                return i;
            }
        }
        return -1;
    }


    // return # of s2 contained by s1 and "leftover" after last match
    private ReturnValue contains(String s1, String s2){
        int p1 = 0, p2 = 0;
        int count = 0;

        int leftOverStart = 0;

        while (p1 < s1.length()){
            if (s1.charAt(p1) == s2.charAt(p2)){
                p2++;
            }
            if (p2 == s2.length()){
                count++;
                p2 = 0;
                leftOverStart = p1 + 1;
            }
            p1++;
        }
        return new ReturnValue(count, s1.substring(leftOverStart));

    }
}
