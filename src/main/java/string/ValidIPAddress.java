package string;

public class ValidIPAddress {
    public String validIPAddress(String IP) {
        if (IP.indexOf('.') > 0 && isIPv4(IP)){
            return "IPv4";
        } else if (IP.indexOf(':') > 0 && isIPv6(IP)){
            return "IPv6";
        } else{
            return "Neither";
        }
    }

    private boolean isIPv4(String IP){
        String[] segments = IP.split("\\.", 4);
        if (segments.length != 4){
            return false;
        }
        for (int i = 0; i < 4; i++){
            String segment = segments[i];
            if (segment.length() == 0 || segment.length() > 3){
                return false;
            }
            if (segment.length() > 1 && segment.charAt(0) == '0'){
                return false;
            }
            int num = 0;
            for (int pos = 0; pos < segment.length() ; pos++){
                if (!isNum(segment.charAt(pos))){
                    return false;
                }
                num = num * 10 + segment.charAt(pos) - '0';
            }
            if (num > 255){
                return false;
            }
        }
        return true;

    }

    private boolean isIPv6(String IP){
        String[] segments = IP.split(":", 8);
        if (segments.length != 8){
            return false;
        }
        for (int i = 0; i < 8; i++){
            String segment = segments[i];
            if (segment.length() == 0 || segment.length() > 4){
                return false;
            }
            for (int pos = 0; pos < segment.length(); pos++){
                if (!isNum(segment.charAt(pos)) && !isValidChar(segment.charAt(pos))){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isNum(char c){
        return c >= '0' && c <= '9';
    }

    private boolean isValidChar(char c){
        return c >=  'a' && c <= 'f' || c >= 'A' && c <= 'F';
    }
}
