package string;

import java.util.*;

public class IsomorphicStrings {
    public boolean isIsomorphic(String s, String t) {
        if (s.length() != t.length()){
            return false;
        }
        char[] mapS = new char[256];
        char[] mapT = new char[256];
        for (int i = 0; i < s.length(); i++){
            if (mapS[s.charAt(i)] == 0 && mapT[t.charAt(i)] == 0){
                mapS[s.charAt(i)] = t.charAt(i);
                mapT[t.charAt(i)] = s.charAt(i);
            } else{
                if (mapS[s.charAt(i)] != t.charAt(i) || mapT[t.charAt(i)] != s.charAt(i)){
                    return false;
                }
            }
        }
        return true;
    }


    public boolean isIsomorphic_method2(String s, String t) {
        if (s.length() != t.length()){
            return false;
        }
        int[] map = new int[512];
        Arrays.fill(map, -1);
        for (int i = 0; i < s.length(); i++){
           if (map[s.charAt(i)] != map[t.charAt(i) + 256]){
               return false;
           }
            map[s.charAt(i)] = map[t.charAt(i) + 256] = i;
        }
        return true;
    }
}
