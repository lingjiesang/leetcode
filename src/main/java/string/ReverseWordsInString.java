package string;

/**
 * Clarification:
 1. What constitutes a word?
    A sequence of non-space characters constitutes a word.
 2. Could the input string contain leading or trailing spaces?
    Yes. However, your reversed string should not contain leading or trailing spaces.
 3. How about multiple spaces between two words?
    Reduce them to a single space in the reversed string.
 */
public class ReverseWordsInString {
    /**
     * 151. I
     */
    public String reverseWords(String s){
        String[] words = s.split("\\s+");

        StringBuilder sb = new StringBuilder();

        for (int i = words.length - 1; i >= 0; i--) {
            sb.append(words[i]).append(' ');
        }
        return sb.toString().trim();
    }

    /**
     * 151. I, inplace
     */
    public String reverseWords_inplace(String s){
        char[] words = s.trim().toCharArray();
        int len = 0;
        for (int i = 0; i < words.length; i++){
            if (i != 0 && words[i] == ' ' && words[i - 1] == ' '){
                continue;
            }
            words[len++] = words[i];
        }

        reverse(words, 0, len - 1);

        int start = 0;
        while (start < len){
            int end = start;
            while (end + 1 < len && words[end + 1] != ' '){
                end++;
            }
            reverse(words, start, end);
            start = end + 2;
        }

        return new String(words).substring(0, len);
    }


    private void reverse(char[] words, int start, int end){
        while (start < end){
            char temp = words[start];
            words[start] = words[end];
            words[end] = temp;
            start++;
            end--;
        }
    }

    /**
     * 186. II
     */
    public String reverseWords(char[] s){
        reverse(s, 0, s.length - 1);
        int start = 0;
        while (start < s.length){
            int end = start;
            while (end + 1 < s.length && s[end + 1] != ' '){
                end++;
            }
            reverse(s, start, end);
            start = end + 2;
        }

        return new String(s);
    }
}
