package string;

import java.util.*;

public class LongestRepeatingCharacterReplacement {
    /**
     * 14ms
     * ref: https://discuss.leetcode.com/topic/63416/sliding-window-similar-to-finding-longest-substring-with-k-distinct-characters/6
     */
    public int characterReplacement_Simple(String s, int k) {
        if (s == null || s.length() == 0) return 0;
        int start = 0;
        int end = 0;
        int[] counts = new int[26];
        int majorCount = 0;
        int result = 0;

        while (end < s.length()) {
            if (counts[s.charAt(end) - 'A']++ >= majorCount) {
                majorCount = counts[s.charAt(end) - 'A'];
            }
            while (end - start + 1 - majorCount > k) {
                if (counts[s.charAt(start++) - 'A']-- == majorCount) {
                   majorCount = getMajorCount(counts);
                }
            }
            result = Math.max(result, end - start + 1);
            end++;
        }
        return result;
    }

    private int getMajorCount(int[] counts){
        int majorCount = 0;
        for (int count : counts) {
            if (count > majorCount)
                majorCount = count;
        }
        return majorCount;
    }


    /**
     * using PriorityQueue to keep "major character" in the window (102ms)
     */
    class Elem{
        char c;
        int count;
        Elem(char c, int count){
            this.c = c;
            this.count = count;
        }
    }

    private void includeChar(char c, Elem[] map, PriorityQueue<Elem> pq){
        if (map[c - 'A'] != null){
            pq.remove(map[c - 'A']);
            map[c - 'A'].count++;
            pq.offer(map[c - 'A']);
        } else{
            map[c - 'A'] = new Elem(c, 1);
            pq.offer(map[c - 'A']);
        }
    }

    private void excludeChar(char c, Elem[] map, PriorityQueue<Elem> pq){
        pq.remove(map[c - 'A']);
        map[c - 'A'].count--;
        if (map[c - 'A'].count == 0){
            map[c - 'A'] = null;
        } else{
            pq.offer(map[c - 'A']);
        }
    }

    // don't need to change "major" char
    private int getChange(int start, int end, PriorityQueue<Elem> pq){
        return (end - start + 1) - pq.peek().count;
    }

    public int characterReplacement(String s, int k) {
        if (s.length() <= k){
            return s.length();
        }

        PriorityQueue<Elem> pq = new PriorityQueue<>((a, b) -> (b.count - a.count));
        Elem[] map = new Elem[26];
        for (int i = 0; i < k; i++){
            char c = s.charAt(i);
            includeChar(c, map, pq);
        }
        int result = k;

        int start = 0, end = k;
        while (end < s.length()){
            includeChar(s.charAt(end), map, pq);
            while (getChange(start, end, pq) > k){
                excludeChar(s.charAt(start++), map, pq);
            }
            result = Math.max(result, end - start + 1);
            end++;
        }
        return result;

    }
}
