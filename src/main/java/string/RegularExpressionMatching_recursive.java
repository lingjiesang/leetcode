package string;

public class RegularExpressionMatching_recursive {

    public boolean isMatch(String s, String p){
        if (s == null || p == null){
            return false;
        }
        return _isMatch(s, p, 0, 0);
    }

    private boolean _isMatch(String s, String p, int sPos, int pPos) {
        if (sPos == s.length()) {
            if (pPos == p.length()) {
                return true;
            }
            return pPos + 1 < p.length() && p.charAt(pPos + 1) == '*' && _isMatch(s, p, sPos, pPos + 2);
        }
        if (pPos == p.length()) {
            return false;
        }

        if (p.charAt(pPos) == '*'){
            return false;
        }

        /**
         * The key to write recursive to if has '*' advance pPos by 2!
         */
        if (pPos + 1 < p.length() && p.charAt(pPos + 1) == '*') {
            if (s.charAt(sPos) == p.charAt(pPos) || p.charAt(pPos) == '.') {
                return _isMatch(s, p, sPos, pPos + 2) || _isMatch(s, p, sPos + 1, pPos);
            } else {
                return _isMatch(s, p, sPos, pPos + 2);
            }
        } else {
            if (s.charAt(sPos) == p.charAt(pPos) || p.charAt(pPos) == '.') {
                return _isMatch(s, p, sPos + 1, pPos + 1);
            } else {
                return false;
            }
        }
    }

}
