package string;

import java.util.*;

public class RearrangeStringKDistanceApart {
    /**
     * we first count the occurrence of each character in the original string
     *
     * we add k different characters to the new string at a time
     * these characters are the ones with largest remaining count
     *
     * ref:
     * https://discuss.leetcode.com/topic/48091/c-unordered_map-priority_queue-solution-using-cache
     * https://discuss.leetcode.com/topic/48260/java-15ms-solution-with-two-auxiliary-array-o-n-time
     */
    public String rearrangeString(String str, int k) {
        if (k == 0){
            return str;
        }
        int len = str.length();

        int[] counts = new int[26];
        for (char c : str.toCharArray()){
            counts[c - 'a']++;
        }

        PriorityQueue<int[]> pq = new PriorityQueue(new Comparator<int[]>(){
            public int compare(int[] a, int[] b){
                if (b[1] != a[1]){
                    return b[1] - a[1];
                } else{
                    return a[0] - b[0]; // if two char have the same count, use alphabetic order
                }
            }
        });

        for (int i = 0; i < 26; i++){
            if (counts[i] > 0){
                pq.add(new int[]{i, counts[i]});
            }
        }

        StringBuilder result = new StringBuilder();
        while (len > 0){
            List<int[]> cache = new ArrayList<>();

            int binSize = Math.min(len, k);
            for (int i = 0; i < binSize; i++){
                if (pq.isEmpty()){
                    return "";
                }
                int[] charAndCount = pq.poll();
                result.append((char)(charAndCount[0] + 'a'));
                if (--charAndCount[1] > 0){
                    cache.add(charAndCount);
                }
                len--;
            }
            pq.addAll(cache); // this does not clean cache!
        }
        return result.toString();
    }
}
