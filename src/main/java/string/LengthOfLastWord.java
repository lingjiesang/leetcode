package string;

public class LengthOfLastWord {
    public int lengthOfLastWord(String s) {
        if (s == null) {
            return 0;
        }
        s = s.trim();
        return s.length() - 1 - s.lastIndexOf(" ");
    }
}
