package string;

import java.util.*;

public class GroupShiftedStrings {
    public List<List<String>> groupStrings(String[] strings) {
        List<List<String>> result = new ArrayList<>();
        Map<String, List<String>> map = new HashMap<>();
        for (String s : strings){
            String base = getBase(s);
            if (!map.containsKey(base)){
                map.put(base, new ArrayList<>());
            }
            map.get(base).add(s);
        }

        for (List<String> list : map.values()){
            result.add(list);
        }
        return result;
    }

    private String getBase(String s){
        if (s.length() == 0){
            return "";
        }
        char[] base = new char[s.length()];
        int offset = s.charAt(0) - 'a';
        base[0] = 'a';
        for (int i = 1; i < s.length(); i++){
            char c = (char)(s.charAt(i) - offset);
            if (c < 'a'){
                c += 26;
            }
            base[i] = c;
        }
        return new String(base);
    }
}
