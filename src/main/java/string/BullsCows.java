package string;

public class BullsCows {
    public String getHint(String secret, String guess) {
        int[] counts = new int[10];
        for (char c : secret.toCharArray()){
            counts[c - '0']++;
        }
        int bulls = 0;
        int cows = 0;

        for (int i = 0; i < guess.length(); i++){
            if (guess.charAt(i) == secret.charAt(i)){
                bulls++;
            }
            if (counts[guess.charAt(i) - '0']-- > 0){
                cows++;
            }
        }
        cows -= bulls;
        return bulls + "A" + cows + "B";
    }
}
