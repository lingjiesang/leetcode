package string;

import java.util.*;

public class Codec {
    // Encodes a list of strings to a single string.
    public String encode(List<String> strs) {
        StringBuilder sb = new StringBuilder();
        for (String str : strs) {
            sb.append(str.length()).append('/').append(str);
        }
        return new String(sb);
    }

    // Decodes a single string to a list of strings.
    public List<String> decode(String s) {
        int start = 0;
        List<String> result = new ArrayList<>();
        while (start < s.length()) {
            int end = s.indexOf('/', start);
            int len = Integer.valueOf(s.substring(start, end));
            result.add(s.substring(end + 1, end + 1 + len));
            start = end + 1 + len;
        }
        return result;
    }

    /**
     * method 2: use delimiter*
     * ref: https://discuss.leetcode.com/topic/24013/java-with-escaping/2
     */
    public class Codec_others {
        public String encode(List<String> strs) {
            StringBuffer out = new StringBuffer();
            for (String s : strs)
                out.append(s.replace("#", "##")).append(" # ");
            return out.toString();
        }

        public List<String> decode(String s) {
            List strs = new ArrayList();
            String[] array = s.split(" # ", -1);
            for (int i = 0; i < array.length - 1; ++i)
                strs.add(array[i].replace("##", "#"));
            return strs;
        }
    }
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.decode(codec.encode(strs));

