package string;

import java.util.*;

public class FirstUniqueCharInString {
    public int firstUniqChar(String s){
        int[] map = new int[256];
        Arrays.fill(map, s.length());
        for (int i = 0; i < s.length(); i++){
            if (map[s.charAt(i)] != -1){
                if (map[s.charAt(i)] < s.length()){
                    map[s.charAt(i)] = -1;
                } else{
                    map[s.charAt(i)] = i;
                }
            }
        }

        int pos = s.length();
        for (int i : map){
            if (i != -1){
                pos = Math.min(pos, i);
            }
        }
        return pos == s.length() ? -1 : pos;
    }
}
