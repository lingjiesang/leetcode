package string;

public class EditDistanceOne {
    public boolean isOneEditDistance(String s, String t) {
        if (s.length() < t.length()){
            return isOneEditDistance(t, s);
        }
        if (s.length() - t.length() > 1){
            return false;
        }
        if (s.length() == t.length()){
            return isReplaceOne(s.toCharArray(), t.toCharArray());
        } else{
            return isDelOne(s, t);
        }
    }

    private boolean isReplaceOne(char[] s, char[] t){
        boolean diffByOne = false;
        for (int i = 0; i < s.length; i++){
            if (s[i] != t[i]){
                if (diffByOne){
                    return false;
                } else{
                    diffByOne = true;
                }
            }
        }
        return diffByOne;
    }

    private boolean isDelOne(String s, String t){
        for (int i = 0; i < t.length(); i++){
            if (s.charAt(i) != t.charAt(i)){
                return s.substring(i + 1).equals(t.substring(i));
            }
        }
        return true;
    }
}
