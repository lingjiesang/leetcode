package string;

public class ZigZagConversion {
    public String convert(String s, int numRows) {
        if (numRows <= 1 || s == null || s.length() == 0){
            return s;
        }
        StringBuilder converted = new StringBuilder();

        int interval = (numRows - 1) * 2;
        for (int row = 0; row < numRows; row++){
            if (row == 0 || row == numRows - 1){
                for (int i = row; i < s.length(); i += interval){
                    converted.append(s.charAt(i));
                }
            } else{
                int halfInterval = interval - row * 2;
                for (int i = row; i < s.length(); ){
                    converted.append(s.charAt(i));
                    i += halfInterval; //this clause need to be before changing halfInterval!!!
                    halfInterval = interval - halfInterval;
                }
            }
        }
        return converted.toString();
    }

    public static void main(String[] args){
        ZigZagConversion soln = new ZigZagConversion();
        soln.convert("ABCD", 4);
    }
}
