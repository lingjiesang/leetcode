package string;

public class CompareVersionNumber {

    public int compareVersion(String version1, String version2) {
        int pos1 = 0, pos2 = 0;
        while (pos1 < version1.length() || pos2 < version2.length()){
            int num1 = 0, num2 = 0;
            while (pos1 < version1.length() && version1.charAt(pos1) != '.'){
                num1 = num1 * 10 + version1.charAt(pos1++) - '0';
            }
            while (pos2 < version2.length() && version2.charAt(pos2) != '.'){
                num2 = num2 * 10 + version2.charAt(pos2++) - '0';
            }
            if (num1 != num2) {
                return num1 < num2 ? -1 : 1;
            }
            while (pos1 < version1.length() && version1.charAt(pos1) == '.') {
                pos1++;
            }
            while (pos2 < version2.length() && version2.charAt(pos2) == '.') {
                pos2++;
            }
        }
        return 0;
    }

    /**
     * version 1.0.0 vs version 1  : return 0!
     * method1 is much faster!
     */
    public int compareVersion_method2(String version1, String version2) {
        String[] nums1 = version1.split("\\.");
        String[] nums2 = version2.split("\\.");

        for (int i = 0; i < nums1.length || i < nums2.length; i++){
            int num1 = i < nums1.length ? Integer.valueOf(nums1[i]) : 0;
            int num2 = i < nums2.length ? Integer.valueOf(nums2[i]) : 0;
            if (num1 != num2){
                return num1 < num2 ? -1 : 1;
            }
        }
        return 0;
    }



}
