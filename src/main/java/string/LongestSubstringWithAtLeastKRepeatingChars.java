package string;

public class LongestSubstringWithAtLeastKRepeatingChars {
    public int longestSubstring(String s, int k) {
        if (s.length() < k){ // no substring possible
            return 0;
        }
        int[] counts = new int[26];
        for (char c : s.toCharArray()){
            counts[c - 'a']++;
        }

        if (allCharAtLeastK(counts, k)){  // longest substring is self:
            return s.length();
        }
        int start = 0;
        int longestLen = 0;
        for (int i = 0; i <= s.length(); i++){
            if (i == s.length() || counts[s.charAt(i) - 'a'] < k){
                longestLen = Math.max(longestLen, longestSubstring(s.substring(start, i), k));
                start = i + 1;
            }
        }
        return longestLen;
    }
    private boolean allCharAtLeastK(int[] counts, int k){
        for (int count : counts){
            if (count > 0 && count < k){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        LongestSubstringWithAtLeastKRepeatingChars soln = new LongestSubstringWithAtLeastKRepeatingChars();
        System.out.println(soln.longestSubstring("abbbbbbcaa", 2));
    }
}

