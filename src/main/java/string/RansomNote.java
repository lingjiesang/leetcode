package string;

public class RansomNote {
    public boolean canConstruct(String ransomNote, String magazine) {
        int[] counts = new int[256];
        for (char c : magazine.toCharArray()){
            counts[c]++;
        }
        for (char c : ransomNote.toCharArray()){
            if (counts[c]-- <= 0){
                return false;
            }
        }
        return true;
    }
}
