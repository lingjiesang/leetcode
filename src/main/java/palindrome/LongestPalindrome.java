package palindrome;

import java.util.*;

public class LongestPalindrome {
    public String longestPalindrome(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        char[] chars = new char[s.length() * 2 + 1];
        for (int i = 0; i < s.length(); i++) {
            chars[i * 2] = '#';
            chars[i * 2 + 1] = s.charAt(i);
        }
        chars[s.length() * 2] = '#';

        int[] parLens = new int[s.length() * 2 + 1];

        int maxLen = 1, maxLenStart = 0;
        int right = 0, center = 0;
        parLens[0] = 1;

        for (int i = 1; i < chars.length; i++) {

            if (i < right) { // key condition (was using i_mirror >= 0 and wrong about it)
                int i_mirror = center * 2 - i;
                parLens[i] = Math.min((right - i) * 2 + 1, parLens[i_mirror]); // key: i_mirror coverage limits by coverage of the center!
            } else {
                parLens[i] = 1;
            }

            if (i + (parLens[i] - 1) / 2 >= right) {
                // update parLens[i] by more comparison
                int width = (parLens[i] - 1) / 2;

                // + 1 in condition before comparison so don't need -- after looping!
                while (i + width + 1 < chars.length && i - width - 1 >= 0 && chars[i + width + 1] == chars[i - width - 1]) {
                    width++;
                }
                parLens[i] = 2 * width + 1;

                //update center and right
                center = i;
                right = center + (parLens[i] - 1) / 2;

            }

            if (parLens[i] > maxLen) {
                maxLen = parLens[i];
                maxLenStart = i - (parLens[i] - 1) / 2;
            }
        }
        // System.out.println(s.length());
        // System.out.println(maxLenStart + " " + maxLen);
        return s.substring(maxLenStart / 2, maxLenStart / 2 + (maxLen - 1) / 2);
    }
}
