package palindrome;

/**
 * Created by lingjie on 6/5/17.
 */
public class FindClosestPalindrome {
    public String nearestPalindromic(String n) {
        if (n == null || n.length() == 0) {
            throw new IllegalArgumentException("not valid input");
        }

        // single digit:
        if (n.length() == 1) {
            return String.valueOf((char) (n.charAt(0) - 1));
        }

        char[] num = n.toCharArray();

        // if n like 100000000, 10000000001
        if (isTwoMinsOfNDigits(num)) {
            return maxOfOneLessDigit(num);
        }

        // if n like 99999
        if (isMaxOfNDigits(num)) {
            return addTwo(num);
        }

        String newN = makePalindrome(num);

        if (newN.equals(n)) {
            return changeOnePair(num);
        } else {
            return newN;
        }
    }

    private boolean isTwoMinsOfNDigits(char[] num) {
        int len = num.length;
        if (num[0] != '1') {
            return false;
        }

        if (num[len - 1] > '1') {
            return false;
        }
        for (int i = 1; i < len - 1; i++) {
            if (num[i] != '0') {
                return false;
            }
        }
        return true;
    }

    private boolean isMaxOfNDigits(char[] num) {
        for (int i = 0; i < num.length; i++) {
            if (num[i] != '9') {
                return false;
            }
        }
        return true;
    }

    private String maxOfOneLessDigit(char[] num) {
        for (int i = 0; i < num.length; i++) {
            num[i] = '9';
        }
        return new String(num).substring(1);
    }

    private String addTwo(char[] num) {
        for (int i = 0; i < num.length - 1; i++) {
            num[i] = '0';
        }
        num[num.length - 1] = '1';
        return '1' + new String(num);
    }

    private String makePalindrome(char[] num) {
        int len = num.length;
        int leftPointer = len / 2 - 1;
        int rightPointer = len / 2;
        if (len % 2 == 1) {
            rightPointer = len / 2 + 1;
        }

        while (leftPointer >= 0) {
            if (num[leftPointer] != num[rightPointer]) {
                num[rightPointer] = num[leftPointer];
            }
            leftPointer--;
            rightPointer++;
        }

        return new String(num);
    }

    private String changeOnePair(char[] num) {
        int len = num.length;
        int leftPointer = len / 2 - 1;
        int rightPointer = len / 2;
        if (num[leftPointer] != '0') {
            num[leftPointer] = (char) (num[leftPointer] - 1);
            num[rightPointer] = (char) (num[rightPointer] - 1);
        } else {
            num[leftPointer] = (char) (num[leftPointer] + 1);
            num[rightPointer] = (char) (num[rightPointer] + 1);
        }
        return new String(num);
    }

    public static void main(String[] args) {
        FindClosestPalindrome soln = new FindClosestPalindrome();
        soln.nearestPalindromic("1000");
    }
}
