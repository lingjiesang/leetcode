package palindrome;

import java.util.*;

public class PalindromePermutation {
    /**
     * 266. I
     */
    public boolean canPermutePalindrome(String s) {
        int[] counts = new int[256];
        for (char c : s.toCharArray()){
            counts[c]++;
        }
        int countOdd = 0;
        for (int count : counts){
            if (count % 2 != 0){
                countOdd++;
            }
        }
        return countOdd <= 1;
    }

    /**
     * 267.II
     */
    public List<String> generatePalindromes(String s) {
        List<String> result = new ArrayList<>();
        int[] counts = new int[256];
        for (char c : s.toCharArray()){
            counts[c]++;
        }
        int countOdd = 0;
        char oddChar = 0;
        for (int i = 0; i < counts.length; i++){
            if (counts[i] % 2 != 0){
                countOdd++;
                oddChar = (char)i;
            }
        }
        if (countOdd > 1){
            return result;
        }
        char[] oneSoln = new char[s.length()];
        if (oddChar != 0){
            oneSoln[s.length() / 2] = oddChar;
            counts[oddChar]--;
        }
        dfs(0, oneSoln, result, counts);
        return result;
    }

    private void dfs(int start, char[] oneSoln, List<String> result, int[] counts){
        if (start == oneSoln.length / 2){
            result.add(new String(oneSoln));
            return;
        }
        for (int i = 0; i < counts.length; i++){
            if (counts[i] > 0){
                oneSoln[start] = oneSoln[oneSoln.length - 1 - start] = (char)i;
                counts[i] -= 2;
                dfs(start + 1, oneSoln, result, counts);
                counts[i] += 2;
            }
        }
    }

}
