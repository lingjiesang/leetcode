package palindrome;

public class ShortestPalindrome {
    public String shortestPalindrome(String s) {
        String target = s + "#" + new StringBuilder(s).reverse().toString();
        int[] prefixTable = buildPrefixTable(target);

        int palindromLength = prefixTable[prefixTable.length - 1];

        return new StringBuilder(s.substring(palindromLength)).reverse().append(s).toString();
    }

    // prefixLength[i] is the length of the prefix that match suffix ending at position i
    //       (prefixLength[i] is always the position TO BE compared, if we know target[0...prefixLength[i] - 1) already match a suffix)
    // prefixLength[i] = 0 means no possible match between prefix and suffix ending at position i

    private int[] buildPrefixTable(String target){

        int[] prefixLength = new int[target.length()];
        prefixLength[0] = 0; // prefix cannot fully overlap suffix

        int prefixEnd = 0;
        int suffixEnd = 1;

        while (suffixEnd < target.length()){
            if (target.charAt(suffixEnd) == target.charAt(prefixEnd)){
                prefixLength[suffixEnd] = prefixEnd + 1;
                prefixEnd++;
                suffixEnd++;
            } else{
                if (prefixEnd != 0){
                    prefixEnd = prefixLength[prefixEnd - 1]; //key
                } else{
                    prefixLength[suffixEnd] = 0;
                    suffixEnd++;
                }
            }
        }

        return prefixLength;

    }
}