package palindrome;

import java.util.*;

public class PalindromePairs {
    public List<List<Integer>> palindromePairs(String[] words) {
        Map<String, Integer> wordIndices = new HashMap<>();
        for (int i = 0; i < words.length; i++){
            wordIndices.put(reverse(words[i]), i);
        }

        List<List<Integer>> results = new ArrayList<>();
        for (int wordIndex1 = 0; wordIndex1 < words.length; wordIndex1++){
            String w1 = words[wordIndex1];

            for (int divPos = 0; divPos <= w1.length(); divPos++){

                String w1Left = w1.substring(0, divPos);
                String w1Right = w1.substring(divPos, w1.length());


                if (isPalindrome(w1Left)){
                    int wordIndex2 = wordIndices.getOrDefault(w1Right, wordIndex1);
                    if (wordIndex2 != wordIndex1){
                        results.add(Arrays.asList(wordIndex2, wordIndex1));
                    }
                }

                if (divPos == w1.length()) continue;

                if (isPalindrome(w1Right)){
                    int wordIndex2 = wordIndices.getOrDefault(w1Left, wordIndex1);
                    if (wordIndex2 != wordIndex1){
                        results.add(Arrays.asList(wordIndex1, wordIndex2));
                    }
                }
            }
        }
        return results;
    }

    private String reverse(String s){
        return (new StringBuilder(s)).reverse().toString();
    }

    private boolean isPalindrome(String s){
        int st = 0, ed = s.length() - 1;
        while (st < ed){
            if (s.charAt(st) != s.charAt(ed)){
                return false;
            }
            st++; ed--;
        }
        return true;
    }
}
