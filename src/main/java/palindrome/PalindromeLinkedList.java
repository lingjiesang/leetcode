package palindrome;

import linkedlist.ListNode;

import java.util.*;

public class PalindromeLinkedList {
    /**
     * method1: reverse half of the list
     */
    public boolean isPalindrome_method1(ListNode head) {
        if (head == null || head.next == null) {
            return true;
        }

        ListNode slow = head;
        ListNode fast = head.next;
        while (fast != null && fast.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        ListNode mid = slow.next;
        slow.next = null;
        ListNode tail = reverse(mid);
        ListNode prevTail = tail;
        boolean result = true;
        while (tail != null) {
            if (head.val != tail.val) {
                result = false;
            }
            if (tail.next == null){
                prevTail = tail;
            }
            head = head.next;
            tail = tail.next;
        }
        slow.next = reverse(prevTail);
        return result;
    }

    public ListNode reverse(ListNode head) {
        ListNode curt = head;
        ListNode pre = null;
        while (curt != null) {
            ListNode next = curt.next;
            curt.next = pre;
            pre = curt;
            curt = next;
        }
        return pre;
    }

    /**
     * method2 : recursive
     */
    private class Result{
        ListNode endNode;
        boolean valid;
        Result(ListNode endNode, boolean valid){
            this.endNode = endNode;
            this.valid = valid;
        }
    }
    public boolean isPalindrome(ListNode head) {
        int len = getLen(head);
        return isPalindrome(head, len).valid;
    }

    private Result isPalindrome(ListNode head, int len){
        if (len <= 1){
            return new Result(head, true);
        }
        if (len == 2){
            return new Result(head.next, head.val == head.next.val);
        }

        Result mid = isPalindrome(head.next, len - 2);
        return new Result(mid.endNode.next, mid.valid && mid.endNode.next.val == head.val);
    }

    private int getLen(ListNode head){
        int result = 0;
        while (head != null){
            result++;
            head = head.next;
        }
        return result;
    }
}