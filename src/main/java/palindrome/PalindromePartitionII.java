package palindrome;


public class PalindromePartitionII {


    public int minCut(String s) {
        // write your code here
        boolean[][] isPalindrome = new boolean[s.length()][s.length()];
        int[] minCutNums = new int[s.length()];   // minCutNums[i]: minimal number of cuts required for s[0...i]

        for (int i = 0; i < s.length(); i++){
            // calculate minCut for [0...i]
            int min = i;
            // [0...j - 1], [j...i], (i.e. last cut is between j -1 and j)
            for (int j = 0; j <= i; j++){
                isPalindrome[j][i] = (j + 1 > i - 1 || isPalindrome[j + 1][i - 1]) && s.charAt(j) == s.charAt(i) ;
                if (isPalindrome[j][i]){
                    if (j == 0){
                        min = 0;
                    } else {
                        min = Math.min(min, minCutNums[j - 1] + 1);
                    }
                }
            }
            minCutNums[i] = min;
        }
        return minCutNums[s.length() - 1];

    }
}