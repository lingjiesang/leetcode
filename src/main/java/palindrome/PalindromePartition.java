package palindrome;

import java.util.*;

public class PalindromePartition {
    public List<List<String>> partition(String s) {
        List<List<String>> result = new ArrayList<>();
        if (s == null || s.length() == 0){
            return result;
        }
        boolean[][] isPalindrome = getIsPalindrome(s.toCharArray());
        dfs(s, isPalindrome, 0, new ArrayList<>(), result);
        return result;
    }

    private void dfs(String s, boolean[][] isPalindrome, int start, List<String> oneSoln, List<List<String>> result){
        if (start == s.length()){
            result.add(new ArrayList<>(oneSoln));
            return;
        }

        for (int i = start; i < s.length(); i++){
            if (isPalindrome[start][i]){
                oneSoln.add(s.substring(start, i + 1));
                dfs(s, isPalindrome, i + 1, oneSoln, result);
                oneSoln.remove(oneSoln.size() - 1);
            }
        }
    }

    private boolean[][] getIsPalindrome(char[] chars){
        int n = chars.length;
        boolean[][] isParlindrome = new boolean[n][n];

        for (int step = 0; step < n; step++){
            for (int start = 0; start + step < n; start++){
                int end = start + step;
                isParlindrome[start][end] = chars[start] == chars[end]
                        && (start + 1 >= end - 1 || isParlindrome[start + 1][end - 1]);
            }
        }
        return isParlindrome;
    }
}
