package backtracking;


import java.util.*;

//public class WordBreakII {
//    public List<String> wordBreak(String rawStr, Set<String> wordDict) {
//        List<String> allBreaks = new ArrayList<>();
//
//        //if i is in breakStarts.get(j):  rawStr[0...i-1] is breakable, [i...j] is in wordDict"
//        // i.e.  i is the start positions for breaks ending at j
//        List<List<Integer>> breakStarts = new ArrayList<>();
//
//        int maxLen = 0;
//        for (String word : wordDict){
//            maxLen = Math.max(maxLen, word.length());
//        }
//
//        int lastBreakEnd = -1;
//        for (int breakEnd = 0; breakEnd < rawStr.length(); breakEnd++){
//            if (breakEnd - lastBreakEnd > maxLen){
//                return allBreaks; // premature prune
//            }
//            List<Integer> breakStart = new ArrayList<>();
//            for (int pos = Math.max(0, breakEnd - maxLen + 1); pos <= breakEnd; pos++){
//                if ((pos == 0 || !breakStarts.get(pos - 1).isEmpty())
//                        && wordDict.contains(rawStr.substring(pos, breakEnd + 1))){
//                    breakStart.add(pos);
//                    lastBreakEnd = breakEnd;
//                }
//            }
//            breakStarts.add(breakStart);
//        }
//
//        if (!breakStarts.get(rawStr.length() - 1).isEmpty()){
//            backtrack(rawStr, rawStr.length() - 1, breakStarts, "", allBreaks);
//        }
//        return allBreaks;
//    }
//
//    private void backtrack(String rawStr, int breakEnd, List<List<Integer>> breakStarts,
//                           String oneBreak, List<String> allBreaks){
//        if (breakEnd < 0){
//            allBreaks.add(oneBreak.substring(0, oneBreak.length() - 1));
//            return;
//        }
//        for (int breakStart : breakStarts.get(breakEnd)){
//            backtrack(rawStr, breakStart - 1, breakStarts,
//                    rawStr.substring(breakStart, breakEnd + 1) + " " + oneBreak,
//                    allBreaks);
//        }
//    }
//
//}


public class WordBreakII {
    public List<String> wordBreak(String s, Set<String> wordDict) {
        List<String> sentences = new ArrayList<>();
        if (s == null || s.length() == 0 || wordDict == null || wordDict.size() == 0){
            return sentences;
        }

        int maxWordLen = 0;
        for (String word : wordDict){
            maxWordLen = Math.max(maxWordLen, word.length());
        }

        //parents.get(i) = [j, ...] means s[0, ...i] is breakable with last break between j-1 and j:
        //  1. s[j, i]     or s.substring(j, i + 1)  is in wordDict
        //  2. s[0, j - 1] or s.substring(0, j)      is either empty (j == 0) or  breakable (parents.get(j - 1) not empty)

        List<List<Integer>> parents = new ArrayList<>();

        for (int i = 0; i < s.length(); i++){
            List<Integer> parent = new ArrayList<>();
            for (int j = Math.max(0, i - maxWordLen + 1); j <= i ; j++){
                if ((j == 0 || !parents.get(j - 1).isEmpty()) && wordDict.contains(s.substring(j, i + 1))) {
                    parent.add(j);
                }
            }
            parents.add(parent);
        }

        if (!parents.get(s.length() - 1).isEmpty()){
            backTrack(s.length() - 1, parents, s, "", sentences);
        }
        return sentences;
    }

    private void backTrack(int end, List<List<Integer>> parents, String s,
                           String sentence, List<String> sentences){
        if (end < 0){
            sentences.add(sentence.trim());
            return;
        }
        for (int parent : parents.get(end)){
            backTrack(parent - 1, parents, s, s.substring(parent, end + 1) + " " + sentence, sentences);
        }
    }
}