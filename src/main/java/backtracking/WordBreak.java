package backtracking;

import java.util.*;

public class WordBreak {
    public boolean wordBreak(String s, Set<String> wordDict) {
        int maxLen = 0;
        for (String word : wordDict){
            maxLen = Math.max(word.length(), maxLen);
        }
        //validBreak[i] = true: break after i is valid!
        boolean[] validBreaks = new boolean[s.length()];

        for (int i = 0; i < s.length(); i++){
            for (int j = Math.max(0, i - maxLen + 1); j <= i; j++){
                if ((j == 0 || validBreaks[j - 1]) && wordDict.contains(s.substring(j, i + 1))){
                    validBreaks[i] = true;
                    break;
                }
            }
        }
        return validBreaks[s.length() - 1];

    }
}
