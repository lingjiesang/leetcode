package backtracking;

import java.util.*;

public class NQueensII {
    int count;
    public int totalNQueens(int n) {
        count = 0;
        dfs(0, n, new int[n]);
        return count;
    }

    private void dfs(int row, int n, int[] prevRows){
        if (row == n){
            count++;
            return;
        }
        for (int i = 0; i < n; i++){
            if (isValid(row, i, prevRows)){
                prevRows[row] = i;
                dfs(row + 1, n, prevRows);
            }
        }
    }

    private boolean isValid(int row, int col, int[] prevRows){
        for (int i = 0; i < row; i++){
            if (prevRows[i] == col || prevRows[i] - i == col - row || prevRows[i] + i == col + row){
                return false;
            }
        }
        return true;
    }
}
