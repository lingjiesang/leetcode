package backtracking;

import java.util.*;

public class ConcatenatedWords {
    class TrieNode{
        Map<Character, TrieNode> subtree;
        String word;

        TrieNode(){
            subtree = new HashMap<>();
        }
    }
    public List<String> findAllConcatenatedWordsInADict(String[] words) {
        Arrays.sort(words, new Comparator<String>(){
            public int compare(String a, String b){
                return a.length() - b.length();
            }
        });

        List<String> result = new ArrayList<>();
        TrieNode root = new TrieNode();

        for (String word : words){
            addToTrie(root, word, result);
        }
        return result;
    }


    private void addToTrie(TrieNode root, String word, List<String> result){
        TrieNode node = root;
        boolean isComposed = false;
        for (int i = 0; i < word.length(); i++){
            char c = word.charAt(i);
            if (!node.subtree.containsKey(c)){
                node.subtree.put(c, new TrieNode());
            }
            node = node.subtree.get(c);
            if (node.word != null && !isComposed){
                isComposed = dfs(root, word, i + 1, result);
            }
        }
        node.word = word;
    }


    private boolean dfs(TrieNode root, String word, int pos, List<String> result){
        if (pos == word.length()){
            result.add(word);
            return true;
        }

        TrieNode node = root;
        for (int i = pos; i < word.length(); i++){
            char c = word.charAt(i);
            if (!node.subtree.containsKey(c)){
                return false;
            }
            node = node.subtree.get(c);
            if (node.word != null && dfs(root, word, i + 1, result)){
                return true;
            }
        }
        return false;
    }
}
