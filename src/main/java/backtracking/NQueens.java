package backtracking;

import java.util.*;

public class NQueens {
    List<List<String>> allSolns;
    public List<List<String>> solveNQueens(int n) {
        allSolns = new ArrayList<>();
        dfs(0, n, new int[n]);
        return allSolns;
    }

    private void addSoln(int[] prevRows){
        List<String> oneSoln = new ArrayList<>();
        for (int i = 0 ; i < prevRows.length; i++){
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < prevRows[i]; j++){
                sb.append('.');
            }
            sb.append('Q');
            for (int j = prevRows[i] + 1; j < prevRows.length; j++){
                sb.append('.');
            }
            oneSoln.add(sb.toString());
        }
        allSolns.add(oneSoln);
    }

    private void dfs(int row, int n, int[] prevRows){
        if (row == n){
            addSoln(prevRows);
        }
        for (int i = 0; i < n; i++){
            if (isValid(row, i, prevRows)){
                prevRows[row] = i;
                dfs(row + 1, n, prevRows);
            }
        }
    }

    private boolean isValid(int row, int col, int[] prevRows){
        for (int i = 0; i < row; i++){
            if (prevRows[i] == col || prevRows[i] - i == col - row || prevRows[i] + i == col + row){
                return false;
            }
        }
        return true;
    }
}
