package facebook;


import java.util.*;

/**
 * ref: http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=206292&pid=2588780&page=1&extra=page%3D1%26filter%3Dsortid%26sortid%3D311#pid2588780
 *
 * 这貌似是hackerrank的原题，https://www.hackerrank.com/challenges/even-tree
 */

public class BreakTree {

    public List<TreeNode> breakTree(TreeNode root){
        List<TreeNode> result = new ArrayList<>();
        countAndBreak(result, root);
        return result;
    }

    private int countAndBreak(List<TreeNode> result, TreeNode root){
        if (root == null){
            return 0;
        }

        int count = 1;
        Iterator<TreeNode> iter = root.subtree.iterator();
        while (iter.hasNext()){
            int childCount = countAndBreak(result, iter.next());
            if (childCount == 0){
                iter.remove();
            } else{
                count += childCount;
            }
        }

        if (count % 2 == 0){
            result.add(root);
            return 0;
        } else{
            return count;
        }
    }


    public static void main(String[] args){
        TreeNode root = new TreeNode(0);

        TreeNode firstChild = new TreeNode(1);
        firstChild.addChild(new TreeNode(2));

        root.addChild(firstChild);

        root.addChild(new TreeNode(3));
        root.addChild(new TreeNode(4));
        root.addChild(new TreeNode(5));

        BreakTree soln = new BreakTree();
        List<TreeNode> result = soln.breakTree(root);
        System.out.println(result.size());
    }
}
