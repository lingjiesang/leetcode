package facebook;

import java.util.*;

public class TreeNode{
    int val;
    List<TreeNode> subtree;
    public TreeNode(int val){
        this.val = val;
        subtree = new ArrayList<>();
    }

    public void addChild(TreeNode child){
        subtree.add(child);
    }
}