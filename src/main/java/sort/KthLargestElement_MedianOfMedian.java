package sort;

import java.util.*;

public class KthLargestElement_MedianOfMedian {
    public int findKthLargest(int[] nums, int k) {
        return Kth(nums, 0, nums.length - 1, nums.length + 1 - k);
    }

    private int median(int[] nums, int st, int ed) {
        int[] temp = new int[ed - st + 1];
        for (int i = st; i <= ed; i++) {
            temp[i - st] = nums[i];
        }
        Arrays.sort(temp);
        return temp[(ed - st + 1) / 2];
    }

    private int Kth(int[] nums, int st, int ed, int k) {
        /* find pivot */
        //int pivot = nums[ed];

        int pivot = nums[new Random().nextInt(ed + 1 - st) + st];

        if (ed > st) { // only find pivot this way because if ed == st, nums_sub == nums, loop forever...
            int[] nums_sub = new int[(ed - st) / 5 + 1];
            for (int i = st; i <= ed; i += 5) {
                nums_sub[(i - st) / 5] = median(nums, i, Math.min(i + 4, ed));
            }
            pivot = Kth(nums_sub, 0, nums_sub.length - 1, (nums_sub.length + 1) / 2);
        }

        /**
         * partition so that:
         * nums[st, less]: less than pivot
         * nums[less + 1, equal] : equal to pivot
         * nums[equal + 1, ed): more than pivot
         *
         * as pivot is known to exist, the following must satisfy:
         *      0 <= more + 1 <= equal < nums.length
         */
        int less = st - 1;
        int more = ed + 1;
        int curr = st;

        while (curr < more) {
            if (nums[curr] < pivot) {
                swap(nums, ++less, curr);
                curr++;
            } else if (nums[curr] > pivot) {
                swap(nums, curr, --more);
            } else {
                curr++;
            }
        }

        if (less + 1 - st >= k) {
            return Kth(nums, st, less, k);
        } else if (more - st >= k) {
            return nums[more - 1];
        } else {
            return Kth(nums, more, ed, k - (more - st));
        }
    }


    public void swap(int[] nums, int i, int j) {
        if (i != j) {  // check i != j this is necessary here!!!
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }

    public static void main(String[] args) {
        int[] nums = {7,6,5,4,3,2,1};
        KthLargestElement_MedianOfMedian soln = new KthLargestElement_MedianOfMedian();
        System.out.println(soln.findKthLargest(nums, 2));
    }


}
