package sort;

import java.util.*;

public class SortColors {
    public void sortColors(int[] nums) {
        int lastRed = -1, firstBlue = nums.length;

        for (int i = 0; i < firstBlue; i++){
            if (nums[i] == 0){
                swap(nums, ++lastRed, i);
            } else if (nums[i] == 2){
                swap(nums, --firstBlue, i--);
            }
        }
    }

    private void swap(int[] nums, int i, int j){
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
