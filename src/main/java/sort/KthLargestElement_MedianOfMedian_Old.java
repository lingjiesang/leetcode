package sort;

import java.util.*;

/**
 * "old" 因为partition写的有问题,程序也不够简洁...
 *
 * similar implementation (Kth Smallest Element of List) is seen in
 *      "matrix.KthSmallestElementInSortedMatrix_Linear"
 */

public class KthLargestElement_MedianOfMedian_Old {

    public int findKthLargest(int[] nums, int k) {
        /**
         * when nums.length == 1 (obvious k would also be 1), nums.sub will be the same as nums!
         * As pivot = findKthLargest(nums_sub, 0) is called inside findKthLargest(nums, 0) ==> loop forever!
         * thus we need to check if k == 1 or do:
         *  if (nums.length == 1){
         *      assert (k == 1);
         *      return nums[0];
         *  }
         */
        if (k > nums.length || k <= 0){
            return -1; // error!
        }
        if (k == 1){
            return max(nums);
        }

        int[] nums_sub = new int[(nums.length + 4) / 5];
        for (int i = 0; i < nums.length; i += 5){
            int len = Math.min(5, nums.length - i);
            int[] section = new int[len];
            for (int j = 0; j < len; j++){
                section[j] = nums[i + j];
            }
            Arrays.sort(section);
            nums_sub[i / 5] = section[len / 2];
        }
        int pivot = findKthLargest(nums_sub, (nums_sub.length + 1) / 2);
        int[] locs = partition(nums, pivot);
        if (locs[0] <= k - 1 && locs[1] >= k - 1){
            return pivot;
        } else if (locs[0] > k - 1){
            int[] nums_high = new int[locs[0]];
            for (int i = 0; i < locs[0]; i++){
                nums_high[i] = nums[i];
            }
            return findKthLargest(nums_high, k);
        } else {
            int[] nums_low = new int[nums.length - locs[1] - 1];
            for (int i = locs[1] + 1; i < nums.length; i++){
                nums_low[i - (locs[1] + 1)] = nums[i];
            }
            return findKthLargest(nums_low, k - locs[1] - 1);
        }
    }

    /**
     * 这个partition不是完全对
     * 两次swap在 i = more + 1, equal + 1 = more (more和equal都在++前)会刚好把两个元素swap两次,结果什么都没发生!
     *
     * 所以要避免这种连着swap两次的写法!!!
     *
     * partition so that:
     * nums[0, more]: more than pivot
     * nums[more + 1, equal] : equal to pivot
     * nums[equal + 1, nums.length): less than pivot
     *
     * as pivot is known to exist, the following must satisfy:
     *      0 <= more + 1 <= equal < nums.length
     */
    private int[] partition(int[] nums, int pivot){
        int more = -1, equal = -1;
        for (int i = 0; i < nums.length; i++){
            if (nums[i] > pivot){
                swap(nums, ++more, ++equal);
                swap(nums, i, more);
            } else if (nums[i] == pivot){
                swap(nums, i, ++equal);
            }
        }
        return new int[]{more + 1, equal};
    }

    public void swap(int[] nums, int i, int j){
        if (i != j) {  // check i != j this is necessary here!!!
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }

    private int max(int[] nums){
        int max = Integer.MIN_VALUE;
        for (int num : nums){
            max = Math.max(max, num);
        }
        return max;
    }

    public static void main(String[] args){
        int[] nums = {-1, 2, 0};
        KthLargestElement_MedianOfMedian_Old soln = new KthLargestElement_MedianOfMedian_Old();
        System.out.println(soln.findKthLargest(nums, 3));
    }


}
