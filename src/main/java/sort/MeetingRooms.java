package sort;

import java.util.*;

public class MeetingRooms {
    /**
     * 252. I
     */
    public boolean canAttendMeetings(Interval[] intervals) {
        if (intervals.length == 0){
            return true;
        }
        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });

        int lastEnd = intervals[0].end;
        for (int i = 1; i < intervals.length; i++){
            if (intervals[i].start < lastEnd){
                return false;
            }
            lastEnd = intervals[i].end;
        }
        return true;
    }

    /**
     * 253. II
     */
    public int minMeetingRooms(Interval[] intervals) {
        if (intervals.length == 0){
            return 0;
        }
        Arrays.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                return o1.start - o2.start;
            }
        });

        PriorityQueue<Integer> rooms = new PriorityQueue<>();
        rooms.add(intervals[0].end);
        for (int i = 1; i < intervals.length; i++){
            if (intervals[i].start >= rooms.peek()){
                rooms.poll();
            }
            rooms.add(intervals[i].end);
        }
        return rooms.size();

    }
}
