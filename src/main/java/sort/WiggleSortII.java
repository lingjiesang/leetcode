package sort;

public class WiggleSortII {
    public void wiggleSort(int[] nums) {
        if (nums == null || nums.length == 0){
            return;
        }
        int n = nums.length;
        int k = nums.length / 2;
        int median = kth(nums, 0, n - 1, k);

        int less = -1, more = n;
        for (int curt = 0; curt < more; curt++){
            if (nums[map(curt, n)] < median) {
                swap(nums, map(++less, n), map(curt, n));
            } else if (nums[map(curt, n)] > median) {
                swap(nums, map(--more, n), map(curt--, n));
            }
        }
    }

    /**
     * map from logical index to actual (physical) index
     */
    private int map(int index, int n){
        int mid = (n + 1) / 2;
        if (index < mid){
            if (n % 2 == 0){
                return n - index * 2 - 2;
            } else{
                return n - index * 2 - 1;
            }
        } else{
            return n * 2 - index * 2 - 1;
        }
    }

    /**
     *   return value of k-th element in nums
     *
     *   k is 0-based, i.e. smallest element has k = 0
     */
    public int kth(int[] nums, int st, int ed, int k){
        int less = st - 1;
        int more = ed + 1;
        int pivot = nums[(st + ed) / 2];
        for (int curt = st; curt < more; curt++){
            if (nums[curt] > pivot) {
                swap(nums, ++less, curt);
            } else if (nums[curt] < pivot) {
                swap(nums, --more, curt--);
            }
        }
        if (less - st + 1 > k){  // k is 0-based, because "less - st + 1 >= k + 1", i.e. at least k + 1 element smaller than pivot
            return kth(nums, st, less, k);
        } else if (more - st > k){ // at least k + 1 element smaller or equal than pivot, but because not satisfy last, at most k element smaller than pivot
            return nums[more - 1];
        } else{
            return kth(nums, more, ed, k - (more - st));
        }
    }

    public void swap(int[] nums, int i, int j){
        if (i != j){
            int temp = nums[i];
            nums[i] = nums[j];
            nums[j] = temp;
        }
    }
}
