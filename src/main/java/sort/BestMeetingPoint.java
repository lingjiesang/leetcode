package sort;

import java.util.*;

public class BestMeetingPoint {
    /**
     * median is the best meeting point!
     */
    public int minTotalDistance(int[][] grid) {
        if (grid.length == 0 || grid[0].length == 0){
            return 0;
        }
        int m = grid.length;
        int n = grid[0].length;

        int[] y = new int[m * n];
        int lenY = 0;
        for (int i = 0; i < m; i++){
            for (int j = 0; j < n; j++){
                if (grid[i][j] == 1){
                    y[lenY++] = i;
                }
            }
        }

        int[] x = new int[m * n];
        int lenX = 0;
        for (int j = 0; j < n; j++){
            for (int i = 0; i < m; i++){
                if (grid[i][j] == 1){
                    x[lenX++] = j;
                }
            }
        }

        return getMin(y, lenY) + getMin(x, lenX);
    }

    private int getMin(int[] nums, int numsLen){
        int st = 0, ed = numsLen - 1;
        int result = 0;
        while (st < ed){
            result += nums[ed--] - nums[st++];
        }
        return result;
    }
}
