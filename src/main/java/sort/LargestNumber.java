package sort;

import java.util.*;

public class LargestNumber {
    public String largestNumber(int[] nums) {

        String[] numStrs = new String[nums.length];
        for (int i = 0; i < nums.length; i++){
            numStrs[i] = Integer.toString(nums[i]);
        }
        Arrays.sort(numStrs, new Comparator<String>(){
            @Override
            public int compare(String a, String b){
                return (b + a).compareTo(a + b);
            }
        });

        StringBuilder sb = new StringBuilder();
        boolean isStartZero = true;
        for (String numStr : numStrs){
            if (isStartZero && numStr.charAt(0) == '0'){
                continue;
            } else{
                sb.append(numStr);
                isStartZero = false;
            }
        }
        if (sb.length() == 0){
            return "0";
        } else{
            return sb.toString();
        }
    }
}
