package sort;

import java.util.*;

public class HIndex {

    /**
     * I
     */
    public int hIndex(int[] citations){
        if (citations.length == 0){
            return 0;
        }
        int n = citations.length;
        int[] bucket = new int[n + 1];
        for (int citation : citations){
            if (citation >= n){
                bucket[n]++;
            } else if (citation >= 0){
                bucket[citation]++;
            }
        }

        int highCites = 0;
        for (int i = n; i > 0; i--){
            highCites += bucket[i];
            if (highCites >= i){
                return i;
            }
        }
        return 0;
    }

    /**
     * II
     */
    public int hIndex_II(int[] citations) {
        if (citations.length == 0){
            return 0;
        }

        int n = citations.length;
        int start = 0, end = n - 1;

        while (start < end){
            int mid = start + (end - start) / 2;
            if (citations[mid] >= n - mid){
                end = mid;
            } else{
                start = mid + 1;
            }
        }
        if (citations[start] >= n - start){
            return n - start;
        } else{
            return 0;
        }

    }
}
