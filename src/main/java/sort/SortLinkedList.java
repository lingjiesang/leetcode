package sort;

import linkedlist.ListNode;

public class SortLinkedList {
    /**
     * 147
     *
     * insertion sort idea:
     * A[0, ..., n - 1]
     * for j = 1 : n - 1
     *  key = A[j]
     *  i = j - 1
     *  while (i >= 0 && A[j] > key)
     *    A[i + 1] = A[i]
     *    i--
     *  A[i + 1] = key
     */
    public ListNode insertionSortList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode dummy = new ListNode(Integer.MIN_VALUE);
        dummy.next = head;

        ListNode pre = head;
        ListNode curt = head.next;

        while (curt != null) {
            if (curt.val >= pre.val) {
                curt = curt.next;
                pre = pre.next;
                continue;
            }

            // remove "curt" from current location
            pre.next = curt.next;

            // insert "curt" into appropriate location
            ListNode pt = dummy; //find pt : rightmost node whose value is small than curt's value
            while (pt.next.val < curt.val) {
                pt = pt.next;
            }
            curt.next = pt.next;
            pt.next = curt;

            // move to next node (pre not change)
            curt = pre.next;
        }

        return dummy.next;
    }


    /**
     * 148
     * method1: merge sort
     */
    private ListNode curt;

    public ListNode sortList_Merge(ListNode head) {
        if (head == null) {
            return null;
        }
        int length = getLength(head);
        curt = head;
        return mergeSort(head, length);
    }

    private ListNode mergeSort(ListNode head, int length) {
        if (length == 1) {
            curt = curt.next;
            head.next = null;
            return head;
        }

        ListNode left = mergeSort(head, length / 2);

        ListNode right = mergeSort(curt, length - length / 2);

        ListNode dummy = new ListNode(0);
        head = dummy;
        while (left != null & right != null) {
            if (left.val <= right.val) {
                head.next = left;
                left = left.next;
                head = head.next;
            } else {
                head.next = right;
                right = right.next;
                head = head.next;
            }
        }
        while (left != null) {
            head.next = left;
            left = left.next;
            head = head.next;
        }
        while (right != null) {
            head.next = right;
            right = right.next;
            head = head.next;
        }
        return dummy.next;
    }

    private int getLength(ListNode head) {
        int length = 0;
        ListNode pt = head;
        while (pt != null) {
            length++;
            pt = pt.next;
        }
        return length;
    }


    /**
     * 148
     * online method
     * ref: https://discuss.leetcode.com/topic/18100/java-merge-sort-solution
     */

    public ListNode sortList(ListNode head) {
        if (head == null || head.next == null)
            return head;

        // step 1. cut the list to two halves
        ListNode prev = null, slow = head, fast = head;

        while (fast != null && fast.next != null) {
            prev = slow;
            slow = slow.next;
            fast = fast.next.next;
        }

        prev.next = null;

        // step 2. sort each half
        ListNode l1 = sortList(head);
        ListNode l2 = sortList(slow);

        // step 3. merge l1 and l2
        return merge(l1, l2);
    }

    ListNode merge(ListNode l1, ListNode l2) {
        ListNode l = new ListNode(0), p = l;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                p.next = l1;
                l1 = l1.next;
            } else {
                p.next = l2;
                l2 = l2.next;
            }
            p = p.next;
        }

        if (l1 != null)
            p.next = l1;

        if (l2 != null)
            p.next = l2;

        return l.next;
    }


}
