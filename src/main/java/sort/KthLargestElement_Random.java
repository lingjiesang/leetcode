package sort;

import java.util.*;

public class KthLargestElement_Random {


    public int findKthLargest(int[] nums, int k) {
        return Kth(nums, 0, nums.length - 1, nums.length + 1 - k);
    }

    /**
     * find k-th smallest element
     * method 1: partition from two side
     *
     * partition so that :
     * [st, less] : elements less than pivot
     * [less + 1, more - 1]: elements equal to pivot
     * [more, ed] : elements more than pivot
     *
     */
    private int Kth(int[] nums, int st, int ed, int k) {
        int pivot = nums[new Random().nextInt(ed + 1 - st) + st];

        int less = st - 1;
        int more = ed + 1;
        int curr = st;

        while (curr < more) {
            if (nums[curr] < pivot) {
                swap(nums, ++less, curr);
                curr++;
            } else if (nums[curr] > pivot) {
                swap(nums, curr, --more);
            } else{
                curr++;
            }
        }

        if (less - st  + 1 >= k ) { // number of elements in [st, less] >= k
            return Kth(nums, st, less, k);
        } else if (more - st >= k ) { // number of elements in [st, more - 1] >= k
            return nums[more - 1];
        } else {
            return Kth(nums, more, ed, k - (more - st));
        }
    }

    private void swap(int[] nums, int i, int j) {
        if (i != j) {
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }
}

