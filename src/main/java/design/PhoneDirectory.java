package design;

import java.util.*;

/**
 * 453ms
 */
public class PhoneDirectory {

    /** Initialize your data structure here
     @param maxNumbers - The maximum numbers that can be stored in the phone directory. */
    boolean[] numberOccupied;
    int availNumbers;
    int nextAvailNum;
    public PhoneDirectory(int maxNumbers) {
        numberOccupied = new boolean[maxNumbers];
        availNumbers = maxNumbers;
        nextAvailNum = 0;
    }

    /** Provide a number which is not assigned to anyone.
     @return - Return an available number. Return -1 if none is available. */

    private void next(){
        if (availNumbers == 0) {
            nextAvailNum = -1;
        } else {
            nextAvailNum++;
            while (true) {
                if (nextAvailNum == numberOccupied.length) {
                    nextAvailNum = 0;
                }
                if (!numberOccupied[nextAvailNum]) {
                    return;
                }
                nextAvailNum++;
            }
        }
    }

    public int get() {
        if (availNumbers == 0){
            return -1;
        }
        int result = nextAvailNum;
        numberOccupied[nextAvailNum] = true;
        availNumbers--;
        next();
        return result;
    }

    /** Check if a number is available or not. */
    public boolean check(int number) {
        if (number < 0 || number >= numberOccupied.length){
            return false;
        }
        return !numberOccupied[number];
    }

    /** Recycle or release a number. */
    public void release(int number) {
        if (number >= 0 && number < numberOccupied.length && numberOccupied[number]){
            numberOccupied[number] = false;
            if (availNumbers++ == 0){
                next();
            }
        }
    }
}

/**
 * Your PhoneDirectory object will be instantiated and called as such:
 * PhoneDirectory obj = new PhoneDirectory(maxNumbers);
 * int param_1 = obj.get();
 * boolean param_2 = obj.check(number);
 * obj.release(number);
 */