package design;

import javafx.geometry.Pos;

import java.util.*;

public class SnakeGame {

    private class Position {
        public int x;
        public int y;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public boolean equals(Position p) {
            return this.x == p.x && this.y == p.y;
        }

        public Position clone() {
            return new Position(this.x, this.y);
        }
    }

    //因为用到 occupiedBySnake, memory limit exceed on leetcode!
    //看 meSnakeGame_method2 就好
    private boolean[][] occupiedBySnake;
    private Deque<Position> snake;
    private Deque<Position> foodPositions;
    private int width;
    private int height;
    private int score;

    /**
     * Initialize your data structure here.
     *
     * @param width  - screen width
     * @param height - screen height
     * @param food   - A list of food positions
     *               E.g food = [[1,1], [1,0]] means the first food is positioned at [1,1], the second is at [1,0].
     */
    public SnakeGame(int width, int height, int[][] food) {
        this.width = width;
        this.height = height;

        occupiedBySnake = new boolean[height][width];
        occupiedBySnake[0][0] = true;
        snake = new ArrayDeque<>();
        snake.offerLast(new Position(0, 0));

        foodPositions = new ArrayDeque<>();
        for (int[] f : food) {
            foodPositions.offerLast(new Position(f[0], f[1]));
        }
        score = 0;
    }

    /**
     * Moves the snake.
     *
     * @param direction - 'U' = Up, 'L' = Left, 'R' = Right, 'D' = Down
     * @return The game's score after the move. Return -1 if game over.
     * Game over when snake crosses the screen boundary or bites its body.
     */
    public int move(String direction) {
        Position newSnakeHead = snake.peekFirst().clone();

        switch (direction) {
            case "U":
                if (--newSnakeHead.x < 0) {
                    return -1;
                }
                break;
            case "D":
                if (++newSnakeHead.x == height) {
                    return -1;
                }
                break;
            case "L":
                if (--newSnakeHead.y < 0) {
                    return -1;
                }
                break;
            case "R":
                if (++newSnakeHead.y == width) {
                    return -1;
                }
        }


        if (!foodPositions.isEmpty() && newSnakeHead.equals(foodPositions.peekFirst())) {
            foodPositions.pollFirst();
            score++;
        } else {
            Position snakeTail = snake.pollLast();
            occupiedBySnake[snakeTail.x][snakeTail.y] = false;
        }

        // 先处理"尾巴"再处理"头",因为尾巴如果移走了是不会造成相撞的!
        if (occupiedBySnake[newSnakeHead.x][newSnakeHead.y]) {
            return -1;
        }
        snake.offerFirst(newSnakeHead);
        occupiedBySnake[newSnakeHead.x][newSnakeHead.y] = true;
        return score;
    }
}

/**
 * Your SnakeGame object will be instantiated and called as such:
 * SnakeGame obj = new SnakeGame(width, height, food);
 * int param_1 = obj.move(direction);
 */