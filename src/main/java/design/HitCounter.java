package design;

import java.util.*;

public class HitCounter {

    Deque<int[]> counter;
    int totalCount;

    /** Initialize your data structure here. */
    public HitCounter() {
        counter = new ArrayDeque<>();
    }


    /** Record a hit.
     @param timestamp - The current timestamp (in seconds granularity). */
    public void hit(int timestamp) {
        if (!counter.isEmpty() && counter.peekLast()[0] == timestamp){
            counter.peekLast()[1]++;
        } else{
            counter.offerLast(new int[]{timestamp, 1});
        }
        totalCount += 1;
    }

    /** Return the number of hits in the past 5 minutes.
     @param timestamp - The current timestamp (in seconds granularity). */
    public int getHits(int timestamp) {
        while (!counter.isEmpty() && counter.peekFirst()[0] <= timestamp - 300){
            totalCount -= counter.pollFirst()[1];
        }
        return totalCount;
    }
}

/**
 * Your HitCounter object will be instantiated and called as such:
 * HitCounter obj = new HitCounter();
 * obj.hit(timestamp);
 * int param_2 = obj.getHits(timestamp);
 */