package design;

import java.util.*;

public class TicTacToe {
    private int n;
    private int[] rowScores;
    private int[] colScores;
    private int diagonalScore;
    private int antiDiagonalScore;

    /**
     * Initialize your data structure here.
     */
    public TicTacToe(int n) {
        this.n = n;
        rowScores = new int[n];
        colScores = new int[n];
    }

    /**
     * Player {player} makes a move at ({row}, {col}).
     *
     * @param row    The row of the board.
     * @param col    The column of the board.
     * @param player The player, can be either 1 or 2.
     * @return The current winning condition, can be either:
     * 0: No one wins.
     * 1: Player 1 wins.
     * 2: Player 2 wins.
     */
    public int move(int row, int col, int player) {
        int score = player == 1 ? 1 : -1;
        int maxScore = 0;
        maxScore = Math.max(maxScore, Math.abs(rowScores[row] += score));
        maxScore = Math.max(maxScore, Math.abs(colScores[col] += score));

        if (row == col) {
            maxScore = Math.max(maxScore, Math.abs(diagonalScore += score));
        }
        if (row + col == n - 1) {
            maxScore = Math.max(maxScore, Math.abs(antiDiagonalScore += score));
        }
        if (maxScore == n){
            return player;
        } else{
            return 0;
        }
    }
}

/**
 * Your TicTacToe object will be instantiated and called as such:
 * TicTacToe obj = new TicTacToe(n);
 * int param_1 = obj.move(row,col,player);
 */