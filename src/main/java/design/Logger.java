package design;

import java.util.*;

public class Logger {

    /** Initialize your data structure here. */

    Map<String, Integer> lastPrintTime;
    public Logger() {
        lastPrintTime = new HashMap<>();
    }

    /** Returns true if the message should be printed in the given timestamp, otherwise returns false.
     If this method returns false, the message will not be printed.
     The timestamp is in seconds granularity. */
    public boolean shouldPrintMessage(int timestamp, String message) {
        if (!lastPrintTime.containsKey(message) || lastPrintTime.get(message) <= timestamp - 10){
            lastPrintTime.put(message, timestamp);
            return true;
        }
        return false;
    }
}

/**
 * Your Logger object will be instantiated and called as such:
 * Logger obj = new Logger();
 * boolean param_1 = obj.shouldPrintMessage(timestamp,message);
 */