package templates;


public class KMP {
    public int[] getTable(String pattern){
        int index = 0;
        int[] table = new int[pattern.length()];
        table[0] = 0;
        int i = 1; // i from 1 in getTable    //** i from 0 if match
        while(i < pattern.length()){
            if (pattern.charAt(i) == pattern.charAt(index)){
                table[i] = index + 1; // ** no need in match
                i++;
                index++;
            } else{
                if (index != 0){
                    index = table[index - 1];
                } else{
                    table[i] = 0; // ** no need in match
                    i++;
                }
            }
        }
        return table;
    }
    // se more in StrStr_KMP
}
