package templates;

import java.util.*;

public class Trie {
    class TrieNode {
        public TrieNode[] subtrees;
        public String word;

        public TrieNode(){
            subtrees = new TrieNode[26];
        }
    }
    private TrieNode root;

    public Trie() {
        root = new TrieNode();
    }

    // Inserts a word into the trie.
    public void insert(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()){
            if (node.subtrees[c - 'a'] == null){
                node.subtrees[c - 'a'] = new TrieNode();
            }
            node = node.subtrees[c - 'a'];
        }
        node.word = word;

    }

    // Returns if the word is in the trie.
    public boolean search(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()){
            if (node.subtrees[c - 'a'] == null){
                return false;
            }
            node = node.subtrees[c - 'a'];
        }
        return node.word != null;
    }

    // Returns if there is any word in the trie
    // that starts with the given prefix.
    public boolean startsWith(String prefix) {
        TrieNode node = root;
        for (char c : prefix.toCharArray()){
            if (node.subtrees[c - 'a'] == null){
                return false;
            }
            node = node.subtrees[c - 'a'];
        }
        return true;
    }
}


// depreciated
//public class Trie{
//    public class TrieNode{
//        public String str;
//        public HashMap<Character, TrieNode> subtree;
//        public boolean isString;
//        public TrieNode(){
//            subtree = new HashMap<>();
//        }
//    }
//    public TrieNode root;
//
//    public Trie(){
//        root = new TrieNode();
//    }
//
//    public void insert(String word){
//        TrieNode curt = root;
//        for (int i = 0; i < word.length(); i++){
//            char c = word.charAt(i);
//            if (!curt.subtree.containsKey(c)){
//                curt.subtree.put(c, new TrieNode());
//            }
//            curt = curt.subtree.get(c);
//        }
//        curt.str = word;
//        curt.isString = true;
//    }
//}
