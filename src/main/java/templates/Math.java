package templates;

import java.util.*;

public class Math {
    /**
     * get greatest common divisor
     */
    int getGCD(int a, int b){
        if (b == 0) return a;
        return getGCD(b, a % b);
    }
}
