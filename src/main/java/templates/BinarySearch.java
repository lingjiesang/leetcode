package templates;

import java.util.*;

public class BinarySearch {

    /**
     * 解是否存在(check start and end, or more), 必须或者在binary search内解决,或者在调用函数前解决
     * 不要默认解一定存在!
     *
     * binary search永远不是对称的, 因为偶数个元素时,med不是正中间
     *
     * while (start < end){
     *     ...
     *     if (...){
     *          start = mid; //error, 死循环(when end == start + 1, mid == start)
     *     }
     * }
     */

    /**
     * find minimal index (between start and end, both inclusive) in nums where nums[index] > target
     * if no such element exist, return -1
     */
    private int higher(int[] nums, int start, int end, int target) {
        while (start < end) {
            int mid = start + (end - start) / 2;
            if (nums[mid] <= target) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return nums[start] > target ? start : -1;
    }

    /**
     * find the minimal index in nums (between start and end, both inclusive), where nums[index] >= target
     */
    private int ceiling(int[] nums, int start, int end, int target) {
        while (start < end) {
            int mid = start + (end - start) / 2;
            // do this only if elements in nums are distinct from each other
            if (nums[mid] == target) {
                return mid;
            }
            // end "do this"
            if (nums[mid] < target) {
                start = mid + 1;
            } else {
                end = mid;
            }
        }
        return nums[start] >= target ? start : -1;
    }


    private int ceiling_easy(int[] nums, int start, int end, int target) {
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;

            // do this only if elements in nums are distinct from each other
            if (nums[mid] == target) {
                return mid;
            }
            // end "do this"

            if (nums[mid] < target) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if (nums[start] >= target) {
            return start;
        } else if (nums[end] >= target) {
            return end;
        } else {
            return -1;
        }
    }



    /**
     * find the maximal index in nums (between start and end, both inclusive), where nums[index] < target
     */
    private int lower(int[] nums, int start, int end, int target) {
        if (nums[start] >= target) {
            return -1;
        }

        while (start < end) {
            int mid = start + (end - start) / 2;

            if (nums[mid] < target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        if (nums[start] < target){
            return start;
        } else{
            return start - 1;
        }
    }


    /**
     * find maximal index in nums (between start and end, both inclusive), where nums[index] <= target
     */
    private int floor(int[] nums, int start, int end, int target) {
        if (nums[start] > target) {
            return -1;
        }

        while (start < end) {
            int mid = start + (end - start) / 2;

            if (nums[mid] <= target) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }
        if (nums[start] <= target){
            return start;
        } else{
            return start - 1;
        }
    }

    private int floor_easy(int[] nums, int start, int end, int target) {
        while (start + 1 < end) {
            int mid = start + (end - start) / 2;

            // do this only if elements in nums are distinct from each other
            if (nums[mid] == target) {
                return mid;
            }
            // end "do this"

            if (nums[mid] < target) {
                start = mid;
            } else {
                end = mid;
            }
        }
        if (nums[end] <= target) {
            return end;
        } else if (nums[start] <= target) {
            return start;
        } else {
            return -1;
        }
    }


}
