package templates;

import java.lang.*;
import java.util.*;

public class KthElement_Linear {
    /**
     * 以下所有写法中默认 kth 存在,即 1 <= k <= ed - st + 1
     * 调用函数前必须确保这一点
     */

    /**
     * 第一种写法,kth and partition分开写
     * 2-way partition, less和more同从左边起
     */
    public int kth_method1(int[] nums, int k, int st, int ed) {
        while (st < ed) {
            int index = partition(nums, st, ed);
            if (index == k) {
                return nums[index];
            } else if (index < k) {
                st = index + 1;
            } else {
                ed = index - 1;
            }
        }
        return nums[st];
    }

    /**
     * 2-way partition according to nums[ed], and return its new index
     * elementr left to returned index: < nums[index]
     * elements right to returned index: >= nums[index]
     */
    private int partition(int[] nums, int st, int ed) {
        int rand = new Random().nextInt(ed + 1 - st) + st;
        swap(nums, rand, ed);

        int pivot = nums[ed];
        int less = st - 1;
        int more = st;
        while (more < ed) {
            if (nums[more] < pivot) {
                swap(nums, ++less, more);
            }
            more++;
        }
        swap(nums, ++less, ed);
        return less;
    }

    /**
     * 第二种写法,kth and partition合起来写
     * 2-way partition, less和more同从左边起
     */
    private int kth_method2(int[] nums, int st, int ed, int k) {
        if (nums == null || nums.length == 0) {
            return 0;
        }

        int less = st - 1;
        int more = st;

        int rand = new Random().nextInt(ed + 1 - st) + st;
        swap(nums, rand, ed);
        int pivot = nums[ed];
        while (more < ed) {
            if (nums[more] < pivot) {
                swap(nums, ++less, more);
            }
            more++;
        }
        swap(nums, ++less, ed);

        if (less - st == k) {
            return nums[less];
        } else if (less - st < k) {
            return kth_method2(nums, less + 1, ed, k - (less - st + 1));
        } else {
            return kth_method2(nums, st, less - 1, k);
        }
    }

    /**
     *  第三种写法
     *  3-way partition, less 从左边起, more 从右边起 (比第四种好)
     *      [st, less] : elements less than pivot
     *      [less + 1, more - 1]: elements equal to pivot
     *      [more, ed] : elements more than pivot
     */
    public int kth_method3(int[] nums, int st, int ed, int k){

        /* find pivot (median of median, better than random pivot) */
        int pivot = nums[ed];
        if (ed > st) { // only find pivot this way because if ed == st, nums_sub == nums, loop forever...
            int[] nums_sub = new int[(ed - st) / 5 + 1];
            for (int i = st; i <= ed; i += 5) {
                nums_sub[(i - st) / 5] = median(nums, i, java.lang.Math.min(i + 4, ed));
            }
            pivot = kth_method3(nums_sub, 0, nums_sub.length - 1, (nums_sub.length + 1) / 2);
        }

        // or random pivot
//        int pivot = nums[new Random().nextInt(ed + 1 - st) + st];

        int less = st - 1;
        int more = ed + 1;
        int curr = st;

        while (curr < more){
            if (nums[curr] < pivot) {
                swap(nums, ++less, curr);
                curr++;
            }
            else if (nums[curr] > pivot){
                swap(nums, curr, --more);
            } else{
                curr++;
            }
        }

        if (less + 1 - st >= k){
            return kth_method3(nums, st, less, k);
        } else if (more - st >= k) {
            return nums[more - 1];
        } else {
            return kth_method3(nums, more, ed, k - (more - st));
        }
    }

    private int median(int[] nums, int st, int ed) {
        int[] temp = new int[ed - st + 1];
        for (int i = st; i <= ed; i++) {
            temp[i - st] = nums[i];
        }
        Arrays.sort(temp);
        return temp[(ed - st + 1) / 2];
    }



    private void swap(int[] nums, int i, int j) {
        if (i != j) {
            nums[i] ^= nums[j];
            nums[j] ^= nums[i];
            nums[i] ^= nums[j];
        }
    }
}
